/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

enum
{
	INPUT_DONE = 0,		// event handled
	INPUT_NOTME,		// the handler cannot process the event
	INPUT_IGNORE,		// event should be ignored
	INPUT_TERMINATE,	// don't wait for more events
};

#define EVENT_MOUSE_LEFT -1
#define EVENT_MOUSE_RIGHT -3
#define EVENT_SCROLL_UP -4
#define EVENT_SCROLL_DOWN -5
#define EVENT_DRAG_START -125
#define EVENT_DRAG_STOP -126
#define EVENT_MOTION -127

struct area
{
	unsigned left, right, top, bottom;
	int (*callback)(int, unsigned, unsigned, uint16_t, void *, double);
	// TODO ? event mask
};

int input_mouse_terminate(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress);
int input_mouse_cancel(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress);

int input_process(const struct area *restrict areas, size_t areas_count, void (*display)(const void *, double), int (*timer)(void *, double), void *restrict context);
