/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

struct battles
{
	size_t count;
	struct battle_info
	{
		struct troop_stack **fighters;
		size_t fighters_count;
		uint32_t players; // bit mask of players in the battle
		enum battle_type type;
		struct hexagon *location;
	} battle[];
};

static inline int unit_available(const struct region *restrict region, const struct unit *restrict unit)
{
	return (region->garrison.hexagon->buildings & unit->requires) == unit->requires;
}

static inline int vessel_available(const struct hexagon *restrict hexagon, const struct vessel *restrict vessel)
{
	return (hexagon->buildings & vessel->requires) == vessel->requires;
}

static inline int population_enough(const struct region *restrict region, const struct unit *restrict unit)
{
	return (region->population >= unit->troops_count);
}

size_t buildings_available(struct game *restrict game, struct hexagon *restrict hexagon, unsigned char buildings[static BUILDINGS_COUNT], unsigned player);

void troop_stack_stamina(struct troop_stack *restrict troop_stack);

void turn_prepare(struct game *restrict game);

int turn_move(struct game *restrict game);
int turn_orders(struct game *restrict game);

void turn_riot(struct game *restrict game);
void turn_players(struct game *restrict game, size_t hexagons_total);

int training_enqueue(struct game *restrict game, struct region *restrict region, size_t unit_index);
void training_dequeue(struct game *restrict game, struct region *restrict region, size_t position);

int building_build(struct game *restrict game, struct hexagon *restrict hexagon, size_t index, unsigned player);
void building_cancel(struct game *restrict game, struct hexagon *restrict hexagon);

int vessel_build(struct game *restrict game, struct hexagon *restrict hexagon, size_t index);
void vessel_cancel(struct game *restrict game, struct hexagon *restrict hexagon);

struct troop_stack *troop_stack_create(struct game *restrict game, struct hexagon *restrict hexagon, unsigned owner, int public);
int troop_stack_hide(struct game *restrict game, struct hexagon *restrict hexagon, unsigned player, size_t index);

struct battles *turn_battles_init(struct game *restrict game);
void turn_battles_term(struct game *restrict game, struct battles *restrict battles);
