/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BATTLEFIELD_WIDTH 25
#define BATTLEFIELD_HEIGHT 25

#define BATTLEFIELD_PAWNS_LIMIT 7 /* TODO why is this so big? 5 seems fine? */

#define REACHABLE_LIMIT (BATTLEFIELD_WIDTH * BATTLEFIELD_HEIGHT)

#define OWNER_NONE 0 /* sentinel alliance value used for walls */ /* TODO fix this: neutral players should not own walls */

#define ASSAULT_LIMIT 5

#define PATH_QUEUE_LIMIT 8

// Instead of defining DISTANCE_MELEE directly, define its reciprocal to work-around C language limitation.
// http://stackoverflow.com/questions/38054023/c-fixed-size-array-treated-as-variable-size
// TODO try to fix this without the work-around
#define STEPS_FIELD 4
#define DISTANCE_MELEE ((PAWN_RADIUS * 2) + (1.0 / STEPS_FIELD)) /* 1.25 */
#define DISTANCE_RANGED (PAWN_RADIUS * 2) /* 1.0 */

// BATTLE_MOVEMENT_STEPS is chosen as to ensure that enemies are close enough to fight on the step before they collide.
#define BATTLE_MOVEMENT_STEPS (unsigned)(2 * UNIT_SPEED_LIMIT * STEPS_FIELD)

#define GATES_LIMIT 3

#define TAU (2 * M_PI)

#define POSITION_NONE (struct position){NAN, NAN}

struct position
{
	float x, y;
};

#define ARRAY_GLOBAL
#define ARRAY_NAME array_path
#define ARRAY_TYPE struct position
#include "generic/array.g"

struct battle_info;
struct battlefield;

struct pawn
{
	struct troop *troop;
	unsigned char owner;
	unsigned count, hurt, attackers;

	struct position position; // current pawn position
	struct position position_next; // expected pawn position at the next step

	struct array_path path; // pathfinding-generated positions corresponding to pawn command

	unsigned startup;

	unsigned stop_time; // step when the pawn stopped moving

	// WARNING: Player-specific input variables.
	struct pawn_command
	{
		struct
		{
			size_t count;
			struct position data[PATH_QUEUE_LIMIT];
		} moves; // list of user-specified locations to visit
		enum pawn_action {ACTION_HOLD, ACTION_GUARD, ACTION_FIGHT, ACTION_SHOOT, ACTION_ASSAULT} action; // TODO maybe rename ACTION_FIGHT to ACTION_FOLLOW
		union
		{
			struct pawn *pawn; // for ACTION_FIGHT
			struct position position; // for ACTION_GUARD and ACTION_SHOOT
			struct battlefield *field; // for ACTION_ASSAULT
		} target;
	} command;
};

enum {POSITION_RIGHT = 0x1, POSITION_TOP = 0x2, POSITION_LEFT = 0x4, POSITION_BOTTOM = 0x8, SIDE_RIGHT = 0x10, SIDE_TOP = 0x20, SIDE_LEFT = 0x40, SIDE_BOTTOM = 0x80, POSITION = 0xf, SIDE = 0xf0};
struct battlefield
{
	struct tile tile;
	enum blockage {BLOCKAGE_NONE, BLOCKAGE_TERRAIN, BLOCKAGE_WALL, BLOCKAGE_GATE, BLOCKAGE_TOWER} blockage;
	unsigned char blockage_location;

	// WARNING: Player-specific input variables.
	unsigned char gate_switch;

	signed char owner; // for BLOCKAGE_GATE
	unsigned char open; // for BLOCKAGE_GATE
	unsigned strength; // for BLOCKAGE_WALL, BLOCKAGE_GATE and BLOCKAGE_TOWER // TODO rename to health or store hurt instead (like for pawns)
	const struct unit *unit; // for BLOCKAGE_WALL, BLOCKAGE_GATE and BLOCKAGE_TOWER
//	struct pawn *pawn; // for BLOCKAGE_TOWER
	struct tile ladder; // for BLOCKAGE_TOWER

	struct pawn *pawns[BATTLEFIELD_PAWNS_LIMIT]; // used during player input; initialized by round_prepare()
};

enum victim_index {VICTIMS_MELEE, VICTIMS_RANGED, /* must be last */ VICTIMS_COUNT};

struct battle
{
	struct battlefield field[BATTLEFIELD_HEIGHT][BATTLEFIELD_WIDTH]; // tile coordinates -> battlefield information

	size_t pawns_count;
	struct pawn *pawns;

	struct hexagon *hexagon;
	int assault;

	struct battlefield *gates[GATES_LIMIT];
	size_t gates_count;

	struct
	{
		size_t pawns_count, pawns_waiting;
		struct pawn **pawns;
		enum {PLAYER_DEAD, PLAYER_ALIVE /*, PLAYER_RETREAT */} state;
		unsigned char startup;
	} players[PLAYERS_LIMIT];
	unsigned players_count;

	unsigned round;

	// initialized at each round
	struct obstacles *obstacles;
	struct adjacency_list *graph[PLAYERS_LIMIT]; // for each player

	// animation cache
	struct
	{
		struct pawn_animation
		{
			struct position position[BATTLE_MOVEMENT_STEPS];
			int visible;
			struct position shoot;
			unsigned victims[VICTIMS_COUNT];
		} *pawns;
		struct battlefield_animation
		{
			enum blockage blockage;
			unsigned char blockage_location;
			unsigned char open;
		} field[BATTLEFIELD_HEIGHT][BATTLEFIELD_WIDTH];
		size_t steps;
		size_t steps_damage[VICTIMS_COUNT];
	} animation;

#if defined(AI_DEBUG)
	struct reachable
	{
		double distance[BATTLEFIELD_HEIGHT * 2 - 1][BATTLEFIELD_WIDTH * 2 - 1];
	} rating;
#endif
};

static inline struct battlefield *battle_field(struct battle *restrict battle, struct tile tile)
{
	return &battle->field[tile.y][tile.x];
}

// Calculates the euclidean distance between a and b.
static inline double battlefield_distance(struct position a, struct position b)
{
	double dx = b.x - a.x, dy = b.y - a.y;
	return sqrt(dx * dx + dy * dy);
}

static inline int pawn_waiting(const struct pawn *pawn)
{
	return (isnan(pawn->position.x) || isnan(pawn->position.y));
}

int battle_init(const struct game *restrict game, struct battle *restrict battle, struct battle_info *restrict info);
void battle_term(const struct game *restrict game, struct battle *restrict battle);
 
struct pawn *pawn_find(const struct battlefield *restrict field, struct position position, float radius);
struct position pawn_position_formation(const struct battle *restrict battle, unsigned char player);

void battle_retreat(struct battle *restrict battle, unsigned char player);
int battle_end(const struct game *restrict game, struct battle *restrict battle);
