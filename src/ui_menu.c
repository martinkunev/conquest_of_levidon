/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include <GL/glx.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "world.h"
#include "font.h"
#include "interface.h"
#include "display.h"
#include "input.h"
#include "ui.h"
#include "ui_menu.h"
#include "menu.h"

#define TITLE "Conquest of Levidon (beta)"
#define TITLE_SAVE "Save game"

#define TABS_X 32
#define TABS_Y 32
#define TAB_PADDING 2

#define PLAYERS_INDICATOR_SIZE 32
#define PLAYERS_PADDING 8

#define WORLDS_X 32
#define WORLDS_Y 56

// TODO fix these values
#define TOOLTIP_X 256
#define TOOLTIP_Y 750

#define MARGIN 4

#define TROOPS_VISIBLE 7
#define TROOPS_GARRISON 6

#define MENU_MESSAGE_X 32
#define MENU_MESSAGE_Y 600

#define TITLE_Y 16

#define BUTTON_WIDTH 120
#define BUTTON_HEIGHT 20

#define BUTTON_ENTER_X 900
#define BUTTON_ENTER_Y 696

#define BUTTON_RETURN_X 900
#define BUTTON_RETURN_Y 720

#define BUTTON_EXIT_X 900
#define BUTTON_EXIT_Y 744

#define BUTTON_READY_X 6
#define BUTTON_READY_Y 620

// TODO display long filenames properly

#define S(s) (s), sizeof(s) - 1

#define ERROR_STRING_SAVE "Unable to save game"

// TODO handle long filenames properly

static void show_files(const struct state *state)
{
	struct vector position;
	size_t i;

	// Show active world directory tab.
	position = object_position(&widgets[WorldTabs], state->directory);
	fill_rectangle(position.x, position.y, widgets[WorldTabs].width, widgets[WorldTabs].height, display_colors[Active]);

	// Show world directories tabs.
	position = object_position(&widgets[WorldTabs], 0);
	draw_string(slice("system"), position.x + TAB_PADDING, position.y + TAB_PADDING, &font12, display_colors[TextMenu]);
	position = object_position(&widgets[WorldTabs], 1);
	draw_string(slice("user"), position.x + TAB_PADDING, position.y + TAB_PADDING, &font12, display_colors[TextMenu]);
	position = object_position(&widgets[WorldTabs], 2);
	draw_string(slice("saved"), position.x + TAB_PADDING, position.y + TAB_PADDING, &font12, display_colors[TextMenu]);

	// Show file list.
	draw_rectangle(widgets[Worlds].left - 1, widgets[Worlds].top - 1, widgets[Worlds].span_x + 2, widgets[Worlds].span_y + 2, display_colors[TextMenu]);
	for(i = 0; i < state->worlds->count; ++i)
	{
		if (i == widgets[Worlds].rows)
			break; // TODO scrolling support

		position = object_position(&widgets[Worlds], i);
		if (state->world_index == i) // selected
		{
			fill_rectangle(position.x, position.y, widgets[Worlds].width, widgets[Worlds].height, display_colors[TextMenu]);
			draw_string(slice(state->worlds->names[i]->data, state->worlds->names[i]->size), position.x, position.y + (widgets[Worlds].height - font12.size) / 2, &font12, display_colors[TextSelected]);
		}
		else
		{
			draw_string(slice(state->worlds->names[i]->data, state->worlds->names[i]->size), position.x, position.y + (widgets[Worlds].height - font12.size) / 2, &font12, display_colors[TextMenu]);
		}
	}
}

void if_menu_load(const void *argument, double progress)
{
	const struct state *state = argument;
	const struct game *restrict game = state->game;

	struct box box = string_box(slice(TITLE), &font24);
	draw_string(slice(TITLE), (WINDOW_WIDTH - box.width) / 2, TITLE_Y, &font24, display_colors[TextMenu]);

	// TODO ? separate functions for loaded and !loaded

	if (!state->loaded)
		show_files(state);

	if (state->loaded == LOADED_NEW)
		display_minimap(game);

	if (state->name_size)
		draw_string(slice(state->name, state->name_size), widgets[Worlds].left, widgets[Worlds].bottom + MARGIN, &font12, display_colors[TextMenu]);
	if (!state->loaded)
	{
		struct box box = string_box(slice(state->name, state->name_size), &font12);
		draw_string_cursor(box, widgets[Worlds].left, widgets[Worlds].bottom + MARGIN, &font12, display_colors[TextMenu]);
	}

	if (state->loaded)
	{
		size_t i;
		struct vector position;
		for(i = 0; i < game->players_count; ++i)
		{
			struct slice type;

			position = object_position(&widgets[Players], i);

			display_flag(position.x, position.y, i);
			switch (game->players[i].type)
			{
			case Neutral:
				type = slice("Neutral");
				break;

			case Local:
				type = slice("local player");
				break;

			case Computer:
				type = slice("Computer");
				break;
			}
			draw_string(type, position.x + PLAYERS_INDICATOR_SIZE + PLAYERS_PADDING, position.y, &font12, display_colors[TextMenu]);
		}
	}

	if (state->loaded)
	{
		display_button(slice("Start game"), BUTTON_ENTER_X, BUTTON_ENTER_Y, 0);
		display_button(slice("Cancel"), BUTTON_RETURN_X, BUTTON_RETURN_Y, 0);
	}
	else
	{
		if (state->name_size) display_button(slice("Continue"), BUTTON_ENTER_X, BUTTON_ENTER_Y, 0);
		else display_button(slice(" "), BUTTON_ENTER_X, BUTTON_ENTER_Y, 0);
		display_button(slice("Exit"), BUTTON_EXIT_X, BUTTON_EXIT_Y, 0);
	}
}

void if_menu_save(const void *argument, double progress)
{
	const struct state *state = argument;

	struct box box = string_box(slice(TITLE_SAVE), &font24);
	draw_string(slice(TITLE_SAVE), (WINDOW_WIDTH - box.width) / 2, TITLE_Y, &font24, display_colors[TextMenu]);

	show_files(state);

	if (state->name_size)
		draw_string(slice(state->name, state->name_size), widgets[Worlds].left, widgets[Worlds].bottom + MARGIN, &font12, display_colors[TextMenu]);
	box = string_box(slice(state->name, state->name_position), &font12);
	draw_string_cursor(box, widgets[Worlds].left, widgets[Worlds].bottom + MARGIN, &font12, display_colors[TextMenu]);

	if (state->error_size)
		draw_string(slice(state->error, state->error_size), MENU_MESSAGE_X, MENU_MESSAGE_Y, &font12, display_colors[Error]);

	display_button(slice("Save"), BUTTON_ENTER_X, BUTTON_ENTER_Y, 0);
	display_button(slice("Return to game"), BUTTON_RETURN_X, BUTTON_RETURN_Y, 0);
	display_button(slice("Quit game"), BUTTON_EXIT_X, BUTTON_EXIT_Y, 0);
}

static int tab_select(struct state *restrict state, size_t index)
{
	// TODO is this behavior on error good?
	struct files *worlds_new = menu_worlds(index);
	if (!worlds_new) return -1; // TODO this could be several different errors

	menu_free(state->worlds);
	state->worlds = worlds_new;
	state->directory = index;
	state->world_index = -1;
	state->name_size = 0;
	state->name_position = 0;
	return 0;
}

static void world_select(struct state *restrict state, size_t index)
{
	const struct bytes *filename = state->worlds->names[index];
	state->world_index = index;
	format_bytes(state->name, filename->data, filename->size); // TODO buffer overflow
	state->name_position = state->name_size = filename->size;
}

static int input_continue(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	//const struct game *restrict game = state->game;

	if (code != EVENT_MOUSE_LEFT) return INPUT_NOTME;
	if (state->name_size) return INPUT_TERMINATE;
	else return INPUT_DONE;
}

static int input_none(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	//const struct game *restrict game = state->game;

	// Ignore input if control, alt or super is pressed.
	// TODO is this necessary?
	if (modifiers & (XCB_MOD_MASK_CONTROL | XCB_MOD_MASK_1 | XCB_MOD_MASK_4))
		return INPUT_NOTME;

	switch (code)
	{
	case XK_Up:
		if (state->world_index == 0) return INPUT_IGNORE;
		if (state->world_index < 0) world_select(state, state->worlds->count - 1);
		else world_select(state, state->world_index - 1);
		return 0;

	case XK_Down:
		if (state->world_index == (state->worlds->count - 1)) return INPUT_IGNORE;
		if (state->world_index < 0) world_select(state, 0);
		else world_select(state, state->world_index + 1);
		return 0;

	case XK_Return:
		return input_continue(EVENT_MOUSE_LEFT, x, y, modifiers, argument, progress);

	case 'q': // TODO isn't this buggy
	case XK_Escape:
		return INPUT_TERMINATE;

	case EVENT_MOTION:
	case EVENT_MOUSE_LEFT:
	case EVENT_MOUSE_RIGHT:
		return INPUT_NOTME;

	default:
		if ((code > 31) && (code < 127))
		{
			if (state->name_size == FILENAME_LIMIT) return INPUT_IGNORE; // TODO indicate the buffer is full

			if (state->name_position < state->name_size)
				memmove(state->name + state->name_position + 1, state->name + state->name_position, state->name_size - state->name_position);
			state->name[state->name_position++] = code;
			state->name_size += 1;
		}
		else
		{
			state->name_size = 0;
			state->name_position = 0;
		}
		state->world_index = -1;
		return 0;

	case XK_BackSpace:
		if (!state->name_position) return INPUT_IGNORE;

		if (state->name_position < state->name_size)
			memmove(state->name + state->name_position - 1, state->name + state->name_position, state->name_size - state->name_position);
		state->name_position -= 1;
		state->name_size -= 1;
		state->world_index = -1;

		return 0;

	case XK_Delete:
		if (state->name_position == state->name_size) return INPUT_IGNORE;

		state->name_size -= 1;
		if (state->name_position < state->name_size)
			memmove(state->name + state->name_position, state->name + state->name_position + 1, state->name_size - state->name_position);
		state->world_index = -1;

		return 0;

	case XK_Home:
		if (state->name_position == 0) return INPUT_IGNORE;
		state->name_position = 0;
		return 0;

	case XK_End:
		if (state->name_position == state->name_size) return INPUT_IGNORE;
		state->name_position = state->name_size;
		return 0;

	case XK_Left:
		if (state->name_position == 0) return INPUT_IGNORE;
		state->name_position -= 1;
		return 0;

	case XK_Right:
		if (state->name_position == state->name_size) return INPUT_IGNORE;
		state->name_position += 1;
		return 0;
	}
}

static int input_write(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	const struct game *restrict game = state->game;

	int status;

	if (code != EVENT_MOUSE_LEFT) return INPUT_NOTME;

	status = menu_save(state->directory, state->name, state->name_size, game);
	if (status == ERROR_MEMORY) return ERROR_MEMORY;
	if (!status) return INPUT_TERMINATE;

	state->error = ERROR_STRING_SAVE;
	state->error_size = sizeof(ERROR_STRING_SAVE) - 1;

	return INPUT_DONE;
}

static int input_menu(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	//struct state *state = argument;
	//const struct game *restrict game = state->game;

	// Ignore input if control, alt or super is pressed.
	// TODO is this necessary?
	if (modifiers & (XCB_MOD_MASK_CONTROL | XCB_MOD_MASK_1 | XCB_MOD_MASK_4)) return INPUT_NOTME;

	switch (code)
	{
	case XK_Return:
		return input_write(EVENT_MOUSE_LEFT, x, y, modifiers, argument, progress);
	case XK_Escape:
		return INPUT_TERMINATE;
	default:
		return INPUT_NOTME;
	}
}

static int input_tab(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	//const struct game *restrict game = state->game;

	ssize_t index;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	// Find which tab was clicked.
	index = object_index(&widgets[WorldTabs], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE; // no tab clicked

	if (tab_select(state, index) < 0)
		return INPUT_IGNORE; // TODO
	return INPUT_DONE;
}

static int input_world(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	//const struct game *restrict game = state->game;

	ssize_t index;

	if (code == EVENT_MOTION) return INPUT_NOTME;
	if (code >= 0) return INPUT_NOTME; // ignore keyboard events

	// Find which world was clicked.
	index = object_index(&widgets[Worlds], (struct vector){x, y});
	if ((index < 0) || (index >= state->worlds->count)) goto reset; // no world clicked

	// TODO scrolling

	if (code == EVENT_MOUSE_LEFT)
	{
		world_select(state, index);
		return 0;
	}

	return INPUT_IGNORE;

reset:
	if (state->world_index >= 0)
	{
		state->name_size = 0;
		state->name_position = 0;
		state->world_index = -1;
		return 0;
	}
	else return INPUT_IGNORE;
}

static int input_setup(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	//struct state *state = argument;
	//const struct game *restrict game = state->game;

	switch (code)
	{
	case XK_Return:
		return INPUT_TERMINATE;
	case XK_Escape:
		return ERROR_CANCEL;
	default:
		return INPUT_NOTME;
	}
}

static int input_player(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state *state = argument;
	struct game *restrict game = state->game;

	ssize_t index;

	if (code == EVENT_MOTION) return INPUT_NOTME;
	if (code >= 0) return INPUT_NOTME; // ignore keyboard events

	// TODO customize player alliances, colors, etc.

	// Find which player was clicked. Ignore the neutral player.
	index = object_index(&widgets[Players], (struct vector){x, y});
	if ((index < 0) || (index >= game->players_count)) return INPUT_IGNORE; // no player clicked
	if (index == PLAYER_NEUTRAL) return INPUT_IGNORE;

	if (code == EVENT_MOUSE_LEFT)
	{
		if (game->players[index].type == Local)
		{
			// Make sure there is another player of type Local.
			size_t i;
			for(i = 0; i < game->players_count; ++i)
			{
				if (i == index) continue;
				if (game->players[i].type == Local)
				{
					game->players[index].type = Computer;
					return 0;
				}
			}
			return INPUT_IGNORE;
		}
		else if (game->players[index].type == Computer)
		{
			game->players[index].type = Local;
			return 0;
		}
	}

	return INPUT_IGNORE;
}

int input_menu_load(struct game *restrict game)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH - 1,
			.top = 0,
			.bottom = WINDOW_HEIGHT - 1,
			.callback = input_none
		},
		{
			.left = BUTTON_ENTER_X,
			.right = BUTTON_ENTER_X + BUTTON_WIDTH,
			.top = BUTTON_ENTER_Y,
			.bottom = BUTTON_ENTER_Y + BUTTON_HEIGHT,
			.callback = input_continue
		},
		{
			.left = BUTTON_EXIT_X,
			.right = BUTTON_EXIT_X + BUTTON_WIDTH,
			.top = BUTTON_EXIT_Y,
			.bottom = BUTTON_EXIT_Y + BUTTON_HEIGHT,
			.callback = input_mouse_cancel
		},
		{
			.left = widgets[WorldTabs].left,
			.right = widgets[WorldTabs].right,
			.top = widgets[WorldTabs].top,
			.bottom = widgets[WorldTabs].bottom,
			.callback = input_tab
		},
		{
			.left = widgets[Worlds].left,
			.right = widgets[Worlds].right,
			.top = widgets[Worlds].top,
			.bottom = widgets[Worlds].bottom,
			.callback = input_world
		},
	};

	struct area areas_setup[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH - 1,
			.top = 0,
			.bottom = WINDOW_HEIGHT - 1,
			.callback = input_setup
		},
		{
			.left = BUTTON_ENTER_X,
			.right = BUTTON_ENTER_X + BUTTON_WIDTH,
			.top = BUTTON_ENTER_Y,
			.bottom = BUTTON_ENTER_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate
		},
		{
			.left = BUTTON_RETURN_X,
			.right = BUTTON_RETURN_X + BUTTON_WIDTH,
			.top = BUTTON_RETURN_Y,
			.bottom = BUTTON_RETURN_Y + BUTTON_HEIGHT,
			.callback = input_mouse_cancel
		},
		{
			.left = widgets[Players].left,
			.right = widgets[Players].right,
			.top = widgets[Players].top,
			.bottom = widgets[Players].bottom,
			.callback = input_player
		},
	};

	struct state state;
	int status;

	state.game = game;

	state.worlds = 0;
	if (tab_select(&state, 0) < 0) return -1; // TODO

	// TODO select some world by default (maybe the newest for saves and the oldest for other worlds)
	//#define WORLD_DEFAULT "worlds/balkans"

retry:
	state.loaded = 0;

	while (1)
	{
		status = input_process(areas, sizeof(areas) / sizeof(*areas), if_menu_load, 0, &state);
		if (status) goto finally;

		status = menu_load(state.directory, state.name, state.name_size, game);
		if (status == ERROR_MEMORY) goto finally;
		if (!status)
		{
			state.loaded = menu_new(state.directory) ? LOADED_NEW : LOADED_SAVED;
			break;
		}

		// TODO indicate the error with something in red
		state.name_size = 0;
		state.name_position = 0;
		state.world_index = -1;
	}

	status = input_process(areas_setup, sizeof(areas_setup) / sizeof(*areas_setup), if_menu_load, 0, &state);
	if (status == ERROR_CANCEL)
	{
		world_unload(game);
		goto retry;
	}

finally:
	menu_free(state.worlds);
	return status;
}

int input_menu_save(const struct game *restrict game)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH - 1,
			.top = 0,
			.bottom = WINDOW_HEIGHT - 1,
			.callback = input_none
		},
		{
			.left = BUTTON_ENTER_X,
			.right = BUTTON_ENTER_X + BUTTON_WIDTH,
			.top = BUTTON_ENTER_Y,
			.bottom = BUTTON_ENTER_Y + BUTTON_HEIGHT,
			.callback = input_write
		},
		{
			.left = BUTTON_RETURN_X,
			.right = BUTTON_RETURN_X + BUTTON_WIDTH,
			.top = BUTTON_RETURN_Y,
			.bottom = BUTTON_RETURN_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate
		},
		{
			.left = BUTTON_EXIT_X,
			.right = BUTTON_EXIT_X + BUTTON_WIDTH,
			.top = BUTTON_EXIT_Y,
			.bottom = BUTTON_EXIT_Y + BUTTON_HEIGHT,
			.callback = input_mouse_cancel
		},
		{
			.left = 0,
			.right = WINDOW_WIDTH - 1,
			.top = 0,
			.bottom = WINDOW_HEIGHT - 1,
			.callback = input_menu
		},
		{
			.left = widgets[WorldTabs].left,
			.right = widgets[WorldTabs].right,
			.top = widgets[WorldTabs].top,
			.bottom = widgets[WorldTabs].bottom,
			.callback = input_tab
		},
		{
			.left = widgets[Worlds].left,
			.right = widgets[Worlds].right,
			.top = widgets[Worlds].top,
			.bottom = widgets[Worlds].bottom,
			.callback = input_world
		},
	};

	struct state state;
	int status;

	state.game = (struct game *)game;

	state.worlds = 0;
	if (tab_select(&state, 2) < 0) return -1; // TODO

	state.loaded = 0;

	state.error_size = 0;

	// TODO ? generate some filename

	status = input_process(areas, sizeof(areas) / sizeof(*areas), if_menu_save, 0, &state);

finally:
	menu_free(state.worlds);
	return status;
}
