/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "game.h"
#include "map.h"
#include "economy.h"

#define WORKERS_LIMIT 48

#define POPULATION_SCALE 8 /* determines how fast efficiency decreases with increase of population */
#define RESOURCES_SCALE 0.125 /* determine how efficiency increase slows down with increase of resources */

#define FOOD_YIELD 0.125 /* food yield from 1 populaton given maximum efficiency */
#define FOOD_CONSUMED 0.05 /* food consumed by 1 population */

#define TAX_YIELD 0.0009765625 /* tax paid by 1 population per taxing level */

#define CHILDREN_LIMIT 8
#define LIFE_EXPECTANCY (12 * 50)
#define MORTALITY 0.2

/* TODO
	siege
		when village is under siege, the region does not produce food or gold
		available food is determined by garrison and granary
		consumed food is determined by population and troops
*/

// Update resources by adding change multiplied by a factor.
void resources_update(struct resources *restrict total, const struct resources *restrict change, double factor)
{
	total->gold += change->gold * factor;
	total->food += change->food * factor;
	total->wood += change->wood * factor;
	total->stone += change->stone * factor;
}

// Check whether there are enough resources available for a given purchase.
int resources_enough(const struct resources *restrict total, const struct resources *restrict required)
{
	if (total->gold + required->gold < 0)
		return 0;
	if (total->food + required->food < 0)
		return 0;
	if (total->wood + required->wood < 0)
		return 0;
	if (total->stone + required->stone < 0)
		return 0;
	return 1;
}

static double food_income(double population, double bonus)
{
	// Calculates efficiency of food production per person.
	double efficiency = pow(POPULATION_SCALE / (population + POPULATION_SCALE), RESOURCES_SCALE / (1 + bonus));
	return (FOOD_YIELD * efficiency - FOOD_CONSUMED) * population + 0.5;
}

static void happiness(struct region *restrict region)
{
	// TODO happiness equilibrium is calculated based on taxes, language and religion
	double happiness_equilibrium = 1 - region->tax / 10.0;

	// Happiness approaches the equilibrium value.
	// TODO buildings can reduce negative effects on hapiness equilibrium
	// TODO in addition, a battle decreases hapiness in nearby settlements
	region->happiness += 0.0625 * (happiness_equilibrium - region->happiness);

	if (region->happiness > 1)
		region->happiness = 1;
	else if (region->happiness < 0)
		region->happiness = 0;
}

void economy_income(struct game *restrict game, size_t hexagons_total)
{
	size_t index;

	for(index = 0; index < game->players_count; index += 1)
	{
		game->players[index].income = (struct resources){0};
		game->players[index].food_storage = 0;
		game->players[index].population = 0;
	}

	for(index = 0; index < hexagons_total; index += 1)
	{
		size_t i, j;
		struct hexagon *restrict hexagon = game->hexagons + index;
		size_t player;
		unsigned workers = 0, workers_saturated = 0;

		struct resources income = {0}, expense = {0};
		struct
		{
			double gold, food, wood, stone;
		} bonus = {0};

		const struct region *restrict region;

		// Troop stacks in enemy regions pay double expenses.
		double factor = 1;
		if (hexagon->terrain == TERRAIN_GROUND)
		{
			unsigned char region_owner = game->regions[hexagon->info.region_index].garrison.hexagon->owner;
			if (hexagon->private.count && (hexagon->private.data[0]->owner != region_owner))
				factor = 2;
		}

		for(i = 0; i < hexagon->private.count; i += 1)
		{
			const struct troop_stack *troop_stack = hexagon->private.data[i];

			for(j = 0; j < troop_stack->private.count; j += 1)
			{
				if (troop_stack->private.troops[j]->unit->worker)
					if (troop_stack->private.troops[j]->task != TASK_BUILD)
						workers += troop_stack->private.troops[j]->count;

				resources_update(&expense, &troop_stack->private.troops[j]->unit->expense, troop_stack->private.troops[j]->count * factor);
			}

			resources_update(&expense, &troop_stack->vessel->expense, 1);
		}

		if (!hexagon->buildings)
			continue;

		player = hexagon->owner;
		region = game->regions + hexagon->info.region_index;
		game->players[player].population += region->population;

		if (region->sieged)
			continue;

		for(i = 0; i < BUILDINGS_COUNT; i += 1)
			if (hexagon_built(hexagon, i))
			{
				bonus.gold += BUILDINGS[i].income.gold;
				bonus.food += BUILDINGS[i].income.food;
				bonus.wood += BUILDINGS[i].income.wood;
				bonus.stone += BUILDINGS[i].income.stone;

				game->players[hexagon->owner].food_storage += BUILDINGS[i].food_storage;

				workers_saturated += BUILDINGS[i].workers;
				if (hexagon->vessel_index >= 0)
					workers_saturated += VESSELS[hexagon->vessel_index].workers;

				resources_update(&expense, &BUILDINGS[i].expense, 1);
			}

		if (workers_saturated)
		{
			double ratio;

			// The number of busy workers is limited.
			if (workers > workers_saturated)
				workers = workers_saturated;

			ratio = (double)workers / workers_saturated;
			for(i = 0; i < BUILDINGS_COUNT; i += 1)
				if (hexagon_built(hexagon, i))
					resources_update(&income, &BUILDINGS[i].production, ratio);

/*			if (hexagon->vessel_index >= 0)
				hexagon->vessel_progress += workers_progress(ratio * VESSELS[hexagon->vessel_index].workers);*/
		}

		// Settlements produce gold and food.
		if (hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
		{
			income.food += food_income(region->population, bonus.food);
			income.gold += region->tax * (region->population * TAX_YIELD);
		}
		else
			income.food *= (1 + bonus.food);

		income.gold *= (1 + bonus.gold);
		income.wood *= (1 + bonus.wood);
		income.stone *= (1 + bonus.stone);

		resources_update(&game->players[player].income, &income, 1);
		resources_update(&game->players[player].income, &expense, 1);
	}
}

void economy_turn(struct game *restrict game)
{
	size_t index;

	// Food that cannot be stored disappears.
	for(index = 0; index < game->players_count; index += 1)
		if (game->players[index].treasury.food > game->players[index].food_storage)
			game->players[index].treasury.food = game->players[index].food_storage;

	economy_income(game, game->hexagons_total);

	for(index = 0; index < game->players_count; index += 1)
		resources_update(&game->players[index].treasury, &game->players[index].income, 1);

	for(index = 0; index < game->regions_count; index += 1)
	{
		struct region *region = game->regions + index;
		const struct hexagon *hexagon = region->garrison.hexagon;
		struct player *player;
		unsigned deaths = 0;
		double happiness_change = 0;

		if (!region->population || !hexagon->buildings)
			continue;

		player = game->players + hexagon->owner;

		if (region->sieged)
		{
			int food = 0, food_consumed = 0;
			size_t i;

			// Calculate food available in the sieged settlement.
			for(i = 0; i < BUILDINGS_COUNT; i += 1)
				if (hexagon_built(hexagon, i))
					food += BUILDINGS[i].food_storage;

			// Calculate food consumed in the sieged settlement.
			food_consumed += region->population * FOOD_CONSUMED;
			for(i = 0; i < region->garrison.private.count; i += 1)
				food_consumed -= region->garrison.private.troops[i]->unit->expense.food;

			if (food < food_consumed)
			{
				double food_shortage = 1 - (double)food / food_consumed;
				deaths = region->population * food_shortage * sqrt(food_shortage) + 0.5;
				happiness_change = -food_shortage;
			}
		}
		else
		{
			// Food surplus is calclated with the food left after storage has been filled.
			// Food shortage leads to deaths.
			if (player->treasury.food > player->food_storage)
			{
				double food_surplus = (player->treasury.food - player->food_storage) / (region->population * FOOD_CONSUMED);
				if (food_surplus > 1)
					food_surplus = 1;
				happiness_change = 0.25 * food_surplus;
			}
			else if (player->treasury.food < 0)
			{
				double food_shortage = -player->treasury.food / (player->population * FOOD_CONSUMED);
				deaths = region->population * food_shortage * sqrt(food_shortage) + 0.5;
				happiness_change = -food_shortage;
			}
		}

		// Calculate population change.
		// Divide expected number of children per person by expected lifetime and by the number of parents (2).
		// TODO population should only grow when the city is well developed (maybe play with mortality)
		region->population += region->population * CHILDREN_LIMIT * (region->happiness - MORTALITY) / (2 * LIFE_EXPECTANCY);
		if (region->population > deaths)
			region->population -= deaths;
		else
		{
			region->population = 0;
			continue;
		}

		region->happiness += happiness_change;
		happiness(region);
	}

	for(index = 0; index < game->players_count; index += 1)
	{
		// TODO some action if gold becomes negative (and maybe wood and stone)?
		struct resources *restrict treasury = &game->players[index].treasury;
		if (treasury->food < 0)
			treasury->food = 0;
		if (treasury->gold < 0)
			treasury->gold = 0;
		if (treasury->wood < 0)
			treasury->wood = 0;
		if (treasury->stone < 0)
			treasury->stone = 0;
	}
}
