/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glx.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "image.h"
#include "interface.h"
#include "interface_storage.h"
#include "display.h"
#include "input.h"
#include "ui.h"
#include "ui_editor.h"
#include "edit.h"

#define EDITOR_X 0
#define EDITOR_Y 0
#define EDITOR_WIDTH (WINDOW_WIDTH - TOOLBOX_WIDTH - EDITOR_X)
#define EDITOR_HEIGHT (WINDOW_HEIGHT - EDITOR_Y)

// TODO do i need these?
#define BUTTON_TERRAIN_X (TOOLBOX_X + 8)
#define BUTTON_TERRAIN_Y (TOOLBOX_Y + 8)
#define BUTTON_REGIONS_X (TOOLBOX_X + 8)
#define BUTTON_REGIONS_Y (TOOLBOX_Y + 36)
#define BUTTON_OBJECTS_X (TOOLBOX_X + 8)
#define BUTTON_OBJECTS_Y (TOOLBOX_Y + 64)
#define BUTTON_DUMP_X (TOOLBOX_X + 132)
#define BUTTON_DUMP_Y (TOOLBOX_Y + 720)

#define REGIONNUMBER_X (TOOLBOX_X + 8)
#define REGIONNUMBER_Y (TOOLBOX_Y + 96)

#define REGIONNAME_X (TOOLBOX_X + 8)
#define REGIONNAME_Y (TOOLBOX_Y + 128)
// /TODO

#define MARGIN 16

#define SCROLL_SPEED 25

#define S(s) (s), sizeof(s) - 1

unsigned MAP_WIDTH;
unsigned MAP_HEIGHT;

static struct widget widgets_editor[3];

static inline int region_same(const struct hexagon *restrict hexagon, const struct hexagon *restrict other)
{
	if (other->terrain != TERRAIN_GROUND)
		return 0;
	return (hexagon->info.region_index == other->info.region_index);
}

static void scroll_update(struct state *restrict state, double progress)
{
	int count = SCROLL_SPEED * (progress - state->scroll_start) + 0.5;

	if (isnan(state->scroll_start))
		return; // screen is not scrolling

	if (MAP_WIDTH > EDITOR_WIDTH)
	{
		state->scroll_x = state->scroll_x + count * state->delta_x;
		if (state->scroll_x < 0)
			state->scroll_x = 0;
		else if (state->scroll_x > MAP_WIDTH - EDITOR_WIDTH)
			state->scroll_x = MAP_WIDTH - EDITOR_WIDTH;
	}

	if (MAP_HEIGHT > EDITOR_HEIGHT)
	{
		state->scroll_y = state->scroll_y + count * state->delta_y;
		if (state->scroll_y < 0)
			state->scroll_y = 0;
		else if (state->scroll_y > MAP_HEIGHT - EDITOR_HEIGHT)
			state->scroll_y = MAP_HEIGHT - EDITOR_HEIGHT;
	}
}

static void show_editor(const void *restrict context, double progress)
{
	const struct state *state = context;
	const struct game *restrict game = state->game;

	unsigned x, y;
	size_t i;

	double offset_x, offset_y;

	int base_x = EDITOR_X - state->scroll_x;
	int base_y = EDITOR_Y - state->scroll_y;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	fill_image(&image_world, base_x, base_y, image_world.width, image_world.height, 0);

	for(y = 0; y < game->map_height; y += 1)
	{
		unsigned width = hexagons_width(game, y);
		for(x = 0; x < width; x += 1)
		{
			size_t index = hexagon_index(game, x, y);
			const struct hexagon *restrict hexagon = game->hexagons + index;
			unsigned char color[4];

			if (hexagon->terrain == TERRAIN_MOUNTAIN)
				continue;

			offset_x = hexagon_offset_x(base_x, x, y, HEXAGON_EDGE_EDITOR);
			offset_y = hexagon_offset_y(base_y, x, y, HEXAGON_EDGE_EDITOR);

			switch (hexagon->terrain)
			{
			case TERRAIN_GROUND:
				if ((state->tool != TOOL_TERRAIN) && (state->region_index == hexagon->info.region_index))
					memcpy(color, display_colors[Self], sizeof(color));
				else
					memcpy(color, display_colors[Player], sizeof(color));
				break;

			case TERRAIN_MOUNTAIN:
				memcpy(color, display_colors[Background], sizeof(color));
				break;

			case TERRAIN_WATER:
				memcpy(color, display_colors[Water], sizeof(color));
				break;
			}

			if (hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
			{
				struct army army = {.owner = hexagon->owner};
				display_settlement(game, hexagon_location(game, index), base_x, base_y, GARRISON_NONE, &army, PLAYER_NEUTRAL, HEXAGON_EDGE_EDITOR);
			}
			else if (hexagon->environment & (1 << ENVIRONMENT_FOREST))
				display_object(game, &image_forest, 0, PLAYER_NEUTRAL, hexagon_location(game, index), base_x, base_y, HEXAGON_EDGE_EDITOR);
			else if (hexagon->environment & (1 << ENVIRONMENT_QUARRY))
				display_object(game, &image_quarry, 0, PLAYER_NEUTRAL, hexagon_location(game, index), base_x, base_y, HEXAGON_EDGE_EDITOR);

			color[3] = 128; // make the hexagon half transparent
			display_hexagon(offset_x, offset_y, (HEXAGON_EDGE / HEXAGON_EDGE_EDITOR), color);
		}
	}

	// Draw region borders.
	for(y = 0; y < game->map_height; y += 1)
	{
		unsigned width = hexagons_width(game, y);
		for(x = 0; x < width; x += 1)
		{
			size_t index = hexagon_index(game, x, y);
			const struct hexagon *restrict hexagon = game->hexagons + index;
			int first = 1;

			if (hexagon->terrain != TERRAIN_GROUND)
				continue;

			offset_x = hexagon_offset_x(base_x, x, y, HEXAGON_EDGE_EDITOR);
			offset_y = hexagon_offset_y(base_y, x, y, HEXAGON_EDGE_EDITOR);

			// Check for neighbors to the east.
			if ((x == width - 1) || !region_same(hexagon, game->hexagons + index + 1))
				draw_line(offset_x + SQRT3 * HEXAGON_EDGE_EDITOR + 0.5, offset_y + 1.5 * HEXAGON_EDGE_EDITOR + 0.5, offset_x + SQRT3 * HEXAGON_EDGE_EDITOR + 0.5, offset_y + 0.5 * HEXAGON_EDGE_EDITOR + 0.5, display_colors[Border]);

			// Check for neighbors to the north-east and north-west.
			if (!y || (x == game->map_width - 1) || !region_same(hexagon, game->hexagons + index - game->map_width + 1))
				draw_line(offset_x + SQRT3 * HEXAGON_EDGE_EDITOR / 2 + 0.5, offset_y - 0.5, offset_x + SQRT3 * HEXAGON_EDGE_EDITOR + 0.5, offset_y + 0.5 * HEXAGON_EDGE_EDITOR - 0.5, display_colors[Border]);
			else
				first = 0;
			if (!y || (!x && (width == game->map_width)) || !region_same(hexagon, game->hexagons + index - game->map_width))
				draw_line(offset_x + SQRT3 * HEXAGON_EDGE_EDITOR / 2, offset_y - 0.5, offset_x, offset_y + 0.5 * HEXAGON_EDGE_EDITOR - 0.5, display_colors[Border]);
			else
				first = 0;

			// Check for neighbors to the west.
			if (!x || !region_same(hexagon, game->hexagons + index - 1))
				draw_line(offset_x - 0.5, offset_y + 0.5 * HEXAGON_EDGE_EDITOR - 0.5, offset_x - 0.5, offset_y + 1.5 * HEXAGON_EDGE_EDITOR - 0.5, display_colors[Border]);
			else
				first = 0;

			// Check for neighbors to the south-west and south-east.
			if ((y == game->map_height - 1) || (!x && (width == game->map_width)) || !region_same(hexagon, game->hexagons + index + game->map_width - 1))
				draw_line(offset_x + SQRT3 * HEXAGON_EDGE_EDITOR / 2 - 0.5, offset_y + 2 * HEXAGON_EDGE_EDITOR + 0.5, offset_x - 0.5, offset_y + 1.5 * HEXAGON_EDGE_EDITOR + 0.5, display_colors[Border]);
			if ((y == game->map_height - 1) || (x == game->map_width - 1) || !region_same(hexagon, game->hexagons + index + game->map_width))
				draw_line(offset_x + SQRT3 * HEXAGON_EDGE_EDITOR / 2, offset_y + 2 * HEXAGON_EDGE_EDITOR + 0.5, offset_x + SQRT3 * HEXAGON_EDGE_EDITOR, offset_y + 1.5 * HEXAGON_EDGE_EDITOR + 0.5, display_colors[Border]);

			if (first)
			{
				const struct region *restrict region = game->regions + hexagon->info.region_index;
				draw_string(slice(region->name, region->name_size), offset_x + 2, offset_y + 0.5 * HEXAGON_EDGE_EDITOR, &font10, display_colors[Text]);
			}
		}
	}

	fill_rectangle(TOOLBOX_X, TOOLBOX_Y, TOOLBOX_WIDTH, TOOLBOX_HEIGHT, display_colors[BackgroundBox]);

	switch (state->tool)
	{
		struct vector position;

	case TOOL_TERRAIN:
		// Show brushes.
		position = object_position(&widgets_editor[EditorBrush], TERRAIN_MOUNTAIN);
		fill_rectangle(position.x, position.y, widgets_editor[EditorBrush].width, widgets_editor[EditorBrush].height, display_colors[Border]);
		position = object_position(&widgets_editor[EditorBrush], TERRAIN_GROUND);
		fill_rectangle(position.x, position.y, widgets_editor[EditorBrush].width, widgets_editor[EditorBrush].height, display_colors[Player]);
		position = object_position(&widgets_editor[EditorBrush], TERRAIN_WATER);
		fill_rectangle(position.x, position.y, widgets_editor[EditorBrush].width, widgets_editor[EditorBrush].height, display_colors[Water]);

		// Indicate which brush is selected.
		position = object_position(&widgets_editor[EditorBrush], state->terrain);
		draw_rectangle(position.x - 1, position.y - 1, widgets_editor[EditorBrush].width + 2, widgets_editor[EditorBrush].height + 2, display_colors[Active]);

		break;

	case TOOL_REGIONS:
		if (state->region_index >= 0)
		{
			const struct region *restrict region = game->regions + state->region_index;

			if (region->name_size)
				draw_string(slice(region->name, region->name_size), REGIONNAME_X, REGIONNAME_Y + MARGIN, &font12, display_colors[Text]);
			draw_string_cursor(string_box(slice(region->name, state->name_position), &font12), REGIONNAME_X, REGIONNAME_Y + MARGIN, &font12, display_colors[Text]);

			display_button(slice("done"), REGIONNUMBER_X, REGIONNUMBER_Y, 0);
		}
		else
			display_button(slice("new"), REGIONNUMBER_X, REGIONNUMBER_Y, 0);
		break;

	case TOOL_OBJECTS:
		position = object_position(&widgets_editor[EditorObject], 0);
		fill_rectangle(position.x, position.y, widgets_editor[EditorObject].width, widgets_editor[EditorObject].height, display_colors[Background]);
		position = object_position(&widgets_editor[EditorObject], ENVIRONMENT_SETTLEMENT);
		fill_image(&image_village, position.x, position.y, image_village.width, image_village.height, 0);
		position = object_position(&widgets_editor[EditorObject], ENVIRONMENT_FOREST);
		fill_image(&image_forest, position.x, position.y, image_forest.width, image_forest.height, 0);
		position = object_position(&widgets_editor[EditorObject], ENVIRONMENT_QUARRY);
		fill_image(&image_quarry, position.x, position.y, image_quarry.width, image_quarry.height, 0);

		position = object_position(&widgets_editor[EditorObject], state->object);
		draw_rectangle(position.x - 1, position.y - 1, widgets_editor[EditorObject].width + 1, widgets_editor[EditorObject].height + 1, display_colors[Active]);

		for(i = 0; i < PLAYERS_LIMIT; i += 1)
		{
			position = object_position(&widgets_editor[EditorFlag], i);
			display_flag(position.x, position.y, i);
		}
		position = object_position(&widgets_editor[EditorFlag], state->flag);
		draw_rectangle(position.x - 1, position.y - 1, widgets_editor[EditorFlag].width + 1, widgets_editor[EditorFlag].height + 1, display_colors[Active]);
		break;
	}

	display_button(slice("Terrain"), BUTTON_TERRAIN_X, BUTTON_TERRAIN_Y, (state->tool == TOOL_TERRAIN));
	display_button(slice("Regions"), BUTTON_REGIONS_X, BUTTON_REGIONS_Y, (state->tool == TOOL_REGIONS));
	display_button(slice("Objects"), BUTTON_OBJECTS_X, BUTTON_OBJECTS_Y, (state->tool == TOOL_OBJECTS));

	if (state->tool == TOOL_TERRAIN)
	{
		if (state->consistent)
			display_button(slice("Dump and Exit"), BUTTON_DUMP_X, BUTTON_DUMP_Y, 0);
		else
			display_button(slice("Inconsistent Map"), BUTTON_DUMP_X, BUTTON_DUMP_Y, 0);
//		show_button(S("Discard and Exit"), BUTTON_EXIT_X, BUTTON_EXIT_Y);
	}
}

static int timer_editor(void *restrict context, double progress)
{
	struct state *state = context;
	scroll_update(state, progress);
	return INPUT_DONE;
}

static int input_scroll(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	switch (code)
	{
	case EVENT_MOTION:
		scroll_update(state, progress);

		if (x == 0)
			state->delta_x = -1;
		else if (x == WINDOW_WIDTH - 1)
			state->delta_x = 1;
		else
			state->delta_x = 0;

		if (y == 0)
			state->delta_y = -1;
		else if (y == WINDOW_HEIGHT - 1)
			state->delta_y = 1;
		else
			state->delta_y = 0;

		if (state->delta_x || state->delta_y)
		{
			state->scroll_start = progress;
			return INPUT_DONE;
		}
		else
		{
			int status = isnan(state->scroll_start) ? INPUT_IGNORE : INPUT_DONE;
			state->scroll_start = NAN;
			return status;
		}

	default:
		return INPUT_IGNORE;
	}
}

static int input_terrain_modify(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	const struct game *restrict game = state->game;

	switch (code)
	{
	case EVENT_MOTION:
		if (!(modifiers & XCB_KEY_BUT_MASK_BUTTON_3))
			return INPUT_NOTME;
	case EVENT_MOUSE_LEFT:
		{
			enum terrain terrain_old;

			int index = storage_get(x + state->scroll_x, y + state->scroll_y);
			if (index < 0)
				return INPUT_IGNORE;

			terrain_old = game->hexagons[index].terrain;
			game->hexagons[index].terrain = state->terrain;

			switch (state->terrain)
			{
			case TERRAIN_GROUND:
				{
					struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
					size_t neighbors_count = hexagon_neighbors(game, game->hexagons + index, neighbors);
					size_t region_index;
					size_t i;

					for(i = 0; i < neighbors_count; i += 1)
						if (neighbors[i].hexagon->terrain == TERRAIN_GROUND)
						{
							region_index = neighbors[i].hexagon->info.region_index;
							goto done;
						}
					region_index = 0;
			done:
					game->hexagons[index].info.region_index = region_index;
					game->hexagons[index].environment = 0;
				}
				break;

			case TERRAIN_MOUNTAIN:
			case TERRAIN_WATER:
				if (terrain_old == TERRAIN_GROUND)
				{
					if (game->hexagons[index].environment & (1 << ENVIRONMENT_SETTLEMENT))
						game->regions[game->hexagons[index].info.region_index].garrison.hexagon = 0;
					game->hexagons[index].environment = 0;
				}
				break;
			}

			state->consistent = cleanup((struct game *)game);
		}
		return 0;

	default:
		return INPUT_NOTME;
	}
}

static int input_regions_modify(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	const struct game *restrict game = state->game;

	switch (code)
	{
		int index;
		struct region *restrict region;

	case EVENT_MOTION:
		return INPUT_NOTME;

	case EVENT_MOUSE_LEFT:
		index = storage_get(x + state->scroll_x, y + state->scroll_y);
		if ((index < 0) || (game->hexagons[index].terrain != TERRAIN_GROUND))
			return INPUT_IGNORE;
		if (state->region_index >= 0)
		{
			if ((game->hexagons[index].info.region_index != state->region_index) && (game->hexagons[index].environment & (1 << ENVIRONMENT_SETTLEMENT)))
			{
				// Remove garrison from the hexagon.
				game->regions[game->hexagons[index].info.region_index].garrison.hexagon = 0;
				game->hexagons[index].environment ^= 1 << ENVIRONMENT_SETTLEMENT;
			}

			// Set to which region the hexagon belongs.
			game->hexagons[index].info.region_index = state->region_index;

			region = game->regions + game->hexagons[index].info.region_index;
			if (!region->garrison.hexagon)
			{
				region->garrison.hexagon = game->hexagons + index;
				game->hexagons[index].environment |= 1 << ENVIRONMENT_SETTLEMENT;
				game->hexagons[index].owner = PLAYER_NEUTRAL;
			}
		}
		else
		{
			// Set current region.
			state->region_index = game->hexagons[index].info.region_index;
			state->name_position = game->regions[state->region_index].name_size;
		}
		return 0;

	case EVENT_MOUSE_RIGHT:
		index = storage_get(x + state->scroll_x, y + state->scroll_y);
		if ((index < 0) || (game->hexagons[index].terrain != TERRAIN_GROUND))
			return INPUT_IGNORE;
		region = game->regions + game->hexagons[index].info.region_index;

		if (region->garrison.hexagon)
		{
			region->garrison.hexagon->environment ^= 1 << ENVIRONMENT_SETTLEMENT;
			game->hexagons[index].owner = region->garrison.hexagon->owner;
		}
		else
			game->hexagons[index].owner = PLAYER_NEUTRAL;
		region->garrison.hexagon = game->hexagons + index;
		region->garrison.hexagon->environment |= 1 << ENVIRONMENT_SETTLEMENT;
		return 0;

	default:
		return INPUT_NOTME;
	}
}

static int input_name(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	const struct game *restrict game = state->game;
	struct region *restrict region;

	if (code == EVENT_MOTION)
		return INPUT_NOTME;

	if (state->region_index < 0) return INPUT_IGNORE;
	region = game->regions + state->region_index;

	switch (code)
	{
	default:
		if ((code > 31) && (code < 127))
		{
			if (region->name_size == NAME_LIMIT)
				return INPUT_IGNORE; // TODO indicate that the buffer is full

			if (state->name_position < region->name_size)
				memmove(region->name + state->name_position + 1, region->name + state->name_position, region->name_size - state->name_position);
			region->name[state->name_position++] = code;
			region->name_size += 1;

			return 0;
		}
		return INPUT_IGNORE;

	case XK_BackSpace:
		if (!state->name_position) return INPUT_IGNORE;

		if (state->name_position < region->name_size)
			memmove(region->name + state->name_position - 1, region->name + state->name_position, region->name_size - state->name_position);
		state->name_position -= 1;
		region->name_size -= 1;

		return 0;

	case XK_Delete:
		if (state->name_position == region->name_size) return INPUT_IGNORE;

		region->name_size -= 1;
		if (state->name_position < region->name_size)
			memmove(region->name + state->name_position, region->name + state->name_position + 1, region->name_size - state->name_position);
		return 0;

	case XK_Home:
		if (state->name_position == 0) return INPUT_IGNORE;
		state->name_position = 0;
		return 0;

	case XK_End:
		if (state->name_position == region->name_size) return INPUT_IGNORE;
		state->name_position = region->name_size;
		return 0;

	case XK_Left:
		if (state->name_position == 0) return INPUT_IGNORE;
		state->name_position -= 1;
		return 0;

	case XK_Right:
		if (state->name_position == region->name_size) return INPUT_IGNORE;
		state->name_position += 1;
		return 0;

	case XK_Escape:
		state->region_index = -1;
		return 0;
	}
}

static int input_region(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	const struct game *restrict game = state->game;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_IGNORE;

	if (state->region_index < 0)
	{
		// Button "new". Create new region.

		struct game *restrict game_mutable = (struct game *)game; // TODO fix cast
		struct region *restrict region;

		void *buffer = realloc(game_mutable->regions, (game->regions_count + 1) * sizeof(*game->regions)); // TODO this is not efficient
		if (!buffer)
			abort(); // TODO this may destroy work
		game_mutable->regions = buffer;

		region = game->regions + game->regions_count;
		region_init(game, region);

		state->region_index = game->regions_count;
		state->name_position = region->name_size;
		game_mutable->regions_count += 1;
	}
	else
	{
		// Button "done". Deselect region.
		state->region_index = -1;
	}

	return 0;
}

static int input_objects_modify(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *state = context;
	struct game *restrict game = (struct game *)state->game;
	int index;
	struct region *restrict region;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	index = storage_get(x + state->scroll_x, y + state->scroll_y);
	if ((index < 0) || (game->hexagons[index].terrain != TERRAIN_GROUND))
		return INPUT_IGNORE;

	region = game->regions + game->hexagons[index].info.region_index;
	if ((state->object == ENVIRONMENT_SETTLEMENT) && region->garrison.hexagon)
		region->garrison.hexagon->environment ^= (1 << ENVIRONMENT_SETTLEMENT);
	if (game->hexagons[index].environment & (1 << ENVIRONMENT_SETTLEMENT))
		region->garrison.hexagon = 0;
	if (state->object == ENVIRONMENT_SETTLEMENT)
		region->garrison.hexagon = game->hexagons + index;

	if (state->object)
		game->hexagons[index].environment |= 1 << state->object;
	else
		game->hexagons[index].environment = 0; // special casing to support resetting environment
	game->hexagons[index].owner = state->flag;
	if (state->flag && (state->object == ENVIRONMENT_SETTLEMENT))
		hexagon_build(game->hexagons + index, BuildingTownHall);

	while (game->players_count <= state->flag)
	{
		game->players[game->players_count].type = Neutral;
		game->players[game->players_count].treasury = (struct resources){0};
		game->players[game->players_count].alliance = game->players_count;
		game->players[game->players_count].name_size = 0;
		game->players_count += 1;
	}
	game->players[game->players_count - 1].type = Local;

	return INPUT_DONE;
}

static int input_brush(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	if (code == EVENT_MOUSE_LEFT)
	{
		int index = object_index(&widgets_editor[EditorBrush], (struct vector){x, y});
		if (index >= 0)
		{
			state->terrain = index;
			return 0;
		}
	}
	return INPUT_IGNORE;
}

static int input_dump(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	const struct game *restrict game = state->game;
	if (code == EVENT_MOUSE_LEFT)
	{
		// If the map can be dumped, dump it and exit the editor.
		if (cleanup((struct game *)game))
		{
			state->done = 1;
			return INPUT_TERMINATE;
		}

		state->consistent = 0;
	}
	return INPUT_IGNORE;
}

/*static int input_discard(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	const struct game *restrict game = state->game;
	if (code == EVENT_MOUSE_LEFT)
	{
		// If the map can be dumped, dump it and exit the editor.
		if (cleanup((struct game *)game))
			return INPUT_TERMINATE;

		state->consistent = false;
	}
	return INPUT_IGNORE;
}*/

static int input_object(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	int index;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	index = object_index(&widgets_editor[EditorObject], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE;
	state->object = index;
	return INPUT_DONE;
}

static int input_flag(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	int index;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	index = object_index(&widgets_editor[EditorFlag], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE;
	state->flag = index;
	return INPUT_DONE;
}

static int input_tool_terrain(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	const struct game *restrict game = state->game;
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_IGNORE;
	tool_switch_terrain(game, state);
	return INPUT_TERMINATE;
}

static int input_tool_regions(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	const struct game *restrict game = state->game;
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_IGNORE;
	tool_switch_regions(game, state);
	return INPUT_TERMINATE;
}

static int input_tool_objects(int code, unsigned x, unsigned y, uint16_t modifiers, void *context, double progress)
{
	struct state *restrict state = context;
	const struct game *restrict game = state->game;
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_IGNORE;
	tool_switch_objects(game, state);
	return INPUT_TERMINATE;
}

int input_terrain(const struct game *restrict game, struct state *restrict state)
{
	widgets_editor[EditorBrush] = (struct widget)WIDGET(1, 3, TOOLBOX_X + 8, TOOLBOX_Y + 100, 24, 24, 8);
	widgets_editor[EditorObject] = (struct widget)WIDGET(1, 4, TOOLBOX_X + 8, TOOLBOX_Y + 100, BUILDING_EDGE, BUILDING_EDGE, 2);
	widgets_editor[EditorFlag] = (struct widget)WIDGET(4, 6, TOOLBOX_X + 8, TOOLBOX_Y + 200, 32, 32, 2);

	{
		struct area areas[] = {
			{0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, input_scroll},
			{EDITOR_X, EDITOR_X + EDITOR_WIDTH, EDITOR_Y, EDITOR_Y + EDITOR_HEIGHT, input_terrain_modify},
			{BUTTON_TERRAIN_X, BUTTON_TERRAIN_X + BUTTON_WIDTH - 1, BUTTON_TERRAIN_Y, BUTTON_TERRAIN_Y + BUTTON_HEIGHT - 1, input_tool_terrain},
			{BUTTON_REGIONS_X, BUTTON_REGIONS_X + BUTTON_WIDTH - 1, BUTTON_REGIONS_Y, BUTTON_REGIONS_Y + BUTTON_HEIGHT - 1, input_tool_regions},
			{BUTTON_OBJECTS_X, BUTTON_OBJECTS_X + BUTTON_WIDTH - 1, BUTTON_OBJECTS_Y, BUTTON_OBJECTS_Y + BUTTON_HEIGHT - 1, input_tool_objects},
			{widgets_editor[EditorBrush].left, widgets_editor[EditorBrush].right, widgets_editor[EditorBrush].top, widgets_editor[EditorBrush].bottom, input_brush},
			{BUTTON_DUMP_X, BUTTON_DUMP_X + BUTTON_WIDTH - 1, BUTTON_DUMP_Y, BUTTON_DUMP_Y + BUTTON_HEIGHT - 1, input_dump},
	//		{BUTTON_DUMP_X, BUTTON_DUMP_X + BUTTON_WIDTH - 1, BUTTON_EXIT_Y, BUTTON_EXIT_Y + BUTTON_HEIGHT - 1, input_discard},
		};

		return input_process(areas, sizeof(areas) / sizeof(*areas), show_editor, timer_editor, state);
	}
}

int input_regions(const struct game *restrict game, struct state *restrict state)
{
	struct area areas[] = {
		{0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, input_scroll},
		{0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, input_name},
		{EDITOR_X, EDITOR_X + EDITOR_WIDTH, EDITOR_Y, EDITOR_Y + EDITOR_HEIGHT, input_regions_modify},
		{BUTTON_TERRAIN_X, BUTTON_TERRAIN_X + BUTTON_WIDTH - 1, BUTTON_TERRAIN_Y, BUTTON_TERRAIN_Y + BUTTON_HEIGHT - 1, input_tool_terrain},
		{BUTTON_REGIONS_X, BUTTON_REGIONS_X + BUTTON_WIDTH - 1, BUTTON_REGIONS_Y, BUTTON_REGIONS_Y + BUTTON_HEIGHT - 1, input_tool_regions},
		{BUTTON_OBJECTS_X, BUTTON_OBJECTS_X + BUTTON_WIDTH - 1, BUTTON_OBJECTS_Y, BUTTON_OBJECTS_Y + BUTTON_HEIGHT - 1, input_tool_objects},
		{REGIONNUMBER_X, REGIONNUMBER_X + BUTTON_WIDTH - 1, REGIONNUMBER_Y, REGIONNUMBER_Y + BUTTON_HEIGHT - 1, input_region}
	};

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_editor, timer_editor, state);
}

int input_objects(const struct game *restrict game, struct state *restrict state)
{
	struct area areas[] = {
		{0, WINDOW_WIDTH, 0, WINDOW_HEIGHT, input_scroll},
		{EDITOR_X, EDITOR_X + EDITOR_WIDTH, EDITOR_Y, EDITOR_Y + EDITOR_HEIGHT, input_objects_modify},
		{widgets_editor[EditorObject].left, widgets_editor[EditorObject].right, widgets_editor[EditorObject].top, widgets_editor[EditorObject].bottom, input_object},
		{widgets_editor[EditorFlag].left, widgets_editor[EditorFlag].right, widgets_editor[EditorFlag].top, widgets_editor[EditorFlag].bottom, input_flag},
		{BUTTON_TERRAIN_X, BUTTON_TERRAIN_X + BUTTON_WIDTH - 1, BUTTON_TERRAIN_Y, BUTTON_TERRAIN_Y + BUTTON_HEIGHT - 1, input_tool_terrain},
		{BUTTON_REGIONS_X, BUTTON_REGIONS_X + BUTTON_WIDTH - 1, BUTTON_REGIONS_Y, BUTTON_REGIONS_Y + BUTTON_HEIGHT - 1, input_tool_regions},
		{BUTTON_OBJECTS_X, BUTTON_OBJECTS_X + BUTTON_WIDTH - 1, BUTTON_OBJECTS_Y, BUTTON_OBJECTS_Y + BUTTON_HEIGHT - 1, input_tool_objects},
	};

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_editor, timer_editor, state);
}
