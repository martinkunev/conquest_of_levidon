/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define TITLE_PLAYER_TURN "Player Turn"
#define TITLE_PLAYERS_BATTLE "Battle With Players"

struct battle;

struct state_report
{
	const struct game *game;
	struct slice title;

	unsigned char players[PLAYERS_LIMIT]; // for report_player, report_players and battle_announce
	int skip; // for report_player
	size_t players_count; // for report_players and battle_announce
};

int input_report_player(struct state_report *restrict state);
int input_report_players(struct state_report *restrict state);

int battle_announce(const struct game *restrict game, const struct battle *restrict battle);
int battle_report(const struct game *restrict game, const struct battle *restrict battle, unsigned char winner);
