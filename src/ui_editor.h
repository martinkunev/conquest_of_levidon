/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

struct state
{
	const struct game *restrict game;

	enum tool {TOOL_TERRAIN, TOOL_REGIONS, TOOL_OBJECTS} tool;

	enum terrain terrain; // used for TOOL_TERRAIN
	ssize_t region_index; // used for TOOL_REGIONS and TOOL_OBJECTS
	unsigned object; // used for TOOL_OBJECTS
	size_t name_position; // used for TOOL_REGIONS
	unsigned flag; // used for TOOL_OBJECTS

	int consistent;
	int done;

	double scroll_start;
	int scroll_x, scroll_y;
	int delta_x, delta_y;
};

enum {EditorBrush, EditorObject, EditorFlag};

#define TOOLBOX_WIDTH 256
#define TOOLBOX_HEIGHT 768
#define TOOLBOX_X (WINDOW_WIDTH - TOOLBOX_WIDTH)
#define TOOLBOX_Y 0

#define HEXAGON_EDGE_EDITOR 24.0

extern unsigned MAP_WIDTH, MAP_HEIGHT;

void if_load(const char *restrict world);
void if_unload(void);
void if_editor(const void *restrict context, double progress);

int input_terrain(const struct game *restrict game, struct state *restrict state);
int input_regions(const struct game *restrict game, struct state *restrict state);
int input_objects(const struct game *restrict game, struct state *restrict state);
