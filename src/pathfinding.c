/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>

#include "basic.h"
#include "bitmap.h"
#include "pathfinding.h"

static inline int heap_above(const struct path_node *restrict a, const struct path_node *restrict b);

#define HEAP_SOURCE
#define HEAP_TYPE struct path_node *
#define HEAP_ABOVE(a, b) heap_above((a), (b))
#define HEAP_MOVED(heap, position) ((heap)->data[position]->heap_index = (position))
#include "generic/heap.g"

static inline int heap_above(const struct path_node *restrict a, const struct path_node *restrict b)
{
	return a->distance + a->heuristic <= b->distance + b->heuristic;
}

static ssize_t find_closest(struct heap *restrict closest, struct path_node *restrict traverse_info, struct neighbor *restrict neighbor, size_t last, unsigned char *restrict allowed)
{
	size_t next;

	// Update path from last to its neighbors.
	for(; neighbor; neighbor = neighbor->next)
	{
		double distance;

		if (allowed && !bitmap_get(allowed, neighbor->index))
			continue;

		distance = traverse_info[last].distance + neighbor->distance;
		if (distance < traverse_info[neighbor->index].distance)
		{
			traverse_info[neighbor->index].distance = distance;
			traverse_info[neighbor->index].path_link = traverse_info + last;
			heap_prioritize(closest, traverse_info[neighbor->index].heap_index);
		}
	}

	next = closest->data[0] - traverse_info;
	if (traverse_info[next].distance == INFINITY)
		return ERROR_MISSING; // no more reachable vertices
	heap_pop(closest);

	return next;
}

// Traverses the graph using the A* algorithm.
// Stops when it finds path to vertex_target or all vertices reachable from vertex_origin are traversed (and no such path is found).
// If allowed is not NULL, it is treated as a bitmap specifying which graph vertices to include in the search.
// Returns information about the paths found or NULL on memory error.
struct path_node *path_traverse(struct adjacency_list *restrict graph, size_t vertex_origin, size_t vertex_target, unsigned char *restrict allowed, double (*distance_heuristic)(float x0, float y0, float x1, float y1))
{
	struct path_node *traverse_info;
	struct heap closest;
	ssize_t vertex;
	size_t i;

	assert(graph->count);

	// Initialize traversal information.
	traverse_info = malloc(graph->count * sizeof(*traverse_info));
	if (!traverse_info)
		goto finally; // memory error
	for(i = 0; i < graph->count; ++i)
	{
		double heuristic = 0;
		if (distance_heuristic)
			heuristic = distance_heuristic(graph->list[i].x, graph->list[i].y, graph->list[vertex_target].x, graph->list[vertex_target].y);

		traverse_info[i].distance = INFINITY;
		traverse_info[i].heuristic = heuristic;
		traverse_info[i].path_link = 0;
	}
	traverse_info[vertex_origin].distance = 0;
	traverse_info[vertex_origin].path_link = 0;

	// Find the shortest path to target.
	if (graph->count > 1)
	{
		closest.data = malloc((graph->count - 1) * sizeof(*closest.data));
		if (!closest.data)
		{
			free(traverse_info);
			return 0; // memory error
		}

		closest.count = 0;
		for(i = 0; i < graph->count; ++i)
		{
			if (i == vertex_origin)
				continue;
			if (allowed && !bitmap_get(allowed, i))
				continue;

			closest.data[closest.count] = traverse_info + i;
			traverse_info[i].heap_index = closest.count;
			closest.count += 1;
		}

		vertex = vertex_origin;
		do
		{
			vertex = find_closest(&closest, traverse_info, graph->list[vertex].neighbors, vertex, allowed);
			if (vertex == vertex_target)
				break; // found
			if (vertex < 0)
				break; // no more reachable vertices
		} while (closest.count);
		free(closest.data);
	}

finally:
	return traverse_info;
}

void graph_term(struct adjacency_list *restrict graph)
{
	struct neighbor *item, *next;
	size_t i;

	if (!graph)
		return;

	for(i = 0; i < graph->count; ++i)
	{
		item = graph->list[i].neighbors;
		while (item)
		{
			next = item->next;
			free(item);
			item = next;
		}
	}
	free(graph);
}
