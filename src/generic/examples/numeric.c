/*
 * Conquest of Levidon
 * Copyright (C) 2020  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#if defined(UNIT_TESTING)
# include <stdarg.h>
# include <stddef.h>
# include <setjmp.h>
# include <cmocka.h> /* libcmocka */
#endif

#define HASHMAP_SOURCE
#include "numeric.h"

#define HASHMAP_NAME hashmap_collide
#define HASHMAP_HASH(data, size) 0
#define HASHMAP_TYPE int
#include "hashmap.g"

int hashmap_prepare(struct hashmap *restrict hashmap)
{
#define INSERT(s, v) hashmap_insert(hashmap, (const unsigned char *)(s), sizeof(s) - 1, (v))
	if (!INSERT("zero", 0))
		return -1;
	if (!INSERT("one", 1))
		return -1;
	if (!INSERT("two", 2))
		return -1;
	if (!INSERT("three", 3))
		return -1;
	if (!INSERT("four", 4))
		return -1;
	if (!INSERT("five", 5))
		return -1;
	if (!INSERT("six", 6))
		return -1;
	if (!INSERT("seven", 7))
		return -1;
	if (!INSERT("eight", 8))
		return -1;
	if (!INSERT("nine", 9))
		return -1;
#undef INSERT
	return 0;
}

int numeric(struct hashmap *restrict hashmap, const unsigned char *restrict data, size_t size)
{
	int *value = hashmap_get(hashmap, data, size);
	if (!value)
		return -1;
	return *value;
}

#if defined(UNIT_TESTING)

static void test_numeric(void **state)
{
	struct hashmap hashmap = {0};
	if (hashmap_prepare(&hashmap) < 0)
		abort();

#define slice(s) (const unsigned char *)(s), sizeof(s) - 1
	assert_int_equal(numeric(&hashmap, slice("five")), 5);
	assert_int_equal(numeric(&hashmap, slice("four")), 4);
	assert_int_equal(numeric(&hashmap, slice("zero")), 0);
	assert_int_equal(numeric(&hashmap, slice("nine")), 9);
	assert_int_equal(numeric(&hashmap, slice("seven")), 7);

	assert_int_equal(numeric(&hashmap, slice("ten")), -1);
	assert_int_equal(numeric(&hashmap, slice("")), -1);
	assert_int_equal(numeric(&hashmap, slice("fiv")), -1);
#undef slice

	hashmap_term(&hashmap);
}

static void test_update(void **state)
{
	struct hashmap hashmap = {0};
	const unsigned char key[] = {'x'};
	int value, *p;

	if (!hashmap_insert(&hashmap, key, sizeof(key), 1))
		abort();

	p = hashmap_insert(&hashmap, key, sizeof(key), 1);
	assert_non_null(p);
	assert_int_equal(*p, 1);

	*p = 2;

	p = hashmap_get(&hashmap, key, sizeof(key));
	assert_non_null(p);
	assert_int_equal(*p, 2);

	hashmap_term(&hashmap);
}

static void test_remove(void **state)
{
	struct hashmap hashmap = {0};
	const unsigned char key[] = {'x'};
	int value, *p;

	if (!hashmap_insert(&hashmap, key, sizeof(key), 42))
		abort();

	p = hashmap_get(&hashmap, key, sizeof(key));
	assert_non_null(p);
	assert_int_equal(*p, 42);

	value = 0;
	assert_int_equal(hashmap_remove(&hashmap, key, sizeof(key), &value), 0);
	assert_int_equal(value, 42);

	p = hashmap_get(&hashmap, key, sizeof(key));
	assert_null(p);

	hashmap_term(&hashmap);
}

static void test_iterate(void **state)
{
	struct hashmap hashmap = {0};
	struct hashmap_iterator it;
	struct hashmap_entry *item;
	int *p;

	if (!hashmap_insert(&hashmap, (const unsigned char *)"0", 1, 0))
		abort();
	if (!hashmap_insert(&hashmap, (const unsigned char *)"1", 1, 1))
		abort();
	if (!hashmap_insert(&hashmap, (const unsigned char *)"2", 1, 2))
		abort();

	for(item = hashmap_first(&hashmap, &it); item; item = hashmap_next(&hashmap, &it))
	{
		char expected;
		assert_true((0 <= item->value) && (item->value <= 2));
		expected = item->value + '0';
		assert_int_equal(item->key_size, 1);
		assert_memory_equal(item->key_data, &expected, 1);
	}

	item = hashmap_first(&hashmap, &it);
	while (item)
	{
		if (*item->key_data == '0')
			item = hashmap_remove_current(&hashmap, &it);
		else
			item = hashmap_next(&hashmap, &it);
	}

	p = hashmap_get(&hashmap, (const unsigned char *)"0", 1);
	assert_null(p);

	hashmap_term(&hashmap);
}

static void test_collision(void **state)
{
	const unsigned char *a = (void *)"a";
	const unsigned char *b = (void *)"b";
	const unsigned char *c = (void *)"c";

	struct hashmap_collide hashmap = {0};
	int *value;

	if (!hashmap_collide_insert(&hashmap, a, 1, 1))
		abort();
	if (!hashmap_collide_insert(&hashmap, b, 1, 2))
		abort();
	if (!hashmap_collide_insert(&hashmap, c, 1, 3))
		abort();

	assert_non_null(hashmap_collide_get(&hashmap, b, 1));

	hashmap_collide_remove(&hashmap, b, 1, 0);

	assert_non_null(hashmap_collide_get(&hashmap, a, 1));
	assert_null(hashmap_collide_get(&hashmap, b, 1));
	assert_non_null(hashmap_collide_get(&hashmap, c, 1));

	hashmap_collide_term(&hashmap);
}

int main()
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(test_numeric),
		cmocka_unit_test(test_update),
		cmocka_unit_test(test_remove),
		cmocka_unit_test(test_iterate),
		cmocka_unit_test(test_collision),
	};

	return cmocka_run_group_tests(tests, 0, 0);
}

#endif
