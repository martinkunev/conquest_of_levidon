/*
 * Conquest of Levidon
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#if defined(UNIT_TESTING)
# define _DEFAULT_SOURCE
#endif

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#if defined(UNIT_TESTING)
# include <stdarg.h>
# include <stddef.h>
# include <setjmp.h>
# include <cmocka.h>
#endif

#include <arpa/inet.h>

struct item
{
	int i;
	int sum;
};

struct it
{
	struct item min, max;
	struct item last;
	int done;
};

struct avl_node;
static void update_sum(struct avl_node *node);

#define AVL_TYPE struct item
#define AVL_MOVED(node) update_sum(node)
#include "avl.g"

#define NODES_COUNT 1024

static struct item values[NODES_COUNT];

/*void print(struct avl_node *t)
{
	if (t)
	{
		if (t->next[0])
		{
			printf("[");
			print(t->next[0]);
			printf("]");
		}
		printf(" %d ", t->key, t->factor);
		//printf("%d (%d)", t->key, t->factor);
		if (t->next[1])
		{
			printf("[");
			print(t->next[1]);
			printf("]");
		}
	}
}*/

static unsigned check_height(struct avl_node *t)
{
	if (t)
	{
		unsigned a = check_height(t->next[0]);
		unsigned b = check_height(t->next[1]);
		assert_int_equal(t->factor, (int)a - (int)b);
		assert_true(t->factor >= -1);
		assert_true(t->factor <= 1);
		return ((a >= b) ? a : b) + 1;
	}
	return 0;
}

static void check_sorted(struct avl_node *t)
{
	if (t->next[0])
	{
		check_sorted(t->next[0]);
		assert_true(ntohl(t->next[0]->value.i) < ntohl(t->value.i));
	}
	if (t->next[1])
	{
		assert_true(ntohl(t->value.i) < ntohl(t->next[1]->value.i));
		check_sorted(t->next[1]);
	}
}

static void test_avl_insert(void **state)
{
	struct avl tree = {0};
	size_t i;
	for(i = 0; i < NODES_COUNT; i += 1)
	{
		struct item value = {.i = random() % 4096}, *result;
		values[i] = value;

		// this may overwrite the previous value
		result = avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

		assert_non_null(result);
		assert_int_equal(result->i, value.i);
		check_height(tree.root);
	}
	avl_term(&tree);
}

static void test_avl_get(void **state)
{
	struct avl tree = {0};
	size_t i;
	int key;

	key = 0;
	assert_false(avl_get(&tree, (const unsigned char *)&key, sizeof(key)));

	for(i = 0; i < NODES_COUNT; i += 1)
	{
		struct item value = {.i = random() % 4096};
		values[i] = value;

		// this may overwrite the previous value
		avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);
	}

	key = 4096;
	assert_false(avl_get(&tree, (const unsigned char *)&key, sizeof(key)));

	for(size_t i = 0; i < NODES_COUNT; i += 1)
	{
		int key = values[i].i;
		struct item *value = avl_get(&tree, (const unsigned char *)&key, sizeof(key));
		assert_true(value);
		assert_int_equal(value->i, values[i].i);
	}

	avl_term(&tree);
}

static int callback(struct avl_node *node, void *argument)
{
	struct it *it = argument;

	assert_true(ntohl(node->value.i) > ntohl(it->last.i));
	assert_true(ntohl(node->value.i) >= ntohl(it->min.i));
	assert_false(it->done);

	it->last = node->value;
	it->done = (ntohl(node->value.i) > ntohl(it->max.i));

	return it->done;
}
static void test_avl_iterate(void **state)
{
	struct avl tree = {0};
	struct it it = {.min.i = htonl(248), .max.i = htonl(1958), .last.i = 0};
	struct item value;
	size_t i;

	value = (struct item){.i = htonl(4096)};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	for(i = 1; i < NODES_COUNT; i += 1)
	{
		value = (struct item){.i = htonl(random() % 4096)};
		values[i] = value;

		// this may overwrite the previous value
		avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);
	}

	avl_iterate(&tree, (const unsigned char *)&it.min, sizeof(it.min), callback, &it);
	assert_true(it.done);

	avl_term(&tree);
}

static void test_avl_remove(void **state)
{
	struct avl tree = {0};
	int key;
	struct item value;

	value = (struct item){.i = 1};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 9};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 6};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 12};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 7};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 6};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 8};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	value = (struct item){.i = 3};
	avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);

	check_height(tree.root);

	key = 9;
	assert_true(avl_remove(&tree, (const unsigned char *)&key, sizeof(key), 0) == 0);
	check_height(tree.root);
	assert_int_equal(tree.count, 6);

	key = 6;
	assert_true(avl_remove(&tree, (const unsigned char *)&key, sizeof(key), &value) == 0);
	check_height(tree.root);
	assert_int_equal(value.i, key);
	assert_int_equal(tree.count, 5);

	key = 6;
	value.i = 37;
	assert_false(avl_remove(&tree, (const unsigned char *)&key, sizeof(key), &value) == 0);
	assert_int_equal(value.i, 37);
	check_height(tree.root);
	assert_int_equal(tree.count, 5);

	key = 5;
	assert_false(avl_remove(&tree, (const unsigned char *)&key, sizeof(key), 0) == 0);
	check_height(tree.root);
	assert_int_equal(tree.count, 5);

	avl_term(&tree);
}

static void test_avl_sorted(void **state)
{
	struct avl tree = {0};
	size_t i;

	for(i = 0; i < NODES_COUNT; i += 1)
	{
		struct item value = (struct item){.i = htonl(random() % 4096)};

		// this may overwrite the previous value
		avl_insert(&tree, (const unsigned char *)&value.i, sizeof(value.i), value);
	}

	check_sorted(tree.root);

	avl_term(&tree);
}

static void update_sum(struct avl_node *node)
{
	int sum = node->value.i;
	if (node->next[0])
		sum += node->next[0]->value.sum;
	if (node->next[1])
		sum += node->next[1]->value.sum;
	node->value.sum = sum;
}

static int check_sum(struct avl_node *t)
{
	int sum = t->value.i;
	if (t->next[0])
		sum += check_sum(t->next[0]);
	if (t->next[1])
		sum += check_sum(t->next[1]);
	assert_int_equal(t->value.sum, sum);
	return sum;
}

static void test_avl_sum(void **state)
{
	struct avl t = {0};

	avl_insert(&t, (const unsigned char *)"one", 3, (struct item){.i = 1});
	avl_insert(&t, (const unsigned char *)"two", 3, (struct item){.i = 2});
	avl_insert(&t, (const unsigned char *)"seven", 5, (struct item){.i = 7});
	avl_insert(&t, (const unsigned char *)"four", 4, (struct item){.i = 4});

	check_sum(t.root);

	avl_term(&t);
}

int main(int argc, char *argv[])
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(test_avl_insert),
		cmocka_unit_test(test_avl_get),
		cmocka_unit_test(test_avl_iterate),
		cmocka_unit_test(test_avl_remove),
		cmocka_unit_test(test_avl_sorted),
		cmocka_unit_test(test_avl_sum),
	};

	srandom(1);

	return cmocka_run_group_tests(tests, 0, 0);
}
