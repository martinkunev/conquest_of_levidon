This C source library provides generic data structures, designed for performance, customizability and ease of use. For flexibility reasons the data structures are minimalistic and transparent - they provide only the key functionality intrinsic for the data structure and some data structure members are intended to be accessed directly.

The source is compatible with the C99 standard.

Optionally, you can customize any data structure by overriding the default parameters.

includes:

* dynamic array
* binary heap
* hash table
* AVL tree

* appendable string
* heap sort
* graph data structure
* dijkstra

## Array

An array whose elements are stored sequentially in memory and have the same type. Can be resized dynamically.

	struct array
	{
		size_t count; // number of elements currently stored
		size_t capacity; // size of memory allocated for data in bytes
		void *data; // data allocated for the elements
	};

	struct array bar = {0}; // initialize

	free(bar.data); // terminate

`int array_expand(struct array *restrict array, size_t count);`

	Ensures there is enough memory in array to store count elements.
	Returns `0` on success or negative error code on failure.

**Parameters**

	`ARRAY_NAME` - name of struct and prefix for function names
		default: `array`

	`ARRAY_TYPE` - data type of the elements of the array
		default: `void *`

### Examples

	#define ARRAY_NAME arr
	#define ARRAY_TYPE int
	#include "array.g"

	static void func(unsigned count)
	{
		size_t i;
		struct arr squares = {0};

		if (arr_expand(&squares, count) < 0)
			abort();
		for(i = 0; i < count; i += 1)
			squares.data[i] = i * i;
		squares.count = count;

		free(squares.data);
	}

	int main(void)
	{
		func(100);
		return 0;
	}

For implementation of an appendable string, see `strings.h` and `strings.c`.

### Use as a static type

In a source file:

	// optional parameters go here
	#include "array.g"

The definitions prefixed with `ARRAY_` will be undefined by the generic (they will not be visible after the include). Multiple arrays can be defined in the same source file (as long as their names are distinct).

### Use as a global type

In a header file (e.g. `foo.h`):

	#define ARRAY_GLOBAL
	// optional parameters go here
	#include "array.g"

In a source file:

	#define ARRAY_SOURCE
	#include "foo.h"

The definitions prefixed with `ARRAY_` will be undefined by the generic (they will not be visible after the include). A header file can not define more than one global array.

## Heap

A binary heap (linearly-stored complete binary tree where children have lower priority than their parent) implementation of priority queue. The source assumes memory management is handled by an external mechanism.

	struct heap
	{
		void *data; // Array with the elements.
		size_t count; // Number of elements actually in the heap.
	};

	struct heap bar = {.data = data, .count = count}; // initialize from an array `data` with `count` elements
	heap_heapify(&bar); // ensure the array `data` satisfies the heap property

`void heap_push(struct heap *heap, void *value);`

	Adds a new element to the heap.

`void heap_pop(struct heap *heap);`

	Removes the element with the highest priority from the heap.

`void heap_prioritize(struct heap *heap, size_t index);`

	Rearranges heap after the priority of element at `index` increased.

`void heap_heapify(struct heap *heap);`

	Turns the array `data` into a heap by reordering array elements.

**Parameters**

	`HEAP_NAME` - name of struct and prefix for function names
		default: `heap`

	`HEAP_TYPE` - data type of the elements of the heap
		default: `void *`

	`HEAP_ABOVE` - function of two heap elements, returning whether the first has higher priority
		int HEAP_ABOVE(HEAP_TYPE a, HEAP_TYPE b);
		default: ((a) >= (b))

	`HEAP_MOVED` - function called when an element is placed or moved to `index` in the heap
		void HEAP_MOVED(struct heap *heap, size_t index);
		default: (no action)

	The arguments passed to the functions are guaranteed to produce no side effects. Therefore a macro can be used instead of a function and any parameter can appear more than once in the macro.

### Examples

For an implementation of in-place unstable sorting, see `sort.h` and `sort.c`.
For an implementation of the Dijkstra's shortest path algorithm, using a binary heap, see `dijkstra.h` and `dijkstra.c`.

### Use as a static type

In a source file:

	// optional parameters go here
	#include "heap.g"

The definitions prefixed with `HEAP_` will be undefined by the generic (they will not be visible after the include). Multiple heaps can be defined in the same source file (as long as their names are distinct).

### Use as a global type

In a header file (e.g. `foo.h`):

	#define HEAP_GLOBAL
	// optional parameters go here
	#include "heap.g"

In a source file:

	#define HEAP_SOURCE
	#include "foo.h"

The definitions prefixed with `HEAP_` will be undefined by the generic (they will not be visible after the include). A header file can not define more than one global heap.

## Hash table

Hash map (associative array implemented using a hash function and table lookup), storing key as a byte array and a corresponding value. Uses separate chaining.

	struct hashmap
	{
		size_t count;
		size_t buckets_count;
		struct hashmap_entry
		{
			struct hashmap_entry *const next;
			const size_t key_size;
			HASHMAP_TYPE value;
			const uint_fast32_t hash;
			const unsigned char key_data[];
		} **buckets;
	};

	struct hashmap foo = {0}; // initialize

	struct hashmap_iterator it;
	struct hashmap_entry *item;
	for(item = hashmap_first(&foo, &it); item; item = hashmap_next(&foo, &it)) // iterate over hashmap
		bar(item);

	hashmap_term(&foo); // terminate

`void **hashmap_get(const struct hashmap *restrict hashmap, const unsigned char *restrict key_data, size_t key_size);`

	Looks up an element with a given key and returns a pointer to its value. Returns NULL if there is no such element.

`void **hashmap_insert(struct hashmap *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, void *value);`

	Inserts a value for a given key and returns a pointer to the value. The key is copied into the hash table. If an element with the given key already exists, returns a pointer to it.

`int hashmap_remove(struct hashmap *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, void **value_old);`

	Removes an element with a given key. Stores the removed value into the object pointed to by value_old (if value_old is not NULL). Returns 0 on success or negative value on failure.

`struct hashmap_entry *hashmap_first(const struct hashmap *restrict hashmap, struct hashmap_iterator *restrict iterator);`

	Initializes an iterator to the hash table and returns pointer to the first entry. Returns NULL if the hash table is empty.

`struct hashmap_entry *hashmap_next(const struct hashmap *restrict hashmap, struct hashmap_iterator *restrict iterator);`

	Returns a pointer to the next iteration entry in the hash table. Returns NULL if there are no more entries to iterate.

`struct hashmap_entry *hashmap_remove_current(struct hashmap *restrict hashmap, struct hashmap_iterator *restrict iterator);`

	Removes the entry last returned by hashmap_first or hashmap_next for the iterator. Returns a pointer to the next iteration entry in the hash table. Returns NULL if there are no more entries to iterate.

`void hashmap_term(struct hashmap *hashmap);`

	Frees the memory associated with hash table (values stored in the hash table are not freed).

**Parameters**

	`HASHMAP_NAME` - name of struct and prefix for function names
		default: `hashmap`

	`HASHMAP_TYPE` - data type of the elements of the hash table
		default: `void *`

	`HASHMAP_HASH` - function calculating the hash of a key
		uint32_t HASHMAP_HASH(const unsigned char data[], size_t size);
		default: SDBM hash

	The arguments passed to the function are guaranteed to produce no side effects. Therefore a macro can be used instead of function and any parameter can appear more than once in the macro.

### Example

For an implementation of converting digit strings to numbers, see `numeric.h` and `numeric.c`.
TODO implement example with union with pointer, long, double and unsigned char[]

### Use as a static type

In a source file:

	// optional parameters go here
	#include "hashmap.g"

The definitions prefixed with `HASHMAP_` will be undefined by the generic (they will not be visible after the include). Multiple hashmaps can be defined in the same source file (as long as their names are distinct).

### Use as a global type

In a header file (e.g. `foo.h`):

	#define HASHMAP_GLOBAL
	// optional parameters go here
	#include "hashmap.g"

In a source file:

	#define HASHMAP_SOURCE
	#include "foo.h"

The definitions prefixed with `HASHMAP_` will be undefined by the generic (they will not be visible after the include). A header file can not define more than one global hashmap.

## AVL tree

AVL binary search tree (height-balanced, self-balancing), storing key as a byte array and a corresponding value.

	struct avl
	{
		size_t count;
		struct avl_node
		{
			struct avl_node *next[2]; // chldren of the node
			const size_t key_size;
			AVL_TYPE value;
			signed char factor; // left height - right height
			const unsigned char key_data[];
		} *root;
	};

	struct avl foo = {0}; // initialize

	avl_term(&foo); // terminate

`AVL_TYPE *avl_get(struct avl *avl, const unsigned char *restrict key_data, size_t key_size);`

	Looks up an element with a given key and returns a pointer to its value. Returns NULL if there is no such element.

`AVL_TYPE *avl_insert(struct avl *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE value);`

	Inserts a value for a given key and returns a pointer to the value. The key is copied into the tree. If an element with the given key already exists, returns a pointer to it.

`int avl_remove(struct avl *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE *value_old);`

	Removes an element with a given key. Stores the removed value into the object pointed to by value_old (if value_old is not NULL). Returns 0 on success or negative value on failure.

`void avl_iterate(struct avl *avl, const unsigned char *restrict key_data, size_t key_size, int (*callback)(struct NAME(_node) *, void *), void *argument);`

	Visits nodes in the tree in ascending order, calling callback for each visited node, starting from the node with the specified key (if there is no such node, starts from the next node in ascending order). The callback is called with the current node and `argument`. A non-zero return value from the callback indicates to stop the iteration (0 means to continue).

`void avl_term(struct avl *avl);`

	Frees the memory associated with the tree (values stored in the tree are not freed).

**Parameters**

	`AVL_NAME` - name of struct and prefix for function names
		default: `avl`

	`AVL_TYPE` - data type of the elements of the tree
		default: `int`

	`AVL_COMPARE` - function comparing two keys in the tree (positive means a is before b; 0 means a and b are the same; negative means a is after b)
		int AVL_COMPARE(const unsigned char *restrict a, const unsigned char *restrict b, size_t n);
		default: `memcmp`

	`AVL_MOVED` - function called when a node is moved to a new position in the tree (due to rebalancing). the function can be used to access children and update metadata
		void AVL_MOVED(struct avl_node *node);
		default: (no action)

	The arguments passed to the functions are guaranteed to produce no side effects. Therefore a macro can be used instead of function and any parameter can appear more than once in the macro.

### Example

TODO

For an implementation of converting digit strings to numbers, see `avltest.c`.
TODO implement example with union with pointer, long, double and unsigned char[]

### Use as a static type

In a source file:

	// optional parameters go here
	#include "avl.g"

The definitions prefixed with `AVL_` will be undefined by the generic (they will not be visible after the include). Multiple AVLs can be defined in the same source file (as long as their names are distinct).

### Use as a global type

In a header file (e.g. `foo.h`):

	#define AVL_GLOBAL
	// optional parameters go here
	#include "avl.g"

In a source file:

	#define AVL_SOURCE
	#include "foo.h"

The definitions prefixed with `AVL_` will be undefined by the generic (they will not be visible after the include). A header file can not define more than one global avl.

<!-- TODO disjoint set? graph data structures? -->
