/*
 * Conquest of Levidon
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

// TODO why doesn't avl_remove return the old value instead of having it in an argument

#if !defined(AVL_NAME)
# define AVL_NAME avl
#endif

#if !defined(AVL_TYPE)
# define AVL_TYPE int
#endif

#if !defined(AVL_COMPARE)
// Returns positive number if a is before b, 0 if a == b, negative number if a is after b.
// TODO make the interface of this more intuitive - just pass key and key size for both arguments
# define AVL_COMPARE(a, a_size, b) memcmp((b)->key_data, (a), (a_size))
#endif

#if !defined(AVL_MOVED)
// Callback for updating aggregated values for a node. When called, both children of the passed node are in a consistent state.
// TODO when is this called? when the subtree of the node is updated?
# define AVL_MOVED(node)
#endif

#define NAME_CAT_EXPAND(a, b) a ## b
#define NAME_CAT(a, b) NAME_CAT_EXPAND(a, b)
#define NAME(suffix) NAME_CAT(AVL_NAME, suffix)
#define STRING_EXPAND(string) #string
#define STRING(string) STRING_EXPAND(string)

#if defined(AVL_GLOBAL)
# define STATIC
#else
# define AVL_SOURCE
# define STATIC static
#endif

// TODO implement union, intersection, difference
// TODO range requests. the iterator must have the option to not descend down to subtrees?

struct AVL_NAME
{
	size_t count;
	struct NAME(_node)
	{
		struct NAME(_node) *next[2]; // chldren of the node
		const size_t key_size; // TODO size_t is too big
		AVL_TYPE value;
		signed char factor; // left height - right height
		const unsigned char key_data[];
	} *root;
};

// A newly created avl must be initialized with zeroes: struct AVL_NAME avl = {0};

STATIC AVL_TYPE *NAME(_get)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size);

STATIC AVL_TYPE *NAME(_insert)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE value);
STATIC int NAME(_remove)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE *value_old);

STATIC void NAME(_iterate)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, int (*callback)(struct NAME(_node) *, void *), void *argument);

STATIC void NAME(_term)(struct AVL_NAME *avl);

#if defined(AVL_SOURCE)

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct NAME(_node_mutable)
{
	struct NAME(_node) *next[2];
	size_t key_size;
	AVL_TYPE value;
	signed char factor;
	unsigned char key_data[];
};

static AVL_TYPE *NAME(_get_internal)(struct NAME(_node) *t, const unsigned char *restrict key_data, size_t key_size)
{
	while (t)
	{
		int diff = AVL_COMPARE(key_data, key_size, t);
		if (!diff) return &t->value;
		t = t->next[diff < 0];
	}
	return 0;
}
STATIC AVL_TYPE *NAME(_get)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size)
{
	return NAME(_get_internal)(avl->root, key_data, key_size);
}

static signed char NAME(_balance)(struct NAME(_node) *restrict *restrict branch)
{
	struct NAME(_node) *node = *branch;
	unsigned char index = (node->factor < 0);
	struct NAME(_node) *child = node->next[index];
	signed char factor_sign = (int [2]){1, -1}[index];

	assert(node->factor == -2 || node->factor == 2);

	// Make sure the taller subtree doesn't lean in the other direction.
	// Rotate it if necessary (this won't change its height).
	if (factor_sign * child->factor < 0)
	{
		struct NAME(_node) *grandchild = child->next[index ^ 1];

		node->next[index] = grandchild;
		child->next[index ^ 1] = grandchild->next[index];
		grandchild->next[index] = child;

		child->factor = (factor_sign * grandchild->factor < 0) ? factor_sign : 0;
		grandchild->factor = factor_sign + ((grandchild->factor == factor_sign) ? grandchild->factor : 0);

		AVL_MOVED(child);

		child = grandchild;
	}

	assert(factor_sign * child->factor >= 0);

	*branch = child;
	node->next[index] = child->next[index ^ 1];
	child->next[index ^ 1] = node;

	node->factor = factor_sign - child->factor;
	child->factor = child->factor ? 0 : -factor_sign;

	AVL_MOVED(node);
	AVL_MOVED(child);

	// Height decreases when node doesn't lean the same way as the tree leaned initially.
	return (node->factor == factor_sign) ? 0 : -1;
}

static AVL_TYPE *NAME(_insert_key)(struct NAME(_node) *restrict *restrict branch, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE value, int *restrict height_change, int *new)
{
	struct NAME(_node) *t = *branch;

	if (t) // the node is occupied
	{
		unsigned char index;
		AVL_TYPE *result;
		int diff = AVL_COMPARE(key_data, key_size, t);

		if (!diff) // the key is already in the tree
			return &t->value;
		index = (diff < 0);

		// Insert the node in the subtree. Exit if the height hasn't changed.
		result = NAME(_insert_key)(t->next + index, key_data, key_size, value, height_change, new);
		if (*height_change)
		{
			// Calculate the new balance factor. Rebalance the tree if necessary.
			t->factor += (int [2]){1, -1}[index];
			if ((t->factor < -1) || (t->factor > 1))
				*height_change = NAME(_balance)(branch) + 1;
			else
			{
				*height_change = (t->factor != 0);
				AVL_MOVED(t);
			}
		}
		else if (*new)
			AVL_MOVED(t);
		return result;
	}
	else // the node is vacant
	{
		struct NAME(_node_mutable) *node = malloc(sizeof(*node) + key_size);
		if (!node)
			return 0;

		node->key_size = key_size;
		node->value = value;
		node->factor = 0;
		node->next[0] = node->next[1] = 0;
		memcpy(node->key_data, key_data, key_size);

		*branch = (struct NAME(_node) *)node;
		*height_change = 1;
		*new = 1;

		// TODO should I call this?
		AVL_MOVED(*branch);

		return &node->value;
	}
}
STATIC AVL_TYPE *NAME(_insert)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE value)
{
	int height_change = 0;
	int new = 0;
	AVL_TYPE *result = NAME(_insert_key)(&avl->root, key_data, key_size, value, &height_change, &new);
	if (new)
		avl->count += 1;
	return result;
}

// Calculate the new blance factor after an item removal. Rebalance the tree if necessary.
static signed char NAME(_remove_factor)(struct NAME(_node) **branch, size_t index)
{
	struct NAME(_node) *node = *branch;

	node->factor -= (int [2]){1, -1}[index];
	if ((node->factor < -1) || (node->factor > 1))
		return NAME(_balance)(branch);
	else
	{
		AVL_MOVED(node);
		assert(-!node->factor == (node->factor ? 0 : -1));
		return node->factor ? 0 : -1;
	}
}

static int NAME(_move_closest)(struct NAME(_node) **branch, const unsigned char *restrict key_data, size_t key_size, struct NAME(_node_mutable) *restrict position)
{
	struct NAME(_node) *t = *branch;
	unsigned char index = (AVL_COMPARE(key_data, key_size, t) < 0);
	struct NAME(_node) **child = t->next + index;

	if (*child) // the subtree is not empty
	{
		// Remove the node from the subtree. Exit if the height hasn't changed.
		if (!NAME(_move_closest)(child, key_data, key_size, position))
		{
			AVL_MOVED(t);
			return 0;
		}

		return NAME(_remove_factor)(branch, index);
	}
	else // the subtree is empty; the closest node has to be moved
	{
		// Copy node data to its new position.
		position->key_size = t->key_size;
		memcpy(position->key_data, t->key_data, t->key_size);
		position->value = t->value;

		// Remove current node.
		*branch = t->next[index ^ 1];
		free(t);

		return -1;
	}
}

static signed char NAME(_remove_key)(struct NAME(_node) **branch, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE *value_old, int *restrict changed)
{
	struct NAME(_node) *t = *branch;
	int diff = AVL_COMPARE(key_data, key_size, t);

	if (!diff) // this is the key to be removed
	{
		*changed = 1;
		if (value_old)
			*value_old = t->value;

		if (t->next[0])
		{
			if (t->next[1])
			{
				int height_change;
				unsigned char index = (t->factor < 0);

				// Replace the current node with the closest by key node in the taller subtree. Exit if height hasn't changed.
				height_change = NAME(_move_closest)(t->next + index, key_data, key_size, (struct NAME(_node_mutable) *)t);
				if (!height_change)
				{
					AVL_MOVED(t);
					return 0;
				}

				// See if the height change in the subtree caused the entire tree to change height.
				return NAME(_remove_factor)(branch, index);
			}
			else *branch = t->next[0];
		}
		else *branch = t->next[1];

		free(t);

		return -1;
	}

	// Find the node in the subtree where it should be
	{
		unsigned char index = (diff < 0);
		struct NAME(_node) **child = t->next + index;

		// If such node doesn't exist, there is nothing to remove or balance.
		if (!*child) // the subtree is empty
			return 0;

		// Remove the node from the subtree. Exit if the height hasn't changed.
		if (!NAME(_remove_key)(child, key_data, key_size, value_old, changed))
		{
			if (*changed)
				AVL_MOVED(t);
			return 0;
		}

		return NAME(_remove_factor)(branch, index);
	}
}
STATIC int NAME(_remove)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, AVL_TYPE *value_old)
{
	int changed = 0;

	if (avl->root)
	{
		NAME(_remove_key)(&avl->root, key_data, key_size, value_old, &changed);
		if (changed)
			avl->count -= 1;
	}

	return changed ? 0 : -1;
}

// Returns whether the iteration has been stopped.
static int iterate(struct NAME(_node) *node, const unsigned char *restrict key_data, size_t key_size, int (*callback)(struct NAME(_node) *, void *), void *argument)
{
	if (!node)
		return 0;

	if (key_data)
	{
		int diff = AVL_COMPARE(key_data, key_size, node);

		if (!diff) // key found
		{
			// Iteration starts here.
			return (*callback)(node, argument) || iterate(node->next[1], 0, 0, callback, argument);
		}
		else if (diff > 0)
		{
			// Iteration will start in the left subtree.
			return iterate(node->next[0], key_data, key_size, callback, argument) || (*callback)(node, argument) || iterate(node->next[1], 0, 0, callback, argument);
		}
		else
		{
			// Iteration will start in the right subtree.
			return iterate(node->next[1], key_data, key_size, callback, argument);
		}
	}

	// Iteration has started.
	return iterate(node->next[0], key_data, key_size, callback, argument) || (*callback)(node, argument) || iterate(node->next[1], key_data, key_size, callback, argument);
}
STATIC void NAME(_iterate)(struct AVL_NAME *avl, const unsigned char *restrict key_data, size_t key_size, int (*callback)(struct NAME(_node) *, void *), void *argument)
{
	// TODO maybe return whether it was user-cancelled
	iterate(avl->root, key_data, key_size, callback, argument);
}

static void NAME(_term_internal)(struct NAME(_node) *node)
{
	if (!node)
		return;
	NAME(_term_internal)(node->next[0]);
	NAME(_term_internal)(node->next[1]);
	free(node);
}
STATIC void NAME(_term)(struct AVL_NAME *avl)
{
	// WARNING: In practice it may be useful to iterate once over the tree to free memory for values.
	NAME(_term_internal)(avl->root);
}

#endif /* AVL_SOURCE */

#undef STATIC

#undef STRING
#undef STRING_EXPAND
#undef NAME
#undef NAME_CAT
#undef NAME_CAT_EXPAND

#undef AVL_HASH
#undef AVL_TYPE
#undef AVL_NAME

#undef AVL_GLOBAL
#undef AVL_SOURCE
