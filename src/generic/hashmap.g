/*
 * Conquest of Levidon
 * Copyright (C) 2021  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

// TODO the key comparison function should be customizable (it can be made faster for custom cases)
// TODO double indirection in the iterator is ugly
// TODO try to reduce the number of casts (currently there are 4)

#if !defined(HASHMAP_NAME)
# define HASHMAP_NAME hashmap
#endif

#if !defined(HASHMAP_TYPE)
// TODO make it a hashset if HASHMAP_TYPE is not defined
# define HASHMAP_TYPE void *
#endif

#if !defined(HASHMAP_HASH)
// Used to calculate the hash of a key:
// uint_fast32_t HASHMAP_HASH(const unsigned char data[static], size_t size);
# define HASHMAP_HASH(data, size) NAME(_hash)((data), (size))
#endif

#define NAME_CAT_EXPAND(a, b) a ## b
#define NAME_CAT(a, b) NAME_CAT_EXPAND(a, b)
#define NAME(suffix) NAME_CAT(HASHMAP_NAME, suffix)
#define STRING_EXPAND(string) #string
#define STRING(string) STRING_EXPAND(string)

#if defined(HASHMAP_GLOBAL)
# define STATIC
#else
# define HASHMAP_SOURCE
# define STATIC static
#endif

struct HASHMAP_NAME
{
	size_t count;
	size_t buckets_count;
	struct NAME(_entry)
	{
		struct NAME(_entry) *const next;
		const size_t key_size; // TODO size_t is too big
		HASHMAP_TYPE value;
		const uint_fast32_t hash;
		const unsigned char key_data[];
	} **buckets;
};

struct NAME(_iterator)
{
	size_t index;
	struct NAME(_entry) **entry;
};

// A newly created hashmap must be initialized with zeroes: struct HASHMAP_NAME hashmap = {0};

STATIC HASHMAP_TYPE *NAME(_get)(const struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size);

STATIC HASHMAP_TYPE *NAME(_insert)(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE value);
//HASHMAP_TYPE *NAME(_insert_fast)(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE value);
STATIC int NAME(_remove)(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE *value_old);

STATIC struct NAME(_entry) *NAME(_first)(const struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator);
STATIC struct NAME(_entry) *NAME(_next)(const struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator);
STATIC struct NAME(_entry) *NAME(_remove_current)(struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator);

STATIC void NAME(_term)(struct HASHMAP_NAME *hashmap);

#if defined(HASHMAP_SOURCE)

#if !defined(HASHMAP_SIZE_BASE)
# define HASHMAP_SIZE_BASE 32
#endif

struct NAME(_entry_mutable)
{
	struct NAME(_entry) *next;
	size_t key_size;
	HASHMAP_TYPE value;
	uint_fast32_t hash;
	unsigned char key_data[];
};

// Why use separate chaining?
// http://stackoverflow.com/questions/1603712/when-should-i-do-rehashing-of-entire-hash-table/1604428#1604428

static inline int NAME(_key_eq)(const struct NAME(_entry) *restrict bucket, const unsigned char *restrict key_data, size_t key_size, uint_fast32_t hash)
{
	return ((bucket->hash == hash) && (key_size == bucket->key_size) && !memcmp(key_data, bucket->key_data, bucket->key_size));
	//return ((key_size == bucket->key_size) && !memcmp(key_data, bucket->key_data, bucket->key_size));
}

// TODO: check this sdbm hash algorithm
static uint_fast32_t NAME(_hash)(const unsigned char data[], size_t size)
{
	uint_fast32_t result = 0;
	size_t i;
	for(i = 0; i < size; ++i)
		result = data[i] + (result << 6) + (result << 16) - result;
	return result;
}

static inline size_t NAME(_index)(const struct HASHMAP_NAME *hashmap, uint_fast32_t hash)
{
	return hash & (hashmap->buckets_count - 1);
}

// Returns -1 on memory error.
STATIC int NAME(_init)(struct HASHMAP_NAME *hashmap, size_t buckets_count)
{
	size_t i;

	hashmap->count = 0;
	hashmap->buckets_count = buckets_count;
	hashmap->buckets = malloc(buckets_count * sizeof(*hashmap->buckets));
	if (!hashmap->buckets)
		return -1;

	// Set each bucket separately to ensure entries is initialized properly.
	for(i = 0; i < buckets_count; ++i)
		hashmap->buckets[i] = 0;

	return 0;
}

// Returns 0 on missing error.
STATIC HASHMAP_TYPE *NAME(_get)(const struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size)
{
	// Look for the requested key among the entries with the corresponding hash.

	uint_fast32_t hash;
	struct NAME(_entry) *entry;
	size_t index;

	if (!hashmap->buckets)
		return 0; // missing

	hash = HASHMAP_HASH(key_data, key_size);
	index = NAME(_index)(hashmap, hash);
	for(entry = hashmap->buckets[index]; entry; entry = entry->next)
		if (NAME(_key_eq)(entry, key_data, key_size, hash))
			return &entry->value; // found

	return 0; // missing
}

// Returns -1 on memory error.
static int NAME(_expand)(struct HASHMAP_NAME *hashmap)
{
	size_t i;

	size_t buckets_count;
	struct NAME(_entry) **buckets;

	uint_fast32_t mask = hashmap->buckets_count;

	buckets_count = hashmap->buckets_count * 2;
	buckets = malloc(buckets_count * sizeof(*buckets));
	if (!buckets) return -1;

	for(i = 0; i < hashmap->buckets_count; ++i)
	{
		struct NAME(_entry) *entry, *entry_next;

		struct NAME(_entry) **slot_even = (buckets + i);
		struct NAME(_entry) **slot_odd = (buckets + (mask | i));

		*slot_even = 0;
		*slot_odd = 0;

		for(entry = hashmap->buckets[i]; entry; entry = entry_next)
		{
			struct NAME(_entry_mutable) *entry_mutable = (struct NAME(_entry_mutable) *)entry;
			entry_next = entry->next;

			if (entry->hash & mask)
			{
				entry_mutable->next = *slot_odd;
				*slot_odd = entry;
			}
			else
			{
				entry_mutable->next = *slot_even;
				*slot_even = entry;
			}
		}
	}

	free(hashmap->buckets);

	hashmap->buckets = buckets;
	hashmap->buckets_count = buckets_count;
	return 0;
}

// Returns 0 on memory error.
static HASHMAP_TYPE *NAME(_insert_internal)(struct NAME(_entry) **restrict bucket, const unsigned char *restrict key_data, size_t key_size, uint_fast32_t hash, HASHMAP_TYPE value)
{
	struct NAME(_entry_mutable) *entry_mutable;
	struct NAME(_entry) *entry = malloc(sizeof(*entry) + key_size);
	if (!entry)
		return 0;
	entry_mutable = (struct NAME(_entry_mutable) *)entry;

	// Set entry key and value.
	entry_mutable->key_size = key_size;
	entry_mutable->hash = hash;
	entry_mutable->value = value;
	memcpy(entry_mutable->key_data, key_data, key_size);

	// Insert the entry into the bucket.
	entry_mutable->next = *bucket;
	*bucket = entry;

	return &entry->value;
}

// Returns 0 on memory error.
STATIC HASHMAP_TYPE *NAME(_insert)(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE value)
{
	uint_fast32_t hash;
	struct NAME(_entry) *entry;
	size_t index;

	if (!hashmap->buckets)
		if (NAME(_init)(hashmap, HASHMAP_SIZE_BASE) < 0)
			return 0;

	hash = HASHMAP_HASH(key_data, key_size);

	// Look for an entry with the specified key in the hashmap.
	index = NAME(_index)(hashmap, hash);
	for(entry = hashmap->buckets[index]; entry; entry = entry->next)
		if (NAME(_key_eq)(entry, key_data, key_size, hash))
			return &entry->value; // the specified key exists in the hashmap

	// Enlarge the hashmap if the chances of collision are too high.
	if (hashmap->count >= hashmap->buckets_count)
	{
		if (NAME(_expand)(hashmap) < 0)
			return 0;

		// Update target insert bucket.
		index = NAME(_index)(hashmap, hash);
	}

	HASHMAP_TYPE *result = NAME(_insert_internal)(hashmap->buckets + index, key_data, key_size, hash, value);
	if (result)
		hashmap->count += 1;
	return result;
}

// TODO is this function safe (it seems to not check whether the key is already in the map)?
/*STATIC HASHMAP_TYPE *hashmap_insert_fast(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE value)
{
	uint_fast32_t hash = HASHMAP_HASH(key_data, key_size);
	size_t index;

	// Enlarge the hashmap if the chances of collision are too high.
	if (hashmap->count >= hashmap->buckets_count)
		if (hashmap_expand(hashmap) < 0)
			return 0;

	// Find target insert bucket.
	index = hashmap_index(hashmap, hash);

	HASHMAP_TYPE *result = insert(hashmap->buckets + index, key_data, key_size, hash, value);
	if (result) hashmap->count += 1;
	return result;
}*/

// Returns -1 on missing error.
STATIC int NAME(_remove)(struct HASHMAP_NAME *restrict hashmap, const unsigned char *restrict key_data, size_t key_size, HASHMAP_TYPE *value_old)
{
	// Look for the requested key among the items with the corresponding hash.

	uint_fast32_t hash;
	struct NAME(_entry) **bucket, *entry;

	if (!hashmap->buckets)
		return -1; // missing

	hash = HASHMAP_HASH(key_data, key_size);

	// TODO decrease number of buckets?

	// Look for an entry with the specified key in the hashmap.
	for(bucket = hashmap->buckets + NAME(_index)(hashmap, hash); entry = *bucket; bucket = (struct NAME(_entry) **)&entry->next)
		if (NAME(_key_eq)(entry, key_data, key_size, hash))
		{
			if (value_old) *value_old = entry->value;

			*bucket = entry->next;
			free(entry);
			hashmap->count -= 1;

			return 0;
		}

	return -1; // missing
}

// Returns 0 if the hashmap is empty.
STATIC struct NAME(_entry) *NAME(_first)(const struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator)
{
	size_t index = 0;
	for(index = 0; index < hashmap->buckets_count; index += 1)
	{
		if (hashmap->buckets[index])
		{
			iterator->index = index;
			iterator->entry = hashmap->buckets + index;
			return *iterator->entry;
		}
	}
	return 0; // empty
}

// Returns -1 if there are no more entries.
static int NAME(_iterate)(const struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator)
{
	size_t index;

	if ((*iterator->entry)->next)
	{
		iterator->entry = (struct NAME(_entry) **)&(*iterator->entry)->next;
		return 0;
	}

	for(index = iterator->index + 1; index < hashmap->buckets_count; index += 1)
		if (hashmap->buckets[index])
		{
			iterator->index = index;
			iterator->entry = hashmap->buckets + index;
			return 0;
		}

	return -1; // no more entries
}

// Returns 0 if there are no more entries.
// TODO merge this and NAME(_iterate)
STATIC struct NAME(_entry) *NAME(_next)(const struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator)
{
	if (NAME(_iterate)(hashmap, iterator) < 0)
		return 0; // no more entries
	return *iterator->entry;
}

STATIC struct NAME(_entry) *NAME(_remove_current)(struct HASHMAP_NAME *restrict hashmap, struct NAME(_iterator) *restrict iterator)
{
	struct NAME(_entry) **current = iterator->entry;
	struct NAME(_entry) *entry = *current;
	size_t index;

	*current = entry->next;
	hashmap->count -= 1;
	free(entry);

	if (*current) // there are more entries in the current bucket
	{
		iterator->entry = current;
		return *iterator->entry;
	}

	for(index = iterator->index + 1; index < hashmap->buckets_count; index += 1)
		if (hashmap->buckets[index])
		{
			iterator->index = index;
			iterator->entry = hashmap->buckets + index;
			return *iterator->entry;
		}		

	return 0; // no more entries
}

STATIC void NAME(_term)(struct HASHMAP_NAME *hashmap)
{
	size_t i;
	for(i = 0; i < hashmap->buckets_count; ++i)
	{
		struct NAME(_entry) *entry, *entry_next;
		for(entry = hashmap->buckets[i]; entry; entry = entry_next)
		{
			entry_next = entry->next;
			free(entry);
		}
	}
	free(hashmap->buckets);
}

#endif /* HASHMAP_SOURCE */

#undef STATIC

#undef STRING
#undef STRING_EXPAND
#undef NAME
#undef NAME_CAT
#undef NAME_CAT_EXPAND

#undef HASHMAP_HASH
#undef HASHMAP_TYPE
#undef HASHMAP_NAME

#undef HASHMAP_GLOBAL
#undef HASHMAP_SOURCE
