/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

struct djset
{
	struct djelement
	{
		struct djelement *parent;

#if defined(DJSET_ITERATION)
		struct djelement *next;
		struct djelement *class_previous, *class_last; // set only for the root of each class
#endif
		size_t class_count; // set only for the root of each class

		unsigned rank;
		union
		{
			long integer;
			double real;
			void *pointer;
			unsigned char raw[sizeof(void *)];
		} value;
	} *elements;
#if defined(DJSET_ITERATION)
	struct djelement *class_first;
#endif
	size_t count;
	size_t classes;
};

void djset_init(struct djset *restrict djset, struct djelement *restrict elements, size_t count);
struct djelement *djset_find(struct djset *restrict djset, size_t i);
void djset_union(struct djset *restrict djset, size_t i, size_t j);

#if defined(DJSET_ITERATION)
struct djelement *djset_class_first(struct djset *djset);
struct djelement *djset_class_next(struct djset *djset, struct djelement *current);
struct djelement *djset_next(struct djset *djset, struct djelement *current);
#endif
