/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glx.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "interface.h"
#include "image.h"
#include "font.h"
#include "display.h"
#include "ui.h"

#if defined(NDEBUG)
# define PREFIX_IMG PREFIX "share/conquest_of_levidon/img/"
#else
# define PREFIX_IMG "img/"
#endif

#define TABS_X 32
#define TABS_Y 32

#define PLAYERS_X 780
#define PLAYERS_Y 48
#define PLAYERS_INDICATOR_SIZE 32
#define PLAYERS_PADDING 8

#define WORLDS_X 32
#define WORLDS_Y 56

#define MINIMAP_X 24
#define MINIMAP_Y 48

#define REPORT_X 32
#define REPORT_Y 80

#define TROOPS_BAR_X 58
#define TROOPS_BAR_Y 19
#define TROOPS_BAR_WIDTH 3
#define TROOPS_BAR_HEIGHT 34

#define MINIMAP_SCALE 6

const unsigned char display_colors[][4] = {
	[Mountain] = {128, 64, 0, 255},
	[Water] = {0, 128, 192, 255},
	[Hidden] = {64, 64, 64, 128},

	[Background] = {0, 0, 0, 255},
	[Text] = {0, 0, 0, 255},
	[TextForeground] = {255, 255, 255, 255},
	[TextMenu] = {255, 255, 255, 255},
	[TextSelected] = {0, 0, 0, 255},
	[Success] = {0, 128, 0, 255},
	[Error] = {255, 0, 0, 255},

	[BackgroundBox] = {176, 176, 176, 255},
	[Border] = {0, 0, 0, 255},
	[Active] = {0, 192, 192, 255},
	[TabBorder] = {0, 57, 77, 255},

	[Progress] = {0, 0, 0, 128},

	[Self] = {0, 192, 0, 255},
	[Ally] = {0, 0, 255, 255},
	[Enemy] = {255, 0, 0, 255},

	[Player + 0] = {160, 160, 160, 255},
	[Player + 1] = {0, 96, 0, 255},
	[Player + 2] = {128, 0, 0, 255},
	[Player + 3] = {0, 255, 255, 255},
	[Player + 4] = {255, 255, 0, 255},
	[Player + 5] = {0, 0, 128, 255},
	[Player + 6] = {255, 160, 0, 255},
	[Player + 7] = {0, 176, 255, 255},
	[Player + 8] = {128, 96, 0, 255},
	[Player + 9] = {192, 0, 160, 255},
	[Player + 10] = {128, 0, 255, 255},
	[Player + 11] = {255, 96, 160, 255},
	[Player + 12] = {160, 128, 96, 255},
	[Player + 13] = {0, 144, 144, 255},
	[Player + 14] = {160, 160, 255, 255},
	[Player + 15] = {224, 224, 224, 255},
	[Player + 16] = {32, 32, 32, 255},
	[Player + 17] = {192, 255, 0, 255},
	[Player + 18] = {48, 0, 96, 255},
	[Player + 19] = {128, 192, 128, 255},
	[Player + 20] = {96, 64, 64, 255},
	[Player + 21] = {64, 64, 96, 255},
	[Player + 22] = {0, 64, 64, 255},
	[Player + 23] = {255, 193, 173, 255},
};

struct font font7, font10, font12, font24;

struct image image_world;
struct image image_panel, image_battlefield;
struct image image_tab_active, image_tab_inactive, image_tab_left, image_tab_right;
struct image image_terrain_ground, image_terrain_coast, image_terrain_mountain, image_terrain_water;
struct image image_gold, image_wood, image_stone, image_food, image_time;
struct image image_village, image_flag, image_hexagon_flag, image_hexagon_flag_mask;
struct image image_units[UNITS_COUNT], image_units_mask[UNITS_COUNT];
struct image image_unit_ship, image_unit_ship_mask, image_unit_ship_gray;
struct image image_buildings[BUILDINGS_COUNT], image_buildings_gray[BUILDINGS_COUNT];
struct image image_settlement[GARRISONS_COUNT], image_settlement_mask[GARRISONS_COUNT];
struct image image_army, image_ship, image_ship_small, image_ship_small_mask, image_forest, image_quarry;
struct image image_order_add, image_order_dismiss;
struct image image_construction, image_movement, image_dismiss, image_anchor, image_assault;
struct image image_move, image_move_end;
struct image image_pawn_assault, image_pawn_fight, image_pawn_guard, image_pawn_shoot;
struct image image_garrison_wall_lr, image_garrison_wall_tb, image_garrison_wall_tl, image_garrison_wall_tr;
struct image image_garrison_gate_closed_l, image_garrison_gate_closed_r, image_garrison_gate_closed_b, image_garrison_gate_open_l, image_garrison_gate_open_r, image_garrison_gate_open_b;
struct image image_garrison_tower_l, image_garrison_tower_r, image_garrison_tower_b;
struct image image_gate_open, image_gate_closed, image_gate_open_gray, image_gate_closed_gray;
struct image image_fight, image_shoot_u, image_shoot_d, image_shoot_l, image_shoot_r;

const struct widget widgets[] = {
	[Worlds] = WIDGET(24, 1, WORLDS_X, WORLDS_Y, 240, 20, 0),
	[WorldTabs] = WIDGET(1, 3, TABS_X, TABS_Y, 80, 24, 2),
	[Players] = WIDGET(PLAYERS_LIMIT / 2, 2, PLAYERS_X, PLAYERS_Y, 120, PLAYERS_INDICATOR_SIZE, PLAYERS_PADDING),
	[GarrisonTroops] = WIDGET(1, GARRISON_TROOPS_LIMIT, 26, 140, TROOP_EDGE, TROOP_EDGE, 2),
	[TrainQueue] = WIDGET(1, 4, 80, 210, TROOP_EDGE, TROOP_EDGE, 2),
	[Ship] = WIDGET(1, 1, 220, 210, TROOP_EDGE, TROOP_EDGE, 0),
	[Buildings] = WIDGET(3, 5, 6, 250, BUILDING_EDGE, BUILDING_EDGE, 1),
	[Troops] = WIDGET(2, 6, 26, TROOPS_ARROW_Y + 36 + TROOPS_PADDING, TROOP_EDGE, TROOP_EDGE + 10, 2),
	[TroopsTabs] = WIDGET(1, 5, TROOPS_ARROW_LEFT_X + TROOPS_ARROW_WIDTH + 1, TROOPS_ARROW_Y, 40, 36, 0),
	[Resources] = WIDGET(4, 1, 5, 667, 120, 16, 2),
	[Orders] = WIDGET(2, 6, 26, 580, TROOP_EDGE, TROOP_EDGE, 2),
};

uint32_t cursors[UNITS_COUNT + 1];

void if_load(const char *restrict world)
{
	if (font_load(&font7, 8) < 0)
		LOG_WARNING("Cannot load font with size 8");
	if (font_load(&font10, 10) < 0)
		LOG_WARNING("Cannot load font with size 10");
	if (font_load(&font12, 12) < 0)
		LOG_WARNING("Cannot load font with size 12");
	if (font_load(&font24, 24) < 0)
		LOG_WARNING("Cannot load font with size 24");

	if (world)
		image_load_png(&image_world, world, 0);

	image_load_png(&image_panel, PREFIX_IMG "panel.png", 0);
	image_load_png(&image_battlefield, PREFIX_IMG "battlefield.png", 0);

	image_load_png(&image_tab_active, PREFIX_IMG "tab_active.png", 0);
	image_load_png(&image_tab_inactive, PREFIX_IMG "tab_inactive.png", 0);
	image_load_png(&image_tab_left, PREFIX_IMG "tab_left.png", 0);
	image_load_png(&image_tab_right, PREFIX_IMG "tab_right.png", 0);

	image_load_png(&image_terrain_ground, PREFIX_IMG "terrain_ground.png", 0);
	image_load_png(&image_terrain_coast, PREFIX_IMG "terrain_coast.png", 0);
	image_load_png(&image_terrain_mountain, PREFIX_IMG "terrain_mountain.png", 0);
	image_load_png(&image_terrain_water, PREFIX_IMG "terrain_water.png", 0);

	image_load_png(&image_flag, PREFIX_IMG "flag.png", 0);
	image_load_png(&image_hexagon_flag, PREFIX_IMG "hexagon_flag.png", 0);
	image_load_png(&image_hexagon_flag_mask, PREFIX_IMG "hexagon_flag.png", filter_mask);

	image_load_png(&image_gold, PREFIX_IMG "gold.png", 0);
	image_load_png(&image_wood, PREFIX_IMG "wood.png", 0);
	image_load_png(&image_stone, PREFIX_IMG "stone.png", 0);
	image_load_png(&image_food, PREFIX_IMG "food.png", 0);
	image_load_png(&image_time, PREFIX_IMG "time.png", 0);

	image_load_png(image_units + UnitPeasant, PREFIX_IMG "unit_peasant.png", 0);
	image_load_png(image_units + UnitWorker, PREFIX_IMG "unit_worker.png", 0);
	image_load_png(image_units + UnitMilitia, PREFIX_IMG "unit_militia.png", 0);
	image_load_png(image_units + UnitPikeman, PREFIX_IMG "unit_pikeman.png", 0);
	image_load_png(image_units + UnitArcher, PREFIX_IMG "unit_archer.png", 0);
	image_load_png(image_units + UnitLongbow, PREFIX_IMG "unit_longbow.png", 0);
	image_load_png(image_units + UnitLightCavalry, PREFIX_IMG "unit_light_cavalry.png", 0);
	image_load_png(image_units + UnitBatteringRam, PREFIX_IMG "unit_battering_ram.png", 0);

	image_load_png(image_units_mask + UnitPeasant, PREFIX_IMG "unit_peasant.png", filter_mask);
	image_load_png(image_units_mask + UnitWorker, PREFIX_IMG "unit_worker.png", filter_mask);
	image_load_png(image_units_mask + UnitMilitia, PREFIX_IMG "unit_militia.png", filter_mask);
	image_load_png(image_units_mask + UnitPikeman, PREFIX_IMG "unit_pikeman.png", filter_mask);
	image_load_png(image_units_mask + UnitArcher, PREFIX_IMG "unit_archer.png", filter_mask);
	image_load_png(image_units_mask + UnitLongbow, PREFIX_IMG "unit_longbow.png", filter_mask);
	image_load_png(image_units_mask + UnitLightCavalry, PREFIX_IMG "unit_light_cavalry.png", filter_mask);
	image_load_png(image_units_mask + UnitBatteringRam, PREFIX_IMG "unit_battering_ram.png", filter_mask);

	image_load_png(&image_unit_ship, PREFIX_IMG "unit_ship.png", 0);
	image_load_png(&image_unit_ship_mask, PREFIX_IMG "unit_ship.png", filter_mask);
	image_load_png(&image_unit_ship_gray, PREFIX_IMG "unit_ship.png", filter_grayscale);

	image_load_png(image_buildings + BuildingTownHall, PREFIX_IMG "building_town_hall.png", 0);
	image_load_png(image_buildings + BuildingIrrigation, PREFIX_IMG "building_irrigation_canals.png", 0);
	image_load_png(image_buildings + BuildingGranary, PREFIX_IMG "building_granary.png", 0);
	image_load_png(image_buildings + BuildingBarracks, PREFIX_IMG "building_barracks.png", 0);
	image_load_png(image_buildings + BuildingArcheryRange, PREFIX_IMG "building_archery_range.png", 0);
	image_load_png(image_buildings + BuildingStables, PREFIX_IMG "building_stables.png", 0);
	image_load_png(image_buildings + BuildingForge, PREFIX_IMG "building_forge.png", 0);
	image_load_png(image_buildings + BuildingWorkshop, PREFIX_IMG "building_workshop.png", 0);
	image_load_png(image_buildings + BuildingPort, PREFIX_IMG "building_port.png", 0);
	image_load_png(image_buildings + BuildingPalisade, PREFIX_IMG "building_palisade.png", 0);
	image_load_png(image_buildings + BuildingFortress, PREFIX_IMG "building_fortress.png", 0);
	image_load_png(image_buildings + BuildingSawmill, PREFIX_IMG "building_sawmill.png", 0);
	image_load_png(image_buildings + BuildingQuarry, PREFIX_IMG "building_quarry.png", 0);
	image_load_png(image_buildings + BuildingWatchTower, PREFIX_IMG "building_watch_tower.png", 0);
	image_load_png(image_buildings + BuildingShipyard, PREFIX_IMG "building_shipyard.png", 0);

	image_load_png(image_buildings_gray + BuildingTownHall, PREFIX_IMG "building_town_hall.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingIrrigation, PREFIX_IMG "building_irrigation_canals.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingGranary, PREFIX_IMG "building_granary.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingBarracks, PREFIX_IMG "building_barracks.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingArcheryRange, PREFIX_IMG "building_archery_range.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingStables, PREFIX_IMG "building_stables.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingForge, PREFIX_IMG "building_forge.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingWorkshop, PREFIX_IMG "building_workshop.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingPort, PREFIX_IMG "building_port.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingPalisade, PREFIX_IMG "building_palisade.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingFortress, PREFIX_IMG "building_fortress.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingSawmill, PREFIX_IMG "building_sawmill.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingQuarry, PREFIX_IMG "building_quarry.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingWatchTower, PREFIX_IMG "building_watch_tower.png", filter_grayscale);
	image_load_png(image_buildings_gray + BuildingShipyard, PREFIX_IMG "building_shipyard.png", filter_grayscale);

	image_load_png(image_settlement + GARRISON_NONE, PREFIX_IMG "settlement.png", 0);
	image_load_png(image_settlement + GARRISON_PALISADE, PREFIX_IMG "settlement_palisade.png", 0);
	image_load_png(image_settlement + GARRISON_FORTRESS, PREFIX_IMG "settlement_fortress.png", 0);

	image_village = image_settlement[GARRISON_NONE];

	image_load_png(image_settlement_mask + GARRISON_NONE, PREFIX_IMG "settlement.png", filter_mask);
	image_load_png(image_settlement_mask + GARRISON_PALISADE, PREFIX_IMG "settlement_palisade.png", filter_mask);
	image_load_png(image_settlement_mask + GARRISON_FORTRESS, PREFIX_IMG "settlement_fortress.png", filter_mask);

	image_load_png(&image_army, PREFIX_IMG "army.png", 0);
	image_load_png(&image_ship, PREFIX_IMG "ship.png", 0);
	image_load_png(&image_ship_small, PREFIX_IMG "ship_small.png", 0);
	image_load_png(&image_ship_small_mask, PREFIX_IMG "ship_small.png", filter_mask);
	image_load_png(&image_forest, PREFIX_IMG "forest.png", 0);
	image_load_png(&image_quarry, PREFIX_IMG "quarry.png", 0);

	image_load_png(&image_order_add, PREFIX_IMG "order_add.png", 0);
	image_load_png(&image_order_dismiss, PREFIX_IMG "order_dismiss.png", 0);

	image_load_png(&image_construction, PREFIX_IMG "construction.png", 0);
	image_load_png(&image_movement, PREFIX_IMG "movement.png", 0);
	image_load_png(&image_dismiss, PREFIX_IMG "dismiss.png", 0);
	image_load_png(&image_anchor, PREFIX_IMG "anchor.png", 0);
	image_load_png(&image_assault, PREFIX_IMG "assault.png", 0);

	image_load_png(&image_move, PREFIX_IMG "move.png", 0);
	image_load_png(&image_move_end, PREFIX_IMG "move_end.png", 0);

	image_load_png(&image_pawn_assault, PREFIX_IMG "pawn_assault.png", 0);
	image_load_png(&image_pawn_fight, PREFIX_IMG "pawn_fight.png", 0);
	image_load_png(&image_pawn_guard, PREFIX_IMG "pawn_guard.png", 0);
	image_load_png(&image_pawn_shoot, PREFIX_IMG "pawn_shoot.png", 0);

	// Assault garrison images.

	image_load_png(&image_garrison_wall_lr, PREFIX_IMG "palisade_wall_lr.png", 0);
	image_load_png(&image_garrison_wall_tb, PREFIX_IMG "palisade_wall_tb.png", 0);
	image_load_png(&image_garrison_wall_tl, PREFIX_IMG "palisade_wall_tl.png", 0);
	image_load_png(&image_garrison_wall_tr, PREFIX_IMG "palisade_wall_tr.png", 0);

	image_load_png(&image_garrison_gate_closed_l, PREFIX_IMG "palisade_gate_closed_l.png", 0);
	image_load_png(&image_garrison_gate_closed_r, PREFIX_IMG "palisade_gate_closed_r.png", 0);
	image_load_png(&image_garrison_gate_closed_b, PREFIX_IMG "palisade_gate_closed_b.png", 0);
	image_load_png(&image_garrison_gate_open_l, PREFIX_IMG "palisade_gate_open_l.png", 0);
	image_load_png(&image_garrison_gate_open_r, PREFIX_IMG "palisade_gate_open_r.png", 0);
	image_load_png(&image_garrison_gate_open_b, PREFIX_IMG "palisade_gate_open_b.png", 0);

	image_load_png(&image_garrison_tower_l, PREFIX_IMG "palisade_tower_l.png", 0);
	image_load_png(&image_garrison_tower_r, PREFIX_IMG "palisade_tower_r.png", 0);
	image_load_png(&image_garrison_tower_b, PREFIX_IMG "palisade_tower_b.png", 0);

	// TODO use separate fortress images

	image_load_png(&image_gate_open, PREFIX_IMG "gate_open.png", 0);
	image_load_png(&image_gate_closed, PREFIX_IMG "gate_close.png", 0);

	image_load_png(&image_gate_open_gray, PREFIX_IMG "gate_open.png", filter_grayscale);
	image_load_png(&image_gate_closed_gray, PREFIX_IMG "gate_close.png", filter_grayscale);

	image_load_png(&image_fight, PREFIX_IMG "fight.png", 0);
	image_load_png(&image_shoot_u, PREFIX_IMG "shoot_up.png", 0);
	image_load_png(&image_shoot_d, PREFIX_IMG "shoot_down.png", 0);
	image_load_png(&image_shoot_l, PREFIX_IMG "shoot_left.png", 0);
	image_load_png(&image_shoot_r, PREFIX_IMG "shoot_right.png", 0);

#if defined(ENABLE_CURSOR)
	cursor_load_png(cursors + UnitPeasant, PREFIX_IMG "unit_peasant.png");
	cursor_load_png(cursors + UnitWorker, PREFIX_IMG "unit_worker.png");
	cursor_load_png(cursors + UnitMilitia, PREFIX_IMG "unit_militia.png");
	cursor_load_png(cursors + UnitPikeman, PREFIX_IMG "unit_pikeman.png");
	cursor_load_png(cursors + UnitArcher, PREFIX_IMG "unit_archer.png");
	cursor_load_png(cursors + UnitLongbow, PREFIX_IMG "unit_longbow.png");
	cursor_load_png(cursors + UnitLightCavalry, PREFIX_IMG "unit_light_cavalry.png");
	cursor_load_png(cursors + UnitBatteringRam, PREFIX_IMG "unit_battering_ram.png");

	cursor_load_png(cursors + UNITS_COUNT, PREFIX_IMG "cursor_default.png");

	if_cursor_set(cursors[UNITS_COUNT]);
#endif
}

void if_unload(void)
{
	size_t i;

#if defined(ENABLE_CURSOR)
	for(i = 0; i < sizeof(cursors) / sizeof(*cursors); i += 1)
		if_cursor_term(cursors[i]);
#endif

	image_unload(&image_shoot_r);
	image_unload(&image_shoot_l);
	image_unload(&image_shoot_d);
	image_unload(&image_shoot_u);
	image_unload(&image_fight);

	image_unload(&image_gate_closed_gray);
	image_unload(&image_gate_open_gray);

	image_unload(&image_gate_closed);
	image_unload(&image_gate_open);

	image_unload(&image_garrison_tower_b);
	image_unload(&image_garrison_tower_r);
	image_unload(&image_garrison_tower_l);

	image_unload(&image_garrison_gate_open_b);
	image_unload(&image_garrison_gate_open_r);
	image_unload(&image_garrison_gate_open_l);
	image_unload(&image_garrison_gate_closed_b);
	image_unload(&image_garrison_gate_closed_r);
	image_unload(&image_garrison_gate_closed_l);

	image_unload(&image_garrison_wall_tr);
	image_unload(&image_garrison_wall_tl);
	image_unload(&image_garrison_wall_tb);
	image_unload(&image_garrison_wall_lr);

	image_unload(&image_pawn_shoot);
	image_unload(&image_pawn_guard);
	image_unload(&image_pawn_fight);
	image_unload(&image_pawn_assault);

	image_unload(&image_move_end);
	image_unload(&image_move);

	image_unload(&image_assault);
	image_unload(&image_anchor);
	image_unload(&image_dismiss);
	image_unload(&image_movement);
	image_unload(&image_construction);

	image_unload(&image_order_dismiss);
	image_unload(&image_order_add);

	image_unload(&image_quarry);
	image_unload(&image_forest);
	image_unload(&image_ship_small_mask);
	image_unload(&image_ship_small);
	image_unload(&image_ship);
	image_unload(&image_army);

	for(i = 0; i < GARRISONS_COUNT; i += 1)
	{
		image_unload(image_settlement_mask + i);
		image_unload(image_settlement + i);
	}

	for(i = 0; i < BUILDINGS_COUNT; i += 1)
		image_unload(image_buildings + i);

	image_unload(&image_unit_ship_gray);
	image_unload(&image_unit_ship_mask);
	image_unload(&image_unit_ship);

	for(i = 0; i < UNITS_COUNT; i += 1)
	{
		image_unload(image_units_mask + i);
		image_unload(image_units + i);
	}

	image_unload(&image_time);
	image_unload(&image_food);
	image_unload(&image_stone);
	image_unload(&image_wood);
	image_unload(&image_gold);

	image_unload(&image_hexagon_flag_mask);
	image_unload(&image_hexagon_flag);
	image_unload(&image_flag);

	image_unload(&image_terrain_water);
	image_unload(&image_terrain_mountain);
	image_unload(&image_terrain_coast);
	image_unload(&image_terrain_ground);

	image_unload(&image_tab_right);
	image_unload(&image_tab_left);
	image_unload(&image_tab_inactive);
	image_unload(&image_tab_active);

	image_unload(&image_battlefield);
	image_unload(&image_panel);

	image_unload(&image_world);

	font_unload(&font24);
	font_unload(&font12);
	font_unload(&font10);
	font_unload(&font7);
}

// Array index corresponds to the first vertex, counterclockwise.
static const struct vector hexagon_vertices[] = {
	[East] = {SQRT3 * HEXAGON_EDGE + 0.5, 1.5 * HEXAGON_EDGE + 0.5},
	[NorthEast] = {SQRT3 * HEXAGON_EDGE + 0.5, 0.5 * HEXAGON_EDGE + 0.5},
	[NorthWest] = {0.5 * SQRT3 * HEXAGON_EDGE + 0.5, 0},
	[West] = {0, 0.5 * HEXAGON_EDGE + 0.5},
	[SouthWest] = {0, 1.5 * HEXAGON_EDGE + 0.5},
	[SouthEast] = {0.5 * SQRT3 * HEXAGON_EDGE + 0.5, 2.0 * HEXAGON_EDGE + 0.5},
};

void display_hexagon(double offset_x, double offset_y, double scale, const unsigned char color[static 4])
{
	glColor4ubv(color);

	glBegin(GL_POLYGON);
	glVertex2f(offset_x + hexagon_vertices[East].x / scale + 0.5, offset_y + hexagon_vertices[East].y / scale + 0.5);
	glVertex2f(offset_x + hexagon_vertices[NorthEast].x / scale + 0.5, offset_y + hexagon_vertices[NorthEast].y / scale - 0.5);
	glVertex2f(offset_x + hexagon_vertices[NorthWest].x / scale, offset_y + hexagon_vertices[NorthWest].y / scale - 0.5);
	glVertex2f(offset_x + hexagon_vertices[West].x / scale - 0.5, offset_y + hexagon_vertices[West].y / scale - 0.5);
	glVertex2f(offset_x + hexagon_vertices[SouthWest].x / scale - 0.5, offset_y + hexagon_vertices[SouthWest].y / scale + 0.5);
	glVertex2f(offset_x + hexagon_vertices[SouthEast].x / scale, offset_y + hexagon_vertices[SouthEast].y / scale + 0.5);
	glEnd();
}

void display_button(struct slice label, unsigned x, unsigned y, int selected)
{
	struct box box = string_box(label, &font12);
	draw_rectangle(x - 1, y - 1, BUTTON_WIDTH + 2, BUTTON_HEIGHT + 2, display_colors[Border]);
	fill_rectangle(x, y, BUTTON_WIDTH, BUTTON_HEIGHT, display_colors[selected ? Active : BackgroundBox]);
	draw_string(label, x + (BUTTON_WIDTH - box.width) / 2, y + (BUTTON_HEIGHT - box.height) / 2, &font12, display_colors[Text]);
}

void display_string_center(struct slice string, struct vector offset, struct box box, struct font *restrict font, const unsigned char color[static 4])
{
    struct box content = string_box(string, &font10);
	int x = offset.x + (box.width - content.width) / 2;
	int y = offset.y + (box.height - content.height) / 2;
    draw_string(string, x, y, &font10, display_colors[Text]);
}

void display_settlement(const struct game *restrict game, struct tile location, int left, int top, size_t index, const struct army *restrict army, unsigned char player, unsigned edge)
{
	display_object(game, image_settlement + index, army, player, location, left, top, edge);
}

void display_object(const struct game *restrict game, const struct image *restrict image, const struct army *restrict army, unsigned char player, struct tile location, int left, int top, unsigned edge)
{
	double x = hexagon_offset_x(left, location.x, location.y, edge);
	double y = hexagon_offset_y(top, location.x, location.y, edge);
	double x_image = x + (int)(hexagon_vertices[East].x * (edge / HEXAGON_EDGE) - image->width) / 2;
	double y_image = y + (int)(hexagon_vertices[SouthEast].y * (edge / HEXAGON_EDGE) - image->height) / 2;

	fill_image(image, x_image, y_image, image->width, image->height, 0);

	if (army)
	{
		unsigned height;
		const unsigned char *color;

		if (army->owner == player)
			color = display_colors[Self];
		else if (allies(game, army->owner, player))
			color = display_colors[Ally];
		else
			color = display_colors[Enemy];

		fill_image(&image_hexagon_flag, x_image, y_image, image_hexagon_flag.width, image_hexagon_flag.height, 0);
		fill_image_mask(&image_hexagon_flag_mask, x_image, y_image, image_hexagon_flag_mask.width, image_hexagon_flag_mask.height, display_colors[Player + army->owner]);

		// Display army bars.
		x += TROOPS_BAR_X;
		y += TROOPS_BAR_Y;
		if (army->field)
		{
			height = TROOPS_BAR_HEIGHT * army->field / TROOPS_CAPACITY_MAX + 0.5;
			if (height > TROOPS_BAR_HEIGHT)
				height = TROOPS_BAR_HEIGHT;
			else if (height < 1)
				height = 1;
			draw_rectangle(x - 1, y + TROOPS_BAR_HEIGHT - height - 1, TROOPS_BAR_WIDTH + 2, height + 2, display_colors[Background]);
			fill_rectangle(x, y + TROOPS_BAR_HEIGHT - height, TROOPS_BAR_WIDTH, height, color);
			x -= 1 + TROOPS_BAR_WIDTH;
		}
		if (army->garrison && allies(game, army->owner, player))
		{
			height = TROOPS_BAR_HEIGHT * army->garrison / TROOPS_CAPACITY_MAX + 0.5;
			if (height > TROOPS_BAR_HEIGHT)
				height = TROOPS_BAR_HEIGHT;
			else if (height < 1)
				height = 1;
			draw_rectangle(x - 1, y + TROOPS_BAR_HEIGHT - height - 1, TROOPS_BAR_WIDTH + 2, height + 2, display_colors[Background]);
			fill_rectangle(x, y + TROOPS_BAR_HEIGHT - height, TROOPS_BAR_WIDTH, height, color);
			x -= 1 + TROOPS_BAR_WIDTH;
		}
	}
}

void display_troop(const struct unit *restrict unit, int count, enum color owner, unsigned x, unsigned y, enum color text)
{
	fill_image(&image_units[unit - UNITS], x, y, image_units[unit - UNITS].width, image_units[unit - UNITS].height, 0);
	fill_image_mask(&image_units_mask[unit - UNITS], x, y, image_units_mask[unit - UNITS].width, image_units_mask[unit - UNITS].height, display_colors[owner]);

	if (count)
	{
		char buffer[FORMAT_SIZE_BASE10(count)];
		size_t length = format_int(buffer, count, 10) - (uint8_t *)buffer;
		unsigned edge = image_units[unit - UNITS].width;
		draw_string(slice(buffer, length), x + (edge - length * font10.size) / 2, y + edge, &font10, display_colors[text]);
	}
}

void display_troop_stack(const struct troop_stack *restrict troop_stack, unsigned x, unsigned y, const struct game *restrict game, unsigned player_current)
{
	const struct troop *troop = 0;
	size_t i;

	double quantity = 0;
	unsigned height;
	enum color color;
	struct image *image, *mask;

	if (player_current == troop_stack->owner)
	{
		color = Self;

		for(i = 0; i < troop_stack->private.count; i += 1)
		{
			troop = troop_stack->private.troops[i];
			quantity += (double)troop->count / troop->unit->troops_count;
		}
	}
	else
	{
		color = (allies(game, player_current, troop_stack->owner) ? Ally : Enemy);

		for(i = 0; i < troop_stack->public.count; i += 1)
		{
			troop = troop_stack->public.troops[i];
			quantity += (double)troop->count / troop->unit->troops_count;
		}
	}

	if (troop_stack->vessel->autonomous)
	{
		image = &image_unit_ship;
		mask = &image_unit_ship_mask;
	}
	else
	{
		assert(troop);

		i = troop->unit - UNITS;
		image = &image_units[i];
		mask = &image_units_mask[i];
	}

	fill_image(image, x, y, image->width, image->height, 0);
	fill_image_mask(mask, x, y, mask->width, mask->height, display_colors[Player + troop_stack->owner]);

	height = image->height * quantity / TROOPS_CAPACITY_MAX + 0.5;
	if (height > image->height)
		height = image->height;
	if (height < 1)
		height = 1;

	fill_rectangle(x + image->width + 1, y, TROOPS_BAR_WIDTH, image->height, display_colors[Background]);
	fill_rectangle(x + image->width + 1, y + image->height - height, TROOPS_BAR_WIDTH, height, display_colors[color]);
}

void display_building(const struct building *restrict building, unsigned position, int built)
{
	size_t index = building - BUILDINGS;
	struct vector location = object_position(&widgets[Buildings], position);
	if (built)
		fill_image(&image_buildings[index], location.x, location.y, image_buildings[index].width, image_buildings[index].height, 0);
	else
		fill_image(&image_buildings_gray[index], location.x, location.y, image_buildings_gray[index].width, image_buildings_gray[index].height, 0);
}

void display_unit(const struct region *restrict region, const struct unit *restrict unit)
{
	size_t index = unit - UNITS;
	struct vector position = object_position(&widgets[Orders], index);
	fill_image(&image_units[index], position.x, position.y, image_units[index].width, image_units[index].height, 0);
}

void display_flag(unsigned x, unsigned y, unsigned player)
{
	fill_rectangle(x + 4, y + 4, 24, 12, display_colors[Player + player]);
	fill_image(&image_flag, x, y, image_flag.width, image_flag.height, 0);
}

void display_minimap(const struct game *restrict game)
{
	// Fill each hexagon with the color of its owner.
	// Show garrison location.
	size_t i;
	size_t hexagons_total = hexagons_count(game);
	for(i = 0; i < hexagons_total; i += 1)
	{
		struct tile location = hexagon_location(game, i);
		double offset_x, offset_y;
		const struct region *restrict region = 0;
		enum color color;

		if (game->hexagons[i].terrain == TERRAIN_MOUNTAIN)
			continue;

		offset_x = hexagon_offset_x(MINIMAP_X, location.x, location.y, HEXAGON_EDGE / MINIMAP_SCALE);
		offset_y = hexagon_offset_y(MINIMAP_Y, location.x, location.y, HEXAGON_EDGE / MINIMAP_SCALE);

		if (game->hexagons[i].terrain == TERRAIN_GROUND)
		{
			region = game->regions + game->hexagons[i].info.region_index;
			if (game->hexagons[i].buildings)
				color = Player + game->hexagons[i].owner;
			else if (region->garrison.hexagon->buildings)
				color = Player + region->garrison.hexagon->owner;
			else
				color = Player + PLAYER_NEUTRAL;
		}
		else
			color = Water; // game->hexagons[i].terrain == TERRAIN_WATER

		display_hexagon(offset_x, offset_y, MINIMAP_SCALE, display_colors[color]);
		if (game->hexagons[i].environment & (1 << ENVIRONMENT_SETTLEMENT))
		{
			unsigned x = offset_x + hexagon_vertices[NorthWest].x / MINIMAP_SCALE + 0.5;
			unsigned y = offset_y + 0.5 * hexagon_vertices[SouthEast].y / MINIMAP_SCALE + 0.5;
			fill_rectangle(x - 1, y - 1, 2, 2, display_colors[Text]);
		}
	}
}
