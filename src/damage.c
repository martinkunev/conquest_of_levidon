/*
 * Conquest of Levidon
 * Copyright (C) 2022  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>

#include "game.h"

//#define DAMAGE_NEUTRAL 0.5 /* damage dealt given equal attack and defense */
//#define STRENGTH_HALF 2 /* attack-defense difference required for damage 1/2 */

#define DAMAGE_SCALE 0.5 /* */

const double damage_boost[WEAPONS_COUNT][ARMORS_COUNT] =
{
// ARMOR:					NONE		LEATHER		CHAINMAIL	PLATE		WOODEN		STONE
	[WEAPON_NONE] =	{		0.0,		0.0,		0.0,		0.0,		0.0,		0.0},
	[WEAPON_CLUB] = {		1.0,		0.8,		0.7,		0.2,		0.0,		0.0},
	[WEAPON_ARROW] = {		1.0,		0.8,		0.7,		0.2,		0.0,		0.0},
	[WEAPON_CLEAVING] = {	1.0,		0.9,		0.7,		0.3,		0.5,		0.0},
	[WEAPON_POLEARM] = {	1.0,		1.0,		0.7,		0.4,		0.2,		0.0},
	[WEAPON_BLADE] = {		1.0,		1.0,		0.8,		0.5,		0.2,		0.0},
	[WEAPON_BLUNT] = {		1.0,		1.0,		1.0,		0.5,		1.0,		0.7},
};

static inline double logistic(double value)
{
    return 1 / (1 + exp(-value));
}

static double probability_kill(double a, double d, enum weapon weapon, enum armor armor, double duration)
{
	// TODO some of these calculations should be performed compile-time
	//double offset = log(1 / DAMAGE_NEUTRAL - 1);
	//return damage_boost[weapon][armor] * logistic(offset * (a - d) / STRENGTH_HALF - offset);
	//return logistic(offset * (a - d) / STRENGTH_HALF - offset);

	return damage_boost[weapon][armor] * logistic(DAMAGE_SCALE * (a - d)) * duration;

	/*
	if (result > 0.99)
		return 1;
	if (result < 0.01)
		return 0;
	return result;*/
}

static void print_damage(enum weapon weapon, enum armor armor, const char *text)
{
	int a, d;

	printf("%s\n   ", text);
	for(d = 0; d < 12; d += 1)
		printf("% 4u ", d);
	putchar('\n');
	for(a = 0; a < 12; a += 1)
	{
		printf("% 2u ", a);
		for(d = 0; d < 12; d += 1)
		{
			double v = probability_kill(a, d, weapon, armor);
			printf("%.2f ", v);
		}
		putchar('\n');
	}
	putchar('\n');
}

int main()
{
	// TODO compile-time support for loop through all enum values

	print_damage(WEAPON_CLUB, ARMOR_NONE, "club -> none");
	print_damage(WEAPON_CLUB, ARMOR_LEATHER, "club -> leather");
	print_damage(WEAPON_CLUB, ARMOR_CHAINMAIL, "club -> chainmail");
	print_damage(WEAPON_CLUB, ARMOR_PLATE, "club -> plate");
	print_damage(WEAPON_CLUB, ARMOR_WOODEN, "club -> wooden");
	print_damage(WEAPON_CLUB, ARMOR_STONE, "club -> stone");

	print_damage(WEAPON_ARROW, ARMOR_NONE, "arrow -> none");
	print_damage(WEAPON_ARROW, ARMOR_LEATHER, "arrow -> leather");
	print_damage(WEAPON_ARROW, ARMOR_CHAINMAIL, "arrow -> chainmail");
	print_damage(WEAPON_ARROW, ARMOR_PLATE, "arrow -> plate");
	print_damage(WEAPON_ARROW, ARMOR_WOODEN, "arrow -> wooden");
	print_damage(WEAPON_ARROW, ARMOR_STONE, "arrow -> stone");

	print_damage(WEAPON_CLEAVING, ARMOR_NONE, "cleaving -> none");
	print_damage(WEAPON_CLEAVING, ARMOR_LEATHER, "cleaving -> leather");
	print_damage(WEAPON_CLEAVING, ARMOR_CHAINMAIL, "cleaving -> chainmail");
	print_damage(WEAPON_CLEAVING, ARMOR_PLATE, "cleaving -> plate");
	print_damage(WEAPON_CLEAVING, ARMOR_WOODEN, "cleaving -> wooden");
	print_damage(WEAPON_CLEAVING, ARMOR_STONE, "cleaving -> stone");

	print_damage(WEAPON_POLEARM, ARMOR_NONE, "polearm -> none");
	print_damage(WEAPON_POLEARM, ARMOR_LEATHER, "polearm -> leather");
	print_damage(WEAPON_POLEARM, ARMOR_CHAINMAIL, "polearm -> chainmail");
	print_damage(WEAPON_POLEARM, ARMOR_PLATE, "polearm -> plate");
	print_damage(WEAPON_POLEARM, ARMOR_WOODEN, "polearm -> wooden");
	print_damage(WEAPON_POLEARM, ARMOR_STONE, "polearm -> stone");

	print_damage(WEAPON_BLADE, ARMOR_NONE, "blade -> none");
	print_damage(WEAPON_BLADE, ARMOR_LEATHER, "blade -> leather");
	print_damage(WEAPON_BLADE, ARMOR_CHAINMAIL, "blade -> chainmail");
	print_damage(WEAPON_BLADE, ARMOR_PLATE, "blade -> plate");
	print_damage(WEAPON_BLADE, ARMOR_WOODEN, "blade -> wooden");
	print_damage(WEAPON_BLADE, ARMOR_STONE, "blade -> stone");

	print_damage(WEAPON_BLUNT, ARMOR_NONE, "blunt -> none");
	print_damage(WEAPON_BLUNT, ARMOR_LEATHER, "blunt -> leather");
	print_damage(WEAPON_BLUNT, ARMOR_CHAINMAIL, "blunt -> chainmail");
	print_damage(WEAPON_BLUNT, ARMOR_PLATE, "blunt -> plate");
	print_damage(WEAPON_BLUNT, ARMOR_WOODEN, "blunt -> wooden");
	print_damage(WEAPON_BLUNT, ARMOR_STONE, "blunt -> stone");

	return 0;
}
