/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <xcb/xcb.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "battle.h"
#include "movement.h"
#include "input.h"
#include "interface.h"
#include "image.h"
#include "display.h"
#include "ui.h"
#include "ui_battle.h"
#include "round.h"
#include "combat.h"

#if defined(VERBOSE)
# include "pathfinding.h"
#endif

#define S(s) (s), sizeof(s) - 1

#define BATTLEFIELD_MARGIN 9

#define MARGIN 4
#define FIELD_SIZE 30

#define ANIMATION_LENGTH 2

struct state_battle
{
	struct game *game;
	struct battle *battle;
	unsigned char player;

	size_t step;

	struct battlefield *field;
	struct pawn *pawn;
	struct position position;
};

enum {WidgetDefault, WidgetPanel, Battlefield, WidgetReady, WidgetReplay, Pawn, PawnInfo, Gate, WidgetPawns};

static const struct widget widgets_battle[] =
{
	[WidgetDefault] = WIDGET(1, 1, 0, 0, 1024, 768, 0),
	[WidgetPanel] = WIDGET(1, 1, PANEL_LEFT, PANEL_TOP, PANEL_WIDTH, PANEL_HEIGHT, 0),
	[Battlefield] = WIDGET(BATTLEFIELD_WIDTH, BATTLEFIELD_HEIGHT, 256 + BATTLEFIELD_MARGIN, BATTLEFIELD_MARGIN, FIELD_SIZE, FIELD_SIZE, 0),
	[WidgetReady] = WIDGET(1, 1, BUTTON_END_X, BUTTONS_Y, BUTTON_WIDTH, BUTTON_HEIGHT, 0),
	[WidgetReplay] = WIDGET(1, 1, BUTTON_END_X, TURN_Y - 30, BUTTON_WIDTH, BUTTON_HEIGHT, 0),
	[Pawn] = WIDGET(1, 1, 5, 60, TROOP_EDGE, TROOP_EDGE, 0),
	[PawnInfo] = WIDGET(2, 1, 8, 120, 240, 16, 8),
	[Gate] = WIDGET(1, 1, 5, 10, TROOP_EDGE, TROOP_EDGE, 0),
	[WidgetPawns] = WIDGET(6, 5, 12, 12, TROOP_EDGE, TROOP_EDGE + 16, 2),
};

static inline struct vector battlefield_position(struct position position, float radius)
{
	struct vector result;
	result.x = widgets_battle[Battlefield].left + (position.x - radius) * FIELD_SIZE - 0.5; // image is bigger than field
	result.y = widgets_battle[Battlefield].top + (position.y - radius) * FIELD_SIZE - 0.5; // image is bigger than field
	return result;
}

static void show_field(size_t x, size_t y, enum blockage blockage, unsigned char blockage_location, unsigned char open)
{
	const struct image *image;
	struct vector position;

	switch (blockage)
	{
	case BLOCKAGE_WALL:
		switch (blockage_location & POSITION)
		{
		case POSITION_LEFT | POSITION_RIGHT:
			image = &image_garrison_wall_lr;
			break;

		case POSITION_TOP | POSITION_BOTTOM:
			image = &image_garrison_wall_tb;
			break;

		case POSITION_TOP | POSITION_LEFT:
			image = &image_garrison_wall_tl;
			break;

		case POSITION_TOP | POSITION_RIGHT:
			image = &image_garrison_wall_tr;
			break;
		}
		break;

	case BLOCKAGE_GATE:
		switch (blockage_location & SIDE)
		{
		case SIDE_LEFT:
			image = open ? &image_garrison_gate_open_l : &image_garrison_gate_closed_l;
			break;

		case SIDE_RIGHT:
			image = open ? &image_garrison_gate_open_r : &image_garrison_gate_closed_r;
			break;

		case SIDE_BOTTOM:
			image = open ? &image_garrison_gate_open_b : &image_garrison_gate_closed_b;
			break;
		}
		break;

	case BLOCKAGE_TOWER:
		switch (blockage_location & SIDE)
		{
		case SIDE_LEFT:
			image = &image_garrison_tower_l;
			break;

		case SIDE_RIGHT:
			image = &image_garrison_tower_r;
			break;

		case SIDE_BOTTOM:
			image = &image_garrison_tower_b;
			break;
		}
		break;

	default:
		return;
	}

	position = battlefield_position((struct position){x, y}, 0);
	fill_image(image, position.x, position.y, image->width, image->height, 0);
}

static void show_victims(const struct battle *restrict battle, size_t pawn_index, enum victim_index victim_index, size_t step)
{
	const struct pawn *restrict pawn = battle->pawns + pawn_index;
	const struct pawn_animation *restrict animation = battle->animation.pawns + pawn_index;

	struct vector position;
	char buffer[FORMAT_SIZE_BASE10(animation->victims[victim_index])], *end;

	if (!animation->visible || !animation->victims[victim_index])
		return;

	assert(image_fight.width == image_fight.height);
	if (victim_index == VICTIMS_RANGED)
		position = battlefield_position(animation->position[step], (image_fight.width / 2.0) / FIELD_SIZE);
	else
		position = battlefield_position(pawn->position, (image_fight.width / 2.0) / FIELD_SIZE);
	fill_image(&image_fight, position.x, position.y, image_fight.width, image_fight.height, 0);
	end = format_uint(buffer, animation->victims[victim_index], 10);
	display_string_center(slice(buffer, end - buffer), position, (struct box){image_fight.width, image_fight.height}, &font10, display_colors[TextForeground]);
}

static void show_animation(const void *argument, double progress)
{
	const struct state_battle *state = argument;
	struct battle *restrict battle = state->battle;
	size_t step;

	size_t x, y;
	size_t i;

	// background
	fill_rectangle(widgets_battle[WidgetPanel].left, widgets_battle[WidgetPanel].top, widgets_battle[WidgetPanel].width, widgets_battle[WidgetPanel].height, display_colors[Player + PLAYER_NEUTRAL]);
	fill_image(&image_panel, CONTROLS_X, CONTROLS_Y, CONTROLS_WIDTH, CONTROLS_HEIGHT, 1);

	// background
	fill_image(&image_battlefield, widgets_battle[Battlefield].left, widgets_battle[Battlefield].top, widgets_battle[Battlefield].span_x, widgets_battle[Battlefield].span_y, 1);

	// obstacles
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			const struct battlefield_animation *field = &battle->animation.field[y][x];
			show_field(x, y, field->blockage, field->blockage_location, field->open);
		}

	step = state->step;
	if (step < state->battle->animation.steps_damage[VICTIMS_RANGED])
		step = 0;
	else
		step -= state->battle->animation.steps_damage[VICTIMS_RANGED];

	// Display pawns.
	for(i = 0; i < battle->pawns_count; ++i)
	{
		const struct pawn *restrict pawn = battle->pawns + i;
		struct vector position;

		if (!battle->animation.pawns[i].visible)
			continue;

		if (step >= state->battle->animation.steps)
			position = battlefield_position(pawn->position, PAWN_RADIUS);
		else
			position = battlefield_position(battle->animation.pawns[i].position[step], PAWN_RADIUS);
		display_troop(pawn->troop->unit, 0, Player + pawn->owner, position.x, position.y, Text);
	}

	if (state->step < state->battle->animation.steps_damage[VICTIMS_RANGED])
	{
		double progress = ((double)state->step / state->battle->animation.steps_damage[VICTIMS_RANGED]);

		// Show shooting animation.
		for(i = 0; i < battle->pawns_count; ++i)
		{
			struct position start = battle->animation.pawns[i].position[0];
			struct position end = battle->animation.pawns[i].shoot;
			struct vector position;
			double dx, dy;
			struct image *image;

			if (!battle->animation.pawns[i].visible || isnan(end.x) || isnan(end.y))
				continue;

			dx = fabs(end.x - start.x);
			dy = fabs(end.y - start.y);
			if (dx >= dy)
			{
				if (end.x > start.x)
					image = &image_shoot_r;
				else
					image = &image_shoot_l;
			}
			else if (end.y > start.y)
				image = &image_shoot_d;
			else
				image = &image_shoot_u;

			assert(image->width == image->height);
			position = battlefield_position((struct position){start.x * (1 - progress) + end.x * progress, start.y * (1 - progress) + end.y * progress}, (image->width / 2.0) / FIELD_SIZE);
			fill_image(image, position.x, position.y, image->width, image->height, 0);
		}
	}
	else
	{
		// Show shooting victims during pawn movement and fighting victims after that.
		for(i = 0; i < battle->pawns_count; ++i)
			show_victims(battle, i, (step < state->battle->animation.steps) ? VICTIMS_RANGED : VICTIMS_MELEE, step);
	}
}

static void show_battlefield(const struct game *restrict game, const struct battle *restrict battle, unsigned player)
{
	size_t x, y;
	size_t i;

	// background
	//fill_image(&image_battlefield, widgets_battle[Battlefield].left - BATTLEFIELD_MARGIN, widgets_battle[Battlefield].top - BATTLEFIELD_MARGIN, widgets_battle[Battlefield].span_x + 2 * BATTLEFIELD_MARGIN, widgets_battle[Battlefield].span_y + 2 * BATTLEFIELD_MARGIN, 1);
	fill_image(&image_battlefield, widgets_battle[Battlefield].left, widgets_battle[Battlefield].top, widgets_battle[Battlefield].span_x, widgets_battle[Battlefield].span_y, 1);

	// obstacles
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			const struct battlefield *field = &battle->field[y][x];
			show_field(x, y, field->blockage, field->blockage_location, field->open);
		}

	// Display pawns.
	for(i = 0; i < battle->pawns_count; ++i)
	{
		struct vector position;

		const struct pawn *restrict pawn = battle->pawns + i;
		if (!pawn->count || pawn_waiting(pawn))
			continue;

		position = battlefield_position(pawn->position, PAWN_RADIUS);

		/* TODO support allied display mode
		if (pawn->owner == player)
			color = Self;
		else if (allies(game, pawn->owner, player))
			color = Ally;
		else
			color = Enemy;
		*/

		display_troop(pawn->troop->unit, pawn->count, Player + pawn->owner, position.x, position.y, Text);
	}

#if defined(VERBOSE)
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			struct vector position = battlefield_position((struct position){x, y}, 0);
			char buffer[256], *end;
			draw_rectangle(position.x, position.y, FIELD_SIZE, FIELD_SIZE, display_colors[Text]);
			end = format_int(format_byte_one(format_int(buffer, x, 10), ','), y, 10);
			draw_string(slice(buffer, end - buffer), position.x, position.y, &font10, display_colors[Text]);
		}

	for(i = 0; i < battle->obstacles->count; i += 1)
	{
		struct obstacle *o = battle->obstacles->obstacle + i;
		struct vector p = battlefield_position((struct position){o->left, o->top}, 0);
		draw_rectangle(p.x, p.y, (o->right - o->left) * FIELD_SIZE, (o->bottom - o->top) * FIELD_SIZE, display_colors[Enemy]);
	}

	for(i = 0; i < battle->graph[player]->count; i += 1)
	{
		struct vector p = battlefield_position((struct position){battle->graph[player]->list[i].x, battle->graph[player]->list[i].y}, 0);
		fill_rectangle(p.x - 2, p.y - 2, 4, 4, display_colors[Enemy]);
	}
#endif

#if defined(AI_DEBUG)
    for(y = 0; y < sizeof(battle->rating.distance) / sizeof(*battle->rating.distance); y += 1)
    {
        for(x = 0; x < sizeof(*battle->rating.distance) / sizeof(**battle->rating.distance); x += 1)
        {
			struct position p = {0.5 * x + 0.5, 0.5 * y + 0.5};
			struct vector position = battlefield_position(p, 0);
			char buffer[256];
			size_t l = sprintf(buffer, "%u", (unsigned)(reachable_get(&battle->rating, p.x, p.y) * 1000) % 100);
			draw_string(slice(buffer, l), position.x, position.y, &font7, display_colors[Text]);
		}
	}
#endif
}

// TODO fix this - currently only first pawn move is shown
static void show_pawn_action(const struct pawn *restrict pawn)
{
	size_t i;

	// Show pawn movement target.
	struct position position = pawn->position, next;
	for(i = 0; i < pawn->path.count; ++i)
	{
		struct vector from, to;

		next = pawn->path.data[i];

		from = battlefield_position(position, 0);
		to = battlefield_position(next, 0);
		draw_line(from.x, from.y, to.x, to.y, display_colors[Progress]);

		position = next;
	}

	switch (pawn->command.action)
	{
		struct vector position;
	case ACTION_GUARD:
		position = battlefield_position(pawn->command.target.position, PAWN_RADIUS);
		fill_image(&image_pawn_guard, position.x, position.y, image_pawn_guard.width, image_pawn_guard.height, 0);
		break;

	case ACTION_SHOOT:
		position = battlefield_position(pawn->command.target.position, PAWN_RADIUS);
		fill_image(&image_pawn_shoot, position.x, position.y, image_pawn_shoot.width, image_pawn_shoot.height, 0);
		break;

	case ACTION_FIGHT:
		position = battlefield_position(pawn->command.target.pawn->position, PAWN_RADIUS);
		fill_image(&image_pawn_fight, position.x, position.y, image_pawn_fight.width, image_pawn_fight.height, 0);
		break;

	case ACTION_ASSAULT:
		position = battlefield_position((struct position){pawn->command.target.field->tile.x + 0.5, pawn->command.target.field->tile.y + 0.5}, PAWN_RADIUS);
		fill_image(&image_pawn_assault, position.x, position.y, image_pawn_assault.width, image_pawn_assault.height, 0);
		break;
	}
}

static void show_battle(const void *argument, double progress)
{
	const struct state_battle *state = argument;
	const struct game *restrict game = state->game;
	struct battle *restrict battle = state->battle;
	unsigned player = state->player;

	char buffer[256], *end;

	// background
	fill_rectangle(widgets_battle[WidgetPanel].left, widgets_battle[WidgetPanel].top, widgets_battle[WidgetPanel].width, widgets_battle[WidgetPanel].height, display_colors[Player + player]);
	fill_image(&image_panel, CONTROLS_X, CONTROLS_Y, CONTROLS_WIDTH, CONTROLS_HEIGHT, 1);

    end = format_int(format_bytes(buffer, S("Round ")), battle->round, 10);
    draw_string(slice(buffer, end - buffer), TURN_X, TURN_Y, &font12, display_colors[Text]);

	if (battle->round)
		display_button(slice("Replay"), widgets_battle[WidgetReplay].left, widgets_battle[WidgetReplay].top, 0);

	// TODO force quit game
    display_button(slice("End Round"), widgets_battle[WidgetReady].left, widgets_battle[WidgetReady].top, 0);

	show_battlefield(game, battle, player);

	if (state->pawn)
	{
		struct vector position = object_position(&widgets_battle[Pawn], 0);
		const struct unit *unit = state->pawn->troop->unit;
		display_troop(unit, state->pawn->count, Player + state->pawn->owner, position.x, position.y, Text);
		draw_string(slice(unit->name, unit->name_size), widgets_battle[Pawn].right + MARGIN, widgets_battle[Pawn].top, &font12, display_colors[Text]);

		end = format_int(format_bytes(buffer, S("speed: ")), unit->speed, 10);
		position = object_position(&widgets_battle[PawnInfo], 0);
		draw_string(slice(buffer, end - buffer), position.x, position.y, &font12, display_colors[Text]);

		end = format_int(format_bytes(buffer, S("health: ")), unit->health, 10);
		position = object_position(&widgets_battle[PawnInfo], 1);
		draw_string(slice(buffer, end - buffer), position.x, position.y, &font12, display_colors[Text]);

		/*end = format_bytes(format_bytes(buffer, S("armor ")), unit->armor, 10);
		if (unit->ranged.weapon)
			; */

		if (state->pawn->owner == state->player)
			show_pawn_action(state->pawn);
	}

	if (state->field && state->field->blockage)
	{
		end = buffer;

		switch (state->field->blockage)
		{
		case BLOCKAGE_GATE:
			{
				struct image *image;
				if (state->field->owner == state->player)
					image = (state->field->open ^ state->field->gate_switch) ? &image_gate_open : &image_gate_closed;
				else
					image = state->field->open ? &image_gate_open_gray : &image_gate_closed_gray;
				fill_image(image, widgets_battle[Gate].left, widgets_battle[Gate].top, image->width, image->height, 0);

				end = format_bytes(end, S("gate"));
			}
			break;

		case BLOCKAGE_TOWER:
			//end = format_bytes(end, S("tower"));
			//break;
		case BLOCKAGE_WALL:
			end = format_bytes(end, S("wall"));
			break;
		}

		end = format_bytes(end, S(" strength: "));
		end = format_uint(end, state->field->strength, 10);

		// TODO better localization of text
		draw_string(slice(buffer, end - buffer), widgets_battle[Gate].right, widgets_battle[Gate].top, &font12, display_colors[Text]);
	}
}

static void show_formation(const void *argument, double progress)
{
	const struct state_battle *state = argument;
	const struct game *restrict game = state->game;
	struct battle *restrict battle = state->battle;
	unsigned player = state->player;

	char buffer[256], *end;

	size_t i;
	size_t index;

	// background
	fill_rectangle(widgets_battle[WidgetPanel].left, widgets_battle[WidgetPanel].top, widgets_battle[WidgetPanel].width, widgets_battle[WidgetPanel].height, display_colors[Player + player]);
	fill_image(&image_panel, CONTROLS_X, CONTROLS_Y, CONTROLS_WIDTH, CONTROLS_HEIGHT, 1);

    end = format_int(format_bytes(buffer, S("Round ")), battle->round, 10);
    draw_string(slice(buffer, end - buffer), TURN_X, TURN_Y, &font12, display_colors[Text]);

	if (battle->round)
		display_button(slice("Replay"), widgets_battle[WidgetReplay].left, widgets_battle[WidgetReplay].top, 0);

	// TODO force quit game
    display_button(slice("Continue"), BUTTON_END_X, BUTTONS_Y, 0);

	show_battlefield(game, battle, player);

	index = 0;
	for(i = 0; i < battle->players[player].pawns_count; i += 1)
	{
		const struct pawn *pawn = battle->players[player].pawns[i];

		if (pawn_waiting(pawn))
		{
			struct vector position = object_position(&widgets_battle[WidgetPawns], index);
			display_troop(pawn->troop->unit, pawn->count, Player + player, position.x, position.y, Text);
			index += 1;
			if (index == widgets_battle[WidgetPawns].count)
				break;
		}
	}
}

// Issue a pawn command corresponding to a given target.
// On success, returns 0. On error no changes are made and negative error code is returned.
static int pawn_command(const struct game *restrict game, struct battle *restrict battle, struct pawn *restrict pawn, struct position target, double reachable[BATTLEFIELD_HEIGHT][BATTLEFIELD_WIDTH])
{
	struct battlefield *target_field = &battle->field[(size_t)target.y][(size_t)target.x];
	struct pawn *target_pawn = pawn_find(target_field, target, PAWN_RADIUS);

	// TODO tower support
	// TODO make sure the target position is reachable

	// If there is a pawn at the target position, use the pawn as a target.
	if (target_pawn)
	{
		if (allies(game, pawn->owner, target_pawn->owner))
			return pawn_move_queue(pawn, target, battle->graph[pawn->owner], battle->obstacles);
		else if (!pawn->command.moves.count && combat_shoot(game, battle, pawn, target_pawn->position, 0))
			return 0;
		else if (combat_fight(game, battle, pawn, target_pawn, 0))
			return 0;
		else
			return ERROR_MISSING; // no command can be executed
	}
	else if (((target_field->blockage == BLOCKAGE_WALL) || (target_field->blockage == BLOCKAGE_GATE) || (target_field->blockage == BLOCKAGE_TOWER)) && !allies(game, pawn->owner, target_field->owner) && combat_assault(game, battle, pawn, target_field, 0))
		return 0;
	else
		return pawn_move_queue(pawn, target, battle->graph[pawn->owner], battle->obstacles);
}

static int input_default(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	return INPUT_IGNORE;
}

static int input_battlefield(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_battle *state = argument;
	const struct game *restrict game = state->game;
	struct battle *restrict battle = state->battle;

	struct battlefield *field;
	struct position position;

	switch (code)
	{
	case ' ':
		return INPUT_NOTME;

	default:
		return INPUT_IGNORE;

	case EVENT_MOUSE_LEFT:
	case EVENT_MOUSE_RIGHT:
		break;
	}

	position = (struct position){(double)x / FIELD_SIZE, (double)y / FIELD_SIZE};
	field = &battle->field[(int)position.y][(int)position.x];

	if (code == EVENT_MOUSE_LEFT)
	{
		state->field = field;
		state->pawn = pawn_find(field, position, PAWN_RADIUS);
		return INPUT_DONE;
	}

	assert(code == EVENT_MOUSE_RIGHT);

	if (!state->pawn || (state->pawn->owner != state->player))
		return INPUT_IGNORE;

	// TODO maybe update distances for reachable locations for the current pawn (to reflect path)

	// if CONTROL is pressed, shoot
	// if SHIFT is pressed, don't overwrite the current command
	if (modifiers & XCB_MOD_MASK_CONTROL)
	{
		if (combat_shoot(game, battle, state->pawn, position, 0))
			return INPUT_DONE;
		return INPUT_IGNORE;
	}
	else
	{
		int status;

		if (!(modifiers & XCB_MOD_MASK_SHIFT))
			pawn_stay(state->pawn);
		state->pawn->command.action = 0;

		switch (status = pawn_command(game, battle, state->pawn, position, 0))
		{
		case 0:
			return INPUT_DONE;

		case ERROR_INPUT:
		case ERROR_MISSING:
			// TODO restore pawn path and action if pawn_command fails
			return INPUT_IGNORE;

		default:
			return status;
		}
	}
}

static int input_gate(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_battle *state = argument;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	if (!state->field || (state->field->blockage != BLOCKAGE_GATE) || (state->field->owner != state->player))
		return INPUT_NOTME;

	state->field->gate_switch ^= 1;
	return INPUT_DONE;
}

static int input_pawn(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_battle *state = argument;
	struct battle *restrict battle = state->battle;
	unsigned player = state->player;

	size_t index, current;
	size_t i;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	index = object_index(&widgets_battle[WidgetPawns], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE;

	current = 0;
	for(i = 0; i < battle->players[player].pawns_count; i += 1)
	{
		struct pawn *pawn = battle->players[player].pawns[i];

		if (pawn_waiting(pawn))
		{
			if (index == current)
			{
				pawn_place(pawn, state->position);
				round_prepare_pawn(battle, pawn);
				battle->players[player].pawns_waiting -= 1;
				return INPUT_TERMINATE;
			}

			current += 1;
		}
	}

	return INPUT_IGNORE;
}

static int timer_animation(void *argument, double progress)
{
	struct state_battle *state = argument;
	size_t step = (progress / ANIMATION_LENGTH) * BATTLE_MOVEMENT_STEPS + 0.5;
	unsigned steps = state->battle->animation.steps_damage[VICTIMS_RANGED] + state->battle->animation.steps + state->battle->animation.steps_damage[VICTIMS_MELEE];

	if (step >= steps)
		return INPUT_TERMINATE;
	if (step > state->step)
	{
		state->step = step;
		return INPUT_DONE;
	}
	return INPUT_IGNORE;
}

static int input_none(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	if (code == ' ')
		return INPUT_TERMINATE;
	return INPUT_IGNORE;
}

static int input_replay(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_battle *state = argument;
	struct battle *battle = state->battle;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	if (!battle->round)
		return INPUT_IGNORE;

	state->step = 0;
	return input_process(0, 0, show_animation, &timer_animation, state);
}

int input_battle(const struct game *restrict game, struct battle *restrict battle, unsigned char player)
{
	const struct area areas[] =
	{
		{
			.left = 0,
			.right = WINDOW_WIDTH - 1,
			.top = 0,
			.bottom = WINDOW_HEIGHT - 1,
			.callback = input_none
		},
		WIDGET_AREA(widgets_battle[WidgetReady], &input_mouse_terminate),
		WIDGET_AREA(widgets_battle[WidgetReplay], &input_replay),
		WIDGET_AREA(widgets_battle[Battlefield], &input_battlefield),
		WIDGET_AREA(widgets_battle[Gate], &input_gate),
	};

	struct state_battle state;

	state.game = (struct game *)game; // TODO fix cast
	state.battle = battle;
	state.player = player;

	state.step = 0;

	state.field = 0;
	state.pawn = 0;

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_battle, 0, &state);
}

int input_formation(const struct game *restrict game, struct battle *restrict battle, unsigned char player)
{
	const struct area areas[] =
	{
		WIDGET_AREA(widgets_battle[WidgetPawns], &input_pawn),
		WIDGET_AREA(widgets_battle[WidgetReady], &input_mouse_terminate),
		WIDGET_AREA(widgets_battle[WidgetReplay], &input_replay),
	};

	struct state_battle state;

	// Determine position where the pawn will be placed.
	// Do nothing if no position is available.
	state.position = pawn_position_formation(battle, player);
	if (isnan(state.position.x) || isnan(state.position.y))
		return 0;

	state.game = (struct game *)game; // TODO fix cast
	state.battle = battle;
	state.player = player;

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_formation, 0, &state);
}

int input_animation(const struct game *restrict game, struct battle *restrict battle)
{
	struct state_battle state;

	state.game = (struct game *)game; // TODO fix cast
	state.battle = battle;

	state.step = 0;

	return input_process(0, 0, show_animation, &timer_animation, &state);
}
