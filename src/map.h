/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define REGIONS_LIMIT 4096

#define GARRISON_TROOPS_LIMIT 6

#define TRAIN_QUEUE 4

struct troop
{
	const struct unit *unit;
	unsigned count;

	// WARNING: Player-specific input variables.
	enum {TASK_NONE, TASK_BUILD, TASK_DISMISS} task;
};

struct vessel
{
	char name[NAME_LIMIT];
	size_t name_size;

	size_t capacity;
	unsigned speed;

	unsigned char autonomous; // whether the vessel can exist without troops TODO: i'm currently using this to determine whether it runs on water

	unsigned workers;

	uint64_t requires;
	struct resources cost, expense; // cost - one-time payment for ordering; expense - daily resources change
	unsigned time;

	// TODO more ship info - food_supply, battle characteristics
};

struct building
{
	char name[NAME_LIMIT];
	size_t name_size;

	const char *description;

	uint64_t requires;
	uint64_t environment;
	struct resources cost, expense; // cost - one-time payment for building; expense - daily resources change
	unsigned time;

	struct
	{
		double gold, food, wood, stone;
	} income;
	struct resources production;
	unsigned workers;
	unsigned food_storage;

	unsigned sight;
	unsigned char port;
	enum {GARRISON_NONE, GARRISON_PALISADE = 1, GARRISON_FORTRESS, /* must be last */ GARRISONS_COUNT} garrison;
};

enum {VesselNone, VesselGround, VesselWater, /* must be last */ VESSELS_COUNT};

extern const struct vessel VESSELS[];

enum {BuildingTownHall, BuildingIrrigation, BuildingGranary, BuildingBarracks, BuildingArcheryRange, BuildingStables, BuildingForge, BuildingWorkshop, BuildingPort, BuildingPalisade, BuildingFortress, BuildingSawmill, BuildingQuarry, BuildingWatchTower, BuildingShipyard, /* must be last */ BUILDINGS_COUNT};

extern const struct building BUILDINGS[];

struct region // TODO rename to settlement
{
	char name[NAME_LIMIT];
	size_t name_size;

	// TODO player-specicific
	struct
	{
		struct hexagon *hexagon;
		struct troops private;
		// TODO implement this: const struct garrison_info *restrict info;
	} garrison;

	// Automatically initialized at each turn.
	unsigned char sieged;

	unsigned population;
	double happiness;

	// TODO add fertility

	unsigned train_progress;

	// WARNING: Player-specific input variables.
	// TODO should these be volatile?
	const struct unit *train[TRAIN_QUEUE];
	unsigned tax; // in the interval [0, 8]
};

// Used as flags by applying left shift.
enum {ENVIRONMENT_SETTLEMENT = 1, ENVIRONMENT_FOREST, ENVIRONMENT_QUARRY, ENVIRONMENT_COAST}; // TODO water, horses, roads

struct hexagon
{
	enum terrain {TERRAIN_MOUNTAIN, TERRAIN_GROUND, TERRAIN_WATER} terrain;
	struct
	{
		size_t region_index;
		//enum {ROAD_E = 0x1, ROAD_NE = 0x2, ROAD_NW = 0x4, ROAD_W = 0x8, ROAD_SW = 0x10, ROAD_SE = 0x20} roads;
	} info;

	uint64_t environment;

	uint64_t buildings;
	double build_progress;
	double vessel_progress;
	unsigned char owner; // applicable only when there are buildings

	// WARNING: Player-specific input variables.
	// TODO should these be volatile?
	int build_index; // -1 == no building under construction
	int vessel_index; // -1 == no vessel under construction
	struct array_troop_stacks private;

	// Automatically initialized at each turn.
	struct array_troop_stacks public;
	struct army
	{
		double field, garrison;
		unsigned char owner;
	} armies;
	size_t garrison;
	unsigned sight;
	unsigned char port;
	// TODO cache more building stats (half of struct buildings is relevant here)

	// state for turn transition
	unsigned char moving_owner;
	uint32_t occupants_players, moving_players;
	size_t moving_count;
};

enum direction {East, NorthEast, NorthWest, West, SouthWest, SouthEast, /* must be last */ DIRECTIONS_COUNT};

struct hexagon_neighbor
{
	struct hexagon *hexagon;
	enum direction direction;
};

struct tile
{
	int x, y; // use signed int to ensure subtraction doesn't wrap around.
};

struct tile hexagon_location(const struct game *restrict game, size_t index);
size_t hexagon_neighbors(const struct game *restrict game, const struct hexagon *restrict hexagon, struct hexagon_neighbor neighbors[static DIRECTIONS_COUNT]);
enum direction hexagon_neighbor_direction(const struct game *restrict game, int from, int to);
unsigned hexagon_distance_locations(struct tile al, struct tile bl);
unsigned hexagon_distance(const struct game *restrict game, int from, int to);
int hexagon_is_neighbor(const struct game *restrict game, const struct hexagon *restrict hexagon, const struct hexagon *restrict neighbor);

double heuristic_map(float x0, float y0, float x1, float y1);

static inline size_t hexagon_index(const struct game *restrict game, unsigned x, unsigned y)
{
	return game->map_width * y - y / 2 + x;
}

static inline unsigned hexagons_width(const struct game *restrict game, unsigned row)
{
	return game->map_width - (row % 2);
}

static inline size_t hexagons_count(const struct game *restrict game)
{
	return game->map_width * game->map_height - game->map_height / 2;
}

struct adjacency_list *hexagons_graph_build(struct game *restrict game);
void hexagons_graph_free(struct adjacency_list *restrict graph);

const struct unit *unit_find(struct slice name);
int building_find(struct slice name);

int troop_spawn(struct troops *restrict troops, const struct unit *restrict unit, unsigned count);

void troop_move(struct troops *restrict source, struct troops *restrict target, size_t index);

static inline void hexagon_build(struct hexagon *restrict hexagon, unsigned index)
{
	hexagon->buildings |= (1 << index);
}

static inline int hexagon_built(const struct hexagon *restrict hexagon, unsigned index)
{
	return hexagon->buildings & (1 << index);
}

struct array_troop_stacks *troop_stacks(struct hexagon *restrict hexagon, unsigned player);

struct garrison_info
{
	unsigned capacity; // number of troops the garrison can accomodate
	struct unit wall, tower, gate;
};

static inline const struct garrison_info *garrison_info(const struct hexagon *hexagon)
{
	static const struct garrison_info info[] =
	{
		[GARRISON_PALISADE] = {
			.capacity = 4,
			.wall = {.health = 160, .armor = ARMOR_WOODEN},
			.tower = {.health = 200, .armor = ARMOR_WOODEN},
			.gate = {.health = 80, .armor = ARMOR_WOODEN},
		},
		[GARRISON_FORTRESS] = {
			.capacity = 6,
			.wall = {.health = 200, .armor = ARMOR_STONE},
			.tower = {.health = 250, .armor = ARMOR_STONE},
			.gate = {.health = 120, .armor = ARMOR_WOODEN},
		},
	};

	return info + hexagon->garrison;
}

int map_movement_queue(const struct game *restrict game, struct troop_stack *restrict troop_stack, size_t to, unsigned char *restrict visible);
unsigned char *map_visible_alloc(struct game *restrict game, unsigned player);
