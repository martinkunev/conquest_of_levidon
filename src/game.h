/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define SQRT3 1.73205080756887729353 /* sqrt(3) */

#define NAME_LIMIT 32

#define PLAYERS_LIMIT 24 /* must be even for proper visualization in ui_menu */

// WARNING: Player 0 and alliance 0 are hard-coded as neutral.
#define PLAYER_NEUTRAL 0

#define UNIT_SPEED_LIMIT 8
#define TERRAIN_BONUS_LIMIT 2

// WARNING: Troop stack speed (hexagons per turn) must not exceed this.
#define MOVEMENT_STEPS (UNIT_SPEED_LIMIT * TERRAIN_BONUS_LIMIT)

// TODO rename this?
#define TROOPS_CAPACITY_MAX 12

struct region;
struct hexagon;
struct troop;
struct vessel;
struct adjacency_list;

struct resources
{
	int gold;
	int food;
	int wood;
	int stone;
};

struct player
{
	// WARNING: Player-specific input variables.
	struct resources treasury;

	enum {Neutral, Local, Computer, Remote} type; // TODO support remote
	unsigned char alliance;

	char name[NAME_LIMIT];
	size_t name_size;

	// synchronization for player actions
	struct
	{
		int in, out;
	} control;
	struct player_info
	{
		int in, out;
	} info;

	// initialized at each turn
	struct resources income;
	unsigned food_storage;
	unsigned population;
};

struct troops
{
	struct troop *troops[TROOPS_CAPACITY_MAX];
	size_t count;
};

struct troop_stack
{
	struct troops public;

	// WARNING: Player-specific input variables.
	struct troops private;
	struct hexagon *movements[1 + MOVEMENT_STEPS]; // movements[0] is not changed during player input
	size_t movements_count;

	unsigned char owner;

	// initialized at each turn
	unsigned char stamina; // minimal speed among the troops

	// state for turn transition
	unsigned char is_public; // whether the troop stack is available in the public list for the hexagon
	size_t index; // position in game->troop_stacks
	enum battle_type {BATTLE_NONE, BATTLE_CAPTURE, BATTLE_OPEN, BATTLE_ASSAULT} fighting;
	double movement_progress;
	size_t movement_position;

	const struct vessel *vessel;
};

#define ARRAY_GLOBAL
#define ARRAY_NAME array_troop_stacks
#define ARRAY_TYPE struct troop_stack *
#include "generic/array.g"

struct game
{
	struct player players[PLAYERS_LIMIT];
	size_t players_count;

	struct region *regions;
	size_t regions_count;

	struct hexagon *hexagons;
	unsigned map_width, map_height;
	size_t hexagons_total;
	struct adjacency_list *hexagons_graph;

	unsigned turn;

	// initialized at each turn
	struct hexagon *alive[PLAYERS_LIMIT];
	size_t players_local[PLAYERS_LIMIT];
	size_t players_local_count;

	// state for turn transition
	struct array_troop_stacks troop_stacks;

	pthread_mutex_t mutex_input; // for accessing input_ready while handling individual players
	uint32_t input_ready, input_all;
};

static inline int allies(const struct game *restrict game, unsigned player0, unsigned player1)
{
	return (game->players[player0].alliance == game->players[player1].alliance);
}

enum {UnitPeasant, UnitWorker, UnitMilitia, UnitPikeman, UnitArcher, UnitLongbow, UnitLightCavalry, UnitBatteringRam, /* must be last */ UNITS_COUNT};

enum weapon {WEAPON_NONE, WEAPON_CLUB, WEAPON_ARROW, WEAPON_CLEAVING, WEAPON_POLEARM, WEAPON_BLADE, WEAPON_BLUNT, /* must be last */ WEAPONS_COUNT};
enum armor {ARMOR_NONE, ARMOR_LEATHER, ARMOR_CHAINMAIL, ARMOR_PLATE, ARMOR_WOODEN, ARMOR_STONE, /* must be last */ ARMORS_COUNT};

struct unit
{
	char name[NAME_LIMIT];
	size_t name_size;

	uint64_t requires;
	struct resources cost, expense; // cost - one-time payment for training; expense - daily resources change
	unsigned char time;
	unsigned char troops_count;

	unsigned char worker; // whether the unit can build and mine
	unsigned char speed;

	unsigned char health;
	enum armor armor;

	struct
	{
		enum weapon weapon;
		double damage;
		double agility;
	} melee;
	struct
	{
		enum weapon weapon;
		double damage;
		unsigned char range;
		int shoot_moving; // whether the unit can move on the same round as shooting TODO adapt ui_battle to handle this; make a unit with this set to true
	} ranged;
};

extern const struct unit UNITS[UNITS_COUNT];

extern unsigned MAP_WIDTH, MAP_HEIGHT;
