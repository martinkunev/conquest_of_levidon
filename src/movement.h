/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define WALL_THICKNESS 0.5
#define WALL_OFFSET ((1 - WALL_THICKNESS) / 2) /* walls are placed in the middle of the field */

#define PAWN_RADIUS 0.5

struct pawn;
struct battle;

struct obstacles
{
	size_t count;
	struct obstacle
	{
		float left, right, top, bottom;
	} obstacle[];
};

#if !defined(AI_DEBUG)
struct reachable
{
	double distance[BATTLEFIELD_HEIGHT * 2 - 1][BATTLEFIELD_WIDTH * 2 - 1];
};
#endif

static inline int position_eq(struct position a, struct position b)
{
	return ((a.x == b.x) && (a.y == b.y));
}

static inline int in_battlefield(double x, double y)
{
    return ((x - PAWN_RADIUS >= 0) && (x + PAWN_RADIUS <= BATTLEFIELD_WIDTH) && (y - PAWN_RADIUS >= 0) && (y + PAWN_RADIUS <= BATTLEFIELD_HEIGHT));
}

int move_blocked_pawn(struct position start, struct position end, struct position pawn, double radius);

int path_visible(struct position origin, struct position target, const struct obstacles *restrict obstacles);

unsigned path_moves_tangent(const struct pawn *restrict pawn, const struct pawn *restrict obstacle, double distance_covered, struct position moves[static restrict 2]);

void pawn_place(struct pawn *restrict pawn, struct position position);
void pawn_stay(struct pawn *restrict pawn);
int pawn_move_queue(struct pawn *restrict pawn, struct position target, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles);

struct adjacency_list *visibility_graph_build(const struct battle *restrict battle, const struct obstacles *restrict obstacles, unsigned vertices_reserved);
int path_distances(const struct pawn *restrict pawn, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles, struct reachable *restrict reachable);

static inline double reachable_get(const struct reachable *restrict reachable, double x, double y)
{
	return reachable->distance[(size_t)(2 * y - 0.5)][(size_t)(2 * x - 0.5)];
}

int movement_plan(const struct game *restrict game, struct battle *restrict battle);
int movement_collisions_resolve(const struct game *restrict game, struct battle *restrict battle, unsigned step);
