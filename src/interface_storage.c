/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glext.h>

#include "basic.h"
#include "game.h"
#include "map.h"
#include "interface.h"
#include "interface_storage.h"
#include "display.h"
#include "ui.h"

static GLuint map_renderbuffer;
static GLuint map_framebuffer;

// TODO rename all of this from storage to something else

void storage_init(unsigned width, unsigned height)
{
	width = width / STORAGE_SCALE + 0.5;
	height = height / STORAGE_SCALE + 0.5;

	glGenRenderbuffers(1, &map_renderbuffer);
	glGenFramebuffers(1, &map_framebuffer);

	glBindRenderbuffer(GL_RENDERBUFFER, map_renderbuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, map_framebuffer);

	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, map_renderbuffer);
}

void storage_term(void)
{
	glDeleteRenderbuffers(1, &map_renderbuffer);
	glDeleteFramebuffers(1, &map_framebuffer);
}

static void if_switch_storage(unsigned width, unsigned height)
{
	width = width / STORAGE_SCALE + 0.5;
	height = height / STORAGE_SCALE + 0.5;

	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, width, 0, height, 0, 1);

	glBindFramebuffer(GL_FRAMEBUFFER, map_framebuffer);
}

static void if_switch_screen(void)
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
}

void storage_hexagons(const struct game *restrict game, unsigned width, unsigned height, unsigned edge)
{
	unsigned x, y;

	if_switch_storage(width, height);

	// Draw hexagons into the storage buffer.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for(y = 0; y < game->map_height; y += 1)
	{
		unsigned width = hexagons_width(game, y);
		for(x = 0; x < width; x += 1)
		{
			double offset_x = hexagon_offset_x(0, x, y, edge / STORAGE_SCALE);
			double offset_y = hexagon_offset_y(0, x, y, edge / STORAGE_SCALE);
			int index = hexagon_index(game, x, y);
			display_hexagon(offset_x, offset_y, STORAGE_SCALE, storage_color(index));
		}
	}
	glFlush();

	if_switch_screen();
}

// WARNING: This function is neither thread-safe nor reentrant.
unsigned char *storage_color(int index)
{
	static unsigned char color[4];

	if (index < 0)
	{
		 color[0] = 0;
		 color[1] = 0;
		 color[2] = 0;
	}
	else
	{
		 color[0] = 255;
		 color[1] = index / 256;
		 color[2] = index % 256;
	}
	color[3] = 255;

	return color;
}

int storage_get(unsigned x, unsigned y)
{
	GLubyte pixel[3];

	x = x / STORAGE_SCALE + 0.5;
	y = y / STORAGE_SCALE + 0.5;

	glBindFramebuffer(GL_READ_FRAMEBUFFER, map_framebuffer);
	glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

	return pixel[0] ? (pixel[1] * 256 + pixel[2]) : -1;
}
