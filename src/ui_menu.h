/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define FILENAME_LIMIT 64

void if_menu_load(const void *argument, double progress);
void if_menu_save(const void *argument, double progress);

struct state
{
	struct game *game;

	struct files *worlds;
	ssize_t world_index;

	size_t directory;

	char name[FILENAME_LIMIT];
	size_t name_size;
	size_t name_position;

	const char *error;
	size_t error_size;

	enum {LOADED_NEW = 1, LOADED_SAVED} loaded;
};

int input_menu_load(struct game *restrict game);
int input_menu_save(const struct game *restrict game);
