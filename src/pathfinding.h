/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

struct adjacency_list
{
	size_t count;
	struct adjacency
	{
		float x, y;
		struct neighbor
		{
			size_t index;
			double distance;
			struct neighbor *next;
		} *neighbors;
	} list[];
};

struct path_node
{
	double distance, heuristic;
	struct path_node *path_link;
	size_t heap_index;
};

struct path_node *path_traverse(struct adjacency_list *restrict graph, size_t vertex_origin, size_t vertex_target, unsigned char *restrict allowed, double (*distance_heuristic)(float x0, float y0, float x1, float y1));

void graph_term(struct adjacency_list *restrict graph);
