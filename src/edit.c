/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "bitmap.h"
#include "game.h"
#include "map.h"
#include "interface.h"
#include "display.h"
#include "ui.h"
#include "ui_editor.h"

void region_init(const struct game *restrict game, struct region *restrict region)
{
	static unsigned regions_created_count = 0;

	size_t i;

	// Set region number as a name.
	if (regions_created_count < game->regions_count)
		regions_created_count = game->regions_count;
	region->name_size = format_uint(region->name, regions_created_count, 10) - (uint8_t *)region->name;
	regions_created_count += 1;

	region->garrison.hexagon = 0;
	region->garrison.private.count = 0;

	region->population = 2000;
	region->happiness = 0.9;

	for(i = 0; i < TRAIN_QUEUE; i += 1)
		region->train[i] = 0;

	region->train_progress = 0;

	/*for(i = 0; i < NEIGHBORS_LIMIT; ++i)
		region->neighbors[i] = 0;*/
}

// Returns whether the map is consistent (contains everything necessary to be a valid map).
int cleanup(struct game *restrict game)
{
	unsigned char region_exist[BITMAP_SIZE(REGIONS_LIMIT)] = {0};
	size_t hexagons_total = hexagons_count(game);
	int consistent = 1;
	size_t i;

	for(i = 0; i < hexagons_total; i += 1)
	{
		const struct hexagon *restrict hexagon = game->hexagons + i;
		if (hexagon->terrain == TERRAIN_GROUND)
			bitmap_set(region_exist, hexagon->info.region_index, 1);
	}

	// Check for inconsistencies in each region.
	for(i = 0; i < game->regions_count; i += 1)
	{
		struct region *restrict region = game->regions + i;
		//const struct hexagon *restrict hexagon;

		// Remove regions without hexagons.
		while (!bitmap_get(region_exist, i))
		{
			size_t regions_left;
            game->regions_count -= 1;
            if (regions_left = game->regions_count - i)
                memmove(game->regions + i, game->regions + i + 1, regions_left * sizeof(*game->regions));
			else
				break;
		}

		if (!region->garrison.hexagon)
			consistent = 0;

		/*if (region->objects.troops != OBJECT_NOWHERE)
		{
			hexagon = game->hexagons + region->objects.troops;
			if (hexagon->terrain != TERRAIN_GROUND)
				region->objects.troops = OBJECT_NOWHERE;
			else if (hexagon->info.region != region)
				region->objects.troops = OBJECT_NOWHERE;
		}

		if (region->objects.shipyard[0] != OBJECT_NOWHERE)
		{
			hexagon = game->hexagons + region->objects.shipyard[0];
			if (hexagon->terrain != TERRAIN_GROUND)
				region->objects.shipyard[0] = OBJECT_NOWHERE;
			else if (hexagon->info.region != region)
				region->objects.shipyard[0] = OBJECT_NOWHERE;

			if (region->objects.shipyard[0] == OBJECT_NOWHERE)
				region->objects.shipyard[1] = OBJECT_NOWHERE;
			else if (region->objects.shipyard[1] == OBJECT_NOWHERE)
				region->objects.shipyard[0] = OBJECT_NOWHERE;
			else
			{
				// The shipyard spans two hexagons - one on the ground and one in water.
				// The ground hexagon must be part of the region to which the shipyard belongs.
				// The ground and water hexagons must be neighbors.

				bool water = false, ship = false;
				struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
				size_t neighbors_count = hexagon_neighbors(region->objects.shipyard[0], game, neighbors);

				while (neighbors_count--)
				{
					enum terrain terrain = game->hexagons[neighbors[neighbors_count].index].terrain;
					if (terrain == TERRAIN_WATER)
					{
						water = true;
						if (region->objects.shipyard[1] == neighbors[neighbors_count].index)
							ship = true;
					}
				}

				if (!water || !ship)
				{
					region->objects.shipyard[0] = OBJECT_NOWHERE;
					region->objects.shipyard[1] = OBJECT_NOWHERE;
				}
			}
		}

		if (region->objects.troops == OBJECT_NOWHERE)
			consistent = 0;*/
	}

	// Check if each ship is in water.
	/*for(i = 0; i < game->ships.count; i += 1)
		while (game->hexagons[game->ships.data[i]->path[0]].terrain != TERRAIN_WATER)
		{
			size_t ships_left;
			free(game->ships.data[i]);
			game->ships.count -= 1;
			if (ships_left = game->ships.count - i)
				memmove(game->ships.data + i, game->ships.data[i + 1], ships_left * sizeof(*game->ships.data[i]));
		}*/

	// TODO maybe ensure no two regions have the same hexagon for the water part of shipyard

	return consistent;
}

void tool_switch_terrain(const struct game *restrict game, struct state *restrict state)
{
	state->game = game;

	state->tool = TOOL_TERRAIN;

	state->terrain = TERRAIN_WATER;

	state->consistent = cleanup((struct game *)game);
}

void tool_switch_regions(const struct game *restrict game, struct state *restrict state)
{
	state->game = game;

	cleanup((struct game *)game);
	state->tool = TOOL_REGIONS;

	state->region_index = -1;
}

void tool_switch_objects(const struct game *restrict game, struct state *restrict state)
{
	state->game = game;

	cleanup((struct game *)game);
	state->tool = TOOL_OBJECTS;

	state->region_index = -1;
	state->object = 0;
	state->flag = 0;
}
