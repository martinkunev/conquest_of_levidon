/*
 * Conquest of Levidon
 * Copyright (C) 2016  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include "basic.h"
#include "log.h"
#include "game.h"
#include "map.h"
#include "battle.h"
#include "movement.h"
#include "combat.h"
#include "computer.h"

#if defined(AI_DEBUG)
# include <string.h>
#endif

#define TEMPERATURE_START 1.0
#define TEMPERATURE_THRESHOLD (1.0 / 4096) /* TODO check this */
#define ANNEALING_COOLDOWN 0.95 /* TODO check this */

/* TODO simulated annealing parameter tuning
ANNEALING_COOLDOWN - try additive instead of multiplicative

state space requirements
- small graph diameter

optimizations
	neighbor generation should favor states with similar utility (in practice this leads to generating fewer bad states (and rejecting fewer states))
		e.g. hamilton cycle: swap only neighbors vs swap any pair of vertices (the latter likely results in a much worse state)
	? avoid going into a deep valley - do they mean minima or maxima?

try without randomness (threshold accepting)
	https://en.wikipedia.org/wiki/Simulated_annealing
	"the smoothening of the cost function landscape at high temperature and the gradual definition of the minima during the cooling process are the fundamental ingredients for the success of simulated annealing"
	at high temperatures the state space should behave as if it's smoother (fewer extrema)

cooling schedule is problem-specific

restarts may help
remember best state so far
*/

enum {ANNEALING_STEPS = 256};

#define ALLY_IMPORTANCE 0.5

/*
against fast enemies fight is not that efficient
use guard instead
guard shouldbe evaluated based i on covered area (from the enemy's available movement)
*/

// TODO the fitness function has weird local maxima islands
// TODO make sure targeted attack is possible even when pawns are far; see where in the state space to put this

// TODO battle mechanics: if an enemy pawn catches you, you need to stop and fight (or else your troops die fast - maybe this can be chosen per pawn)

struct state
{
	struct
	{
		struct battlefield *field;
		unsigned char gate_switch;
	} gates[GATES_LIMIT];
	size_t gates_count;
	struct
	{
		struct pawn *pawn;
		struct pawn_command command;
		struct position *shoot;
		size_t shoot_targets;
	} *pawns;
	size_t pawns_count;
	struct reachable reachable[];
};

struct battle_parameters
{
	unsigned count_self, count_ally, count_enemy;
	double value_kill, value_die;
	double rating_min, rating_max;
};

static void state_term(struct state *state);

static inline long max_int(long a, long b)
{
	return (a >= b) ? a : b;
}

static inline double max_double(double a, double b)
{
	return (a >= b) ? a : b;
}

static inline struct position position_final(const struct pawn *restrict pawn, const struct pawn_command *restrict command)
{
	// TODO if final position is too far, calculate the position tha pawn will reach in 1 round

	// Moves are ignored for ACTION_SHOOT (unless the unit can shoot and move) and for ACTION_FIGHT.
	switch (command->action)
	{
	case ACTION_SHOOT:
		if (pawn->troop->unit->ranged.shoot_moving)
		{
	case ACTION_GUARD:
	case ACTION_HOLD:
	case ACTION_ASSAULT:
			assert(command->moves.count <= 1);
			return command->moves.count ? command->moves.data[0] : pawn->position;
		}
	case ACTION_FIGHT:
		return pawn->position;
	}
}

static unsigned unit_value(const struct unit *restrict unit, const struct battle_parameters *restrict parameters)
{
	// TODO use parameters based on battle (whether assault troops are necessary, whether the enemy has fast troops, etc.)
	// armor, melee, ranged

	// Don't include health as it doesn't determine unit strength, but only determines losses.
	return unit->speed;
}

static double damage_value(const struct pawn *restrict pawn, const struct pawn *restrict target, unsigned damage, double coefficient, const struct battle_parameters *restrict parameters)
{
	unsigned health = target->count * target->troop->unit->health - target->hurt;

	if (damage > health)
		damage = health;
	return coefficient * damage * unit_value(target->troop->unit, parameters) / target->troop->unit->health;
}

// Returns what proportion of the area of circle (b, rb) is in the intersection with circle (a, ra).
static double circle_intersection_proportion(struct position a, double ra, struct position b, double rb)
{
	double d = battlefield_distance(a, b);
	double dr, triangles;
	double intersection;

	if (d >= ra + rb)
		return 0;
	if (d <= fabs(rb - ra))
		return 0;

	dr = ra * ra - rb * rb;
	triangles = sqrt((ra + rb - d) * (ra - rb + d) * (-ra + rb + d) * (ra - rb + d)) / 2;
	intersection = ra * ra * acos((d * d + dr) / (2 * d * ra)) + rb * rb * acos((d * d - dr) / (2 * d * rb)) - triangles;
	return intersection / ((TAU / 2) * rb * rb);
}

static double state_value_attack(const struct game *restrict game, const struct battle *restrict battle, const struct pawn *restrict pawn, const struct pawn_command *restrict command, const struct state *restrict state, const struct battle_parameters *restrict parameters)
{
	switch (command->action)
	{
		size_t i;

	case ACTION_FIGHT:
		{
			const struct pawn *target = command->target.pawn;

			unsigned speed = pawn->troop->unit->speed;
			unsigned enemy_speed = target->troop->unit->speed;
			double distance_left = reachable_get(state->reachable + (pawn - battle->pawns), target->position.x, target->position.y) - enemy_speed - DISTANCE_MELEE;

			double coefficient_min, coefficient_max, coefficient;

			// TODO big values of hurt should be taken into account (they increase victims)

			// Suppose the average of the min and max damage will be dealed.
			// max: Enemy comes toward the pawn. Discount damage according to the number of turns to deal it.
			// min: Enemy moves to a random location uniformly.
			coefficient_min = circle_intersection_proportion(pawn->position, speed, target->position, enemy_speed);
			if (distance_left <= 0)
				coefficient_max = 1;
			else
				coefficient_max = 1 / ceil(1 + distance_left / (speed + enemy_speed));
			coefficient = (coefficient_min + coefficient_max) / 2;
			return damage_value(pawn, target, combat_fight_damage(pawn, pawn->count, target), coefficient, parameters);
		}

	case ACTION_GUARD:
		{
			// For each enemy pawn, calculate the probability of that enemy being attacked by current pawn.
			// Assume current pawn will fight the enemy if it has enough movement left to go to its position (ignore DISTANCE_MELEE).

			struct position final = position_final(pawn, command);
			double guard_distance = guard_radius(pawn->troop->unit->speed - battlefield_distance(pawn->position, final));
			double damage_best = 0;

			for(i = 0; i < battle->pawns_count; i += 1)
			{
				struct pawn *other = battle->pawns + i;
				unsigned other_speed = other->troop->unit->speed;
				double coefficient, damage;

				if (!other->count || pawn_waiting(other))
					continue;
				if (allies(game, other->owner, pawn->owner))
					continue;

				coefficient = circle_intersection_proportion(final, guard_distance, other->position, other_speed);
				if (!coefficient)
					continue; // no damage can be done to this pawn

				damage = damage_value(pawn, other, combat_fight_damage(pawn, pawn->count, other), coefficient, parameters);
				if (damage > damage_best)
					damage_best = damage;

				// TODO remember the area that is guarded. take this into account when calculating distance for enemies attacking (they cannot attack through the guarded zone) - necessary for assault defense
			}

			if (damage_best)
				return damage_best;
			// When ACTION_GUARD has no value, consider it the same as ACTION_HOLD.
		}
	case ACTION_HOLD:
		{
			double distance;
			double distance_min = INFINITY;
			struct pawn *closest;

			struct position final = position_final(pawn, command);

			// Find closest enemy.
			for(i = 0; i < battle->pawns_count; i += 1)
			{
				struct pawn *other = battle->pawns + i;

				if (!other->count || isnan(other->position.x) || isnan(other->position.y))
					continue;
				if (allies(game, pawn->owner, other->owner))
					continue;

				distance = reachable_get(state->reachable + i, final.x, final.y);
				if (distance < distance_min)
				{
					distance_min = distance;
					closest = other;
				}
			}

			// TODO if destination is too far, it will not be reached in 1 turn

			if (distance_min < INFINITY)
			{
				// TODO calculate this coefficient better
				double coefficient = 1 / (2 + (distance_min + closest->troop->unit->speed) / pawn->troop->unit->speed);
//				LOG_DEBUG("coeff = %f       distance = %f         value = %f", coefficient, distance_min, damage_value(pawn, closest, combat_fight_damage(pawn, pawn->count, closest), coefficient, parameters));
				return damage_value(pawn, closest, combat_fight_damage(pawn, pawn->count, closest), coefficient, parameters);
			}
		}
		return 0;

	case ACTION_SHOOT:
		{
			// TODO dirty hack. fix this
			// TODO what is there to fix?

			struct victim_shoot victims[VICTIMS_LIMIT];
			unsigned victims_count = combat_shoot_victims(battle, command, victims);
			double inaccuracy = combat_shoot_inaccuracy(pawn, command, battle->obstacles);

			for(i = 0; i < victims_count; ++i)
			{
				const struct pawn *target = victims[i].pawn;

				if (target->owner == pawn->owner)
					return -damage_value(pawn, target, combat_shoot_damage(pawn, inaccuracy, victims[i].distance, target), 1, parameters);
				else if (allies(game, target->owner, pawn->owner))
					return -damage_value(pawn, target, combat_shoot_damage(pawn, inaccuracy, victims[i].distance, target), ALLY_IMPORTANCE, parameters);
				else
					return damage_value(pawn, target, combat_shoot_damage(pawn, inaccuracy, victims[i].distance, target), 1, parameters);
			}

			// TODO evaluate move after shooting
			if (pawn->troop->unit->ranged.shoot_moving)
				;
		}
		return 0;
	}
}

static double state_value_defend(const struct game *restrict game, const struct battle *restrict battle, const struct pawn *restrict pawn, unsigned char player, const struct state *restrict state, const struct battle_parameters *restrict parameters)
{
	size_t i, j;
	double value = 0;

	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;
		double damage_best = 0;

		if (!pawn->count || pawn_waiting(pawn))
			continue;
		if (allies(game, pawn->owner, player))
			continue;

		// TODO calculate value for allies as well

		for(j = 0; j < state->pawns_count; j += 1)
		{
			struct pawn *target = state->pawns[j].pawn;
			struct pawn_command *command = &state->pawns[j].command;
			struct position final = position_final(target, command);

			unsigned speed, enemy_speed;
			double distance_left;
			double damage;
			double coefficient_min, coefficient_max, coefficient;

			if (!target->count || pawn_waiting(target))
				continue;

			// TODO see which action does the most damage

			speed = target->troop->unit->speed;
			enemy_speed = pawn->troop->unit->speed;
			distance_left = reachable_get(state->reachable + i, final.x, final.y) - enemy_speed - DISTANCE_MELEE;

			// TODO big values of target->hurt should be taken into account

			// Suppose the average of the min and max damage will be dealed.
			// max: Enemy comes toward the pawn. Discount damage according to the number of turns to deal it.
			// min: Enemy moves to a random location uniformly.
			coefficient_min = circle_intersection_proportion(final, DISTANCE_MELEE, pawn->position, speed);
			if (distance_left <= 0)
				coefficient_max = 1;
			else
				coefficient_max = 1 / ceil(1 + distance_left / (speed + enemy_speed));
			coefficient = (coefficient_min + coefficient_max) / 2;
			damage = damage_value(pawn, target, combat_fight_damage(pawn, pawn->count, target), coefficient, parameters); // TODO subtract deaths by shooting from enemy
			if (damage > damage_best)
				damage_best = damage;
		}

		value -= damage_best;
	}

	return value;
}

static double state_rating(const struct game *restrict game, struct battle *restrict battle, unsigned char player, struct state *restrict state, const struct battle_parameters *restrict parameters)
{
	double value = 0;
	double rating;
	size_t i;

	for(i = 0; i < state->pawns_count; i += 1)
	{
		struct pawn *pawn = state->pawns[i].pawn;
		struct pawn_command *command = &state->pawns[i].command;

		if (!pawn->count || pawn_waiting(pawn))
			continue;

		value += state_value_attack(game, battle, pawn, command, state, parameters);
	}
	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;

		if (!pawn->count || pawn_waiting(pawn))
			continue;
		if (allies(game, pawn->owner, player))
			continue;

		value += state_value_defend(game, battle, pawn, player, state, parameters);
	}

	rating = (double)(value - parameters->rating_min) / (parameters->rating_max - parameters->rating_min);

#if defined(AI_DEBUG)
	size_t index = 0;
	switch (state->pawns[index].command.action)
	{
	case ACTION_FIGHT:
//		LOG_DEBUG("fight\t\t| %f", rating);
		break;

	case ACTION_GUARD:
//		LOG_DEBUG("guard %f %f\t| %f", state->pawns[index].command.target.position.x, state->pawns[index].command.target.position.y, rating);
		break;

	case ACTION_HOLD:
		{
			struct pawn *pawn = state->pawns[index].pawn;
			struct pawn_command *cmd = &state->pawns[index].command;
			struct position position = position_final(pawn, cmd);

			battle->rating.distance[(size_t)((position.y - 0.5) * 2)][(size_t)((position.x - 0.5) * 2)] = rating;
		}
		break;
	}
#endif

	return rating;
}

static inline int state_accept(double rating, double rating_new, double temperature)
{
	// Ratings and temperature are normalized (range is [0, 1]).
	// When the temperature is 0, only states with higher rating are accepted.

	double probability;

	if (rating_new > rating)
		return 1;
	else if (rating_new == rating)
		return (temperature > 0);

	probability = exp((rating_new - rating) / temperature);
//	LOG_DEBUG("%f -> %f (T = %f) | P = %f", rating, rating_new, temperature, probability);
	return probability > (random() / (RAND_MAX + 1.0));
}

static int state_init(struct state *restrict state, const struct game *restrict game, struct battle *restrict battle, unsigned char player)
{
	size_t x, y;
	size_t i, j;
	size_t index;

#if defined(AI_DEBUG)
	memset(&battle->rating, 0, sizeof(battle->rating));
#endif

	state->gates_count = 0;
    for(y = 0; y < BATTLEFIELD_HEIGHT; y += 1)
    {
        for(x = 0; x < BATTLEFIELD_WIDTH; x += 1)
        {
            struct battlefield *field = &battle->field[y][x];
            if (field->blockage == BLOCKAGE_GATE)
			{
				assert(state->gates_count < GATES_LIMIT);

				state->gates[state->gates_count].field = field;
				state->gates[state->gates_count].gate_switch = 0;
				state->gates_count += 1;
			}
        }
    }

	state->pawns_count = 0;
	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;
		int status;

		if (!pawn->count || pawn_waiting(pawn))
			continue;

		// Calculate distances from the current pawn to all fields.
		status = path_distances(pawn, battle->graph[pawn->owner], battle->obstacles, state->reachable + i);
		if (status < 0)
			return status;

		if (pawn->owner == player)
		{
			// Cancel pawn command.
			pawn->command.moves.count = 0;
			pawn->command.action = ACTION_HOLD;

			state->pawns_count += 1;
		}
	}

	state->pawns = malloc(state->pawns_count * sizeof(*state->pawns));
	if (!state->pawns)
	{
		state_term(state);
		return ERROR_MEMORY;
	}

	index = 0;
	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;
		size_t shoot_targets;

		if (!pawn->count || pawn_waiting(pawn))
			continue;

		if (pawn->owner != player)
			continue;

		state->pawns[index].pawn = pawn;
		state->pawns[index].command = (struct pawn_command){.action = ACTION_HOLD};

		shoot_targets = 0;
		for(j = 0; j < battle->pawns_count; j += 1)
		{
			struct pawn *target = battle->pawns + j;
			if (!target->count || pawn_waiting(target))
				continue;
			shoot_targets += (!allies(game, player, target->owner) && can_shoot(game, battle, pawn, target->position));
		}

		if (shoot_targets)
		{
			state->pawns[index].shoot = malloc(shoot_targets * sizeof(*state->pawns[index].shoot));
			if (!state->pawns[index].shoot)
			{
				for(i = 0; i < shoot_targets; i += 1)
					free(state->pawns[index].shoot);
				state_term(state);
				return ERROR_MEMORY;
			}
			state->pawns[index].shoot_targets = shoot_targets;

			shoot_targets = 0;
			for(j = 0; j < battle->pawns_count; j += 1)
			{
				struct pawn *target = battle->pawns + j;
				if (!target->count || pawn_waiting(target))
					continue;
				if (!allies(game, player, target->owner) && can_shoot(game, battle, pawn, target->position))
					state->pawns[index].shoot[shoot_targets++] = target->position;
			}
		}
		else
			state->pawns[index].shoot = 0;

		index += 1;
	}

	return 0;
}

static void state_term(struct state *state)
{
	if (state->pawns)
	{
		size_t i;
		for(i = 0; i < state->pawns_count; i += 1)
			free(state->pawns[i].shoot);
		free(state->pawns);
	}
}

static void state_load(const struct game *restrict game, struct battle *restrict battle, unsigned char player, struct state *restrict state)
{
	size_t i;

	for(i = 0; i < state->gates_count; i += 1)
		state->gates[i].field->gate_switch = state->gates[i].gate_switch;

	// TODO movement is not visualized in GUI because path is not set
	for(i = 0; i < state->pawns_count; i += 1)
		state->pawns[i].pawn->command = state->pawns[i].command;
}

static void state_save(const struct game *restrict game, struct battle *restrict battle, unsigned char player, struct state *restrict state)
{
	size_t i;

	for(i = 0; i < state->gates_count; i += 1)
		state->gates[i].gate_switch = state->gates[i].field->gate_switch;

	for(i = 0; i < state->pawns_count; i += 1)
		state->pawns[i].command = state->pawns[i].pawn->command;
}

static unsigned state_neighbor_set(const struct game *restrict game, struct battle *restrict battle, const struct pawn *restrict pawn, struct pawn_command *restrict command, const struct reachable *restrict reachable, double x, double y)
{
	size_t xi = x, yi = y;
	double left, top;
	struct battlefield *field;
	size_t i;
	struct position p;

	// Correct position so that it is valid for moving a pawn.
	if ((x <= 0) || (x >= BATTLEFIELD_WIDTH))
		return 0;
	else if (x < PAWN_RADIUS)
		x = PAWN_RADIUS;
	else if (x > BATTLEFIELD_WIDTH - PAWN_RADIUS)
		x = BATTLEFIELD_WIDTH - PAWN_RADIUS;
	if ((y <= 0) || (y >= BATTLEFIELD_HEIGHT))
		return 0;
	else if (y < PAWN_RADIUS)
		y = PAWN_RADIUS;
	else if (y > BATTLEFIELD_HEIGHT - PAWN_RADIUS)
		y = BATTLEFIELD_HEIGHT - PAWN_RADIUS;

	left = (unsigned)(x * 2) / 2.0;
	top = (unsigned)(y * 2) / 2.0;

	field = &battle->field[yi][xi];

	// TODO fight should be available for bigger distances, but move does not make sense; maybe penalize for move when temperature is too low?

	// TODO handle assault. currently it will fail in the if below
	// TODO reachable is INFINITY for assault targets
	//if (pawn->troop->unit->speed + PAWN_RADIUS < reachable_get(reachable, x, y)) // TODO bad; it makes sense to issue a fight command even if the pawn is farther
	if (pawn->troop->unit->speed * 2 < reachable_get(reachable, x, y)) // TODO bad; it makes sense to issue a fight command even if the pawn is farther
        return 0; // destination is too far

	// Neighbor is valid.
	// Determine what action is appropriate for this location.

	for(i = 0; (i < BATTLEFIELD_PAWNS_LIMIT) && field->pawns[i]; i += 1)
	{
		if (allies(game, pawn->owner, field->pawns[i]->owner))
			continue;

		// Check if pawn position is inside the square [xi, yi], (xi + 0.5, yi + 0.5).
		p = field->pawns[i]->position;
		if ((left <= p.x) && (p.x < left + 0.5) && (top <= p.y) && (p.y < top + 0.5))
		{
			if (!combat_fight_damage(pawn, pawn->count, field->pawns[i]))
				return 0;

			command->moves.count = 0;
			command->action = ACTION_FIGHT;
			command->target.pawn = field->pawns[i];
			return 1;
		}
	}

	command->action = ACTION_HOLD;
	p = (struct position){x, y};
	if (position_eq(p, pawn->position))
		command->moves.count = 0;
	else
	{
		command->moves.count = 1;
		command->moves.data[0] = p;
	}
	return 1;
}

static void state_neighbor(const struct game *restrict game, struct battle *restrict battle, unsigned char player, struct state *restrict state)
{
	size_t index = random() % (state->pawns_count + (state->gates_count ? 1 : 0));

	if (index == state->pawns_count) // gate switch
	{
		state->gates[random() % state->gates_count].gate_switch ^= 1;
	}
	else // pawn change
	{
		struct pawn *pawn = state->pawns[index].pawn;
		struct position position = position_final(pawn, &state->pawns[index].command);
		struct reachable *reachable = state->reachable + (pawn - battle->pawns);

		struct pawn_command commands[5]; // at most 1 directions of movement and 1 change of action
		unsigned commands_count = 0;
		unsigned shoot_first;

		size_t command;

		// Initialize movement commands.
		commands_count += state_neighbor_set(game, battle, pawn, commands + commands_count, reachable, position.x - 0.5, position.y);
		commands_count += state_neighbor_set(game, battle, pawn, commands + commands_count, reachable, position.x + 0.5, position.y);
		commands_count += state_neighbor_set(game, battle, pawn, commands + commands_count, reachable, position.x, position.y - 0.5);
		commands_count += state_neighbor_set(game, battle, pawn, commands + commands_count, reachable, position.x, position.y + 0.5);

		// Initialize action change commands.
		switch (state->pawns[index].command.action)
		{
		case ACTION_HOLD:
			commands[commands_count].action = ACTION_GUARD;
			commands[commands_count].moves = state->pawns[index].command.moves;
			commands[commands_count].target.position = position;
			commands_count += 1;
			break;

		case ACTION_GUARD:
			commands[commands_count].action = ACTION_HOLD;
			commands[commands_count].moves = state->pawns[index].command.moves;
			commands_count += 1;
			break;
		}

		// Initialize shoot commands.
		shoot_first = commands_count;
		if (state->pawns[index].shoot)
			commands_count += state->pawns[index].shoot_targets;

		// Choose a command.
		command = random() % commands_count;
		if (command >= shoot_first)
		{
			// TODO if target is too far or an enemy is blocking the shooter, handle this well
			// TODO bug: the computer can shoot even when an enemy is next to it

			// TODO do this calculation properly (using combat.c primitives)

			unsigned range = pawn->troop->unit->ranged.range;
			struct position target = state->pawns[index].shoot[command - shoot_first];

			if (!path_visible(pawn->position, target, battle->obstacles))
				range -= 1;
			if (battlefield_distance(pawn->position, target) > range)
				return;

			// TODO set movement?
			state->pawns[index].command.action = ACTION_SHOOT;
			state->pawns[index].command.target.position = target;
		}
		else
			state->pawns[index].command = commands[command];
	}
}

// Determine the behavior of the computer using simulated annealing.
int computer_battle(const struct game *restrict game, struct battle *restrict battle, unsigned char player)
{
	int status = ERROR_MEMORY;

	struct state *state = 0;

	struct battle_parameters parameters = {0};
	size_t i;

	double rating_best, rating;
	double temperature;
	unsigned step;

	// Initialize the state of the agent.
	state = falloc(state, state->reachable, battle->pawns_count);
	if (!state)
		return status;
	status = state_init(state, game, battle, player);
	if (status < 0)
	{
		free(state);
		return status;
	}

	// Find the maximum value obtainable from a kill and from a death of a single troop.
	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;
		unsigned value;

		if (!pawn->count || pawn_waiting(pawn)) // TODO do I need to change this when morale is implemented?
			continue;

		value = unit_value(pawn->troop->unit, &parameters);

		if (pawn->owner == player)
		{
			parameters.value_die = max_int(parameters.value_die, value);
			parameters.count_self += pawn->count;
		}
		else if (allies(game, pawn->owner, player))
		{
			parameters.value_die = max_int(parameters.value_die, value * ALLY_IMPORTANCE);
			parameters.count_ally += pawn->count;
		}
		else
		{
			parameters.value_kill = max_int(parameters.value_kill, value);
			parameters.count_enemy += pawn->count;
		}
	}
	parameters.rating_min = - parameters.value_die * (parameters.count_self + parameters.count_ally);
	parameters.rating_max = parameters.value_kill * parameters.count_enemy;

	// Choose suitable commands.
	rating_best = state_rating(game, battle, player, state, &parameters);
	for(temperature = TEMPERATURE_START; temperature >= TEMPERATURE_THRESHOLD; temperature *= ANNEALING_COOLDOWN)
	{
		LOG_DEBUG("T = %f", temperature);

		for(step = 0; step < ANNEALING_STEPS; ++step)
		{
			state_neighbor(game, battle, player, state);
			rating = state_rating(game, battle, player, state, &parameters);

			// Choose the new state if it's better than the current one.
			// Choose the new state with some probability otherwise.
			if (state_accept(rating_best, rating, temperature))
			{
				//LOG_DEBUG("%f -> %f (T = %f)", rating_best, rating, temperature);

				rating_best = rating;
				state_load(game, battle, player, state);
			}
			else
				state_save(game, battle, player, state);
		}

		state_save(game, battle, player, state);
	}

	LOG_DEBUG("T = %f", temperature);

	status = 0;

finally:
	state_term(state);
	free(state);
	return status;
}
