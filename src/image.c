/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#include <GL/gl.h>

#include "log.h"
#include "interface.h"
#include "image.h"

#define MAGIC_NUMBER_SIZE 8

static png_byte **load_png(const char *restrict filename, GLint *restrict format, unsigned *restrict image_width, unsigned *restrict image_height)
{
	int img;
	struct stat info;
	char header[MAGIC_NUMBER_SIZE];

	FILE *img_stream;
	png_structp png_ptr;
	png_infop info_ptr, end_ptr;

	// Open the file for reading
	if ((img = open(filename, O_RDONLY)) < 0)
	{
		LOG_WARNING("Unable to open %s", filename);
		return 0;
	}

	// Check file size and file type
	// TODO read() may not read the whole magic
	fstat(img, &info);
	if ((info.st_size < MAGIC_NUMBER_SIZE) || (read(img, header, MAGIC_NUMBER_SIZE) != MAGIC_NUMBER_SIZE) || png_sig_cmp(header, 0, MAGIC_NUMBER_SIZE))
	{
		close(img);
		LOG_WARNING("Invalid file format of %s", filename);
		return 0;
	}

	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	if (!png_ptr)
	{
		close(img);
		return 0;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, 0);
		close(img);
		return 0;
	}
	end_ptr = png_create_info_struct(png_ptr); // TODO is this necessary
	if (!end_ptr)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
		close(img);
		return 0;
	}

	// TODO: the i/o is done the stupid way. it must be rewritten
	img_stream = fdopen(img, "r");
	if (!img_stream)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
		close(img);
		return 0;
	}
	png_init_io(png_ptr, img_stream);

	// Tell libpng that we already read MAGIC_NUMBER_SIZE bytes.
	png_set_sig_bytes(png_ptr, MAGIC_NUMBER_SIZE);

	// Read all the info up to the image data.
	png_read_info(png_ptr, info_ptr);

	// get info about png
	int bit_depth, color_type;
	png_uint_32 width, height;
	png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, 0, 0, 0);

	if (bit_depth != 8)
	{
		LOG_WARNING("Unsupported bit depth %d for image %s", bit_depth, filename);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
		fclose(img_stream);
		return 0;
	}

	switch (color_type)
	{
	case PNG_COLOR_TYPE_RGB:
		*format = GL_RGB;
		break;

	case PNG_COLOR_TYPE_RGB_ALPHA:
		*format = GL_RGBA;
		break;

	default:
		LOG_WARNING("Unsupported color type %d for image %s", color_type, filename);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
		fclose(img_stream);
		return 0;
	}

	// Row size in bytes.
	// glTexImage2d requires rows to be 4-byte aligned
	unsigned rowbytes = png_get_rowbytes(png_ptr, info_ptr);
	rowbytes += 3 - ((rowbytes - 1) % 4);

	png_byte **rows = malloc(height * (sizeof(*rows) + rowbytes * sizeof(**rows)));
	if (!rows)
	{
		LOG_WARNING("Unable to allocate memory");
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
		fclose(img_stream);
		return 0;
	}

	// set the individual row_pointers to point at the correct offsets
	size_t i;
	png_byte *buffer = (png_byte *)&rows[height];
	for(i = 0; i < height; i++)
		rows[i] = buffer + i * rowbytes;

	// read the png into image_data through row_pointers
	png_read_image(png_ptr, rows);

	png_destroy_read_struct(&png_ptr, &info_ptr, &end_ptr);
	fclose(img_stream);

	*image_width = width;
	*image_height = height;
	return rows;
}

int image_load_png(struct image *restrict image, const char *restrict filename, void (*modify)(const struct image *restrict, png_byte **))
{
	GLint format;
	png_byte **rows = load_png(filename, &format, &image->width, &image->height);
	if (!rows)
		return -1;

	if (modify)
		modify(image, rows);

	// Generate the OpenGL texture object.
	glGenTextures(1, &image->texture);
	glBindTexture(GL_TEXTURE_2D, image->texture);
	glTexImage2D(GL_TEXTURE_2D, 0, format, image->width, image->height, 0, format, GL_UNSIGNED_BYTE, rows[0]);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	free(rows);

	return 0;
}

void image_unload(struct image *restrict image)
{
	glDeleteTextures(1, &image->texture);
}

int cursor_load_png(uint32_t *restrict cursor, const char *restrict filename)
{
	GLint format;
	png_byte **rows;
	png_byte *pixels;
	unsigned width, height;

	size_t i;

	rows = load_png(filename, &format, &width, &height);
	if (!rows || (format != GL_RGBA))
	{
		free(rows);
		return -1;
	}
	pixels = rows[0];

	// Swap bytes for red and blue.
	for(i = 0; i < width * height; i += 1)
	{
		png_byte byte = pixels[i * 4];
		pixels[i * 4] = pixels[i * 4 + 2];
		pixels[i * 4 + 2] = byte;
	}

	*cursor = if_cursor_init(pixels, width, height);
	free(rows);

	return 0;
}

// Convert image to grayscale.
void filter_grayscale(const struct image *restrict image, png_byte **rows)
{
	// TODO breaks without alpha channel?
	for(size_t y = 0; y < image->height; ++y)
	{
		for(size_t x = 0; x < image->width; ++x)
		{
			unsigned r = rows[y][x * 4];
			unsigned g = rows[y][x * 4 + 1];
			unsigned b = rows[y][x * 4 + 2];

			unsigned luminosity = 0.21 * r + 0.72 * g + 0.07 * b;
			//unsigned n = sqrt((r * r + g * g + b * b) / 3);
			rows[y][x * 4] = luminosity;
			rows[y][x * 4 + 1] = luminosity;
			rows[y][x * 4 + 2] = luminosity;
		}
	}
}

// Convert image to mask of all non-white pixels.
void filter_mask(const struct image *restrict image, png_byte **rows)
{
	// TODO breaks without alpha channel?
	for(size_t y = 0; y < image->height; ++y)
		for(size_t x = 0; x < image->width; ++x)
			if ((rows[y][x * 4] != 255) || (rows[y][x * 4 + 1] != 255) || (rows[y][x * 4 + 2] != 255))
				rows[y][x * 4 + 3] = 0;
}
