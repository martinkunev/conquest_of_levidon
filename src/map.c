/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "log.h"
#include "bitmap.h"
#include "pathfinding.h"
#define ARRAY_SOURCE
#include "game.h"
#define ARRAY_SOURCE
#include "map.h"

#define MAP_VISIBILITY 3

#define NAME(string) .name = string, .name_size = sizeof(string) - 1

// TODO training war machines should be allowed everywhere

const struct unit UNITS[] =
{
	[UnitPeasant] =
	{
		NAME("Peasant"),
		.requires = (1 << BuildingTownHall),
		.cost = {.gold = -20}, .expense = {.food = -1},
		.time = 1,
		.troops_count = 30,

		.speed = 4,

		.health = 4, .armor = ARMOR_NONE,
		.melee = {.weapon = WEAPON_CLUB, .damage = 1.0, .agility = 0.75},
	},
	[UnitWorker] =
	{
		NAME("Worker"),
		.requires = (1 << BuildingTownHall),
		.cost = {.gold = -30}, .expense = {.food = -1},
		.time = 1,
		.troops_count = 24,

		.speed = 4,
		.worker = 1,

		.health = 4, .armor = ARMOR_NONE,
		.melee = {.weapon = WEAPON_CLUB, .damage = 1.0, .agility = 0.75},
	},
	[UnitMilitia] =
	{
		NAME("Militia"),
		.requires = (1 << BuildingBarracks),
		.cost = {.gold = -30, .wood = -20}, .expense = {.food = -1},
		.time = 1,
		.troops_count = 24,

		.speed = 4,

		.health = 5, .armor = ARMOR_LEATHER,
		.melee = {.weapon = WEAPON_CLEAVING, .damage = 1.5, .agility = 1.0},
	},
	[UnitPikeman] =
	{
		NAME("Pikeman"),
		.requires = (1 << BuildingForge),
		.cost = {.gold = -40, .wood = -20}, .expense = {.food = -1},
		.time = 1,
		.troops_count = 24,

		.speed = 4,

		.health = 5, .armor = ARMOR_CHAINMAIL,
		.melee = {.weapon = WEAPON_POLEARM, .damage = 2.0, .agility = 1.0},
	},
	[UnitArcher] =
	{
		NAME("Archer"),
		.requires = (1 << BuildingArcheryRange),
		.cost = {.gold = -24, .wood = -20}, .expense = {.food = -1},
		.time = 1,
		.troops_count = 24,

		.speed = 4,

		.health = 4, .armor = ARMOR_NONE,
		.melee = {.weapon = WEAPON_CLUB, .damage = 0.75, .agility = 0.75},
		.ranged = {.weapon = WEAPON_ARROW, .damage = 1.0, .range = 6},
	},
	[UnitLongbow] =
	{
		NAME("Longbow"),
		.requires = (1 << BuildingBarracks) | (1 << BuildingArcheryRange),
		.cost = {.gold = -40, .wood = -40}, .expense = {.food = -1},
		.time = 2,
		.troops_count = 24,

		.speed = 4,

		.health = 5, .armor = ARMOR_LEATHER,
		.melee = {.weapon = WEAPON_CLEAVING, .damage = 1.0, .agility = 1.0},
		.ranged = {.weapon = WEAPON_ARROW, .damage = 2.0, .range = 8},
	},
	[UnitLightCavalry] =
	{
		NAME("Light Cavalry"),
		.requires = (1 << BuildingBarracks) | (1 << BuildingStables),
		.cost = {.gold = -50, .wood = -15}, .expense = {.food = -4},
		.time = 2,
		.troops_count = 16,

		.speed = 8,

		.health = 10, .armor = ARMOR_LEATHER,
		.melee = {.weapon = WEAPON_CLEAVING, .damage = 2.0, .agility = 1.25},
	},
	[UnitBatteringRam] =
	{
		NAME("Battering Ram"),
		.requires = (1 << BuildingWorkshop),
		.cost = {.gold = -50, .wood = -100}, .expense = {.food = -6},
		.time = 2,
		.troops_count = 1,

		.speed = 2,

		.health = 60, .armor = ARMOR_WOODEN,
		.melee = {.weapon = WEAPON_BLUNT, .damage = 50.0, .agility = 0.25},
	},
};

const struct building BUILDINGS[] =
{
	[BuildingTownHall] =
	{
		NAME("Town Hall"),
		.requires = 0,
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -80, .wood = -40}, .expense = {.gold = -2},
		.time = 20,

		.description = "Town Hall allows governing people and collecting taxes. Governed people can be transported and trained as workers. Governing the people costs a small amount of gold.",
	},
	[BuildingIrrigation] =
	{
		NAME("Irrigation Canals"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -160}, .expense = {.gold = -5},
		.time = 19,

		.description = "Irrigation canals increase crop production and improve population growth. Maintenance of the canals costs a small amount of gold.",

		.income = {.food = 0.4},
	},
	[BuildingGranary] =
	{
		NAME("Granary"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -100, .wood = -60}, .expense = {0},
		.time = 24,

		.description = "Granary provides place to safely store food for long periods of time. This decreases food losses. During a siege, people can feed on the stored food.",

		.income = {.food = 0.1},
		.food_storage = 50,
	},
	[BuildingBarracks] =
	{
		NAME("Barracks"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -180, .wood = -80, .stone = -40}, .expense = {0},
		.time = 24,

		.description = "Barracks allow training soldiers."
	},
	[BuildingArcheryRange] =
	{
		NAME("Archery Range"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -120, .wood = -50}, .expense = {0},
		.time = 19,

		.description = "Barracks allow training archers."
	},
	[BuildingStables] =
	{
		NAME("Stables"),
		.requires = (1 << BuildingGranary),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -210, .wood = -120, .food = -150}, .expense = {.food = -10},
		.time = 39,

		.description = "Stables are used to breed and feed horses and train them for battle.",
	},
	[BuildingForge] =
	{
		NAME("Forge"),
		.requires = (1 << BuildingBarracks),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -200, .wood = -20, .stone = -100}, .expense = {.stone = -5},
		.time = 24,
	},
	[BuildingWorkshop] =
	{
		NAME("Workshop"),
		.requires = 0,
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -250, .wood = -90}, .expense = {0},
		.time = 30,
	},
	[BuildingPort] =
	{
		NAME("Port"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT) | (1 << ENVIRONMENT_COAST),
		.cost = {.gold = -180, .wood = -200, .stone = -160}, .expense = {.gold = -5},
		.time = 30,

		.description = "Port is used to access the sea for fishing, trading and military purposes. During a siege, people can use the port to deliver food. Running the port costs a small amount of gold.",
		// TODO port - transfer of troops from garrison to water; food bonus (fishing); trading with nearby regions over water (if marketplace is built); allows withstanding siege for longer (unless blocked by ships)

		.income = {.food = 0.2},
		.port = 1,
	},
	[BuildingPalisade] =
	{
		NAME("Palisade"),
		.requires = (1 << BuildingTownHall),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -220, .wood = -300}, .expense = {.gold = -3},
		.time = 24,

		.garrison = GARRISON_PALISADE,
		.sight = 4,
		.food_storage = 50,
	},
	[BuildingFortress] =
	{
		NAME("Fortress"),
		.requires = (1 << BuildingPalisade),
		.environment = (1 << ENVIRONMENT_SETTLEMENT),
		.cost = {.gold = -240, .stone = -350}, .expense = {.gold = -5},
		.time = 55,

		.garrison = GARRISON_FORTRESS,
		.sight = 4,
		.food_storage = 50,
	},

	[BuildingSawmill] =
	{
		NAME("Sawmill"),
		.requires = 0,
		.environment = (1 << ENVIRONMENT_FOREST),
		.cost = {.gold = -120}, .expense = {0},
		.time = 15,

		.production = {.wood = 24},
		.workers = 48,
	},
	[BuildingQuarry] =
	{
		NAME("Quarry"),
		.requires = 0,
		.environment = (1 << ENVIRONMENT_QUARRY),
		.cost = {.gold = -150}, .expense = {0},
		.time = 15,

		.production = {.stone = 36},
		.workers = 72,
	},

	[BuildingWatchTower] =
	{
		NAME("WatchTower"),
		.requires = 0,
		.environment = 0,
		.cost = {.gold = -20, .wood = -100}, .expense = {.gold = -2},
		.time = 15,

		.sight = 4,
	},
	[BuildingShipyard] =
	{
		NAME("Shipyard"),
		.requires = 0,
		.environment = (1 << ENVIRONMENT_COAST),
		.cost = {.gold = -100, .wood = -120, .stone = -80},
		.time = 30,
	},

	// TODO buildings:
	//  aquaduct - decreases mortality; increases population growth
	//  marketplace - trading with neighboring regions
	//  dirt roads, paved roads - faster movement in the region; improve trading
	//  sewers
	//  public baths - require aquaduct
	//  religious buildings
};

const struct vessel VESSELS[] =
{
	[VesselGround] =
	{
		.capacity = 12, .speed = 0, .autonomous = 0
	},

	[VesselWater] =
	{
		NAME("Ship"),

		.capacity = 8, .speed = 9, .autonomous = 1, .workers = 72,

		.requires = (1 << BuildingShipyard),
		.cost = {.gold = -50, .wood = -80}, .expense = {.gold = -8, .food = -8},
		.time = 20
	},
};

/*
void region_turn_process(const struct game *restrict game, struct region *restrict region)
{
	// Region can change ownership if:
	// * it is conquered by enemy troops
	// * it is unguarded and the garrison's owner is an enemy of the region owner
	// Region garrison can change ownership if:
	// * it is assaulted successfully
	// * a siege finishes successfully
	// * it is unguarded, the region's owner is an enemy of the garrison owner and there are enemy troops in the region

	struct troop *troop, *next;

	bool region_guarded = false, region_garrison_guarded = false;
	unsigned invaders_count = 0;
	unsigned char invaders_alliance;

	// Collect information about the region.
	for(troop = region->troops; troop; troop = troop->_next)
	{
		if (troop->move == LOCATION_GARRISON) region_garrison_guarded = true;
		else if (troop->move == region) // make sure the troop is not retreating
		{
			region_guarded = true;
			if (!allies(game, troop->owner, region->owner))
			{
				invaders_count += 1;
				invaders_alliance = game->players[troop->owner].alliance;
			}
		}
	}

	// Liberated regions are re-conquered by the owner of the garrison.
	// Invaded regions are conquered by a random invading troop.
	if (invaders_count)
	{
		if (invaders_alliance == game->players[region->garrison.owner].alliance)
			region->owner = region->garrison.owner;
		else
			region->owner = region_owner_choose(game, region, invaders_count, invaders_alliance);
	}
	else if (!region_guarded && !allies(game, region->owner, region->garrison.owner))
		region->owner = region->garrison.owner;

	// Unguarded garrisons are conquered by enemy troops in the region.
	if (!region_garrison_guarded && region_guarded && !allies(game, region->owner, region->garrison.owner))
		region->garrison.owner = region->owner;

	// Handle siege events.
	if (allies(game, region->owner, region->garrison.owner))
	{
		region->garrison.siege = 0;
	}
	else
	{
		region->garrison.siege += 1;

		// If there are no more provisions in the garrison, kill the troops in it.
		const struct garrison_info *restrict garrison = garrison_info(region);
		assert(garrison);
		if (region->garrison.siege > garrison->provisions) 
		{
			for(troop = region->troops; troop; troop = next)
			{
				next = troop->_next;
				if (troop->location == LOCATION_GARRISON)
				{
					troop_detach(&region->troops, troop);
					free(troop);
				}
			}

			// The siege ends. The garrison is conquered by the owner of the region.
			region->garrison.siege = 0;
			region->garrison.owner = region->owner;
		}
	}
}

int region_garrison_full(const struct region *restrict region, const struct garrison_info *restrict garrison)
{
	unsigned count = 0;
	for(const struct troop *troop = region->troops; troop; troop = troop->_next)
		if ((troop->move == LOCATION_GARRISON) && (troop->owner == region->garrison.owner))
			count += 1;
	return (count == garrison->troops);
}
*/

double heuristic_map(float x0, float y0, float x1, float y1)
{
	struct tile a = {x0, y0};
	struct tile b = {x1, y1};
	assert((int)x0 == x0);
	assert((int)x1 == x1);
	assert((int)y0 == y0);
	assert((int)y1 == y1);
	return hexagon_distance_locations(a, b);
}

struct tile hexagon_location(const struct game *restrict game, size_t index)
{
	struct tile location;

	// Even rows have 1 more hexagon than odd rows.
	// A sequence of even and odd row has a total of (2 * width - 1) hexagons.
	location.x = index % (2 * game->map_width - 1);
	location.y = (index / (2 * game->map_width - 1)) * 2;
	if (location.x >= game->map_width)
	{
		location.x -= game->map_width;
		location.y += 1;
	}

	return location;
}

size_t hexagon_neighbors(const struct game *restrict game, const struct hexagon *restrict hexagon, struct hexagon_neighbor neighbors[static DIRECTIONS_COUNT])
{
	size_t count = 0;

	size_t index = hexagon - game->hexagons;
	struct tile location = hexagon_location(game, index);
	size_t width = hexagons_width(game, location.y);

	// Check for neighbors to the east.
	if (location.x != width - 1)
		neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index + 1, East};

	// Check for neighbors to the north-east and north-west.
	if (location.y)
	{
		if (location.x != game->map_width - 1)
			neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index - game->map_width + 1, NorthEast};
		if (location.x || (width < game->map_width))
			neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index - game->map_width, NorthWest};
	}

	// Check for neighbors to the west.
	if (location.x)
		neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index - 1, West};

	// Check for neighbors to the south-west and south-east.
	if (location.y != game->map_height - 1)
	{
		if (location.x || (width < game->map_width))
			neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index + game->map_width - 1, SouthWest};
		if (location.x != game->map_width - 1)
			neighbors[count++] = (struct hexagon_neighbor){game->hexagons + index + game->map_width, SouthEast};
	}

	return count;
}

enum direction hexagon_neighbor_direction(const struct game *restrict game, int from, int to)
{
	// Convert the difference of indices to a compile-time constant depending on direction.
	int diff = to - from;
	int width = game->map_width;
	const enum direction directions[] = {NorthWest, NorthEast, West, East, SouthWest, SouthEast};
	return directions[(diff > -width) + (2 * diff + 2 * width + 1) / width]; // magic
}

unsigned hexagon_distance_locations(struct tile al, struct tile bl)
{
	unsigned vertical, horizontal, closer;

	vertical = abs(al.y - bl.y);

	// Vertical movement can get us closer to the target in horizontal direction:
	//  on odd rows if target is to the right
	//  on even rows if target is to the left
	horizontal = abs(al.x - bl.x);
	closer = (vertical + ((al.x < bl.x) ? al.y % 2 : !(al.y % 2))) / 2;

	return vertical + ((horizontal > closer) ? (horizontal - closer) : 0);
}

unsigned hexagon_distance(const struct game *restrict game, int from, int to)
{
	struct tile al = hexagon_location(game, from);
	struct tile bl = hexagon_location(game, to);
	return hexagon_distance_locations(al, bl);
}

int hexagon_is_neighbor(const struct game *restrict game, const struct hexagon *restrict hexagon, const struct hexagon *restrict neighbor)
{
	size_t index = hexagon - game->hexagons;
	struct tile location = hexagon_location(game, index);
	size_t width = hexagons_width(game, location.y);

	// Check for neighbors to the east.
	if ((location.x != width - 1) && (&game->hexagons[index + 1] == neighbor))
		return 1;

	// Check for neighbors to the north-east and north-west.
	if (location.y)
	{
		if ((location.x != game->map_width - 1) && (&game->hexagons[index - game->map_width + 1] == neighbor))
			return 1;
		if ((location.x || (width < game->map_width)) && (&game->hexagons[index - game->map_width] == neighbor))
			return 1;
	}

	// Check for neighbors to the west.
	if (location.x && (&game->hexagons[index - 1] == neighbor))
		return 1;

	// Check for neighbors to the south-west and south-east.
	if (location.y != game->map_height - 1)
	{
		if ((location.x || (width < game->map_width)) && (&game->hexagons[index + game->map_width - 1] == neighbor))
			return 1;
		if ((location.x != game->map_width - 1) && (&game->hexagons[index + game->map_width] == neighbor))
			return 1;
	}

	return 0;
}

struct adjacency_list *hexagons_graph_build(struct game *restrict game)
{
	struct adjacency_list *graph;
	size_t hexagons_total = hexagons_count(game);
	size_t i, j;

	graph = malloc(offsetof(struct adjacency_list, list) + hexagons_total * sizeof(*graph->list));
	if (!graph)
		return 0;
	graph->count = hexagons_total;

	for(i = 0; i < hexagons_total; ++i)
	{
		struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
		size_t neighbors_count, neighbor_index;
		struct neighbor *edges;
		struct tile tile;

		tile = hexagon_location(game, i);
		graph->list[i].x = tile.x;
		graph->list[i].y = tile.y;

		neighbors_count = hexagon_neighbors(game, game->hexagons + i, neighbors);
		neighbor_index = 0;
		for(j = 0; j < neighbors_count; j += 1)
		{
			// Neighboring hexagons are connected if they have the same terrain.
			// Water is also connected to the coast.
			struct hexagon *hexagon = game->hexagons + i, *neighbor = neighbors[j].hexagon;
			if (hexagon->terrain == neighbor->terrain)
			{
				if (neighbor_index < j)
					neighbors[neighbor_index] = neighbors[j];
				neighbor_index += 1;
			}
		}
		neighbors_count = neighbor_index;
		if (!neighbors_count)
		{
			graph->list[i].neighbors = 0;
			continue;
		}

		edges = malloc(neighbors_count * sizeof(*graph->list[i].neighbors));
		if (!edges)
		{
			while (i--)
				free(graph->list[i].neighbors);
			free(graph);
			return 0;
		}

		graph->list[i].neighbors = edges;

		for(j = 0; j < neighbors_count; j += 1)
			edges[j] = (struct neighbor){neighbors[j].hexagon - game->hexagons, 1, edges + j + 1};
		edges[neighbors_count - 1].next = 0;
	}

	return graph;
}

void hexagons_graph_free(struct adjacency_list *restrict graph)
{
	if (graph)
	{
		for(size_t i = 0; i < graph->count; i += 1)
			free(graph->list[i].neighbors);
		free(graph);
	}
}

const struct unit *unit_find(struct slice unit)
{
	// TODO maybe use compile-time sort and binary search
	size_t i;
	for(i = 0; i < sizeof(UNITS) / sizeof(*UNITS); i += 1)
		if ((UNITS[i].name_size == unit.size) && !memcmp(UNITS[i].name, unit.data, unit.size))
		{
			assert(UNITS[i].speed <= UNIT_SPEED_LIMIT);
			return UNITS + i;
		}
	LOG_WARNING("Invalid unit name %.*s", (int)unit.size, unit.data);
	return 0;
}

int troop_spawn(struct troops *restrict troops, const struct unit *restrict unit, unsigned count)
{
	struct troop *troop = malloc(sizeof(*troop));
	if (!troop)
		return ERROR_MEMORY;

	troop->unit = unit;
	troop->count = count;
	troop->task = TASK_NONE;

	troops->troops[troops->count++] = troop;
	return 0;
}

int building_find(struct slice name)
{
	// TODO maybe use compile-time sort and binary search
	size_t i;
	for(i = 0; i < sizeof(BUILDINGS) / sizeof(*BUILDINGS); i += 1)
		if ((BUILDINGS[i].name_size == name.size) && !memcmp(BUILDINGS[i].name, name.data, name.size))
			return i;
	LOG_WARNING("Invalid building name %.*s", (int)name.size, name.data);
	return -1;
}

int map_movement_queue(const struct game *restrict game, struct troop_stack *restrict troop_stack, size_t to, unsigned char *restrict visible)
{
	size_t from = troop_stack->movements[troop_stack->movements_count - 1] - game->hexagons;
	struct path_node *traverse_info;
	double distance;

	// Handle ship mooring.
	if (troop_stack->vessel->autonomous)
	{
		if (troop_stack->movements_count - 1 == troop_stack->stamina)
			return ERROR_MISSING; // no stamina left

		if ((game->hexagons[from].terrain == TERRAIN_WATER) && (game->hexagons[to].environment & (1 << ENVIRONMENT_COAST)))
		{
			if (hexagon_is_neighbor(game, game->hexagons + from, game->hexagons + to))
			{
				troop_stack->movements[troop_stack->movements_count++] = game->hexagons + to;
				return 0;
			}
		}
		else if ((game->hexagons[from].environment & (1 << ENVIRONMENT_COAST)) && (game->hexagons[to].terrain == TERRAIN_WATER))
		{
			if (hexagon_is_neighbor(game, game->hexagons + from, game->hexagons + to))
			{
				troop_stack->movements[troop_stack->movements_count++] = game->hexagons + to;
				return 0;
			}
		}

		if (game->hexagons[from].terrain == TERRAIN_GROUND)
			return ERROR_MISSING; // moored ships cannot move
	}

	traverse_info = path_traverse(game->hexagons_graph, from, to, visible, &heuristic_map);
	if (!traverse_info)
		return ERROR_MEMORY;

	if (traverse_info[to].distance > troop_stack->stamina + 1 - troop_stack->movements_count)
		return ERROR_MISSING; // target hexagon unreachable or too far

	distance = traverse_info[to].distance;
	while (to != from)
	{
		assert((size_t)traverse_info[to].distance == traverse_info[to].distance);
		troop_stack->movements[troop_stack->movements_count - 1 + (size_t)traverse_info[to].distance] = game->hexagons + to;
		to = traverse_info[to].path_link - traverse_info;
	}
	troop_stack->movements_count += (size_t)distance;

	free(traverse_info);

	return 0;
}

static int map_visible_find(const struct game *restrict game, size_t origin, unsigned distance, unsigned char *restrict bitmap_hexagons)
{
	struct queue
	{
		size_t index;
		enum direction direction;
		unsigned distance;
		struct queue *next;
	} *queue = 0, *current, **end = &queue;

	struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
	size_t count;
	size_t i;

	// Use BFS with a queue to find visible hexagons.
	// For each neighbor of the origin, search only in directions similar to the neighbor's direction.
	// There is no visibility through mountains.

	count = hexagon_neighbors(game, game->hexagons + origin, neighbors);
	for(i = 0; i < count; i += 1)
	{
		size_t index = neighbors[i].hexagon - game->hexagons;

		bitmap_set(bitmap_hexagons, index, 1);

		if ((distance > 1) && (neighbors[i].hexagon->terrain != TERRAIN_MOUNTAIN))
		{
			struct queue *new = malloc(sizeof(*new));
			if (!new)
				goto error;
			new->index = index;
			new->direction = neighbors[i].direction;
			new->distance = 1;
			new->next = 0;

			*end = new;
			end = &new->next;
		}
	}

	for(current = queue; current; current = current->next)
	{
		count = hexagon_neighbors(game, game->hexagons + current->index, neighbors);
		for(i = 0; i < count; i += 1)
		{
			size_t index = neighbors[i].hexagon - game->hexagons;
			enum direction direction = hexagon_neighbor_direction(game, current->index, index);

			switch ((DIRECTIONS_COUNT + current->direction - direction) % DIRECTIONS_COUNT)
			{
			default:
				continue;

			case 0: // same direction
			case 1: // similar direction
			case 5: // similar direction
				break;
			}

			bitmap_set(bitmap_hexagons, index, 1);

			if ((distance > 1 + current->distance) && (neighbors[i].hexagon->terrain != TERRAIN_MOUNTAIN))
			{
				struct queue *new = malloc(sizeof(*new));
				if (!new)
					goto error;
				new->index = index;
				new->direction = neighbors[i].direction;
				new->distance = 1 + current->distance;
				new->next = 0;

				*end = new;
				end = &new->next;
			}
		}
	}

	while (queue)
	{
		current = queue;
		queue = queue->next;
		free(current);
	}
	return 0;

error:
	while (queue)
	{
		current = queue;
		queue = queue->next;
		free(current);
	}
	return ERROR_MEMORY;
}

unsigned char *map_visible_alloc(struct game *restrict game, unsigned player)
{
	size_t hexagons_total = hexagons_count(game);
	size_t i;

	unsigned char *bitmap_hexagons = malloc(BITMAP_SIZE(hexagons_total));
	if (!bitmap_hexagons)
		return 0;
	memset(bitmap_hexagons, 0, BITMAP_SIZE(hexagons_total));

	for(i = 0; i < hexagons_total; i += 1)
	{
		// Everything in a single hexagon always belongs to a single alliance.
		// Find which alliance is present in the hexagon.

		unsigned visibility = MAP_VISIBILITY;

		if (game->hexagons[i].buildings && allies(game, game->hexagons[i].owner, player))
		{
			if (game->hexagons[i].sight > visibility)
				visibility = game->hexagons[i].sight;
		}
		else
		{
			const struct array_troop_stacks *array = troop_stacks(game->hexagons + i, player);
			if (!array->count || !allies(game, array->data[0]->owner, player))
				continue;
		}

		bitmap_set(bitmap_hexagons, i, 1);
		map_visible_find(game, i, visibility, bitmap_hexagons);
	}

	return bitmap_hexagons;
}

void troop_move(struct troops *restrict source, struct troops *restrict target, size_t index)
{
	struct troop *troop = source->troops[index];
	size_t left = source->count - index - 1;

	if (left)
		memmove(source->troops + index, source->troops + index + 1, left * sizeof(*source->troops));
	source->count -= 1;

	target->troops[target->count++] = troop;
}

// Returns the troop stacks that player can see in hexagon.
struct array_troop_stacks *troop_stacks(struct hexagon *restrict hexagon, unsigned player)
{
	if (hexagon->buildings)
	{
		if (hexagon->owner == player)
			return &hexagon->private;
		else
			return &hexagon->public;
	}
	else if (hexagon->public.count && (hexagon->public.data[0]->owner == player))
		return &hexagon->private;
	else
		return &hexagon->public;
}
