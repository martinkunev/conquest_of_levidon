/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pthread.h>
#include <stdlib.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "world.h"
#include "interface.h"
#include "interface_storage.h"
#include "input.h"
#include "display.h"
#include "ui.h"
#include "ui_editor.h"
#include "edit.h"

#define WORLD_TEMP "/tmp/conquest_of_levidon_world"

#define WORLD_WIDTH 12
#define WORLD_HEIGHT 14

//#define WORLD_WIDTH 72
//#define WORLD_HEIGHT 42

#define S(s) (s), sizeof(s) - 1

// TODO support discard and exit from the editor

static int world_init(struct game *restrict game)
{
	size_t hexagons_total;
	size_t i, j;

	world_empty(game);

	game->players_count = 2;
	game->players[0].type = Neutral;
	game->players[0].treasury = (struct resources){0};
	game->players[0].alliance = 0;
	game->players[0].name_size = 0;
	game->players[1].type = Local;
	game->players[1].treasury = (struct resources){0};
	game->players[1].alliance = 1;
	game->players[1].name_size = 0;

	game->regions_count = 0;
	game->regions = malloc(1 * sizeof(*game->regions));
	if (!game->regions)
		goto error;
	region_init(game, game->regions + 0);
	game->regions_count = 1;

	game->map_width = WORLD_WIDTH;
	game->map_height = WORLD_HEIGHT;

	hexagons_total = hexagons_count(game);
	game->hexagons = malloc(hexagons_total * sizeof(*game->hexagons));
	if (!game->hexagons)
		goto error;
	for(i = 0; i < hexagons_total; i += 1)
	{
		game->hexagons[i].terrain = TERRAIN_GROUND;
		game->hexagons[i].info.region_index = 0;
		game->hexagons[i].environment = 0;

		game->hexagons[i].buildings = 0;
		game->hexagons[i].build_index = -1;
		game->hexagons[i].build_progress = 0;
		game->hexagons[i].vessel_progress = 0;

		for(j = 0; j < PLAYERS_LIMIT; j += 1)
			game->hexagons[i].private = (struct array_troop_stacks){0};
	}

	game->regions[0].garrison.hexagon = game->hexagons + 0;
	game->hexagons[0].environment = 1 << ENVIRONMENT_SETTLEMENT;

	return 0;

error:
	world_unload(game);
	return ERROR_MEMORY;
}

int main(int argc, char *argv[])
{
	struct game game;
	struct state state;
	int status;
	struct box box;

	if (argc < 2)
	{
		write(2, S("Usage: editor <image> [world]\n"));
		return 0;
	}

	if (if_init() < 0)
	{
		LOG_FATAL("Cannot initialize interface.");
		return -1;
	}

	if_load(argv[1]);

	status = ((argc > 2) ? world_load(argv[2], &game) : world_init(&game));
	if (status)
		return status;

	box = hexagons_box(game.map_width, game.map_height, HEXAGON_EDGE_EDITOR);
	MAP_WIDTH = box.width;
	MAP_HEIGHT = box.height;
	storage_init(MAP_WIDTH, MAP_HEIGHT);
	storage_hexagons(&game, MAP_WIDTH, MAP_HEIGHT, HEXAGON_EDGE_EDITOR);

	if_window_default();

	state.scroll_start = NAN;
	state.scroll_x = 0;
	state.scroll_y = 0;

	tool_switch_terrain(&game, &state);
	state.done = 0;

	while (1)
	{
		switch (state.tool)
		{
		case TOOL_TERRAIN:
			status = input_terrain(&game, &state);
			break;

		case TOOL_REGIONS:
			status = input_regions(&game, &state);
			break;

		case TOOL_OBJECTS:
			status = input_objects(&game, &state);
			break;
		}

		if (status < 0)
			break;

		if (state.done)
		{
			status = world_save(&game, WORLD_TEMP);
			if (status < 0)
			{
				LOG_ERROR("Error saving world!");
				continue;
			}
			break;
		}
	}

	storage_term();
	world_unload(&game);
	write(1, S("world written to " WORLD_TEMP "\n"));

	if_unload();
	if_term();

	return 0;
}
