/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "game.h"
#include "pathfinding.h"
#include "map.h"
#include "battle.h"
#include "movement.h"
#include "round.h"
#include "combat.h"

#define ARRAY_SOURCE
#define ARRAY_NAME array_pawns
#define ARRAY_TYPE struct pawn *
#include "generic/array.g"

#define FLOAT_ERROR 0.001

struct collision
{
	struct array_pawns pawns;
	bool enemy; // whether the pawn collides with an enemy
	bool slow; // whether the pawn is too slow to continue moving

	unsigned char fastest_speed; // speed of the fastest pawn colliding with the target pawn
	unsigned fastest_count; // number of fastest pawns colliding with the target pawn
};

static inline struct position position(const struct adjacency *restrict adj)
{
	return (struct position){.x = adj->x, .y = adj->y};
}

static inline int pawns_collide(struct position a, struct position b)
{
    return battlefield_distance(a, b) < (PAWN_RADIUS * 2);
}

static inline double cross_product_norm(double fx, double fy, double sx, double sy)
{
	// TODO this is for right-handed coordinate system
	return (fx * sy - sx * fy);
}

static inline int sign(double number)
{
	return ((number > 0) - (number < 0));
}

void pawn_place(struct pawn *restrict pawn, struct position position)
{
	pawn->position = position;
	pawn->command.moves.count = 0;
	pawn->path.count = 0;
}

void pawn_stay(struct pawn *restrict pawn)
{
	pawn->command.moves.count = 0;
	pawn->path.count = 0;
}

// Attach the vertex at position index to the graph by adding the necessary edges.
static int graph_vertex_attach(struct adjacency_list *restrict graph, size_t index, const struct obstacles *restrict obstacles)
{
	size_t node;
	struct position from, to;

	from = position(&graph->list[index]);

	for(node = 0; node < index; ++node)
	{
		to = position(&graph->list[node]);
		if (path_visible(from, to, obstacles))
		{
			struct neighbor *neighbor;
			double distance = battlefield_distance(from, to);

			neighbor = malloc(sizeof(*neighbor));
			if (!neighbor) return ERROR_MEMORY;
			neighbor->index = node;
			neighbor->distance = distance;
			neighbor->next = graph->list[index].neighbors;
			graph->list[index].neighbors = neighbor;

			neighbor = malloc(sizeof(*neighbor));
			if (!neighbor) return ERROR_MEMORY;
			neighbor->index = index;
			neighbor->distance = distance;
			neighbor->next = graph->list[node].neighbors;
			graph->list[node].neighbors = neighbor;
		}
	}

	return 0;
}

static void graph_vertex_add(struct adjacency_list *restrict graph, float x, float y, unsigned char occupied[static BATTLEFIELD_HEIGHT][BATTLEFIELD_WIDTH])
{
	struct adjacency *node;

	// Don't insert a vertex that is outside the battlefield.
	if ((x < 0) || (x > BATTLEFIELD_WIDTH) || (y < 0) || (y > BATTLEFIELD_HEIGHT))
		return;

	// Don't insert a vertex if the field is occupied.
	if (occupied[(size_t)y][(size_t)x])
		return;
	occupied[(size_t)y][(size_t)x] = 1;

	node = &graph->list[graph->count++];
	node->neighbors = 0;
	node->x = x;
	node->y = y;
}

// Returns the visibility graph as adjacency list.
struct adjacency_list *visibility_graph_build(const struct battle *restrict battle, const struct obstacles *restrict obstacles, unsigned vertices_reserved)
{
	size_t i, j;

	struct adjacency_list *graph, *graph_resized;

	unsigned char occupied[BATTLEFIELD_HEIGHT][BATTLEFIELD_WIDTH] = {0};
	for(i = 0; i < BATTLEFIELD_HEIGHT; ++i)
		for(j = 0; j < BATTLEFIELD_WIDTH; ++j)
			if (battle->field[i][j].blockage)
				occupied[i][j] = 1;

	// Allocate enough memory for the maximum size of the adjacency graph.
	// 4 vertices for the corners around each obstacle
	graph = malloc(offsetof(struct adjacency_list, list) + (obstacles->count * 4 + vertices_reserved) * sizeof(*graph->list));
	if (!graph) return 0;
	graph->count = 0;

	for(i = 0; i < obstacles->count; ++i)
	{
		const struct obstacle *restrict obstacle = obstacles->obstacle + i;
		graph_vertex_add(graph, obstacle->left, obstacle->bottom, occupied);
		graph_vertex_add(graph, obstacle->right, obstacle->bottom, occupied);
		graph_vertex_add(graph, obstacle->right, obstacle->top, occupied);
		graph_vertex_add(graph, obstacle->left, obstacle->top, occupied);
	}

	// Free the unused part of the adjacency graph buffer.
	// Leave space for origin and target vertices.
	graph_resized = realloc(graph, offsetof(struct adjacency_list, list) + sizeof(*graph->list) * (graph->count + vertices_reserved));
	if (!graph_resized) graph_resized = graph;

	// Fill the adjacency list of the visibility graph.
	// Consider that no vertex is connected to itself.
	for(i = 1; i < graph_resized->count; ++i)
		if (graph_vertex_attach(graph_resized, i, obstacles) < 0)
		{
			graph_term(graph_resized);
			return 0;
		}

	// The origin and target vertices will be set by pathfinding functions.
	return graph_resized;
}

// WARNING: The space for the new vertex must have been reserved by visibility_graph_build().
static ssize_t graph_insert(struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles, struct position position)
{
	int status;

	size_t index = graph->count++;
	struct adjacency *node = &graph->list[index];

	node->neighbors = 0;
	node->x = position.x;
	node->y = position.y;

	status = graph_vertex_attach(graph, index, obstacles);
	if (status < 0) return status;

	return index;
}

// Removes the vertex at position index from the graph.
static void graph_remove(struct adjacency_list *restrict graph, size_t index)
{
	size_t i;
	struct neighbor *neighbor, *prev;

	// Remove node from neighbors lists.
	for(i = 0; i < index; ++i)
	{
		neighbor = graph->list[i].neighbors;
		prev = 0;
		while (neighbor)
		{
			if (neighbor->index == index)
			{
				if (prev)
				{
					prev->next = neighbor->next;
					free(neighbor);
					neighbor = prev->next;
				}
				else
				{
					graph->list[i].neighbors = neighbor->next;
					free(neighbor);
					neighbor = graph->list[i].neighbors;
				}
			}
			else
			{
				prev = neighbor;
				neighbor = prev->next;
			}
		}
	}

	// Remove the neighbors of the node.
	neighbor = graph->list[index].neighbors;
	while (neighbor)
	{
		prev = neighbor;
		neighbor = neighbor->next;
		free(prev);
	}

	graph->count -= 1;
}

static double heuristic_battlefield(float x0, float y0, float x1, float y1)
{
	// TODO maybe work with distance^2 instead of distance to avoid doing sqrt

	struct position a = {x0, y0};
	struct position b = {x1, y1};
	return battlefield_distance(a, b);
}

// Stores path from the pawn's current position to destination in pawn->path.
// On error, returns error code and pawn movement queue remains unchanged.
int path_battle_find(struct pawn *restrict pawn, struct position destination, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles)
{
	ssize_t vertex_target, vertex_origin;
	struct path_node *traverse_info;
	struct path_node *node, *prev, *next;
	size_t hops_count;
	int status;

	vertex_target = graph_insert(graph, obstacles, destination);
	if (vertex_target < 0)
		return vertex_target;

	vertex_origin = graph_insert(graph, obstacles, pawn->position);
	if (vertex_origin < 0)
	{
		graph_remove(graph, vertex_target);
		return vertex_origin;
	}

	// Look for a path from the pawn's position to the target vertex.
	traverse_info = path_traverse(graph, vertex_origin, vertex_target, 0, &heuristic_battlefield);
	if (!traverse_info)
	{
		status = ERROR_MEMORY;
		goto finally;
	}
	if (traverse_info[vertex_target].distance == INFINITY)
	{
		status = ERROR_MISSING;
		goto finally;
	}

	// Construct the path to target by starting from target and reversing the path_link pointers.
	node = traverse_info + vertex_target;
	next = 0;
	hops_count = 0;
	while (1)
	{
		prev = node->path_link;
		node->path_link = next;
		next = node;

		if (!prev) break; // there is no previous node, so node is the origin
		node = prev;

		hops_count += 1;
	}

	// Add the selected vertices to the movement queue.
	pawn->path.count = 0;
	array_path_expand(&pawn->path, hops_count);
	while (node = node->path_link)
		pawn->path.data[pawn->path.count++] = position(&graph->list[node - traverse_info]);

	status = 0;

finally:
	free(traverse_info);
	graph_remove(graph, vertex_origin);
	graph_remove(graph, vertex_target);
	return status;
}

// Returns the shortest distance between the pawn and destination. On error, returns NAN.
static double path_distance(struct pawn *restrict pawn, struct position destination, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles)
{
	ssize_t vertex_target, vertex_origin;
	struct path_node *traverse_info;
	double result;

	vertex_target = graph_insert(graph, obstacles, destination);
	if (vertex_target < 0) return NAN;

	vertex_origin = graph_insert(graph, obstacles, pawn->position);
	if (vertex_origin < 0)
	{
		graph_remove(graph, vertex_target);
		return NAN;
	}

	// Look for a path from the pawn's position to the target vertex.
	traverse_info = path_traverse(graph, vertex_origin, vertex_target, 0, &heuristic_map);
	result = traverse_info ? traverse_info[vertex_target].distance : NAN;

finally:
	free(traverse_info);
	graph_remove(graph, vertex_origin);
	graph_remove(graph, vertex_target);
	return result;
}

int path_distances(const struct pawn *restrict pawn, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles, struct reachable *restrict reachable)
{
	ssize_t vertex_origin;
	struct path_node *traverse_info;
	size_t x, y;
	int status;

	// TODO maybe use pawn->command.moves.data[pawn->command.moves.count - 1] for start vertex
	vertex_origin = graph_insert(graph, obstacles, pawn->position);
	if (vertex_origin < 0)
		return vertex_origin;

	// Hack: Look for a path from to the non-existent target graph->count (so that all vertices are traversed).
	traverse_info = path_traverse(graph, vertex_origin, graph->count, 0, 0);
	if (!traverse_info)
	{
		status = ERROR_MEMORY;
		goto finally;
	}

	// Find which tiles are visible from any graph vertex.
	// Store the least distance to each visible tile.
	// TODO this is slow; maybe use a queue of tiles to visit and check only the distance to them (if a tile is reachable, add its neighbors to the queue?)
	for(y = 0; y < sizeof(reachable->distance) / sizeof(*reachable->distance); y += 1)
	{
		for(x = 0; x < sizeof(*reachable->distance) / sizeof(**reachable->distance); x += 1)
		{
			size_t i;
			struct position target = {0.5 * x + 0.5, 0.5 * y + 0.5};

			if (path_visible(pawn->position, target, obstacles))
				reachable->distance[y][x] = battlefield_distance(pawn->position, target);
			else // no straight line path
			{
				assert(vertex_origin == graph->count - 1);
				for(i = 0; i < vertex_origin; ++i)
				{
					double distance = traverse_info[i].distance;
					if ((distance < INFINITY) && path_visible(position(&graph->list[i]), target, obstacles))
					{
						distance += battlefield_distance(position(&graph->list[i]), target);
						if (distance < reachable->distance[y][x])
							reachable->distance[y][x] = distance;
					}
				}
			}
		}
	}

	status = 0;

finally:
	free(traverse_info);
	graph_remove(graph, vertex_origin);
	return status;
}

int pawn_move_queue(struct pawn *restrict pawn, struct position target, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles)
{
	int status;

	if (pawn->command.moves.count == PATH_QUEUE_LIMIT)
		return ERROR_INPUT;

	if (!in_battlefield(target.x, target.y))
		return ERROR_INPUT;

	if (pawn->command.moves.count)
	{
		// Check if the target is reachable.
		double distance = path_distance(pawn, target, graph, obstacles);
		if (isnan(distance))
			return ERROR_MEMORY;
		if (distance == INFINITY)
			return ERROR_MISSING;
	}
	else if (status = path_battle_find(pawn, target, graph, obstacles))
		return status;

	pawn->command.moves.data[pawn->command.moves.count++] = target;
	return 0;
}

static int movement_find_next(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict pawn, struct position position, struct adjacency_list *restrict graph, const struct obstacles *restrict obstacles)
{
	struct position destination;

	// Determine the next destination.
	if (pawn->command.moves.count)
		destination = pawn->command.moves.data[0];
	else switch (pawn->command.action)
	{
	case ACTION_FIGHT:
		// TODO move closer if possible?
		if (can_fight(position, pawn->command.target.pawn))
			return 0;
		destination = pawn->command.target.pawn->position;
		break;

	case ACTION_ASSAULT:
		if (can_assault(position, pawn->command.target.field))
			return 0;
		destination = (struct position){pawn->command.target.field->tile.x, pawn->command.target.field->tile.y};
		break;

	case ACTION_GUARD:
		{
			// Find the closest enemy pawn.
			double distance, distance_min = INFINITY;
			const struct pawn *restrict enemy = 0;
			for(size_t j = 0; j < battle->pawns_count; ++j)
			{
				const struct pawn *restrict other = battle->pawns + j;
				if (!other->count)
					continue;
				if (allies(game, pawn->owner, other->owner))
					continue;
				distance = battlefield_distance(position, other->position);
				if (distance < distance_min)
				{
					distance_min = distance;
					enemy = other;
				}
			}

			// TODO maybe the pawn should look for the next closest pawn if this one is too far
			if (enemy && (battlefield_distance(pawn->command.target.position, enemy->position) <= guard_radius(pawn->troop->unit->speed)))
				destination = enemy->position;
			else if (!position_eq(position, pawn->command.target.position))
				destination = pawn->command.target.position;
			else
				return 0; // nothing to do for this pawn
		}
		break;

	default:
		return 0; // nothing to do for this pawn
	}

	switch (path_battle_find(pawn, destination, graph, obstacles))
	{
	case ERROR_MEMORY:
		return ERROR_MEMORY;

	case ERROR_MISSING: // cannot reach destination at this time
	default: // path found
		return 0;
	}
}

// Calculates the expected position of each pawn at the next step.
int movement_plan(const struct game *restrict game, struct battle *restrict battle)
{
	size_t i;
	for(i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *pawn = battle->pawns + i;
		struct position position = pawn->position;
		double distance, distance_covered;

		distance_covered = (double)pawn->troop->unit->speed / BATTLE_MOVEMENT_STEPS;

		if ((pawn->command.action == ACTION_SHOOT) && !pawn->troop->unit->ranged.shoot_moving)
			continue;

		// Delete path if it corresponds to a target that may have moved.
		if (!pawn->command.moves.count) // path is automatically generated
			switch (pawn->command.action)
			{
			case ACTION_GUARD:
			case ACTION_FIGHT:
			case ACTION_ASSAULT:
				pawn->path.count = 0;
			}

		// Determine which is the move in progress and how much of the distance is covered.
		while (1)
		{
			// Make sure pawn path is precalculated.
			if (!pawn->path.count)
			{
				int status = movement_find_next(game, battle, pawn, position, battle->graph[pawn->owner], battle->obstacles);
				if (status < 0)
					return status;
				if (!pawn->path.count)
				{
					// The pawn has nowhere to move right now.
					pawn->position_next = position;
					break;
				}
			}

			distance = battlefield_distance(position, pawn->path.data[0]);
			if (distance_covered < distance)
			{
				// Calculate the next position of the pawn.
				double progress = distance_covered / distance;
				pawn->position_next.x = position.x * (1 - progress) + pawn->path.data[0].x * progress;
				pawn->position_next.y = position.y * (1 - progress) + pawn->path.data[0].y * progress;
				break;
			}

			distance_covered -= distance;
			position = pawn->path.data[0];

			// Remove the position just reached from the path queue.
			pawn->path.count -= 1;
			if (pawn->path.count)
				memmove(pawn->path.data, pawn->path.data + 1, pawn->path.count * sizeof(*pawn->path.data));
			else if (pawn->command.moves.count) // path corresponds to user-specified move
			{
				// Remove the position just reached from the moves queue.
				pawn->command.moves.count -= 1;
				if (pawn->command.moves.count)
					memmove(pawn->command.moves.data, pawn->command.moves.data + 1, pawn->command.moves.count * sizeof(*pawn->command.moves.data));
			}
		}
	}

	return 0;
}

// Detects which pawns collide with the specified pawn and collects statistics about fastest pawns in the collision.
// TODO ?implement a faster algorithm: http://stackoverflow.com/questions/36401562/find-overlapping-circles
static int collisions_detect(const struct game *restrict game, const struct battle *restrict battle, const struct pawn *restrict pawn, struct collision *restrict collision)
{
	collision->fastest_speed = pawn->troop->unit->speed;
	collision->fastest_count = 1;

	for(size_t i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *other = battle->pawns + i;

		if (!other->count)
			continue;
		if (other == pawn)
			continue;

		if (!pawns_collide(pawn->position_next, other->position_next))
			continue;

		if (array_pawns_expand(&collision->pawns, collision->pawns.count + 1) < 0)
			return ERROR_MEMORY;
		collision->pawns.data[collision->pawns.count++] = other;

		if (allies(game, pawn->owner, other->owner))
		{
			if (position_eq(other->position, other->position_next)) // the pawn collides with a stationary pawn
				continue; // collision resolution doesn't affect stationary pawns

			// Update statistics about fastest pawn with the data of the colliding pawn.
			if (other->troop->unit->speed > collision->fastest_speed)
			{
				collision->fastest_speed = other->troop->unit->speed;
				collision->fastest_count = 1;
			}
			else if (other->troop->unit->speed == collision->fastest_speed)
			{
				collision->fastest_count += 1;
			}
		}
		else collision->enemy = true;
	}

	if (collision->fastest_speed > pawn->troop->unit->speed)
		collision->slow = true;

	return 0;
}

int movement_collisions_resolve(const struct game *restrict game, struct battle *restrict battle, unsigned step)
{
	struct collision *restrict collisions;
	size_t i;
	int status;

	// Each pawn can forsee what will happen in one movement step if nothing changes and can adjust its movement to react to that.
	// If a pawn forsees a collision with an enemy, it will stay at its current position to fight the surrounding enemies.

	// Pawns that forsee a collision with allies will try to make an alternative movement in order to avoid the collision and continue to their destination. Slower pawns will stop and wait for the faster pawns to pass. If the alternative movement of the faster pawn still leads to a collision, that pawn will stay at its current position.

	collisions = malloc(battle->pawns_count * sizeof(*collisions));
	if (!collisions) return ERROR_MEMORY;
	memset(collisions, 0, battle->pawns_count * sizeof(*collisions));
	for(i = 0; i < battle->pawns_count; ++i)
		collisions[i].pawns = (struct array_pawns){0};

	// Modify next positions of pawns until all positions are certain. Positions that are certain cannot be changed.
	// A position being certain is indicated by pawn->position == pawn->position_next.

	// TODO can I optimize this?

	while (1)
	{
		bool changed = false;

		for(i = 0; i < battle->pawns_count; ++i)
		{
			struct pawn *restrict pawn = battle->pawns + i;

			if (!pawn->count)
				continue; // skip dead pawns

			collisions[i].pawns.count = 0;

			if (position_eq(pawn->position, pawn->position_next))
				continue; // skip non-moving pawns

			status = collisions_detect(game, battle, pawn, collisions + i);
			if (status < 0)
				goto finally;

			if (collisions[i].enemy || collisions[i].slow) changed = true;
		}

		if (!changed) break; // break if there are no more collisions to handle here

		// Each pawn that would collide with an enemy will instead stay at its current position.
		// Each pawn that would collide with a faster ally will instead stay at its current position.
		for(i = 0; i < battle->pawns_count; ++i)
		{
			if (collisions[i].enemy)
			{
				battle->pawns[i].position_next = battle->pawns[i].position;
				collisions[i].enemy = false;
			}

			if (collisions[i].slow)
				battle->pawns[i].position_next = battle->pawns[i].position;
		}
	}

	// Now there are only collisions between moving pawns and (other moving pawns with the same speed or non-moving pawns).

	// Try to resolve pawn collisions by modifying paths of the fastest pawns.
	// If the collision is not easy to resolve, stop the pawn from moving.
	for(i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *restrict pawn = battle->pawns + i;
		const double distance_covered = (double)pawn->troop->unit->speed / BATTLE_MOVEMENT_STEPS;

		if (!collisions[i].pawns.count)
			continue;

		// TODO support resolving more complicated collisions (pawns should be smarter)

		if ((collisions[i].fastest_count == 1) && (collisions[i].pawns.count == 1))
		{
			struct pawn *obstacle = collisions[i].pawns.data[0];
			struct position moves[2];
			unsigned moves_count;

			// Find the moves in direction of the tangent of the obstacle pawn.
			// Choose one move randomly from the possible moves.
			moves_count = path_moves_tangent(pawn, obstacle, distance_covered, moves);
			if (moves_count)
				pawn->position_next = moves[random() % moves_count];
		}
		else if ((collisions[i].fastest_count == 2) && (collisions[i].pawns.count == 1))
		{
			struct pawn *obstacle = collisions[i].pawns.data[0];
			struct position moves[2];
			unsigned moves_count;

			int obstacle_side, move_side;

			double direction_x = pawn->position_next.x - pawn->position.x;
			double direction_y = pawn->position_next.y - pawn->position.y;

			// Find the moves in direction of the tangent of the obstacle pawn.
			// Choose the move on the opposite side of the obstacle movement direction with respect to pawn movement direction.

			obstacle_side = sign(cross_product_norm(obstacle->position_next.x - pawn->position.x, obstacle->position_next.y - pawn->position.y, direction_x, direction_y));

			moves_count = path_moves_tangent(pawn, obstacle, distance_covered, moves);
			while (moves_count--)
			{
				move_side = sign(cross_product_norm(moves[moves_count].x - pawn->position.x, moves[moves_count].y - pawn->position.y, direction_x, direction_y));
				if (obstacle_side * move_side <= 0)
				{
					pawn->position_next = moves[moves_count];
					break;
				}
			}
		}
		else
		{
			// There is no easy way to resolve the collision. Make the pawn stay at its current position.
			pawn->position_next = pawn->position;
		}
	}

	// Look for collisions and stop pawns that would collide until all collisions are resolved.
	while (1)
	{
		bool ready = true;

		for(i = 0; i < battle->pawns_count; ++i)
		{
			struct pawn *restrict pawn = battle->pawns + i;

			if (!pawn->count)
				continue; // skip dead pawns
			if (position_eq(pawn->position, pawn->position_next))
				continue; // skip non-moving pawns

			collisions[i].pawns.count = 0;
			status = collisions_detect(game, battle, pawn, collisions + i);
			if (status < 0)
				goto finally;

			if (collisions[i].pawns.count)
				ready = false;
		}

		if (ready) break; // break if there are no more collisions

		// Each pawn that would collide will instead stay at its current position.
		for(i = 0; i < battle->pawns_count; ++i)
			if (collisions[i].pawns.count)
				battle->pawns[i].position_next = battle->pawns[i].position;
	}

	// Update current pawn positions.
	for(i = 0; i < battle->pawns_count; ++i)
	{
		if (position_eq(battle->pawns[i].position_next, battle->pawns[i].position))
			continue; // skip non-moving pawns

		battle->pawns[i].position = battle->pawns[i].position_next;
		battle->pawns[i].stop_time = step;
	}

	status = 0;

finally:
	for(i = 0; i < battle->pawns_count; ++i)
		free(collisions[i].pawns.data);
	free(collisions);

	return status;
}

// Determines the relative position of a point and a line described by a vector.
// Returns 1 if the point is on the right side, -1 if the point is on the left side and 0 if the point is on the line.
static int point_side(struct position p, struct position v0, struct position v1)
{
	float value = (v1.y - v0.y) * p.x + (v0.x - v1.x) * p.y + v1.x * v0.y - v0.x * v1.y;
	if (value > 0) return 1;
	else if (value < 0) return -1;
	else return 0;
}

// Determines whether a move is blocked by an obstacle.
// The obstacle doesn't block moves right next to it ("touching" the obstacle is allowed).
static int move_blocked_obstacle(struct position start, struct position end, const struct obstacle *restrict obstacle)
{
	// http://stackoverflow.com/a/293052/515212

	int side;
	int sides[3] = {0};

	// Determine relative position of the right-top corner of the obstacle.
	side = point_side((struct position){obstacle->right, obstacle->top}, start, end);
	sides[side + 1] = 1;

	// Determine relative position of the left-top corner of the obstacle.
	side = point_side((struct position){obstacle->left, obstacle->top}, start, end);
	sides[side + 1] = 1;

	// Determine relative position of the left-bottom corner of the obstacle.
	side = point_side((struct position){obstacle->left, obstacle->bottom}, start, end);
	sides[side + 1] = 1;

	// Determine relative position of the left-top corner of the obstacle.
	side = point_side((struct position){obstacle->right, obstacle->bottom}, start, end);
	sides[side + 1] = 1;

	if (sides[0] && sides[2]) // if there are corners on both sides of the path
	{
		if ((start.x >= obstacle->right) && (end.x >= obstacle->right)) return 0;
		if ((start.x <= obstacle->left) && (end.x <= obstacle->left)) return 0;
		if ((start.y <= obstacle->top) && (end.y <= obstacle->top)) return 0;
		if ((start.y >= obstacle->bottom) && (end.y >= obstacle->bottom)) return 0;

		return 1;
	}

	return 0;
}

// Checks whether a pawn can see and move directly from origin to target (there are no obstacles in-between).
int path_visible(struct position origin, struct position target, const struct obstacles *restrict obstacles)
{
	// Check if there is an obstacle that blocks the path from origin to target.
	for(size_t i = 0; i < obstacles->count; ++i)
		if (move_blocked_obstacle(origin, target, obstacles->obstacle + i))
			return 0;

	return 1;
}

// WARNING: The move must be a non-zero vector (end != start).
#if defined(UNIT_TESTING)
int move_blocked_pawn(struct position start, struct position end, struct position pawn, double radius)
{
	// http://stackoverflow.com/a/1084899/515212

	// Change the coordinate system so that the path starts from (0, 0).
	end.x -= start.x;
	end.y -= start.y;
	pawn.x -= start.x;
	pawn.y -= start.y;

	double a = end.x * end.x + end.y * end.y;
	double b = -2 * (pawn.x * end.x + pawn.y * end.y);
	double c = pawn.x * pawn.x + pawn.y * pawn.y - radius * radius;
	double discriminant = b * b - 4 * a * c;

	if (discriminant < 0) return 0;

	discriminant = sqrt(discriminant);
	double solution0 = (- b - discriminant) / (2 * a);
	double solution1 = (- b + discriminant) / (2 * a);

	if ((solution0 >= 0) && (solution0 <= 1)) return 1;
	if ((solution1 >= 0) && (solution1 <= 1)) return 1;

	return 0;
}
#endif

/*
// Calculate the position of the pawn in the next round.
// TODO this function assumes that pawn->command.moves.count == 1 and pawn->path are calculated for that path
struct position movement_position(const struct pawn *restrict pawn)
{
	struct position position = pawn->position;
	size_t move_current = 0;
	double distance_covered = pawn->troop->unit->speed, distance;
	double progress;

	// Determine which is the move in progress and how much of the distance is covered.
	while (1)
	{
		if (move_current == pawn->path.count)
			return position;

		distance = battlefield_distance(position, pawn->path.data[move_current]);
		if (distance_covered < distance) // unfinished move
			break;

		distance_covered -= distance;
		position = pawn->path.data[move_current];
		move_current += 1;
	}

	progress = distance_covered / distance;
	position.x = position.x * (1 - progress) + pawn->path.data[move_current].x * progress;
	position.y = position.y * (1 - progress) + pawn->path.data[move_current].y * progress;
	return position;
}
*/

// Sets a move with the specified distance in the direction of the specified point, if that direction does not oppose the original pawn direction.
static unsigned pawn_move_set(struct position *restrict move, const struct pawn *restrict pawn, double distance, double x, double y, struct position origin)
{
	double old_x = pawn->position_next.x - pawn->position.x;
	double old_y = pawn->position_next.y - pawn->position.y;

	// Change movement direction away from the obstacle to prevent rounding error leading to collision.
	x *= 1 + FLOAT_ERROR;
	y *= 1 + FLOAT_ERROR;

	// Determine movement direction vector.
	x -= origin.x;
	y -= origin.y;

	// Use the sign of the dot product to determine whether the angle between the old and the new direction is less than 90 degrees.
	if (x * old_x + y * old_y > -FLOAT_ERROR)
	{
		double length = sqrt(x * x + y * y);
		x = pawn->position.x + x * distance / length;
		y = pawn->position.y + y * distance / length;

		if (in_battlefield(x, y))
		{
			move->x = x;
			move->y = y;
			return 1;
		}

		return 0;
	}

	return 0;
}

// Initializes the possible one-step moves for the given pawn in a tangent direction to the obstacle pawn.
unsigned path_moves_tangent(const struct pawn *restrict pawn, const struct pawn *restrict obstacle, double distance_covered, struct position moves[static restrict 2])
{
	unsigned moves_count = 0;

	const double r = PAWN_RADIUS * 2; // the two pawns must not overlap
	const double r2 = r * r;

	// Find a point (x, y) on the tangent that is at distance r from the tangent point. To do that use the following relations:
	// The points (x, y), the center of the obstacle circle and the tangent point form an isosceles right triangle.
	// The oigin point, the tangent point and (x, y) lie on the tangent. The tangent point is between the other two.

	double x, y;
	double distance2, distance_tangent_point, discriminant, minus_b, temp;

	// Change the coordinate system so that the obstacle circle is at the origin.
	struct position position = {pawn->position.x - obstacle->position.x, pawn->position.y - obstacle->position.y};

	distance2 = position.x * position.x + position.y * position.y;
	distance_tangent_point = ((distance2 >= r2) ? sqrt(distance2 - r2) : 0); // handle the case when the argument is negative due to rounding errors

	// Solutions:
	//  x = (r2 * position.x - r * position.x * distance_tangent_point +- r * position.y * sqrt(distance2 + 2 * r * distance_tangent_point)) / distance2
	//  y = sqrt(2 * r2 - x * x)
	// where r2 - position.x * x - r * distance_tangent_point == position.y * y
	//  x = (r2 * position.x - r * position.x * distance_tangent_point +- r * position.y * sqrt(distance2 + 2 * r * distance_tangent_point)) / distance2
	//  y = - sqrt(2 * r2 - x * x)
	// where r2 - position.x * x - r * distance_tangent_point == - position.y * y

	minus_b = r2 * position.x - position.x * r * distance_tangent_point;
	discriminant = r * position.y * sqrt(distance2 + 2 * r * distance_tangent_point);

	x = (minus_b - discriminant) / distance2;
	y = ((2 * r2 >= x * x) ? sqrt(2 * r2 - x * x) : 0); // handle the case when the argument is negative due to rounding errors
	temp = r2 - position.x * x - r * distance_tangent_point; // exclude false roots that appeared when squaring the original equation
	if (fabs(temp - position.y * y) < FLOAT_ERROR)
		moves_count += pawn_move_set(moves + moves_count, pawn, distance_covered, x, y, position);
	if (fabs(temp + position.y * y) < FLOAT_ERROR)
		moves_count += pawn_move_set(moves + moves_count, pawn, distance_covered, x, -y, position);

	if (discriminant) // don't add the same moves twice
	{
		x = (minus_b + discriminant) / distance2;
		y = ((2 * r2 >= x * x) ? sqrt(2 * r2 - x * x) : 0); // handle the case when the argument is negative due to rounding errors
		temp = r2 - position.x * x - r * distance_tangent_point; // exclude false roots that appeared when squaring the original equation
		if (fabs(temp - position.y * y) < FLOAT_ERROR)
			moves_count += pawn_move_set(moves + moves_count, pawn, distance_covered, x, y, position);
		if (fabs(temp + position.y * y) < FLOAT_ERROR)
			moves_count += pawn_move_set(moves + moves_count, pawn, distance_covered, x, -y, position);
	}

	assert(moves_count <= 2);
	return moves_count;
}
