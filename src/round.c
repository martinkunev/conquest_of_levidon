/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "game.h"
#include "pathfinding.h"
#include "map.h"
#include "battle.h"
#include "movement.h"
#include "round.h"
#include "combat.h"

#define STEPS_DAMAGE 16

// Returns whether a pawn can pass through a field.
int battlefield_passable(const struct battlefield *restrict field)
{
	switch (field->blockage)
	{
	case 0:
		return 1;

	case BLOCKAGE_GATE:
		return field->open;

	default:
		return 0;
	}
}

static inline void obstacle_insert(struct obstacles *obstacles, float left, float right, float top, float bottom)
{
	obstacles->obstacle[obstacles->count] = (struct obstacle){left - PAWN_RADIUS, right + PAWN_RADIUS, top - PAWN_RADIUS, bottom + PAWN_RADIUS};
	obstacles->count += 1;
}

// Finds the obstacles on the battlefield. Constructs and returns a list of the obstacles.
struct obstacles *path_obstacles_alloc(const struct game *restrict game, const struct battle *restrict battle)
{
	size_t x, y;

	struct obstacles *obstacles;
	size_t obstacles_count = 0;

	size_t i;
	float horizontal = -1; // start coordinate of the last detected horizontal wall
	float vertical[BATTLEFIELD_WIDTH]; // start coordinate of the last detected vertical walls
	for(i = 0; i < BATTLEFIELD_WIDTH; ++i)
		vertical[i] = -1;

	// Count the obstacles.
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
	{
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			const struct battlefield *field = &battle->field[y][x];
			unsigned char blockage_location = (battlefield_passable(field) ? 0 : field->blockage_location) & POSITION;

			if (horizontal >= 0)
			{
				if (blockage_location != (blockage_location | POSITION_LEFT | POSITION_RIGHT))
				{
					obstacles_count += 1;
					horizontal = (blockage_location & POSITION_RIGHT) ? x : -1.0;
				}
			}
			else
			{
				if (blockage_location & POSITION_RIGHT)
					horizontal = x;
				else if (blockage_location & POSITION_LEFT)
					obstacles_count += 1;
			}
			if (vertical[x] >= 0)
			{
				if (blockage_location != (blockage_location | POSITION_TOP | POSITION_BOTTOM))
				{
					obstacles_count += 1;
					vertical[x] = (blockage_location & POSITION_BOTTOM) ? y : -1.0;
				}
			}
			else
			{
				if (blockage_location & POSITION_BOTTOM)
					vertical[x] = y;
				else if (blockage_location & POSITION_TOP)
					obstacles_count += 1;
			}
		}

		if (horizontal >= 0)
		{
			obstacles_count += 1;
			horizontal = -1;
		}
	}
	for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
	{
		if (vertical[x] >= 0)
		{
			obstacles_count += 1;
			vertical[x] = -1;
		}
	}

	obstacles = malloc(sizeof(*obstacles) + obstacles_count * sizeof(*obstacles->obstacle));
	if (!obstacles) return 0;
	obstacles->count = 0;

	// Insert the obstacles.
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
	{
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			const struct battlefield *field = &battle->field[y][x];
			unsigned char blockage_location = (battlefield_passable(field) ? 0 : field->blockage_location) & POSITION;

			if (horizontal >= 0)
			{
				if (blockage_location != (blockage_location | POSITION_LEFT | POSITION_RIGHT))
				{
					obstacle_insert(obstacles, horizontal, x + (1 - WALL_OFFSET) * ((blockage_location & POSITION_LEFT) != 0), y + WALL_OFFSET, y + (1 - WALL_OFFSET));
					horizontal = (blockage_location & POSITION_RIGHT) ? (x + WALL_OFFSET) : -1.0;
				}
			}
			else
			{
				if (blockage_location & POSITION_RIGHT)
					horizontal = x + WALL_OFFSET * !(blockage_location & POSITION_LEFT);
				else if (blockage_location & POSITION_LEFT)
					obstacle_insert(obstacles, x, x + (1 - WALL_OFFSET), y + WALL_OFFSET, y + (1 - WALL_OFFSET));
			}
			if (vertical[x] >= 0)
			{
				if (blockage_location != (blockage_location | POSITION_TOP | POSITION_BOTTOM))
				{
					obstacle_insert(obstacles, x + WALL_OFFSET, x + (1 - WALL_OFFSET), vertical[x], y + (1 - WALL_OFFSET) * ((blockage_location & POSITION_TOP) != 0));
					vertical[x] = (blockage_location & POSITION_BOTTOM) ? (y + WALL_OFFSET) : -1.0;
				}
			}
			else
			{
				if (blockage_location & POSITION_BOTTOM)
					vertical[x] = y + WALL_OFFSET * !(blockage_location & POSITION_TOP);
				else if (blockage_location & POSITION_TOP)
					obstacle_insert(obstacles, x + WALL_OFFSET, x + (1 - WALL_OFFSET), y, y + (1 - WALL_OFFSET));
			}
		}

		if (horizontal >= 0)
		{
			obstacle_insert(obstacles, horizontal, BATTLEFIELD_WIDTH, y + WALL_OFFSET, y + (1 - WALL_OFFSET));
			horizontal = -1;
		}
	}
	for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
	{
		if (vertical[x] >= 0)
		{
			obstacle_insert(obstacles, x + WALL_OFFSET, x + (1 - WALL_OFFSET), vertical[x], BATTLEFIELD_HEIGHT);
			vertical[x] = -1;
		}
	}

	return obstacles;
}

static void index_add(struct battle *restrict battle, int x, int y, struct pawn *restrict pawn)
{
	struct battlefield *restrict field;
	size_t i;

	if (pawn_waiting(pawn) || (x < 0) || (x >= BATTLEFIELD_WIDTH) || (y < 0) || (y >= BATTLEFIELD_HEIGHT))
		return;

	field = &battle->field[y][x];

	for(i = 0; field->pawns[i]; ++i)
		assert(i + 1 < sizeof(field->pawns) / sizeof(*field->pawns));
	field->pawns[i] = pawn;
}

// Add pawn pointer in each battle field the pawn intersects.
void round_prepare_pawn(struct battle *restrict battle, struct pawn *restrict pawn)
{
	struct position position = pawn->position;
	int x = position.x;
	int y = position.y;

	index_add(battle, x, y, pawn);

	if (position.y - y < PAWN_RADIUS)
		index_add(battle, x, y - 1, pawn);
	else if (y + 1 - position.y < PAWN_RADIUS)
		index_add(battle, x, y + 1, pawn);

	if (position.x - x < PAWN_RADIUS)
		index_add(battle, x - 1, y, pawn);
	else if (x + 1 - position.x < PAWN_RADIUS)
		index_add(battle, x + 1, y, pawn);

	if (battlefield_distance(position, (struct position){x, y}) < PAWN_RADIUS)
		index_add(battle, x - 1, y - 1, pawn);
	else if (battlefield_distance(position, (struct position){x + 1, y + 1}) < PAWN_RADIUS)
		index_add(battle, x + 1, y + 1, pawn);

	if (battlefield_distance(position, (struct position){x + 1, y}) < PAWN_RADIUS)
		index_add(battle, x + 1, y - 1, pawn);
	else if (battlefield_distance(position, (struct position){x, y + 1}) < PAWN_RADIUS)
		index_add(battle, x - 1, y + 1, pawn);
}

int round_prepare(const struct game *restrict game, struct battle *restrict battle)
{
	size_t x, y;
	size_t i;

	// Clear index of pawns by battle field.
	// Reset gate commands.
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			struct battlefield *field = &battle->field[y][x];
			for(i = 0; i < sizeof(field->pawns) / sizeof(*field->pawns); ++i)
				field->pawns[i] = 0;

			field->gate_switch = 0;
		}

	// Build index of pawns by battle field.
	for(i = 0; i < battle->pawns_count; ++i)
		round_prepare_pawn(battle, battle->pawns + i);

	battle->obstacles = path_obstacles_alloc(game, battle);
	if (!battle->obstacles)
		goto error;

	// Initialize movement graph.
	for(i = 0; i < game->players_count; i += 1)
	{
		if (battle->players[i].state != PLAYER_ALIVE)
			continue;

		battle->graph[i] = visibility_graph_build(battle, battle->obstacles, 2); // 2 vertices (for origin and target)
		if (!battle->graph[i])
			goto error;
	}

	return 0;

error:
	while (i--)
		graph_term(battle->graph[i]);
	free(battle->obstacles);
	battle->obstacles = 0;
	return ERROR_MEMORY;
}

int round_move(const struct game *restrict game, struct battle *restrict battle)
{
	unsigned step;
	int status;
	size_t x, y;
	size_t i, j;

	battle->animation.steps = 0;

	for(i = 0; i < battle->pawns_count; i += 1)
	{
		battle->animation.pawns[i].visible = (battle->pawns[i].count && !pawn_waiting(battle->pawns + i));

		battle->pawns[i].stop_time = 0;
	}

	// Perform pawn movement in steps.
	// Invariant: On each step there are no overlapping pawns.
	for(step = 0; step < BATTLE_MOVEMENT_STEPS; ++step)
	{
		// Remember the position of each pawn for the animation.
		for(i = 0; i < battle->pawns_count; i += 1)
		{
			battle->animation.pawns[i].position[step] = battle->pawns[i].position;
			if (step && !position_eq(battle->animation.pawns[i].position[step - 1], battle->animation.pawns[i].position[step]))
				battle->animation.steps = step + 1;
		}

		// Plan the movement of each pawn.
		status = movement_plan(game, battle);
		if (status < 0)
			return status;

		// Detect collisions caused by moving pawns and resolve them by modifying pawn movement.
		// Set final position of each pawn.
		status = movement_collisions_resolve(game, battle, step + 1);
		if (status < 0)
			return status;
	}

	// Make sure shoot animation is shown for long enough.
	if (battle->animation.steps < battle->animation.steps_damage[VICTIMS_RANGED])
		battle->animation.steps = battle->animation.steps_damage[VICTIMS_RANGED];

	// Remember field states for the animation.
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_WIDTH; ++x)
		{
			struct battlefield *field = &battle->field[y][x];
			battle->animation.field[y][x].blockage = field->blockage;
			battle->animation.field[y][x].blockage_location = field->blockage_location;
			battle->animation.field[y][x].open = field->open;
		}

	// Open/close gates.
	for(i = 0; i < battle->gates_count; i += 1)
	{
		struct battlefield *field = battle->gates[i];
		struct position gate = {field->tile.x + 0.5, field->tile.y + 0.5};

		if (field->blockage != BLOCKAGE_GATE)
			continue; // gate destroyed

		for(j = 0; j < battle->pawns_count; j += 1)
			if (battlefield_distance(battle->pawns[j].position, gate) < PAWN_RADIUS)
				goto next;
		field->open ^= field->gate_switch;
next:
		;
	}

	return 0;
}

static unsigned deaths(const struct pawn *restrict pawn)
{
	unsigned deaths_max, deaths_min;
	unsigned hurt_withstand;
	unsigned deaths_certain;
	double deaths_actual;

	// Maximum deaths are achieved when the damage is concentrated to single troops.
	deaths_max = pawn->hurt / pawn->troop->unit->health;

	// Each attacker can kill at most 1 troop.
	if (deaths_max > pawn->attackers) deaths_max = pawn->attackers;

	// Minimum deaths are achieved when the damage is spread between troops.
	hurt_withstand = (pawn->troop->unit->health - 1) * pawn->count;
	deaths_min = ((pawn->hurt > hurt_withstand) ? (pawn->hurt - hurt_withstand) : 0);

	// Check if the minimum number of deaths is enough to kill all troops.
	if (deaths_min > pawn->count)
		return pawn->count;

	// Attackers tend to attack vulnerable targets which ensures a minimum number of deaths.
	deaths_certain = deaths_max / 2;
	if (deaths_min < deaths_certain) deaths_min = deaths_certain;

	assert(deaths_max >= deaths_min);
	deaths_actual = deaths_min + random() % (deaths_max - deaths_min + 1);
	return ((deaths_actual > pawn->count) ? pawn->count : deaths_actual);
}

int round_cleanup(const struct game *restrict game, struct battle *restrict battle, enum victim_index victim_index)
{
	size_t p;
	size_t x, y;

	int activity = 0;
	unsigned steps_damage = 0;

	// Remove destroyed obstacles.
	for(y = 0; y < BATTLEFIELD_HEIGHT; ++y)
		for(x = 0; x < BATTLEFIELD_HEIGHT; ++x)
		{
			struct battlefield *restrict field = &battle->field[y][x];
			if (((field->blockage == BLOCKAGE_WALL) || (field->blockage == BLOCKAGE_GATE) || (field->blockage == BLOCKAGE_TOWER)) && !field->strength)
			{
				// Fortification destroyed.
				activity = 1;
				field->blockage = BLOCKAGE_NONE;
			}
		}

	// Calculate casualties and remove dead pawns.
	for(p = 0; p < battle->pawns_count; ++p)
	{
		struct pawn *pawn = battle->pawns + p;
		unsigned dead;

		if (!pawn->count)
			continue;

		if (dead = deaths(pawn))
		{
			activity = 1;
			steps_damage = STEPS_DAMAGE;

			pawn->hurt -= dead * pawn->troop->unit->health;
			pawn->count -= dead;

			battle->animation.pawns[p].victims[victim_index] = dead;
		}
		else
			battle->animation.pawns[p].victims[victim_index] = 0;

		pawn->attackers = 0;
	}
	battle->animation.steps_damage[victim_index] = steps_damage;

	// Reset pawn action if the target is no longer valid.
	for(p = 0; p < battle->pawns_count; ++p)
	{
		struct pawn *pawn = battle->pawns + p;
		if (!pawn->count)
			continue;

		// Stop pawns from attacking dead pawns and destroyed obstacles.
		if ((pawn->command.action == ACTION_SHOOT) && !can_shoot(game, battle, pawn, pawn->command.target.position))
			pawn->command.action = 0;
		if ((pawn->command.action == ACTION_FIGHT) && !pawn->command.target.pawn->count)
			pawn->command.action = 0;
		if ((pawn->command.action == ACTION_ASSAULT) && !pawn->command.target.field->blockage)
			pawn->command.action = 0;

		// TODO what if the target pawn is no longer reachable?
	}

	return activity;
}

void round_term(const struct game *restrict game, struct battle *restrict battle)
{
	size_t p;
	for(p = 0; p < game->players_count; p += 1)
	{
		graph_term(battle->graph[p]);
		battle->graph[p] = 0;
	}
	free(battle->obstacles);
	battle->obstacles = 0;

	battle->round += 1;
}
