/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "basic.h"
#include "log.h"
#include "game.h"
#include "map.h"
#include "turn.h"
#include "economy.h"
#include "world.h"
#include "players.h"
#include "battle.h"
#include "round.h"
#include "combat.h"
#include "menu.h"
#include "interface.h"
#include "interface_storage.h"
#include "display.h"
#include "ui.h"
#include "ui_menu.h"
#include "ui_report.h"
#include "ui_battle.h"

#define S(s) s, sizeof(s) - 1

#define WINNER_NOBODY -1
#define WINNER_BATTLE -2

unsigned MAP_WIDTH, MAP_HEIGHT;

/*
Invariants at the beginning of a turn:
- no enemy troops are located in neighboring hexagons (except if in garrison)
- all troops in a garrison are owned by the player owning the garrison
- troops can only assault garrison in the region where they are located
*/

#if 0
enum {ROUNDS_STALE_LIMIT_OPEN = 10, ROUNDS_STALE_LIMIT_ASSAULT = 20};







	battle.round = 1;
	round_activity_last = 1;

	while ((winner = battle_end(game, &battle)) < 0)
	{
		const struct obstacles *obstacles[PLAYERS_LIMIT] = {0};
		struct adjacency_list *graph[PLAYERS_LIMIT] = {0};

		unsigned step;
		size_t i;

		unsigned char alliance_neutral = game->players[PLAYER_NEUTRAL].alliance;

		// TODO if there are no local players, resolve the battle automatically

		obstacles[alliance_neutral] = path_obstacles_alloc(game, &battle, PLAYER_NEUTRAL);
		if (!obstacles[alliance_neutral]) abort();
		graph[alliance_neutral] = visibility_graph_build(&battle, obstacles[alliance_neutral], 2); // 2 vertices for origin and target
		if (!graph[alliance_neutral]) abort();

		battlefield_index_build(&battle);

		for(size_t player = 0; player < game->players_count; ++player)
		{
			size_t alliance = game->players[player].alliance;

			if (!obstacles[alliance])
			{
				obstacles[alliance] = path_obstacles_alloc(game, &battle, player);
				if (!obstacles[alliance]) abort();
			}
			graph[player] = visibility_graph_build(&battle, obstacles[alliance], 2); // 2 vertices for origin and target
			if (!graph[player])
				abort();
		}

		// Ask each player to give commands to their pawns.
		status = players_battle(game, &battle, obstacles, graph);
		if (status < 0)
			goto finally;

		// Deal damage from shooters.
		input_animation_shoot(game, &battle);
		combat_ranged(&battle, obstacles[alliance_neutral]); // treat all gates as closed for shooting
		if (battlefield_clean(game, &battle)) round_activity_last = battle.round;

		// Perform pawn movement in steps.
		// Invariant: Before and after each step there are no overlapping pawns.
		for(step = 0; step < MOVEMENT_STEPS; ++step)
		{
			// TODO open a gate if a pawn passes through it; close it at the end of the round

			// Remember the position of each pawn because it is necessary for the movement animation.
			for(i = 0; i < battle.pawns_count; ++i)
				movements[i][step] = battle.pawns[i].position;

			// Plan the movement of each pawn.
			if (movement_plan(game, &battle, graph, obstacles) < 0)
				abort(); // TODO

			// Detect collisions caused by moving pawns and resolve them by modifying pawn movement.
			// Set final position of each pawn.
			if (movement_collisions_resolve(game, &battle) < 0)
				abort(); // TODO
		}
		// Remember the final position of each pawn because it is necessary for the movement animation.
		for(i = 0; i < battle.pawns_count; ++i)
			movements[i][step] = battle.pawns[i].position;

		input_animation_move(game, &battle, movements);

		// TODO input_animation_fight()
		combat_melee(game, &battle);
		if (battlefield_clean(game, &battle)) round_activity_last = battle.round;

		for(i = 0; i < PLAYERS_LIMIT; ++i)
		{
			free((void *)obstacles[i]); // TODO fix this cast
			graph_term(graph[i]);
		}

		// Cancel the battle if nothing is killed/destroyed for a certain number of rounds.
		if ((battle.round - round_activity_last) >= ((battle_type == BATTLE_ASSAULT) ? ROUNDS_STALE_LIMIT_ASSAULT : ROUNDS_STALE_LIMIT_OPEN))
		{
			// Attacking troops retreat to the region they came from.
			for(i = 0; i < battle.pawns_count; ++i)
			{
				struct troop *restrict troop;

				if (!battle.pawns[i].count) continue;

				troop = battle.pawns[i].troop;
				if (game->players[troop->owner].alliance != battle.defender)
					troop->move = troop->location;
			}

			winner = battle.defender;
			break;
		}

		battle.round += 1;
	}

	input_report_battle(game, &battle);

finally:
	free(movements);
	battlefield_term(game, &battle);
	return winner;
}
#endif

static int play_battle(struct game *restrict game, struct battle_info *restrict info)
{
	struct battle battle;
	int winner;
	int status;

	unsigned round_activity_last = 0;

	switch (info->type)
	{
	case BATTLE_OPEN:
		battle.assault = 0;
		break;

	case BATTLE_ASSAULT:
		battle.assault = 1;
		break;

	default:
		assert(0);
	}
	if (battle_init(game, &battle, info) < 0)
		return ERROR_MEMORY;

	if (game->players_local_count >= 2)
	{
		status = battle_announce(game, &battle);
		if (status < 0)
			goto finally;
	}

	while ((winner = battle_end(game, &battle)) < 0)
	{
		// TODO check if there is a local player in the battle; otherwise, resolve automatically

		status = round_prepare(game, &battle);
		if (status < 0)
			goto finally;

		if (battle.round)
		{
			status = players_formation(game, &battle);
			if (status < 0)
				goto finally;
		}

		// Ask each player to give commands to their pawns.
		status = players_battle(game, &battle);
		if (status < 0)
			goto finally;

		// Deal damage from shooters.
		combat_ranged(game, &battle);
		if (round_cleanup(game, &battle, VICTIMS_RANGED))
			round_activity_last = battle.round;

		status = round_move(game, &battle);
		if (status < 0)
			goto finally;

		combat_melee(game, &battle);
		if (round_cleanup(game, &battle, VICTIMS_MELEE))
			round_activity_last = battle.round;

		status = input_animation(game, &battle);
		if (status < 0)
			goto finally;

		// TODO ? // Cancel the battle if nothing is killed/destroyed for a certain number of rounds.

		round_term(game, &battle);
	}

	// TODO
	// after battle, some troop stacks may have "retreat" flag set; they try to go to a nearby hexagon with no enemy nearby; if they don't succeed they are dismissed
	// some troop stacks may surrender (due to low morale); they are dismissed; "retreat" is set by the player while surrender is automatic

	status = battle_report(game, &battle, winner);

finally:
	battle_term(game, &battle);
	return status;
}

// Returns whether there is a winner. On error, returns error code.
static int play(struct game *restrict game)
{
	uint32_t alliances = 0; // this limits the alliance numbers to the number of bits
	int status;

	status = players_init(game);
	if (status < 0)
		return status;

	do
	{
		size_t index;
		struct battles *battles;

		turn_prepare(game);

		economy_income(game, game->hexagons_total);

		// TODO autosave for debugging; remove
		world_save(game, "/home/martin/.conquest_of_levidon/save/autosave");

		// Ask each player to perform map actions.
		status = players_map(game);
		if (status < 0)
			goto finally;

		status = turn_move(game);
		if (status < 0)
			goto finally;

		battles = turn_battles_init(game);
		if (!battles)
			goto finally;
		// TODO optimmize battle order to reduce battle time and wait time for players (when there are remote players)
		for(index = 0; index < battles->count; index += 1)
		{
			// status = (manual_open ? play_battle(game, region, battle_info[index].type) : calculate_battle(game, region, 0));
			status = play_battle(game, battles->battle + index);
			if (status < 0)
			{
				turn_battles_term(game, battles);
				goto finally;
			}
		}
		// TODO assert(no troops of 2 players on the same hexagon);
		turn_battles_term(game, battles);

		// Start riots if happiness is too low.
		turn_riot(game);

		turn_players(game, game->hexagons_total);

		// Pay expenses and calculate income for next turn.
		economy_turn(game);

		if (turn_orders(game) < 0)
			goto finally;

		// Determine which alliances are not defeated.
		// TODO move this into a function (e.g. players_update())
		alliances = 0;
		game->players_local_count = 0;
		for(index = 0; index < game->players_count; ++index)
		{
			// Set dead players as neutral.
			if (!game->alive[index])
				game->players[index].type = Neutral;

			switch (game->players[index].type)
			{
			case Neutral:
				continue;

			case Local:
				game->players_local[game->players_local_count++] = index;
				break;
			}

			// Remember which alliances are alive.
			alliances |= (1 << game->players[index].alliance);
		}

		if (!game->players_local_count) // no more human-controlled players
		{
			status = 0;
			goto finally;
		}

		game->turn += 1;
	} while (alliances & (alliances - 1)); // while there is more than 1 alliance alive

	status = alliances;

finally:
	players_term(game);
	return status;
}

int main(int argc, char *argv[])
{
	struct game game;
	struct box box;
	int status;

	assert(PLAYERS_LIMIT <= 24);
	assert(!(PLAYERS_LIMIT % 2));
	assert(REGIONS_LIMIT <= 4096);
	assert(PLAYER_NEUTRAL == 0);

	assert(!sigaction(SIGPIPE, &(struct sigaction){.sa_handler = SIG_IGN}, 0));

	srandom(time(0));

	menu_init();

	if (if_init() < 0)
	{
		LOG_FATAL("Cannot initialize interface.");
		return -1;
	}
	if_load(0);
	if_window_default();

	while (1)
	{
		status = input_menu_load(&game);
		if (status < 0)
			return status;

		// Initialize region input recognition.
		box = hexagons_box(game.map_width, game.map_height, HEXAGON_EDGE);
		MAP_WIDTH = box.width;
		MAP_HEIGHT = box.height;
		storage_init(MAP_WIDTH, MAP_HEIGHT);
		storage_hexagons(&game, MAP_WIDTH, MAP_HEIGHT, HEXAGON_EDGE);
		if_window_default();

		status = play(&game);
		/*if (status >= 0)
			input_report_map(&game);*/

		storage_term();
		world_unload(&game);

		if (status == ERROR_CANCEL)
			continue;
		else if (status < 0)
		{
			LOG_FATAL("Error %u");
			return status;
		}
	}

	if_unload();
	if_term();

	menu_term();

	return 0;
}
