/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>

#include <GL/gl.h>

#include "basic.h"
#include "log.h"
#include "image.h"
#include "font.h"
#include "display.h"

struct polygon_draw
{
	const struct vector *point;
	enum {Wait, Ear, Straight} class;
	struct polygon_draw *prev, *next;
};

static inline long cross_product(int fx, int fy, int sx, int sy)
{
	return (fx * sy - sx * fy);
}

static int in_triangle(struct vector p, struct vector a, struct vector b, struct vector c)
{
	// TODO think about equality

	// ab x ap
	int sign0 = cross_product(b.x - a.x, b.y - a.y, p.x - a.x, p.y - a.y) > 0; // TODO can this overflow?

	// bc x bp
	int sign1 = cross_product(c.x - b.x, c.y - b.y, p.x - b.x, p.y - b.y) > 0; // TODO can this overflow?

	// ca x cp
	int sign2 = cross_product(a.x - c.x, a.y - c.y, p.x - c.x, p.y - c.y) > 0; // TODO can this overflow?

	return ((sign0 == sign1) && (sign1 == sign2));
}

static int is_ear(const struct polygon_draw *prev, const struct polygon_draw *curr, const struct polygon_draw *next)
{
	// Check vector product sign in order to find out if the angle between the 3 points is reflex or straight.
	long product = cross_product(prev->point->x - curr->point->x, prev->point->y - curr->point->y, next->point->x - curr->point->x, next->point->y - curr->point->y); // TODO can this overflow?
	if (product < 0) return Wait;
	else if (product == 0) return Straight;

	// Check if there is a vertex in the triangle.
	const struct polygon_draw *item;
	for(item = next->next; item != prev; item = item->next)
		if (in_triangle(*item->point, *prev->point, *curr->point, *next->point))
			return Wait;

	return Ear;
}

void draw_line(int x0, int y0, int x1, int y1, const unsigned char color[4])
{
	glColor4ubv(color);

	glBegin(GL_LINE_LOOP);
	glVertex2i(x0, y0);
	glVertex2i(x1, y1);
	glEnd();
}

void fill_circle(int x, int y, unsigned radius, const unsigned char color[4])
{
	unsigned steps = (unsigned)(radius * TAU + 0.5), step;

	glColor4ubv(color);
	glBegin(GL_POLYGON);
	for(step = 0; step < steps; ++step)
	{
		double angle = step * TAU / steps;
		//glVertex2i(x + cos(angle) * (radius + 0.5), y + sin(angle) * (radius + 0.5));
		glVertex2i(x + cos(angle) * radius, y + sin(angle) * radius);
	}
	glEnd();
}

void fill_rectangle(int x, int y, unsigned width, unsigned height, const unsigned char color[4])
{
	glColor4ubv(color);

	glBegin(GL_QUADS);
	glVertex2i(x + width, y + height);
	glVertex2i(x + width, y);
	glVertex2i(x, y);
	glVertex2i(x, y + height);
	glEnd();
}

void draw_rectangle(int x, int y, unsigned width, unsigned height, const unsigned char color[4])
{
	// http://stackoverflow.com/questions/10040961/opengl-pixel-perfect-2d-drawing

	glColor4ubv(color);

	glBegin(GL_LINE_LOOP);
	glVertex2f(x + width - 0.5, y + height - 0.5);
	glVertex2f(x + width - 0.5, y + 0.5);
	glVertex2f(x + 0.5, y + 0.5);
	glVertex2f(x + 0.5, y + height - 0.5);
	glEnd();
}

void draw_polygon(const struct polygon *restrict polygon, int offset_x, int offset_y, const unsigned char color[static 4], double scale)
{
	size_t i;

	glColor4ubv(color);

	glBegin(GL_LINE_STRIP);
	for(i = 0; i < polygon->vertices_count; ++i)
		glVertex2f(offset_x + (unsigned)(polygon->points[i].x * scale + 0.5), offset_y + (unsigned)(polygon->points[i].y * scale + 0.5));
	glEnd();
}

// Display a region as a polygon, using ear clipping.
int fill_polygon(const struct polygon *restrict polygon, int offset_x, int offset_y, const unsigned char color[static 4], double scale)
{
	// assert(polygon->vertices_count > 2);

	size_t vertices_left = polygon->vertices_count;
	size_t i;

	// Initialize cyclic linked list with the polygon's vertices.
	struct polygon_draw *draw = malloc(vertices_left * sizeof(*draw));
	if (!draw)
		return ERROR_MEMORY;
	draw[0].point = polygon->points;
	draw[0].prev = draw + vertices_left - 1;
	for(i = 1; i < vertices_left; ++i)
	{
		draw[i].point = polygon->points + i;
		draw[i].prev = draw + i - 1;
		draw[i - 1].next = draw + i;
	}
	draw[vertices_left - 1].next = draw;

	struct polygon_draw *vertex;
	vertex = draw;
	do
	{
		vertex->class = is_ear(vertex->prev, vertex, vertex->next);
		vertex = vertex->next;
	} while (vertex != draw);

	glColor4ubv(color);

	while (vertices_left > 3)
	{
		// find a triangle to draw
		switch (vertex->class)
		{
		case Ear:
			break;

		case Straight:
			vertices_left -= 1;
			vertex->prev->next = vertex->next;
			vertex->next->prev = vertex->prev;
		default:
			vertex = vertex->next;
			continue;
		}

		glBegin(GL_POLYGON);
		glVertex2f(offset_x + (unsigned)(vertex->prev->point->x * scale + 0.5), offset_y + (unsigned)(vertex->prev->point->y * scale + 0.5));
		glVertex2f(offset_x + (unsigned)(vertex->point->x * scale + 0.5), offset_y + (unsigned)(vertex->point->y * scale + 0.5));
		glVertex2f(offset_x + (unsigned)(vertex->next->point->x * scale + 0.5), offset_y + (unsigned)(vertex->next->point->y * scale + 0.5));
		glEnd();

		// clip the triangle from the polygon
		vertices_left -= 1;
		vertex->prev->next = vertex->next;
		vertex->next->prev = vertex->prev;
		vertex = vertex->next;

		// update the class of the neighboring triangles
		vertex->prev->class = is_ear(vertex->prev->prev, vertex->prev, vertex);
		vertex->class = is_ear(vertex->prev, vertex, vertex->next);
	}

	glBegin(GL_POLYGON);
	glVertex2f(offset_x + (unsigned)(vertex->prev->point->x * scale + 0.5), offset_y + (unsigned)(vertex->prev->point->y * scale + 0.5));
	glVertex2f(offset_x + (unsigned)(vertex->point->x * scale + 0.5), offset_y + (unsigned)(vertex->point->y * scale + 0.5));
	glVertex2f(offset_x + (unsigned)(vertex->next->point->x * scale + 0.5), offset_y + (unsigned)(vertex->next->point->y * scale + 0.5));
	glEnd();

	free(draw);

	return 0;
}

struct box string_box(struct slice string, struct font *restrict font)
{
	struct box box = {0, font->size};
	while (string.size--)
	{
		struct advance advance = font->advance[(size_t)string.data[string.size]];
		box.width += advance.x;
		if (advance.y > box.height)
			box.height = advance.y;
	}
	return box;
}

void frame_center(struct frame *restrict frame, struct vector center, unsigned width, unsigned height, struct vector offset, double orientation)
{
	// The object is represented as a width*height rectangle at (offset_x, offset_y) from center, rotated by orientation radians.

	// Calculate the relative position of the bounding box before rotation.
	double left = offset.x - width / 2.0;
	double right = offset.x + width / 2.0;
	double top = -offset.y + height / 2.0;
	double bottom = -offset.y - height / 2.0;

	double sino = sin(orientation);
	double coso = cos(orientation);

	// Calculate the absolute position of each corner after rotation.
	frame->tr = (struct vector){center.x + right * coso - top * sino, center.y - (right * sino + top * coso)};
	frame->tl = (struct vector){center.x + left * coso - top * sino, center.y - (left * sino + top * coso)};
	frame->bl = (struct vector){center.x + left * coso - bottom * sino, center.y - (left * sino + bottom * coso)};
	frame->br = (struct vector){center.x + right * coso - bottom * sino, center.y - (right * sino + bottom * coso)};

	frame->width = width;
	frame->height = height;
}

void draw_string_cursor(struct box box, int x, int y, struct font *restrict font, const unsigned char color[static 4])
{
	glColor4ubv(color);

	glBegin(GL_LINES);
	glVertex2f(x + box.width + 1.5, y);
	glVertex2f(x + box.width + 1.5, y + font->size + 1.0);
	glEnd();
}

void draw_string(struct slice string, int x, int y, struct font *restrict font, const unsigned char color[static 4])
{
	glEnable(GL_TEXTURE_2D);

	glPushMatrix();
	glLoadIdentity();
	glTranslatef(x, y, 0);

	glListBase(font->base);
	glColor4ubv(color);
	glCallLists(string.size, GL_UNSIGNED_BYTE, string.data);

	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}

// Fill image at a specified location. The repeat parameter determines whether to to repeat or scale.
void fill_image(const struct image *restrict image, int x, int y, unsigned width, unsigned height, int repeat)
{
//	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glColor4ub(255, 255, 255, 255); // TODO why is this necessary
	glBindTexture(GL_TEXTURE_2D, image->texture);

	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);

	if (repeat)
	{
		glTexCoord2d((double)width / image->width, (double)height / image->height);
		glVertex2f(x + (int)width, y + (int)height);

		glTexCoord2d(0, (double)height / image->height);
		glVertex2f(x, y + (int)height);

		glTexCoord2d(0, 0);
		glVertex2f(x, y);

		glTexCoord2d((double)width / image->width, 0);
		glVertex2f(x + (int)width, y);
	}
	else // scale
	{
		glTexCoord2d(1, 1);
		glVertex2f(x + (int)image->width, y + (int)image->height);

		glTexCoord2d(0, 1);
		glVertex2f(x, y + (int)image->height);

		glTexCoord2d(0, 0);
		glVertex2f(x, y);

		glTexCoord2d(1, 0);
		glVertex2f(x + (int)image->width, y);
	}

	glEnd();
	glDisable(GL_TEXTURE_2D);
}

void fill_image_hexagon(const struct image *restrict image, int x, int y, unsigned char points[6])
{
	static const float pos[][2] =
	{
		1, 0.75,
		1, 0.25,
		0.5, 0,
		0, 0.25,
		0, 0.75,
		0.5, 1,
		1, 0.75,
	};
	size_t from = 0;

	glColor4ub(255, 255, 255, 255); // TODO why is this necessary
	glBindTexture(GL_TEXTURE_2D, image->texture);

	glEnable(GL_TEXTURE_2D);

	do
	{
		for(; !points[from]; from += 1)
			if (from == 5)
			{
				glDisable(GL_TEXTURE_2D);
				return;
			}

		glBegin(GL_POLYGON);

		glTexCoord2d(0.5, 0.5);
		glVertex2f(x + image->width * 0.5, y + image->height * 0.5);

		while (1)
		{
			glTexCoord2d(pos[from][0], pos[from][1]);
			glVertex2f(x + image->width * pos[from][0], y + image->height * pos[from][1]);

			from += 1;
			if (from == 6)
				break;
			if (!points[from])
				break;
		}

		glTexCoord2d(pos[from][0], pos[from][1]);
		glVertex2f(x + image->width * pos[from][0], y + image->height * pos[from][1]);

		glEnd();
	} while (from < 6);

	glDisable(GL_TEXTURE_2D);
}

// Fill non-masked pixels with a color (scale if necessary).
void fill_image_mask(const struct image *restrict image, int x, int y, unsigned width, unsigned height, const unsigned char color[static 4])
{
//	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glColor4ub(color[0], color[1], color[2], color[3]);
	glBindTexture(GL_TEXTURE_2D, image->texture);

	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);

	glTexCoord2d(1, 1);
	glVertex2f(x + (int)image->width, y + (int)image->height);

	glTexCoord2d(0, 1);
	glVertex2f(x, y + (int)image->height);

	glTexCoord2d(0, 0);
	glVertex2f(x, y);

	glTexCoord2d(1, 0);
	glVertex2f(x + (int)image->width, y);

	glEnd();
	glDisable(GL_TEXTURE_2D);
}

// Fill the frame with the image (scale if necessary).
void frame_image(const struct image *restrict image, struct frame *restrict frame)
{
	glColor4ub(255, 255, 255, 255); // TODO why is this necessary
	glBindTexture(GL_TEXTURE_2D, image->texture);

	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);

	glTexCoord2d(1, 1);
	glVertex2f(frame->br.x, frame->br.y);

	glTexCoord2d(0, 1);
	glVertex2f(frame->bl.x, frame->bl.y);

	glTexCoord2d(0, 0);
	glVertex2f(frame->tl.x, frame->tl.y);

	glTexCoord2d(1, 0);
	glVertex2f(frame->tr.x, frame->tr.y);

	glEnd();
	glDisable(GL_TEXTURE_2D);
}
