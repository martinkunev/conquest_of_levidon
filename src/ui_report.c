/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <X11/keysym.h>

#include "basic.h"
#include "format.h"
#include "game.h"
#include "map.h"
#include "battle.h"
#include "image.h"
#include "interface.h"
#include "input.h"
#include "display.h"
#include "ui.h"
#include "ui_menu.h"
#include "ui_map.h"
#include "ui_report.h"

#define MESSAGE_WIDTH 384
#define MESSAGE_HEIGHT 192

#define MARGIN 4
#define TITLE_OFFSET 16
#define BODY_OFFSET 50

#define TITLE_Y 40
#define PLAYERS_Y 80
#define READY_Y 150
#define FLAGS_LINE 8

#define WINNERS_X 16
#define LOSERS_X 528
#define PARTICIPANTS_Y 80
#define FLAGS_Y 120
#define VICTIMS_MARGIN 60

#define BUTTON_READY_X (CONTENT_X + (CONTENT_WIDTH - MESSAGE_WIDTH) / 2 + (MESSAGE_WIDTH - BUTTON_WIDTH) / 2)
#define BUTTON_READY_Y (CONTENT_Y + (CONTENT_HEIGHT - MESSAGE_HEIGHT) / 2 + 128 + 22)

#define S(s) (s), sizeof(s) - 1

struct state_battle
{
	const struct game *game;
	struct slice title;

	struct
	{
		unsigned victims[UNITS_COUNT];
		enum {Absent = 0, Winner, Loser} status;
	} info[PLAYERS_LIMIT];

	unsigned char winners[PLAYERS_LIMIT], losers[PLAYERS_LIMIT];
	size_t winners_count, losers_count;
};

static void show_report_player(const void *argument, double progress)
{
	const struct state_report *state = argument;

	struct box box;
	unsigned x, y;

	show_panel(state->game, state->players[0]);

	x = CONTENT_X + (CONTENT_WIDTH - MESSAGE_WIDTH) / 2;
	y = CONTENT_Y + (CONTENT_HEIGHT - MESSAGE_HEIGHT) / 2;

	fill_rectangle(x, y, MESSAGE_WIDTH, MESSAGE_HEIGHT, display_colors[Player + state->players[0]]);
	fill_image(&image_panel, x + MARGIN, y + MARGIN, MESSAGE_WIDTH - 2 * MARGIN, MESSAGE_HEIGHT - 2 * MARGIN, 1);

	box = string_box(state->title, &font24);
	x += (MESSAGE_WIDTH - box.width - MARGIN - image_flag.width) / 2;
	draw_string(state->title, x, y + TITLE_OFFSET, &font24, display_colors[Text]);
	display_flag(x + box.width + MARGIN, y + TITLE_OFFSET, state->players[0]);

	display_button(slice("Ready"), BUTTON_READY_X, BUTTON_READY_Y, 0);
}

static void show_battle_announce(const void *argument, double progress)
{
	const struct state_report *state = argument;
	struct box box;
	unsigned x, y;
	size_t i;

	x = CONTENT_X + (CONTENT_WIDTH - MESSAGE_WIDTH) / 2;
	y = CONTENT_Y + (CONTENT_HEIGHT - MESSAGE_HEIGHT) / 2;

	fill_rectangle(x, y, MESSAGE_WIDTH, MESSAGE_HEIGHT, display_colors[Player + PLAYER_NEUTRAL]);
	fill_image(&image_panel, x + MARGIN, y + MARGIN, MESSAGE_WIDTH - 2 * MARGIN, MESSAGE_HEIGHT - 2 * MARGIN, 1);

	box = string_box(state->title, &font24);
	x += (MESSAGE_WIDTH - box.width - MARGIN) / 2;
	draw_string(state->title, x, y + TITLE_OFFSET, &font24, display_colors[Text]);

	for(i = 0; i < state->players_count; i += 1)
		display_flag(x + (i % FLAGS_LINE) * image_flag.width, y + BODY_OFFSET + (i / FLAGS_LINE) * image_flag.height, state->players[i]);

	display_button(slice("Ready"), BUTTON_READY_X, BUTTON_READY_Y, 0);
}

static void show_report_players(const void *argument, double progress)
{
	const struct state_report *state = argument;

	struct box box;
	unsigned x, y;
	size_t i;

	box = string_box(state->title, &font24);
	draw_string(state->title, (WINDOW_WIDTH - box.width) / 2, TITLE_Y, &font24, display_colors[TextMenu]);

	if (state->players_count >= FLAGS_LINE)
		x = (WINDOW_WIDTH - FLAGS_LINE * image_flag.width) / 2;
	else
		x = (WINDOW_WIDTH - state->players_count * image_flag.width) / 2;
	y = PLAYERS_Y;

	for(i = 0; i < state->players_count; i += 1)
		display_flag(x + (i % FLAGS_LINE) * FLAGS_LINE, y + (i / FLAGS_LINE) * image_flag.height, state->players[i]);

	display_button(slice("Ready"), (WINDOW_WIDTH - BUTTON_WIDTH) / 2, READY_Y, 0);
}

static void show_report_battle(const void *argument, double progress)
{
	const struct state_battle *state = argument;
	struct box box;
	size_t i, j;

	const struct slice winners = slice("Winners"), losers = slice("Losers");

	box = string_box(state->title, &font24);
	draw_string(state->title, (WINDOW_WIDTH - box.width) / 2, TITLE_Y, &font24, display_colors[TextMenu]);

	box = string_box(winners, &font12);
	draw_string(winners, WINNERS_X, PARTICIPANTS_Y, &font12, display_colors[TextMenu]);

	for(i = 0; i < state->winners_count; i += 1)
	{
		unsigned offset = WINNERS_X + VICTIMS_MARGIN;

		display_flag(WINNERS_X, FLAGS_Y + i * 40, state->winners[i]);

		for(j = 0; j < UNITS_COUNT; j += 1)
		{
			int victims = state->info[state->winners[i]].victims[j];
			if (victims)
				display_troop(UNITS + j, -victims, Player + state->winners[i], offset, FLAGS_Y + i * 40, TextMenu);
			offset += 40;
		}
	}

	box = string_box(losers, &font12);
	draw_string(losers, LOSERS_X, PARTICIPANTS_Y, &font12, display_colors[TextMenu]);

	for(i = 0; i < state->losers_count; i += 1)
	{
		unsigned offset = LOSERS_X + VICTIMS_MARGIN;

		display_flag(LOSERS_X, FLAGS_Y + i * 40, state->losers[i]);

		for(j = 0; j < UNITS_COUNT; j += 1)
		{
			int victims = state->info[state->losers[i]].victims[j];
			if (victims)
				display_troop(UNITS + j, -victims, Player + state->losers[i], offset, FLAGS_Y + i * 40, TextMenu);
			offset += 40;
		}
	}
}

static int input_none(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	return INPUT_IGNORE;
}

static int input_skip(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_report *state = argument;
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;
	state->skip = 1;
	return INPUT_TERMINATE;
}

static int input_end(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	if (code != XK_Return)
		return INPUT_IGNORE;
	return INPUT_TERMINATE;
}

static int input_menu(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_report *state = argument;
	const struct game *restrict game = state->game;
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;
	return input_menu_save(game);
}

int input_report_player(struct state_report *restrict state)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH,
			.top = 0,
			.bottom = WINDOW_HEIGHT,
			.callback = input_end,
		},
		{
			.left = BUTTON_MENU_X,
			.right = BUTTON_MENU_X + BUTTON_WIDTH,
			.top = BUTTONS_Y,
			.bottom = BUTTONS_Y + BUTTON_HEIGHT,
			.callback = input_menu,
		},
		{
			.left = BUTTON_END_X,
			.right = BUTTON_END_X + BUTTON_WIDTH,
			.top = BUTTONS_Y,
			.bottom = BUTTONS_Y + BUTTON_HEIGHT,
			.callback = input_skip,
		},
		{
			.left = BUTTON_READY_X,
			.right = BUTTON_READY_X + BUTTON_WIDTH,
			.top = BUTTON_READY_Y,
			.bottom = BUTTON_READY_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate,
		}
	};

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_report_player, 0, state);
}

int input_report_players(struct state_report *restrict state)
{
	struct area areas[] = {
		{
			.left = (WINDOW_WIDTH - BUTTON_WIDTH) / 2,
			.right = (WINDOW_WIDTH + BUTTON_WIDTH) / 2,
			.top = READY_Y,
			.bottom = READY_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate,
		}
	};

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_report_player, 0, state);
}

int battle_announce(const struct game *restrict game, const struct battle *restrict battle)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH,
			.top = 0,
			.bottom = WINDOW_HEIGHT,
			.callback = input_end,
		},
		{
			.left = BUTTON_READY_X,
			.right = BUTTON_READY_X + BUTTON_WIDTH,
			.top = BUTTON_READY_Y,
			.bottom = BUTTON_READY_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate,
		}
	};
	size_t i;

	struct state_report state;

	state.game = game;
	state.title = slice("Prepare for battle");
	state.players_count = 0;
	for(i = 0; i < game->players_count; i += 1)
		if ((game->players[i].type == Local) && (battle->players[i].state == PLAYER_ALIVE))
			state.players[state.players_count++] = i;

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_battle_announce, 0, &state);
}

int battle_report(const struct game *restrict game, const struct battle *restrict battle, unsigned char winner)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH,
			.top = 0,
			.bottom = WINDOW_HEIGHT,
			.callback = input_end,
		}
	};
	size_t i;

	struct state_battle state;

	state.game = game;
	state.title = slice("Battle report");
	memset(state.info, 0, sizeof(state.info));
	state.winners_count = 0;
	state.losers_count = 0;

	for(i = 0; i < battle->pawns_count; i += 1)
	{
		struct pawn *pawn = battle->pawns + i;

		if (!state.info[pawn->owner].status)
		{
			if (game->players[pawn->owner].alliance == winner)
			{
				state.info[pawn->owner].status = Winner;
				state.winners[state.winners_count++] = pawn->owner;
			}
			else
			{
				state.info[pawn->owner].status = Loser;
				state.losers[state.losers_count++] = pawn->owner;
			}
		}

		state.info[pawn->owner].victims[pawn->troop->unit - UNITS] += pawn->troop->count - pawn->count;
	}

	return input_process(areas, sizeof(areas) / sizeof(*areas), show_report_battle, 0, &state);
}
