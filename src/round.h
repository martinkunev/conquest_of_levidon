/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

struct obstacles *path_obstacles_alloc(const struct game *restrict game, const struct battle *restrict battle);

void round_prepare_pawn(struct battle *restrict battle, struct pawn *restrict pawn);

int round_prepare(const struct game *restrict game, struct battle *restrict battle);
int round_move(const struct game *restrict game, struct battle *restrict battle);
int round_cleanup(const struct game *restrict game, struct battle *restrict battle, enum victim_index victim_index);
void round_term(const struct game *restrict game, struct battle *restrict battle);
