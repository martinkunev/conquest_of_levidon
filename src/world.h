/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

union json;

int world_load(const unsigned char *restrict filepath, struct game *restrict game);
int world_save(const struct game *restrict game, const unsigned char *restrict filepath);
void world_unload(struct game *restrict game);

int world_empty(struct game *restrict game);

int troop_stack_init(const struct game *restrict game, struct troop_stack *restrict troop_stack, union json *restrict stack, struct hexagon *restrict hexagon);
