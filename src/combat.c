/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>

#include "basic.h"
#include "game.h"
#include "map.h"
#include "battle.h"
#include "movement.h"
#include "round.h"
#include "combat.h"

const double damage_boost[WEAPONS_COUNT][ARMORS_COUNT] =
{
// ARMOR:					NONE		LEATHER		CHAINMAIL	PLATE		WOODEN		STONE
	[WEAPON_NONE] =	{		0.0,		0.0,		0.0,		0.0,		0.0,		0.0},
	[WEAPON_CLUB] = {		1.0,		0.8,		0.7,		0.2,		0.0,		0.0},
	[WEAPON_ARROW] = {		1.0,		0.8,		0.7,		0.2,		0.0,		0.0},
	[WEAPON_CLEAVING] = {	1.0,		0.9,		0.7,		0.3,		0.5,		0.0},
	[WEAPON_POLEARM] = {	1.0,		1.0,		0.7,		0.4,		0.2,		0.0},
	[WEAPON_BLADE] = {		1.0,		1.0,		0.8,		0.5,		0.2,		0.0},
	[WEAPON_BLUNT] = {		1.0,		1.0,		1.0,		0.5,		1.0,		0.7},
};

static inline double combat_duration(const struct pawn *fighter)
{
	return 1.0 - (double)fighter->stop_time / BATTLE_MOVEMENT_STEPS;
}

static inline unsigned combat_damage(unsigned attacker_troops, double damage, enum weapon weapon, const struct unit *restrict victim, double duration)
{
	// Damage is proportional to duration of the attack.
	damage *= damage_boost[weapon][victim->armor] * duration;

	// Each attacker deals damage to a single troop.
	// The attacker cannot deal more damage than the health of the troop.
	if (damage > victim->health) damage = victim->health;

	return (unsigned)(damage * attacker_troops + 0.5);
}

unsigned combat_fight_troops(const struct pawn *restrict fighter, unsigned fighter_troops, unsigned victim_troops)
{
	unsigned max;

	// No more than half the troops can attack a single pawn.
	max = fighter->troop->unit->troops_count / 2;
	if (fighter_troops > max)
		fighter_troops = max;

	// No more than two troops can attack a single troop.
	max = victim_troops * 2;
	if (fighter_troops > max)
		fighter_troops = max;

	return fighter_troops;
}

double combat_fight_damage(const struct pawn *restrict fighter, unsigned fighter_troops, const struct pawn *restrict victim)
{
	enum weapon weapon = fighter->troop->unit->melee.weapon;
	double damage = fighter->troop->unit->melee.damage * fighter->troop->unit->melee.agility;
	return combat_damage(fighter_troops, damage, weapon, victim->troop->unit, combat_duration(fighter));
}

// Returns the distance from a position to an obstacle.
double combat_assault_distance(const struct position position, const struct obstacle *restrict obstacle)
{
	float dx, dy;

	if (obstacle->left > position.x) dx = obstacle->left - position.x;
	else if (position.x > obstacle->right) dx = position.x - obstacle->right;
	else dx = 0;

	if (obstacle->top > position.y) dy = obstacle->top - position.y;
	else if (position.y > obstacle->bottom) dy = position.y - obstacle->bottom;
	else dy = 0;

	return sqrt(dx * dx + dy * dy);
}

double combat_assault_damage(const struct pawn *restrict fighter, const struct battlefield *restrict target)
{
	// No more than half the troops can attack the obstacle.
	double damage = combat_damage(fighter->count / 2, fighter->troop->unit->melee.damage, fighter->troop->unit->melee.weapon, target->unit, combat_duration(fighter));
	if (damage > target->strength)
		damage = target->strength;
	return damage;
}

unsigned combat_shoot_victims(const struct battle *restrict battle, const struct pawn_command *restrict command, struct victim_shoot victims[static VICTIMS_LIMIT])
{
	// Shooters deal damage in an area around the target.
	// There is friendly fire.

	// TODO optimize: don't iterate all pawns

	unsigned victims_count = 0;
	for(size_t i = 0; i < battle->pawns_count; ++i)
	{
		double distance = battlefield_distance(command->target.position, battle->pawns[i].position);
		if (distance > DISTANCE_RANGED)
			continue;

		victims[victims_count].pawn = battle->pawns + i;
		victims[victims_count].distance = distance;
		victims_count += 1;
	}
	return victims_count;
}

double combat_shoot_inaccuracy(const struct pawn *restrict shooter, const struct pawn_command *restrict command, const struct obstacles *restrict obstacles)
{
	double distance = battlefield_distance(shooter->position, command->target.position);
	unsigned range = shooter->troop->unit->ranged.range;

	// TODO increase range if in a tower

	if (!path_visible(shooter->position, command->target.position, obstacles))
		range -= 1;

	return distance / range;
}

double combat_shoot_damage(const struct pawn *restrict shooter, double inaccuracy, double distance_victim, const struct pawn *restrict victim)
{
	// The larger the distance to the target, the more the damage is spread around it.

	// Shoot accuracy at the center and at the periphery of the target area for minimum and maximum distance.
	static const double target_accuracy[2][2] = {{1, 0.5}, {0, 0.078125}}; // {{1, 1/2}, {0, 5/64}}

	double impact_center = target_accuracy[0][0] * (1 - inaccuracy) + target_accuracy[0][1] * inaccuracy;
	double impact_periphery = target_accuracy[1][0] * (1 - inaccuracy) + target_accuracy[1][1] * inaccuracy;

	double damage = combat_damage(shooter->count, shooter->troop->unit->ranged.damage, shooter->troop->unit->ranged.weapon, victim->troop->unit, 1);
	double offtarget = distance_victim / DISTANCE_RANGED;

	damage = damage * impact_center * (1 - offtarget) + impact_periphery * offtarget;
	assert(damage >= 0);
	return damage;
}

// Determine whether the target obstacle is close enough to be attacked.
int can_assault(const struct position position, const struct battlefield *restrict target)
{
	float tile_left = ((target->blockage_location & POSITION_LEFT) ? target->tile.x : target->tile.x + WALL_OFFSET);
	float tile_right = ((target->blockage_location & POSITION_RIGHT) ? target->tile.x + 1 : target->tile.x + 1 - WALL_OFFSET);
	float tile_top = ((target->blockage_location & POSITION_TOP) ? target->tile.y : target->tile.y + WALL_OFFSET);
	float tile_bottom = ((target->blockage_location & POSITION_BOTTOM) ? target->tile.y + 1 : target->tile.y + 1 - WALL_OFFSET);

	float dx, dy;

	if (tile_left > position.x) dx = tile_left - position.x;
	else if (position.x > tile_right) dx = position.x - tile_right;
	else dx = 0;

	if (tile_top > position.y) dy = tile_top - position.y;
	else if (position.y > tile_bottom) dy = position.y - tile_bottom;
	else dy = 0;

	return (dx * dx + dy * dy <= DISTANCE_MELEE * DISTANCE_MELEE);
}

int can_fight(const struct position position, const struct pawn *restrict target)
{
	return (battlefield_distance(position, target->position) <= DISTANCE_MELEE);
}

int can_shoot(const struct game *restrict game, const struct battle *battle, const struct pawn *pawn, const struct position target)
{
	// Only ranged units can shoot.
	if (!pawn->troop->unit->ranged.weapon)
		return 0;

	// TODO loop only through the pawns in the area?
	unsigned char shooter_alliance = game->players[pawn->owner].alliance;
	for(size_t i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *other = battle->pawns + i;
		if (!other->count)
			continue;
		if ((game->players[other->owner].alliance != shooter_alliance) && can_fight(other->position, pawn))
			return 0;
	}

	return 1;
}

int combat_fight(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict fighter, struct pawn *restrict victim, int force)
{
	// computer: check if movement is available; store it in commands
	// human: check if movement is available; if so, calculate path and store everything in the pawn

	// TODO verify that the target is reachable

	if (allies(game, fighter->owner, victim->owner))
		return 0;

	fighter->command.action = ACTION_FIGHT;
	fighter->command.target.pawn = victim;

	return 1;
}

int combat_assault(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict fighter, struct battlefield *restrict target, int force)
{
	// TODO verify that the target is reachable

	switch (target->blockage)
	{
	case BLOCKAGE_WALL:
	case BLOCKAGE_GATE:
	case BLOCKAGE_TOWER:
		break;

	default:
		return 0;
	}

	// TODO should this check be here?
	if (!can_assault((fighter->command.moves.count ? fighter->command.moves.data[fighter->command.moves.count - 1] : fighter->position), target))
		return 0;

	fighter->command.action = ACTION_ASSAULT;
	fighter->command.target.field = target;

	return 1;
}

int combat_shoot(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict shooter, struct position target, int force)
{
	unsigned range;

	// Don't allow sooting if there is an enemy pawn nearby.
	if (!can_shoot(game, battle, shooter, target))
		return 0;

	// Check if the target is in shooting range.
	// If there is an obstacle between the pawn and its target, decrease shooting range by 1.
	range = shooter->troop->unit->ranged.range;
	if (!path_visible(shooter->position, target, battle->obstacles))
		range -= 1;
	if (battlefield_distance(shooter->position, target) > range)
		return 0;

	pawn_stay(shooter);
	shooter->command.action = ACTION_SHOOT;
	shooter->command.target.position = target;

	return 1;
}

void combat_melee(const struct game *restrict game, struct battle *restrict battle)
{
	size_t i, j;
	for(i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *fighter = battle->pawns + i;
		unsigned char fighter_alliance = game->players[fighter->owner].alliance;

		if (!fighter->count)
			continue;
		if (fighter->command.action == ACTION_SHOOT)
			continue;

		if (fighter->command.action == ACTION_ASSAULT)
		{
			if (can_assault(fighter->position, fighter->command.target.field))
				fighter->command.target.field->strength -= (unsigned)combat_assault_damage(fighter, fighter->command.target.field);
		}
		else
		{
			struct pawn *victims[VICTIMS_LIMIT];
			unsigned victims_count = 0;
			unsigned victims_troops = 0;

			unsigned targets_attackers[VICTIMS_LIMIT];
			unsigned attackers_left = fighter->count;

			// Fight all enemy pawns nearby.
			for(j = 0; j < battle->pawns_count; ++j)
			{
				struct pawn *victim = battle->pawns + j;
				if (!victim->count)
					continue;
				if (game->players[victim->owner].alliance == fighter_alliance)
					continue;
				if (can_fight(fighter->position, victim))
				{
					assert(victims_count < VICTIMS_LIMIT);
					victims[victims_count++] = victim;
					victims_troops += victim->count;
				}
			}

			if (!victims_count)
				continue;

			// Distribute the attacking troops proportionally between victim troops.
			// Choose targets for the remainder randomly.
			for(j = 0; j < victims_count; ++j)
			{
				// The number of troops, truncated to integer, is smaller than the mathematical value of the expression.
				// This guarantees that attackers_left cannot become negative.
				targets_attackers[j] = fighter->count * victims[j]->count / victims_troops;
				attackers_left -= targets_attackers[j];
			}
			while (attackers_left--)
				targets_attackers[random() % victims_count] += 1;

			for(j = 0; j < victims_count; ++j)
			{
				unsigned fight_troops = combat_fight_troops(fighter, targets_attackers[j], victims[j]->count);
				victims[j]->hurt += (unsigned)combat_fight_damage(fighter, fight_troops, victims[j]);
				victims[j]->attackers += fight_troops;
			}
		}
	}
}

void combat_ranged(const struct game *restrict game, struct battle *restrict battle)
{
	size_t i, j;
	for(i = 0; i < battle->pawns_count; ++i)
	{
		struct pawn *shooter = battle->pawns + i;

		double inaccuracy;
		struct victim_shoot victims[VICTIMS_LIMIT];
		unsigned victims_count;

		if (!shooter->count || pawn_waiting(shooter) || (shooter->command.action != ACTION_SHOOT))
		{
			battle->animation.pawns[i].shoot = POSITION_NONE;
			continue;
		}
		battle->animation.pawns[i].shoot = shooter->command.target.position;

		// Treat all gates as closed for shooting.
		inaccuracy = combat_shoot_inaccuracy(shooter, &shooter->command, battle->obstacles);

		victims_count = combat_shoot_victims(battle, &shooter->command, victims);
		for(j = 0; j < victims_count; ++j)
		{
			// Assume all troops can shoot this victim.
			victims[j].pawn->hurt += (unsigned)combat_shoot_damage(shooter, inaccuracy, victims[j].distance, victims[j].pawn);
			victims[j].pawn->attackers += shooter->count;
		}
	}
}
