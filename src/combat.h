/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

// A pawn can have at most 7 pawns as victims.
// melee: 7 pawns placed at distance <= DISTANCE_MELEE from the attacker
// ranged: 1 pawn placed at the target and 6 pawns placed at distance <= DISTANCE_RANGED from it
#define VICTIMS_LIMIT 7

struct pawn;
struct battlefield;
struct obstacle;

struct victim_shoot
{
	struct pawn *restrict pawn;
	double distance;
};

static inline double guard_radius(unsigned speed)
{
	return speed / 2.0;
}

extern const double damage_boost[WEAPONS_COUNT][ARMORS_COUNT];

unsigned combat_fight_troops(const struct pawn *restrict fighter, unsigned fighter_troops, unsigned victim_troops);
double combat_fight_damage(const struct pawn *restrict fighter, unsigned fighter_troops, const struct pawn *restrict victim);

double combat_assault_distance(const struct position position, const struct obstacle *restrict obstacle);
double combat_assault_damage(const struct pawn *restrict fighter, const struct battlefield *restrict target);

unsigned combat_shoot_victims(const struct battle *restrict battle, const struct pawn_command *restrict command, struct victim_shoot victims[static VICTIMS_LIMIT]);
double combat_shoot_inaccuracy(const struct pawn *restrict shooter, const struct pawn_command *restrict command, const struct obstacles *restrict obstacles);
double combat_shoot_damage(const struct pawn *restrict shooter, double inaccuracy, double distance_victim, const struct pawn *restrict victim);

int can_shoot(const struct game *restrict game, const struct battle *battle, const struct pawn *pawn, const struct position target);
int can_fight(const struct position position, const struct pawn *restrict pawn);
int can_assault(const struct position position, const struct battlefield *restrict field);

int combat_fight(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict fighter, struct pawn *restrict victim, int force);
int combat_assault(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict fighter, struct battlefield *restrict target, int force);
int combat_shoot(const struct game *restrict game, const struct battle *restrict battle, struct pawn *restrict shooter, struct position target, int force);

void combat_melee(const struct game *restrict game, struct battle *restrict battle);
void combat_ranged(const struct game *restrict game, struct battle *restrict battle);
