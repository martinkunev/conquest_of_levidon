/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "bitmap.h"
#include "log.h"
#include "djset.h"
#include "game.h"
#include "map.h"
#include "turn.h"
#include "economy.h"

#define TERRAIN_COEFFICIENT 1 /* TODO this should depend on terrain */

#define RIOT_FACTOR 2

static inline int building_available(const struct hexagon *restrict hexagon, const struct building *restrict building)
{
	if (hexagon->terrain != TERRAIN_GROUND)
		return 0;
	if ((hexagon->environment & building->environment) != building->environment)
		return 0;
	return (hexagon->buildings & building->requires) == building->requires;
}

size_t buildings_available(struct game *restrict game, struct hexagon *restrict hexagon, unsigned char buildings[static BUILDINGS_COUNT], unsigned player)
{
	size_t i;
	int allow_construction;
	size_t buildings_count;

	// Initialize list of available buildings in the hexagon.
	// Starting a building is not allowed if two players are present and nobody owns the hexagon (no buildings).
	if (hexagon->buildings)
		allow_construction = (hexagon->owner == player);
	else if (hexagon->public.count)
		allow_construction = (hexagon->public.data[0]->owner == player);
	else
		allow_construction = 1;

	buildings_count = 0;
	for(i = 0; i < BUILDINGS_COUNT; i += 1)
	{
		if (hexagon_built(hexagon, i))
			buildings[buildings_count++] = i;
		else if (!allow_construction)
			continue;
		else if (building_available(hexagon, BUILDINGS + i))
			buildings[buildings_count++] = i;
		else if (i == hexagon->build_index)
			buildings[buildings_count++] = i;
	}

	return buildings_count;
}

int training_enqueue(struct game *restrict game, struct region *restrict region, size_t unit_index)
{
	const struct unit *unit = UNITS + unit_index;
	struct player *player = game->players + region->garrison.hexagon->owner;
	size_t i;

	if (!resources_enough(&player->treasury, &unit->cost) || !population_enough(region, unit))
		return -1;

	for(i = 0; i < TRAIN_QUEUE; i += 1)
		if (!region->train[i])
		{
			resources_update(&player->treasury, &unit->cost, 1);
			region->train[i] = unit;
			region->population -= unit->troops_count;
			return 0;
		}

	return -1; // queue is full
}

void training_dequeue(struct game *restrict game, struct region *restrict region, size_t position)
{
	struct player *player = game->players + region->garrison.hexagon->owner;

	// Return allocated resources if training hasn't started yet.
	if (position || !region->train_progress)
		resources_update(&player->treasury, &region->train[position]->cost, -1);
	else
		region->train_progress = 0;

	region->population += region->train[position]->troops_count;

	if (position + 1 < TRAIN_QUEUE)
		memmove(region->train + position, region->train + position + 1, (TRAIN_QUEUE - position - 1) * sizeof(*region->train));
	region->train[TRAIN_QUEUE - 1] = 0;
}

int building_build(struct game *restrict game, struct hexagon *restrict hexagon, size_t index, unsigned player)
{
	struct resources treasury;
	unsigned owner;
	size_t i, j;

	if (hexagon->buildings || (hexagon->build_index >= 0))
		owner = hexagon->owner;
	else // no hexagon owner
		owner = player;
	treasury = game->players[owner].treasury;

	// Return spent resources if there is another building ordered, but construction hasn't started.
	if ((hexagon->build_index >= 0) && !hexagon->build_progress)
		resources_update(&treasury, &BUILDINGS[hexagon->build_index].cost, -1);

	// Issue a build command only if the player has enough resources.
	if (!resources_enough(&treasury, &BUILDINGS[index].cost))
		return -1;

	resources_update(&treasury, &BUILDINGS[index].cost, 1);
	game->players[owner].treasury = treasury;

	if (hexagon->build_index >= 0)
		for(i = 0; i < hexagon->private.count; i += 1)
		{
			struct troop_stack *troop_stack = hexagon->private.data[i];
			for(j = 0; j < troop_stack->private.count; j += 1)
				if (troop_stack->private.troops[j]->task == TASK_BUILD)
					troop_stack->private.troops[j]->task = TASK_NONE;
		}

	hexagon->build_index = index;
	hexagon->build_progress = 0;

	// The player who starts the first building will become owner of the hexagon.
	if (!hexagon->buildings)
		hexagon->owner = owner;

	return 0;
}

void building_cancel(struct game *restrict game, struct hexagon *restrict hexagon)
{
	struct player *player = game->players + hexagon->owner;
	size_t i, j;

	if (!hexagon->build_progress)
		resources_update(&player->treasury, &BUILDINGS[hexagon->build_index].cost, -1);
	hexagon->build_index = -1;

	for(i = 0; i < hexagon->private.count; i += 1)
	{
		struct troop_stack *troop_stack = hexagon->private.data[i];
		for(j = 0; j < troop_stack->private.count; j += 1)
			if (troop_stack->private.troops[j]->task == TASK_BUILD)
				troop_stack->private.troops[j]->task = TASK_NONE;
	}
}

int vessel_build(struct game *restrict game, struct hexagon *restrict hexagon, size_t index)
{
	const struct vessel *vessel = VESSELS + index;
	struct player *player = game->players + hexagon->owner;

	if (!resources_enough(&player->treasury, &vessel->cost))
		return -1;
	resources_update(&player->treasury, &vessel->cost, 1);

	hexagon->vessel_index = index;
	hexagon->vessel_progress = 0;

	return 0;
}

void vessel_cancel(struct game *restrict game, struct hexagon *restrict hexagon)
{
	const struct vessel *vessel = VESSELS + hexagon->vessel_index;
	struct player *player = game->players + hexagon->owner;

	if (!hexagon->vessel_progress)
		resources_update(&player->treasury, &vessel->cost, -1);
	hexagon->vessel_index = -1;
}

void troop_stack_stamina(struct troop_stack *restrict troop_stack)
{
	size_t i;

	if (troop_stack->vessel->speed)
	{
		troop_stack->stamina = troop_stack->vessel->speed;
		return;
	}

	troop_stack->stamina = UNIT_SPEED_LIMIT;
	for(i = 0; i < troop_stack->private.count; i += 1)
		if (troop_stack->private.troops[i]->unit->speed < troop_stack->stamina)
			troop_stack->stamina = troop_stack->private.troops[i]->unit->speed;
}

// Aborts all building and vessel constructions and all troop trainings.
static void orders_abort(struct game *restrict game, struct hexagon *restrict hexagon)
{
	size_t i, j;

	hexagon->build_index = -1;
	hexagon->vessel_index = -1;
	for(i = 0; i < hexagon->private.count; i += 1)
	{
		struct troop_stack *troop_stack = hexagon->private.data[i];
		for(j = 0; j < troop_stack->private.count; j += 1)
			if (troop_stack->private.troops[j]->task == TASK_BUILD)
				troop_stack->private.troops[j]->task = TASK_NONE;
	}

	if (hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
	{
		struct region *restrict region = game->regions + hexagon->info.region_index;
		for(i = 0; i < TRAIN_QUEUE; i += 1)
		{
			if (region->train[i])
				region->population += region->train[i]->troops_count;
			region->train[i] = 0;
		}
		region->train_progress = 0;
	}
}

static int hexagon_sieged(struct game *restrict game, const struct hexagon *restrict hexagon)
{
	struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
	unsigned char accessible[DIRECTIONS_COUNT] = {0};
	const struct hexagon *directions[DIRECTIONS_COUNT] = {0};
	size_t count = hexagon_neighbors(game, hexagon, neighbors);
	size_t i;

	assert(hexagon->buildings);

	// Determine which neighbors are accessible.
	for(i = 0; i < count; i += 1)
	{
		accessible[neighbors[i].direction] = 1;
		directions[neighbors[i].direction] = neighbors[i].hexagon;
	}
	for(i = 0; i < count; i += 1)
	{
		switch (neighbors[i].hexagon->terrain)
		{
			const struct array_troop_stacks *array;

		case TERRAIN_WATER:
			if (hexagon->port)
			{
		case TERRAIN_GROUND:
				array = &neighbors[i].hexagon->private;
				if (array->count && !allies(game, array->data[0]->owner, hexagon->owner))
				{
					enum direction j;

					accessible[neighbors[i].direction] = 0;

					j = (neighbors[i].direction + DIRECTIONS_COUNT - 1) % DIRECTIONS_COUNT;
					if (directions[j] && (directions[j]->terrain == neighbors[i].hexagon->terrain))
						accessible[j] = 0;

					j = (neighbors[i].direction + 1) % DIRECTIONS_COUNT;
					if (directions[j] && (directions[j]->terrain == neighbors[i].hexagon->terrain))
						accessible[j] = 0;
				}
				break;
			}
		default:
			accessible[i] = 0;
			break;
		}
	}

	for(i = 0; i < DIRECTIONS_COUNT; i += 1)
		if (accessible[i])
			return 0;
	return 1;
}

void turn_prepare(struct game *restrict game)
{
	size_t i, j;
	size_t index;

	for(i = 0; i < game->hexagons_total; i += 1)
	{
		struct hexagon *restrict hexagon = game->hexagons + i;

		hexagon->garrison = 0;
		hexagon->sight = 0;
		hexagon->port = 0;

		// Cache building and siege information.
		if (hexagon->buildings)
		{
			struct region *region = game->regions + hexagon->info.region_index;

			for(j = 0; j < BUILDINGS_COUNT; j += 1)
			{
				if (!hexagon_built(hexagon, j))
					continue;

				if (BUILDINGS[j].garrison > hexagon->garrison)
					hexagon->garrison = BUILDINGS[j].garrison;
				if (BUILDINGS[j].sight > hexagon->sight)
					hexagon->sight = BUILDINGS[j].sight;
				if (BUILDINGS[j].port)
					hexagon->port = 1;
			}

			if (hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
			{
				region->sieged = (!hexagon->private.count && hexagon_sieged(game, hexagon));
				if (region->sieged)
					orders_abort(game, hexagon);
			}
		}

		// Set troop stack internals - stamina, publicly visible troops.
		for(j = 0; j < hexagon->private.count; j += 1)
		{
			struct troop_stack *troop_stack = hexagon->private.data[j];

			troop_stack_stamina(troop_stack);

			troop_stack->public.count = troop_stack->private.count;
			memcpy(troop_stack->public.troops, troop_stack->private.troops, troop_stack->private.count * sizeof(*troop_stack->public.troops));
		}

		// Set player-private copies of the list of troop stacks.
		if (hexagon->private.data != hexagon->public.data)
		{
			free(hexagon->public.data);
			hexagon->public = hexagon->private;
		}
		else
			hexagon->public.count = hexagon->private.count; // count may have changed due to movement
	}

	// TODO remove code duplication
	// Players with at least 1 region are alive.
	// Store hexagon owned by the player as an indication for being alive.
	for(i = 0; i < game->players_count; i += 1)
		game->alive[i] = 0;
	for(i = 0; i < game->regions_count; i += 1)
		if (game->regions[i].garrison.hexagon->buildings)
			game->alive[game->regions[i].garrison.hexagon->owner] = game->regions[i].garrison.hexagon;

	// Calculate and cache the height of the army bar for each hexagon.
	for(index = 0; index < game->hexagons_total; index += 1)
	{
		struct hexagon *restrict hexagon = game->hexagons + index;

		hexagon->armies.field = 0;
		if (hexagon->private.count)
		{
			const struct troop_stack *restrict troop_stack;

			for(i = 0; i < hexagon->private.count; i += 1)
			{
				troop_stack = hexagon->private.data[i];

				for(j = 0; j < troop_stack->private.count; j += 1)
				{
					const struct troop *restrict troop = troop_stack->private.troops[j];
					hexagon->armies.field += (double)troop->count / troop->unit->troops_count;
				}
			}

			// Get owner from the last troop stack.
			hexagon->armies.owner = troop_stack->owner;
		}

		hexagon->armies.garrison = 0;
		if (hexagon->garrison)
		{
			const struct region *restrict region = game->regions + hexagon->info.region_index;

			assert(hexagon->buildings);

			for(i = 0; i < region->garrison.private.count; i += 1)
			{
				const struct troop *restrict troop = region->garrison.private.troops[i];
				hexagon->armies.garrison += (double)troop->count / troop->unit->troops_count;
			}

			hexagon->armies.owner = hexagon->owner;
		}
		else if (hexagon->buildings)
			hexagon->armies.owner = hexagon->owner;
	}
}

// Handles building construction.
static void turn_build(struct game *restrict game, struct hexagon *restrict hexagon)
{
	size_t i, j;
	unsigned workers = 0;

	if (hexagon->build_index < 0)
		return;

	// Count the workers that are currently building.
	// Garrison workers cannot build.
	for(i = 0; i < hexagon->private.count; i += 1)
	{
		struct troop_stack *troop_stack = hexagon->private.data[i];
		for(j = 0; j < troop_stack->private.count; j += 1)
			if (troop_stack->private.troops[j]->task == TASK_BUILD)
				workers += troop_stack->private.troops[j]->count;
	}

	hexagon->build_progress += workers_progress(workers);
	if (hexagon->build_progress >= BUILDINGS[hexagon->build_index].time)
	{
		hexagon_build(hexagon, hexagon->build_index);
		hexagon->build_index = -1;
		hexagon->build_progress = 0;

		// Set all workers as free.
		for(i = 0; i < hexagon->private.count; i += 1)
		{
			struct troop_stack *troop_stack = hexagon->private.data[i];
			for(j = 0; j < troop_stack->private.count; j += 1)
				troop_stack->private.troops[j]->task = TASK_NONE;
		}
	}
}

// Handles troop training.
static int turn_train(struct game *restrict game, struct region *restrict region)
{
	struct hexagon *hexagon = region->garrison.hexagon;
	const struct unit *unit;
	struct troop_stack *troop_stack;
	size_t i;

	unit = region->train[0];
	if (!unit)
		return 0; // no training in progress

	region->train_progress += 1;
	if (region->train_progress < unit->time)
		return 0; // training not finished

	memmove(region->train, region->train + 1, (TRAIN_QUEUE - 1) * sizeof(*region->train));
	region->train[TRAIN_QUEUE - 1] = 0;
	region->train_progress = 0;

	// Look for a place to spawn the troop in existing troop stacks.
	for(i = 0; i < hexagon->private.count; i += 1)
	{
		troop_stack = hexagon->private.data[i];

		assert(troop_stack->owner == hexagon->owner);

		if (troop_stack->private.count < troop_stack->vessel->capacity)
			return troop_spawn(&troop_stack->private, unit, unit->troops_count);
	}

	// Spawn the troop in a new troop stack.
	troop_stack = troop_stack_create(game, hexagon, hexagon->owner, 1);
	if (!troop_stack)
	{
		LOG_FATAL("memory allocation error");
		return ERROR_MEMORY;
	}
	return troop_spawn(&troop_stack->private, unit, unit->troops_count);
}

static int turn_shipyard(struct game *restrict game, struct hexagon *restrict hexagon)
{
	if (hexagon->vessel_index >= 0)
	{
		const struct vessel *vessel = VESSELS + hexagon->vessel_index;
		struct troop_stack *troop_stack;

		size_t i, j;
		unsigned workers = 0, workers_saturated = 0;
		double ratio;

		for(i = 0; i < hexagon->private.count; i += 1)
		{
			troop_stack = hexagon->private.data[i];
			for(j = 0; j < troop_stack->private.count; j += 1)
				if (troop_stack->private.troops[j]->unit->worker)
					if (troop_stack->private.troops[j]->task != TASK_BUILD)
						workers += troop_stack->private.troops[j]->count;
		}

		for(i = 0; i < BUILDINGS_COUNT; i += 1)
		{
			if (!hexagon_built(hexagon, i))
				continue;
			workers_saturated += BUILDINGS[i].workers;
			workers_saturated += VESSELS[hexagon->vessel_index].workers;
		}

		ratio = (double)workers / workers_saturated;
		hexagon->vessel_progress += workers_progress(ratio * VESSELS[hexagon->vessel_index].workers);
		if (hexagon->vessel_progress < vessel->time)
			return 0;

		hexagon->vessel_index = -1;

		troop_stack = troop_stack_create(game, hexagon, hexagon->owner, 1);
		if (!troop_stack)
			return ERROR_MEMORY;
		troop_stack->vessel = vessel;
	}

	return 0;
}

int turn_orders(struct game *restrict game)
{
	size_t index;
	for(index = 0; index < game->hexagons_total; index += 1)
	{
		struct hexagon *hexagon = game->hexagons + index;
		int status;

		turn_build(game, hexagon);

		if (hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
		{
			status = turn_train(game, game->regions + hexagon->info.region_index);
			if (status < 0)
				return status;
		}

		status = turn_shipyard(game, hexagon);
		if (status < 0)
			return status;
	}
	return 0;
}

static void troop_dismiss(struct region *restrict region, struct troops *restrict troops, size_t index)
{
	size_t left;
	troops->count -= 1;
	left = troops->count - index;
	region->population += troops->troops[index]->count;
	free(troops->troops[index]);
	if (left)
		memmove(troops->troops + index, troops->troops + index + 1, left * sizeof(*troops->troops));
}

// Troop stacks are created when completing a training (public) and by split operations (private).
// New troop stacks are always appended to the end of the array.
struct troop_stack *troop_stack_create(struct game *restrict game, struct hexagon *restrict hexagon, unsigned owner, int public)
{
	struct array_troop_stacks *array = &hexagon->private;
	struct troop_stack *troop_stack;

	if (!public && (array->data == hexagon->public.data))
	{
		// Copy on write.
		*array = (struct array_troop_stacks){.count = hexagon->public.count};
		if (array_troop_stacks_expand(array, hexagon->public.count + 1) < 0)
			return 0;
		if (hexagon->public.count)
			memcpy(array->data, hexagon->public.data, hexagon->public.count * sizeof(*array->data));
	}
	else if (array_troop_stacks_expand(array, array->count + 1) < 0)
		return 0;

	troop_stack = malloc(sizeof(*troop_stack));
	if (!troop_stack)
		return 0;

	troop_stack->is_public = public;

	troop_stack->vessel = VESSELS + ((hexagon->terrain == TERRAIN_WATER) ? VesselWater : VesselGround);
	troop_stack->movements[0] = hexagon;
	troop_stack->movements_count = 1;
	troop_stack->owner = owner;

	troop_stack->private.count = 0;
	troop_stack->public.count = 0;

	array->data[array->count++] = troop_stack;

	return troop_stack;
}

// A troop stack is hidden when a player moves the last remaining troop from it.
int troop_stack_hide(struct game *restrict game, struct hexagon *restrict hexagon, unsigned player, size_t index)
{
	struct array_troop_stacks *array = &hexagon->private;
	struct troop_stack *troop_stack;
	size_t left;

	assert(index < array->count);
	troop_stack = array->data[index];
	assert(!troop_stack->private.count);

	if (array->data == hexagon->public.data)
	{
		// Copy on write.
		*array = (struct array_troop_stacks){.count = hexagon->public.count};
		if (array_troop_stacks_expand(array, hexagon->public.count) < 0)
			return ERROR_MEMORY;
		memcpy(array->data, hexagon->public.data, hexagon->public.count * sizeof(*array->data));
	}

	if (!array->data[index]->is_public)
		free(troop_stack);

	array->count -= 1;
	left = array->count - index;
	if (left)
		memmove(array->data + index, array->data + index + 1, left * sizeof(*array->data));

	return 0;
}

// Removes a troop stack with no troops left. Used after a battle.
static void troop_stack_remove(struct game *restrict game, struct hexagon *restrict hexagon, size_t index)
{
	struct array_troop_stacks *array = &hexagon->private;
	struct troop_stack *troop_stack;
	size_t left;

	assert(index < array->count);
	troop_stack = array->data[index];
	assert(!troop_stack->private.count);

	free(troop_stack);

	array->count -= 1;
	left = array->count - index;
	if (left)
		memmove(array->data + index, array->data + index + 1, left * sizeof(*array->data));
}

static int troop_stack_move(struct game *restrict game, struct troop_stack *restrict troop_stack, struct hexagon *restrict last, struct hexagon *restrict current)
{
	size_t i;

	assert(last != current);

	// TODO add a record of the movement for the UI?

	// Add troop stack to new location.
	if (array_troop_stacks_expand(&current->private, current->private.count + 1) < 0)
		return ERROR_MEMORY;
	current->private.data[current->private.count++] = troop_stack;
	current->occupants_players |= 1 << troop_stack->owner;

	// Remove troop stack from old location.
	last->occupants_players = 0;
	for(i = 0; i < last->private.count; i += 1)
	{
		struct troop_stack *other = last->private.data[i];
		if (other == troop_stack)
		{
			size_t left;
			last->private.count -= 1;
			left = last->private.count - i;
			if (!left)
				break;
			memmove(last->private.data + i, last->private.data + i + 1, left * sizeof(*last->private.data));
			other = last->private.data[i];
		}
		last->occupants_players |= 1 << other->owner;
	}

	return 0;
}

static void turn_troops_tasks(struct game *restrict game, struct hexagon *restrict hexagon, struct troop_stack *restrict local)
{
	size_t k = 0;
	while (k < local->private.count)
		switch (local->private.troops[k]->task)
		{
		case TASK_DISMISS:
			// Dismissed troops go to the population of the current region.
			troop_dismiss(game->regions + hexagon->info.region_index, &local->private, k);
			continue;

		case TASK_BUILD:
			// Moving troops stop building.
			if (local->movements_count != 1)
				local->private.troops[k]->task = TASK_NONE;
		default:
			k += 1;
			break;
		}
}

static enum battle_type hexagon_fighting(const struct game *restrict game, struct hexagon *restrict location, unsigned player)
{
	size_t i;

	struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
	size_t neighbors_count;

	// Check if there is an enemy on the hexagon.
	if (location->private.count && !allies(game, location->private.data[0]->owner, player))
		return BATTLE_OPEN;

	// Check if there is an enemy on a neighboring hexagon.
	neighbors_count = hexagon_neighbors(game, location, neighbors);
	for(i = 0; i < neighbors_count; i += 1)
	{
		const struct hexagon *neighbor = neighbors[i].hexagon;

		if (neighbor->terrain != location->terrain)
			continue;

		if (neighbor->private.count && !allies(game, neighbor->private.data[0]->owner, player))
			return BATTLE_OPEN;
	}

	// Check if the hexagon is controlled by an enemy.
	if (location->buildings && (player != location->owner))
	{
		assert(!allies(game, player, location->owner));

		if (location->garrison && (game->regions[location->info.region_index].garrison.private.count))
			return BATTLE_ASSAULT;
		else
			return BATTLE_CAPTURE;
	}

	return BATTLE_NONE;
}

// Returns the number of existing troop stacks or negative error code.
int turn_move(struct game *restrict game)
{
	unsigned step;
	size_t i;

	size_t hexagons_total = hexagons_count(game);

	uint32_t alliance_mask[PLAYERS_LIMIT] = {0};

	// Create a bit mask of the players in each alliance.
	for(i = 0; i < game->players_count; i += 1)
		alliance_mask[game->players[i].alliance] |= 1 << i;

	game->troop_stacks.count = 0;

	// Initialize a list of troop stacks and set initial state of each troop stack.
	for(i = 0; i < hexagons_total; i += 1)
	{
		struct hexagon *hexagon = game->hexagons + i;
		size_t j;

		hexagon->occupants_players = 0;
		hexagon->moving_players = 0;
		hexagon->moving_count = 0;
		// hexagon->moving_owner = 0;

		if (array_troop_stacks_expand(&game->troop_stacks, game->troop_stacks.count + hexagon->private.count) < 0)
			return ERROR_MEMORY;
		for(j = 0; j < hexagon->private.count; j += 1)
		{
			struct troop_stack *troop_stack = hexagon->private.data[j];

			// Initialize the alliances occupying the hexagon.
			hexagon->occupants_players |= 1 << troop_stack->owner;

			troop_stack->is_public = 1; // TODO should I move this to turn_prepare() together with other code managing public?
			troop_stack->fighting = 0;
			troop_stack->movement_progress = 0;
			troop_stack->movement_position = 0;
			troop_stack->index = game->troop_stacks.count;

			game->troop_stacks.data[game->troop_stacks.count++] = troop_stack;
		}
	}

	// Perform movement in steps. Each troop stack advances its movement on each step.
	for(step = 0; step < MOVEMENT_STEPS; step += 1)
	{
		// Allies cannot be on the same hexagon, but can pass each other.
		// This is achieved by only moving a troop stack once the target hexagon is available.
		// This means a troop stack can skip a hexagon while moving. Care is taken to prevent skipping enemies.

		// Determine where the troop stack is trying to be at this step.
		for(i = 0; i < game->troop_stacks.count; i += 1)
		{
			struct troop_stack *troop_stack = game->troop_stacks.data[i];
			double speed = (double)troop_stack->stamina * TERRAIN_COEFFICIENT / MOVEMENT_STEPS;
			size_t position, position_new;

			assert(speed <= 1);

			if (troop_stack->fighting)
				continue; // no movement when fighting

			position = troop_stack->movement_position;
			if (position == troop_stack->movements_count - 1)
				continue; // destination already reached

			if (troop_stack->movement_progress + 0.5 < troop_stack->movements_count - 1)
				troop_stack->movement_progress += speed;

			position_new = troop_stack->movement_progress + 0.5;
			assert(position_new < troop_stack->movements_count);
			if (position_new != position) // troop stack is advancing to a different hexagon
				troop_stack->movements[position_new]->moving_players |= 1 << troop_stack->owner;
		}

		// Check if a troop stack should move.
		for(i = 0; i < game->troop_stacks.count; i += 1)
		{
			struct troop_stack *troop_stack = game->troop_stacks.data[i];
			size_t position, position_new;
			struct hexagon *hexagon_current, *hexagon_new;

			if (troop_stack->fighting)
				continue; // no movement when fighting

			position = troop_stack->movement_position;
			if (position == troop_stack->movements_count - 1)
				continue; // destination already reached
			position_new = troop_stack->movement_progress + 0.5;

			hexagon_current = troop_stack->movements[position];
			hexagon_new = troop_stack->movements[position_new];
			if (hexagon_new != hexagon_current)
			{
				// Check if position_new is available for the troop stack to move.
				// The hexagon is available if no ally is present (no ally troop stacks and owner is not an ally).

				uint32_t present = (hexagon_new->buildings ? (1 << hexagon_new->owner) : 0) | hexagon_new->occupants_players;
				uint32_t mask = alliance_mask[game->players[troop_stack->owner].alliance] & ~(1 << troop_stack->owner);
				if (hexagon_current->terrain == TERRAIN_WATER)
					mask = ~(1 << troop_stack->owner); // TODO change this when naval battles are supported

				if (!(present & mask)) // no ally located at hexagon_new
				{
					if (!(hexagon_new->moving_players & mask)) // no ally moving to hexagon_new
					{
						troop_stack->movement_position = position_new;
						if (troop_stack_move(game, troop_stack, hexagon_current, hexagon_new) < 0)
							return ERROR_MEMORY;
					}
					else
					{
						// TODO maybe don't do this for enemies on ships

						// Multiple players from the same alliance are moving to hexagon_new.
						// Choose one troop stack uniformly at random and remember its owner.
						// The troop stacks with this owner will move first.
						hexagon_new->moving_count += 1;
						if (!(random() % hexagon_new->moving_count))
							hexagon_new->moving_owner = troop_stack->owner;
					}
				}
			}
		}

		// When there are movement conflicts, move selected troop stacks.
		for(i = 0; i < game->troop_stacks.count; i += 1)
		{
			struct troop_stack *troop_stack = game->troop_stacks.data[i];
			size_t position, position_new;
			struct hexagon *hexagon_current, *hexagon_new;

			if (troop_stack->fighting)
				continue; // no movement when fighting

			position = troop_stack->movement_position;
			if (position == troop_stack->movements_count - 1)
				continue; // destination already reached
			position_new = troop_stack->movement_progress + 0.5;
			if (position_new == position)
				continue; // troop stack is not moving

			hexagon_current = troop_stack->movements[position];
			hexagon_new = troop_stack->movements[position_new];
			if (hexagon_new->moving_count && (hexagon_new->moving_owner == troop_stack->owner))
			{
				troop_stack->movement_position = position_new;
				if (troop_stack_move(game, troop_stack, hexagon_current, hexagon_new) < 0)
					return ERROR_MEMORY;
			}
		}

		// Perform cleanup for the step and check if a troop stack should start fighting.
		for(i = 0; i < game->troop_stacks.count; i += 1)
		{
			struct troop_stack *troop_stack = game->troop_stacks.data[i];
			size_t position, position_new;
			struct hexagon *hexagon_new;

			if (troop_stack->fighting == BATTLE_OPEN)
				continue;

			position = troop_stack->movement_position;
			position_new = troop_stack->movement_progress + 0.5;
			hexagon_new = troop_stack->movements[position_new];

			// Ships stop movement if target hexagon is blocked by an enemy.
			if (hexagon_new->terrain == TERRAIN_WATER)
			{
				// TODO support sea battles; support bridges
				if (hexagon_new->private.count && !allies(game, troop_stack->owner, hexagon_new->private.data[0]->owner))
					troop_stack->movements_count = position + 1;
				continue;
			}

			// TODO it appears I can do this in the previous loop. when an enemy is detected, I need to set all troop stacks to fighting
			troop_stack->fighting = hexagon_fighting(game, troop_stack->movements[position], troop_stack->owner);

			// Prevent skipping enemies by cancelling movement when an enemy is encountered.
			if (position_new != position)
				if (hexagon_fighting(game, hexagon_new, troop_stack->owner))
					troop_stack->movements_count = position + 1;
		}
	}

	for(i = 0; i < game->troop_stacks.count; i += 1)
	{
		struct troop_stack *troop_stack = game->troop_stacks.data[i];

		// Get final position of the troop stack.
		size_t position = troop_stack->movement_position;
		if (!position)
			continue;

		// Remove finished movements.
		assert(position < troop_stack->movements_count);
		memmove(troop_stack->movements, troop_stack->movements + position, (troop_stack->movements_count - position) * sizeof(*troop_stack->movements));
		troop_stack->movements_count -= position;
	}

	return 0;
}

void turn_riot(struct game *restrict game)
{
	size_t i, j, k;
	for(i = 0; i < game->regions_count; i += 1)
	{
		struct region *region = game->regions + i;
		unsigned army = 0;
		struct array_troop_stacks *stacks = &region->garrison.hexagon->private;

		double probability = 1.0 - RIOT_FACTOR * region->happiness;
		if ((probability < 0) || !region->population)
			continue;

		// Any army present in the region reduces the probability of a riot.
		for(j = 0; j < region->garrison.private.count; j += 1)
			army += region->garrison.private.troops[j]->count;
		for(j = 0; j < stacks->count; j += 1)
		{
			struct troop_stack *troop_stack = stacks->data[j];
			for(k = 0; k < troop_stack->private.count; k += 1)
				army += troop_stack->private.troops[k]->count;
		}
		probability -= sqrt(army) / 16;
		if (probability < 0)
			continue;

		// The population riots with some probability.
		if ((random() % 65536) / 65536.0 < probability)
		{
			region->garrison.hexagon->owner = PLAYER_NEUTRAL;
			while (region->garrison.private.count)
				troop_dismiss(region, &region->garrison.private, region->garrison.private.count - 1);
			orders_abort(game, region->garrison.hexagon);
		}
	}
}

// Updates hexagon owners.
// Determines which players are alive and cleans up eliminated players.
void turn_players(struct game *restrict game, size_t hexagons_total)
{
	size_t index;

	for(index = 0; index < game->players_count; index += 1)
		game->alive[index] = 0;

	for(index = 0; index < game->hexagons_total; index += 1)
	{
		struct hexagon *hexagon = game->hexagons + index;

		if (!hexagon->buildings)
			continue;

		if (hexagon->private.count && (hexagon->private.data[0]->owner != hexagon->owner))
		{
			hexagon->owner = hexagon->private.data[0]->owner;

			// TODO pillaging; destruction of buildings?

			orders_abort(game, hexagon);
		}

		// Players with at least 1 settlement are alive.
		// Store a hexagon owned by the player as an indication for being alive.
		game->alive[hexagon->owner] = hexagon;
	}

	for(index = 0; index < game->hexagons_total; index += 1)
	{
		struct array_troop_stacks *array = &game->hexagons[index].private;
		struct region *region = game->regions + game->hexagons[index].info.region_index;

		if (array->count && !game->alive[array->data[0]->owner])
		{
			size_t i;

			for(i = 0; i < array->count; i += 1)
			{
				struct troop_stack *troop_stack = array->data[i];
				while (troop_stack->private.count)
					troop_dismiss(region, &troop_stack->private, troop_stack->private.count - 1);
				free(troop_stack);
			}
			array->count = 0;

			game->hexagons[index].build_index = -1;
		}
	}
}

struct battles *turn_battles_init(struct game *restrict game)
{
	static const size_t battles_empty = 0;

	unsigned battles_count;
	unsigned assaults_count;
	unsigned troop_stacks_fighting;
	struct battles *battles = 0;
	struct troop_stack **fighters;
	size_t battle_index;

	struct djelement *class, *element;

	struct djset djset;
	struct djelement *elements;

	unsigned char *bitmap_hexagons = 0;

	size_t i, j;

	enum {HEXAGONS_COUNT = 0};

	if (!game->troop_stacks.count)
		return (struct battles *)&battles_empty; // no battles this turn

	elements = calloc(game->troop_stacks.count, sizeof(*elements));
	if (!elements)
		return 0; // memory error

	djset_init(&djset, elements, game->troop_stacks.count);

	// Determine equivalence classes of fighting troop stacks.
	assaults_count = 0;
	troop_stacks_fighting = 0;
	for(i = 0; i < game->troop_stacks.count; i += 1)
	{
		struct troop_stack *stack = game->troop_stacks.data[i];
		struct hexagon *hexagon = stack->movements[0];

		struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
		size_t count;

		if (!stack->fighting || (stack->fighting == BATTLE_CAPTURE))
			continue;

		// To select each hexagon just once, process only the first troop stack in the hexagon.
		if (stack != hexagon->private.data[0])
		{
			djset_union(&djset, stack->index, hexagon->private.data[0]->index);
			continue;
		}

		if (stack->fighting == BATTLE_ASSAULT)
		{
			assaults_count += 1;
			troop_stacks_fighting += hexagon->private.count;
			continue;
		}

		// Determine the number of hexagon neighbors participating in battle.
		// Update equivalence classes of fighting troop stacks.

		count = hexagon_neighbors(game, hexagon, neighbors);
		for(j = 0; j < count; j += 1)
		{
			const struct hexagon *neighbor = neighbors[j].hexagon;
			if (neighbor->private.count && neighbor->private.data[0]->fighting)
			{
				djset_union(&djset, stack->index, neighbor->private.data[0]->index);
				elements[i].value.raw[HEXAGONS_COUNT] += 1;
			}
		}
		elements[i].value.raw[HEXAGONS_COUNT] += 1;
	}

	if ((djset.classes == game->troop_stacks.count) && !assaults_count)
	{
		free(elements);
		return (struct battles *)&battles_empty; // no battles this turn
	}

	// Determine fighting location from hexagons with maximum fighting adjacent neighbors.
	for(class = djset_class_first(&djset); class; class = djset_class_next(&djset, class))
	{
		unsigned max_value = 0;
		unsigned max_count = 0;
		struct hexagon *location;

		if (game->troop_stacks.data[class - elements]->fighting != BATTLE_OPEN)
			continue; // fighting location can vary only for open battles

		// Choose battle location for the class.
		element = class;
		do
		{
			size_t index = element - elements;
			struct troop_stack *stack = game->troop_stacks.data[index];
			unsigned char fighting;

			// To select each hexagon just once, process only the first troop stack in the hexagon.
			if (stack != stack->movements[0]->private.data[0])
				continue;

			fighting = elements[index].value.raw[HEXAGONS_COUNT];
			if (fighting > max_value)
			{
				max_value = fighting;
				max_count = 1;
				location = stack->movements[0];
			}
			else if (fighting == max_value)
			{
				max_count += 1;
				if (!(random() % max_count))
					location = stack->movements[0];
			}
		} while (element = djset_next(&djset, element));

		elements[class - elements].value.pointer = location;
	}

	bitmap_hexagons = malloc(BITMAP_SIZE(game->hexagons_total));
    if (!bitmap_hexagons)
		goto finally;
    memset(bitmap_hexagons, 0, BITMAP_SIZE(game->hexagons_total));

	// Count battles and fighting troop stacks.
	// Ignore BATTLE_CAPTURE because there are no enemy troops to fight.
	battles_count = 0;
	for(class = djset_class_first(&djset); class; class = djset_class_next(&djset, class))
	{
		size_t battlefield;

		if (game->troop_stacks.data[class - elements]->fighting != BATTLE_OPEN)
			continue; // fighting location can vary only for open battles

		// One battle per equivalence class.
		battles_count += 1;

		battlefield = (struct hexagon *)elements[class - elements].value.pointer - game->hexagons;

		element = class;
		do
		{
			size_t index = element - elements;
			struct troop_stack *stack = game->troop_stacks.data[index];
			size_t index_current = stack->movements[0] - game->hexagons;

			if (bitmap_get(bitmap_hexagons, index_current))
				continue; // hexagon marked as handled

			if (hexagon_distance(game, index_current, battlefield) > 1)
			{
				if (stack->owner == stack->movements[0]->private.data[0]->owner)
					continue;

				// Opposing troop stacks on a hexagon too far from the main battle.
				// The troop stacks engage in a local battle.
				battles_count += 1;
			}

			troop_stacks_fighting += stack->movements[0]->private.count;
			bitmap_set(bitmap_hexagons, index_current, 1); // hexagon handled
		} while (element = djset_next(&djset, element));
	}
	battles_count += assaults_count;
	assert(battles_count);

	battles = malloc(offsetof(struct battles, battle) + battles_count * sizeof(*battles->battle) + troop_stacks_fighting * sizeof(*fighters));
	if (!battles)
		goto finally; // memory error
	fighters = (struct troop_stack **)(battles->battle + battles_count);
	battles->count = battles_count;

	memset(bitmap_hexagons, 0, BITMAP_SIZE(game->hexagons_total));

	battle_index = 0;
	for(class = djset_class_first(&djset); class; class = djset_class_next(&djset, class))
	{
		size_t battlefield;
		size_t fighters_count;
		struct array_troop_stacks *array;

		// One assault battle if applicable.
		struct troop_stack *first = game->troop_stacks.data[class - elements];
		if (first->fighting == BATTLE_ASSAULT)
		{
			struct hexagon *hexagon = first->movements[0];

			battles->battle[battle_index].players = (1 << first->owner) | (1 << hexagon->owner);
			battles->battle[battle_index].fighters = fighters;
			battles->battle[battle_index].type = BATTLE_ASSAULT;
			battles->battle[battle_index].location = hexagon;

			array = &hexagon->private;
			fighters_count = 0;
			for(i = 0; i < array->count; i += 1)
				fighters[fighters_count++] = array->data[i];

			// Orders in a region participating in a battle are aborted.
			// TODO why isn't this done somewhere else?
			orders_abort(game, hexagon);

			battles->battle[battle_index].fighters_count = array->count;
			fighters += array->count;
			battle_index += 1;

			continue; // nothing left to do
		}

		if (first->fighting != BATTLE_OPEN)
			continue;

		battlefield = (struct hexagon *)elements[class - elements].value.pointer - game->hexagons;

		battles->battle[battle_index].players = 0;
		battles->battle[battle_index].fighters = fighters;
		battles->battle[battle_index].type = BATTLE_OPEN;
		battles->battle[battle_index].location = game->hexagons + battlefield;

		// One battle for the equivalence class.
		element = class;
		fighters_count = 0;
		do
		{
			size_t index = element - elements;
			struct troop_stack *stack = game->troop_stacks.data[index];
			size_t index_current = stack->movements[0] - game->hexagons;

			if (bitmap_get(bitmap_hexagons, index_current))
				continue; // hexagon marked as handled

			if (hexagon_distance(game, index_current, battlefield) > 1)
				continue;

			array = &stack->movements[0]->private;
			for(i = 0; i < array->count; i += 1)
			{
				battles->battle[battle_index].players |= 1 << array->data[i]->owner;
				fighters[fighters_count++] = array->data[i];
			}

			// Orders in a region participating in a battle are aborted.
			// TODO why isn't this done somewhere else?
			orders_abort(game, stack->movements[0]);

			bitmap_set(bitmap_hexagons, index_current, 1); // hexagon handled
		} while (element = djset_next(&djset, element));
		fighters += fighters_count;
		battles->battle[battle_index].fighters_count = fighters_count;
		battle_index += 1;

		// One battle for each hexagon too far from the main battle that has opposing stacks.
		element = class;
		do
		{
			size_t index = element - elements;
			struct troop_stack *stack = game->troop_stacks.data[index];
			size_t index_current = stack->movements[0] - game->hexagons;

			if (bitmap_get(bitmap_hexagons, index_current))
				continue; // hexagon marked as handled

			if (hexagon_distance(game, index_current, battlefield) <= 1)
				continue;

			array = &stack->movements[0]->private;
			if (stack->owner == array->data[0]->owner)
				continue;

			battles->battle[battle_index].players = 0;
			battles->battle[battle_index].fighters = fighters;
			battles->battle[battle_index].type = BATTLE_OPEN;
			battles->battle[battle_index].location = game->hexagons + index_current;

			fighters_count = 0;
			for(i = 0; i < array->count; i += 1)
			{
				battles->battle[battle_index].players |= 1 << array->data[i]->owner;
				fighters[fighters_count++] = array->data[i];
			}

			// Orders in a region participating in a battle are aborted.
			// TODO why isn't this done somewhere else?
			orders_abort(game, stack->movements[0]);

			fighters += fighters_count;
			battles->battle[battle_index].fighters_count = fighters_count;
			battle_index += 1;

			bitmap_set(bitmap_hexagons, index_current, 1); // hexagon handled
		} while (element = djset_next(&djset, element));
	}

	// Shuffle battle order randomly to avoid bias.
	for(i = battles_count; i > 1; i -= 1)
	{
		struct battles swap = battles[i - 1];
		j = random() % i;
		battles[i - 1] = battles[j];
		battles[j] = swap;
	}

finally:
	free(bitmap_hexagons);
	free(elements);
	return battles;
}

void turn_battles_term(struct game *restrict game, struct battles *restrict battles)
{
	size_t i, j, k;

	if (!battles->count)
		return; // nothing to do for battles_empty

	// Remove dead troops and troop stacks.
	for(i = 0; i < battles->count; i += 1)
	{
		for(j = 0; j < battles->battle[i].fighters_count; j += 1)
		{
			int alive = 0;
			struct troop_stack **fighters = battles->battle[i].fighters;

			for(k = 0; k < fighters[j]->private.count; k += 1)
			{
				if (fighters[j]->private.troops[k]->count)
					alive = 1;
				else
				{
					free(fighters[j]->private.troops[k]);
					fighters[j]->private.count -= 1;
					fighters[j]->private.troops[k] = fighters[j]->private.troops[fighters[j]->private.count];
				}
			}

			if (!alive)
			{
				const struct array_troop_stacks *array = &fighters[j]->movements[0]->private;
				for(k = 0; k < array->count; k += 1)
					if (array->data[k] == fighters[j])
					{
						troop_stack_remove(game, fighters[j]->movements[0], k);
						break;
					}
			}
		}
	}

	free(battles);
}
