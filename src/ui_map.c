/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include <GL/gl.h>
#include <xcb/xcb.h>

#include "basic.h"
#include "log.h"
#include "format.h"
#include "bitmap.h"
#include "game.h"
#include "map.h"
#include "turn.h"
#include "economy.h"
#include "font.h"
#include "image.h"
#include "interface.h"
#include "interface_storage.h"
#include "input.h"
#include "display.h"
#include "ui.h"
#include "ui_menu.h"
#include "ui_map.h"

#define NAME_X (PANEL_LEFT + PANEL_BORDER + 1)
#define NAME_Y (PANEL_TOP + PANEL_BORDER + 1)

#define FLAG_X 112
#define FLAG_Y 40

#define TRAINING_X (PANEL_LEFT + PANEL_BORDER + 1)

#define POPULATION_X (PANEL_LEFT + PANEL_BORDER + 1)
#define POPULATION_Y 190

#define HAPPINESS_X (PANEL_LEFT + PANEL_BORDER + 1 + 160)
#define HAPPINESS_Y 196
#define HAPPINESS_WIDTH 80
#define HAPPINESS_HEIGHT 5

#define COMMANDS_X (PANEL_LEFT + PANEL_BORDER + 1)
#define COMMANDS_Y 560

#define TOOLTIP_X 4
#define TOOLTIP_Y (CONTENT_HEIGHT - 20)
#define COST_OFFSET 120

#define SCROLL_SPEED 25

#define S(s) (s), sizeof(s) - 1

//#define VERBOSE /* used for debugging the GUI */

// TODO fix this static variable
static const struct window *w;

struct state_map
{
	struct game *game;
	unsigned char player;

	unsigned char *visible;

	struct hexagon *hexagon;
	struct
	{
		size_t offset;
		size_t selected;
	} troop_stack;
	struct region *region;
	unsigned building;

	unsigned char buildings[64];
	size_t buildings_count;

	unsigned char selected[TROOPS_CAPACITY_MAX];
	unsigned selected_count;
	unsigned selected_workers;

	int drag_index;
	enum {DRAG_GARRISON, DRAG_STACK} drag_source;

	enum {HOVER_NONE, HOVER_UNIT, HOVER_BUILDING, HOVER_VESSEL} hover_type;
	size_t hover_index;

	double scroll_start;
	int scroll_x, scroll_y;
	int delta_x, delta_y;
};

static void scroll_update(struct state_map *restrict state, int delta_x, int delta_y)
{
	if (MAP_WIDTH > CONTENT_WIDTH)
	{
		state->scroll_x = state->scroll_x + delta_x;
		if (state->scroll_x < 0)
			state->scroll_x = 0;
		else if (state->scroll_x > MAP_WIDTH - CONTENT_WIDTH)
			state->scroll_x = MAP_WIDTH - CONTENT_WIDTH;
	}

	if (MAP_HEIGHT > CONTENT_HEIGHT)
	{
		state->scroll_y = state->scroll_y + delta_y;
		if (state->scroll_y < 0)
			state->scroll_y = 0;
		else if (state->scroll_y > MAP_HEIGHT - CONTENT_HEIGHT)
			state->scroll_y = MAP_HEIGHT - CONTENT_HEIGHT;
	}
}

static int needs_scroll(struct state_map *restrict state, double progress)
{
	int count = SCROLL_SPEED * (progress - state->scroll_start) + 0.5;

	if (isnan(state->scroll_start))
		return 0; // screen is not scrolling

	scroll_update(state, count * state->delta_x, count * state->delta_y);

	return 1;
}

static int scroll_timer(void *restrict context, double progress)
{
	struct state_map *state = context;
	needs_scroll(state, progress);
	return INPUT_DONE;
}

static void show_resource(const struct image *restrict image, int treasury, int income, unsigned x, unsigned y)
{
	char buffer[FORMAT_SIZE_BASE10(int)], *end;

	fill_image(image, x, y, image->width, image->height, 0);
	x += image->width + 2;
	y += (widgets[Resources].height - font10.size) / 2;

	end = format_int(buffer, treasury, 10);
	draw_string(slice(buffer, end - buffer), x, y, &font10, display_colors[Text]);
	if (income)
	{
		struct box box = string_box(slice(buffer, end - buffer), &font10);
		x += box.width + 2;

		if (income > 0)
		{
			*buffer = '+';
			end = buffer + 1;
		}
		else
			end = buffer;
		end = format_int(end, income, 10);

		draw_string(slice(buffer, end - buffer), x, y, &font10, display_colors[((income > 0) ? Success : Error)]);
	}
}

static void show_progress(unsigned current, unsigned total, unsigned x, unsigned y, unsigned width, unsigned height)
{
	// Progress is visualized as a sector with length proportional to the remaining time.

	double progress = (double)current / total;
	double angle = progress * TAU;

	if (!current)
	{
		fill_rectangle(x, y, width, height, display_colors[Progress]);
		return;
	}

	glColor4ubv(display_colors[Progress]);

	glBegin(GL_POLYGON);

	glVertex2f(x + width / 2, y + height / 2);
	switch ((unsigned)(progress * 8))
	{
	case 0:
	case 7:
		glVertex2f(x + width, y + (1 - sin(angle)) * height / 2);
		break;

	case 1:
	case 2:
		glVertex2f(x + (1 + cos(angle)) * width / 2, y);
		break;

	case 3:
	case 4:
		glVertex2f(x, y + (1 - sin(angle)) * height / 2);
		break;

	case 5:
	case 6:
		glVertex2f(x + (1 + cos(angle)) * width / 2, y + height);
		break;
	}
	switch ((unsigned)(progress * 8))
	{
	case 0:
		glVertex2f(x + width, y);
	case 1:
	case 2:
		glVertex2f(x, y);
	case 3:
	case 4:
		glVertex2f(x, y + height);
	case 5:
	case 6:
		glVertex2f(x + width, y + height);
	}
	glVertex2f(x + width, y + height / 2);

	glEnd();
}

static void show_cost(const struct resources *restrict treasury, struct slice description, const struct resources *restrict cost, unsigned time, const unsigned char *restrict color)
{
	char buffer[FORMAT_SIZE_BASE10(time)];
	size_t length;

	unsigned offset = COST_OFFSET;

	draw_string(description, TOOLTIP_X, TOOLTIP_Y, &font12, color);

	if (cost->gold)
	{
		fill_image(&image_gold, TOOLTIP_X + offset, TOOLTIP_Y, image_gold.width, image_gold.height, 0);
		offset += image_gold.width;

		length = format_uint(buffer, -cost->gold, 10) - (uint8_t *)buffer;
		draw_string(slice(buffer, length), TOOLTIP_X + offset, TOOLTIP_Y, &font12, display_colors[TextMenu]);
		offset += 40;
	}
	if (cost->food)
	{
		fill_image(&image_food, TOOLTIP_X + offset, TOOLTIP_Y, image_food.width, image_food.height, 0);
		offset += image_food.width;

		length = format_uint(buffer, -cost->food, 10) - (uint8_t *)buffer;
		draw_string(slice(buffer, length), TOOLTIP_X + offset, TOOLTIP_Y, &font12, display_colors[TextMenu]);
		offset += 40;
	}
	if (cost->wood)
	{
		fill_image(&image_wood, TOOLTIP_X + offset, TOOLTIP_Y, image_wood.width, image_wood.height, 0);
		offset += image_wood.width;

		length = format_uint(buffer, -cost->wood, 10) - (uint8_t *)buffer;
		draw_string(slice(buffer, length), TOOLTIP_X + offset, TOOLTIP_Y, &font12, display_colors[TextMenu]);
		offset += 40;
	}
	if (cost->stone)
	{
		fill_image(&image_stone, TOOLTIP_X + offset, TOOLTIP_Y, image_stone.width, image_stone.height, 0);
		offset += image_stone.width;

		length = format_uint(buffer, -cost->stone, 10) - (uint8_t *)buffer;
		draw_string(slice(buffer, length), TOOLTIP_X + offset, TOOLTIP_Y, &font12, display_colors[TextMenu]);
		offset += 40;
	}

	if (time)
	{
		fill_image(&image_time, TOOLTIP_X + offset, TOOLTIP_Y, image_time.width, image_time.height, 0);
		offset += image_time.width;

		length = format_uint(buffer, time, 10) - (uint8_t *)buffer;
		draw_string(slice(buffer, length), TOOLTIP_X + offset, TOOLTIP_Y, &font12, display_colors[TextMenu]);
		offset += 40;
	}
}

void show_panel(const struct game *restrict game, unsigned player)
{
	char buffer[256], *end;

	// background
	fill_rectangle(PANEL_LEFT, PANEL_TOP, PANEL_WIDTH, PANEL_HEIGHT, display_colors[Player + player]);
	fill_image(&image_panel, CONTROLS_X, CONTROLS_Y, CONTROLS_WIDTH, CONTROLS_HEIGHT, 1);

	end = format_int(format_bytes(buffer, S("Turn ")), game->turn, 10);
	draw_string(slice(buffer, end - buffer), TURN_X, TURN_Y, &font12, display_colors[Text]);

	display_button(slice("Menu"), BUTTON_MENU_X, BUTTONS_Y, 0);
	display_button(slice("End Turn"), BUTTON_END_X, BUTTONS_Y, 0);
}

static void display_ship(const struct game *restrict game, unsigned x, unsigned y, struct hexagon_neighbor *restrict neighbors, size_t neighbors_count, unsigned char owner)
{
	size_t i;

	for(i = 0; i < neighbors_count; i += 1)
		if (neighbors[i].hexagon->terrain == TERRAIN_WATER)
		{
			const int offsets[][2] =
			{
				[East] = {SQRT3 * HEXAGON_EDGE - image_ship_small.width / 2, (2 * HEXAGON_EDGE - image_ship_small.height) / 2},
				[NorthEast] = {(3 * SQRT3 * HEXAGON_EDGE / 2 - image_ship_small.width) / 2, (HEXAGON_EDGE / 2 - image_ship_small.height) / 2},
				[NorthWest] = {(SQRT3 * HEXAGON_EDGE / 2 - image_ship_small.width) / 2, (HEXAGON_EDGE / 2 - image_ship_small.height) / 2},
				[West] = {-image_ship_small.width / 2, (HEXAGON_EDGE - image_ship_small.height) / 2},
				[SouthWest] = {(SQRT3 * HEXAGON_EDGE / 2 - image_ship_small.width) / 2, (7 * HEXAGON_EDGE / 2 - image_ship_small.height) / 2},
				[SouthEast] = {(3 * SQRT3 * HEXAGON_EDGE / 2 - image_ship_small.width) / 2, (7 * HEXAGON_EDGE / 2 - image_ship_small.height) / 2},
			};
			const int *offset = offsets[neighbors[i].direction];

			fill_image(&image_ship_small, x + offset[0], y + offset[1], image_ship_small.width, image_ship_small.height, 0);
			fill_image_mask(&image_ship_small_mask, x + offset[0], y + offset[1], image_ship_small.width, image_ship_small.height, display_colors[Player + owner]);
			return;
		}
	assert(0);
}

static void show_map(const void *argument, double progress)
{
	const struct state_map *state = argument;
	struct game *restrict game = (struct game *)state->game; // TODO fix cast
	const struct player *player = game->players + state->player;

	char buffer[256], *end;
	struct vector position;
	int offset_x, offset_y;

	size_t i;
	size_t hexagons_total;

	const struct array_troop_stacks *stacks = 0;

	if (state->hexagon)
		stacks = troop_stacks(state->hexagon, state->player);

	show_panel(game, state->player);

#if defined(VERBOSE)
	fill_rectangle(widgets[GarrisonTroops].left, widgets[GarrisonTroops].top, widgets[GarrisonTroops].span_x, widgets[GarrisonTroops].span_y, display_colors[Text]);
	fill_rectangle(widgets[TrainQueue].left, widgets[TrainQueue].top, widgets[TrainQueue].span_x, widgets[TrainQueue].span_y, display_colors[Text]);
	fill_rectangle(widgets[Buildings].left, widgets[Buildings].top, widgets[Buildings].span_x, widgets[Buildings].span_y, display_colors[Text]);
	fill_rectangle(widgets[Troops].left, widgets[Troops].top, widgets[Troops].span_x, widgets[Troops].span_y, display_colors[Text]);
	fill_rectangle(widgets[TroopsTabs].left, widgets[TroopsTabs].top, widgets[TroopsTabs].span_x, widgets[TroopsTabs].span_y, display_colors[Text]);
	fill_rectangle(widgets[Resources].left, widgets[Resources].top, widgets[Resources].span_x, widgets[Resources].span_y, display_colors[Text]);
	fill_rectangle(widgets[Orders].left, widgets[Orders].top, widgets[Orders].span_x, widgets[Orders].span_y, display_colors[Text]);
#endif

	/* TODO support multi-line string; implement this then
	if (state->building != BUILDINGS_COUNT)
	{
		struct slice s = slice(BUILDINGS[state->building].description, strlen(BUILDINGS[state->building].description));

		struct box box = string_box(s, &font12);
		//draw_string(s, NAME_X, NAME_Y, &font12, display_colors[Text]);
	}
	*/

	if (state->hexagon)
	{
		if (state->hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
			draw_string(slice(state->region->name, state->region->name_size), NAME_X, NAME_Y, &font12, display_colors[Text]);
		else if (state->hexagon->environment & (1 << ENVIRONMENT_FOREST))
			draw_string(slice("Forest"), NAME_X, NAME_Y, &font12, display_colors[Text]);
		else if (state->hexagon->environment & (1 << ENVIRONMENT_QUARRY))
			draw_string(slice("Quarry"), NAME_X, NAME_Y, &font12, display_colors[Text]);

		if (state->hexagon->buildings || (state->hexagon->build_index >= 0))
			display_flag(FLAG_X, FLAG_Y, state->hexagon->owner);

		// buildings
		if (state->hexagon->buildings || (state->hexagon->build_index >= 0) || state->selected_workers)
		{
			for(i = 0; i < state->buildings_count; i += 1)
			{
				if (hexagon_built(state->hexagon, state->buildings[i]) && allies(game, state->hexagon->owner, state->player))
					display_building(BUILDINGS + state->buildings[i], i, 1);
				else if (state->buildings[i] == state->hexagon->build_index)
				{
					display_building(BUILDINGS + state->hexagon->build_index, i, 0);

					position = object_position(&widgets[Buildings], i);
					show_progress(state->hexagon->build_progress, BUILDINGS[state->hexagon->build_index].time, position.x, position.y, BUILDING_EDGE, BUILDING_EDGE);
					offset_x = position.x + widgets[Buildings].width - image_construction.width;
					fill_image(&image_construction, offset_x, position.y, image_construction.width, image_construction.height, 0);
				}
				else if (state->selected_workers)
					display_building(BUILDINGS + state->buildings[i], i, 0);
			}
		}
	}

	if (state->region)
	{
		//const struct slice happiness[] = {slice("Outraged"), slice("Furious"), slice("Angry"), slice("Irritated"), slice("Indifferent"), slice("Happy"), slice("Joyful"), slice("Blissful")};
		const unsigned char colors[][4] = {{255, 0, 0, 255}, {255, 128, 0, 255}, {255, 255, 0, 255}, {192, 192, 64, 255}, {128, 128, 128, 255}, {64, 192, 192, 255}, {0, 192, 128, 255}, {0, 192, 0, 255}};
		size_t index;

		// population
		end = format_bytes(buffer, S("Population: "));
		end = format_int(end, state->region->population, 10);
		draw_string(slice(buffer, end - buffer), POPULATION_X, POPULATION_Y, &font12, display_colors[Text]);

		// happiness
		if (state->region->happiness == 1)
			index = (sizeof(colors) / sizeof(*colors)) - 1;
		else
			index = state->region->happiness * (sizeof(colors) / sizeof(*colors));
		fill_rectangle(HAPPINESS_X, HAPPINESS_Y, HAPPINESS_WIDTH, HAPPINESS_HEIGHT, display_colors[Background]);
		fill_rectangle(HAPPINESS_X, HAPPINESS_Y, HAPPINESS_WIDTH * state->region->happiness, HAPPINESS_HEIGHT, colors[index]);

		if (state->hexagon->buildings && (state->hexagon->owner == state->player))
		{
			// garrison
			size_t capacity = garrison_info(state->hexagon)->capacity;
			for(i = 0; i < capacity; i += 1)
			{
				const struct troop *restrict troop;

				if ((i == state->region->garrison.private.count) && (state->drag_index < 0))
					break;

				troop = state->region->garrison.private.troops[i];
				position = object_position(&widgets[GarrisonTroops], i);

				if (((i == state->drag_index) && (state->drag_source == DRAG_GARRISON)) || (i >= state->region->garrison.private.count))
					fill_rectangle(position.x, position.y, TROOP_EDGE, TROOP_EDGE, display_colors[Background]);
				else
					display_troop(troop->unit, troop->count, Player + state->region->garrison.hexagon->owner, position.x, position.y, Text);
			}

			if (!state->region->sieged)
			{
				// training queue
				end = format_bytes(buffer, S("Training:"));
				draw_string(slice(buffer, end - buffer), TRAINING_X, widgets[TrainQueue].top + (widgets[TrainQueue].span_y - font12.size) / 2, &font12, display_colors[Text]);
				for(i = 0; i < TRAIN_QUEUE; i += 1)
				{
					position = object_position(&widgets[TrainQueue], i);
					if (state->region->train[i])
					{
						display_troop(state->region->train[i], 0, Player + PLAYER_NEUTRAL, position.x, position.y, Text);
						show_progress((i == 0) ? state->region->train_progress : 0, state->region->train[0]->time, position.x, position.y, TROOP_EDGE, TROOP_EDGE);
					}
					else
						fill_rectangle(position.x, position.y, TROOP_EDGE, TROOP_EDGE, display_colors[Background]);
				}

				if (!state->selected_count && (state->drag_index < 0))
				{
					// troops to train
					draw_string(slice("Train troops"), COMMANDS_X, COMMANDS_Y, &font12, display_colors[Text]);
					for(i = 0; i < UNITS_COUNT; i += 1)
						if (unit_available(state->region, UNITS + i))
							display_unit(state->region, UNITS + i);
				}
			}
		}
		else if (state->region->sieged)
		{
			end = format_bytes(buffer, S("Under siege"));
			draw_string(slice(buffer, end - buffer), TRAINING_X, widgets[TrainQueue].top + (widgets[TrainQueue].span_y - font12.size) / 2, &font12, display_colors[Text]);
		}
	}

	if (state->hexagon && state->hexagon->buildings && (state->hexagon->owner == state->player) && (!state->region || !state->region->sieged))
	{
		// ship construction
		if ((state->hexagon->vessel_index >= 0) || (vessel_available(state->hexagon, VESSELS + VesselWater) && state->selected_workers))
		{
			fill_image(&image_unit_ship_gray, widgets[Ship].left, widgets[Ship].top, image_unit_ship_gray.width, image_unit_ship_gray.height, 0);
			if (state->hexagon->vessel_index >= 0)
			{
				position = object_position(&widgets[Ship], 0);
				show_progress(state->hexagon->vessel_progress, VESSELS[state->hexagon->vessel_index].time, position.x, position.y, TROOP_EDGE, TROOP_EDGE);
				offset_x = position.x + widgets[Ship].width - image_construction.width;
				fill_image(&image_construction, offset_x, position.y, image_construction.width, image_construction.height, 0);
			}
		}
	}

	// troop stacks
	if (stacks && stacks->count)
	{
		size_t end;
		unsigned margin;
		unsigned tabs;
		const struct troop_stack *troop_stack = stacks->data[state->troop_stack.selected];

		// tabs
		if (state->troop_stack.offset)
			fill_image(&image_tab_left, TROOPS_ARROW_LEFT_X, TROOPS_ARROW_Y, image_tab_left.width, image_tab_left.height, 0);
		end = state->troop_stack.offset + widgets[TroopsTabs].columns;
		for(i = state->troop_stack.offset; (i < end) && (i < stacks->count); i += 1)
		{
			const struct troop_stack *item = stacks->data[i];

			position = object_position(&widgets[TroopsTabs], i - state->troop_stack.offset);
			display_troop_stack(item, position.x + 2, position.y + 3, game, state->player);
			if ((item->owner == state->player) && (item->movements_count > 1))
			{
				struct hexagon *target = item->movements[item->movements_count - 1];
				struct image *image;
				int offset_x, offset_y;

				if (target->buildings && !allies(game, target->owner, state->player) && !target->armies.field)
					image = &image_assault;
				else
					image = &image_movement;
				offset_x = position.x + 3 + TROOP_EDGE - image->width;
				offset_y = position.y + 3 + TROOP_EDGE - image->height;
				fill_image(image, offset_x, offset_y, image->width, image->height, 0);
			}
			else if (item->vessel->autonomous && (item->movements[0]->terrain != TERRAIN_WATER))
			{
				int offset_x = position.x + 3 + TROOP_EDGE - image_anchor.width;
				int offset_y = position.y + 3;
				fill_image(&image_anchor, offset_x, offset_y, image_anchor.width, image_anchor.height, 0);
			}
			if (i == state->troop_stack.selected)
				fill_image(&image_tab_active, position.x, position.y, image_tab_active.width, image_tab_active.height, 0);
			else
				fill_image(&image_tab_inactive, position.x, position.y, image_tab_inactive.width, image_tab_inactive.height, 0);
		}
		if (stacks->count > end)
			fill_image(&image_tab_right, TROOPS_ARROW_RIGHT_X, TROOPS_ARROW_Y, image_tab_right.width, image_tab_right.height, 0);

		// borders around the troops
		margin = (PANEL_WIDTH - widgets[TroopsTabs].span_x - 2 * TROOPS_PADDING) / 2;
		draw_line(PANEL_LEFT + margin, TROOPS_BORDER_TOP + 1, widgets[TroopsTabs].left, TROOPS_BORDER_TOP + 1, display_colors[TabBorder]);
		tabs = stacks->count - state->troop_stack.offset;
		if (tabs > widgets[TroopsTabs].columns)
			tabs = widgets[TroopsTabs].columns;
		position = object_position(&widgets[TroopsTabs], tabs);
		draw_line(position.x - widgets[TroopsTabs].padding, TROOPS_BORDER_TOP + 1, PANEL_LEFT + PANEL_WIDTH - margin, TROOPS_BORDER_TOP + 1, display_colors[TabBorder]);
		draw_line(PANEL_LEFT + margin + 1, TROOPS_BORDER_TOP, PANEL_LEFT + margin + 1, TROOPS_BORDER_TOP + TROOPS_BORDER_HEIGHT - 1, display_colors[TabBorder]);
		draw_line(PANEL_LEFT + PANEL_WIDTH - margin + 1, TROOPS_BORDER_TOP, PANEL_LEFT + PANEL_WIDTH - margin, TROOPS_BORDER_TOP + TROOPS_BORDER_HEIGHT - 1, display_colors[TabBorder]);
		draw_line(PANEL_LEFT + margin, TROOPS_BORDER_TOP + TROOPS_BORDER_HEIGHT - 1, PANEL_LEFT + PANEL_WIDTH - margin, TROOPS_BORDER_TOP + TROOPS_BORDER_HEIGHT - 1, display_colors[TabBorder]);

		if (allies(game, troop_stack->owner, state->player))
		{
			const struct troops *troops = ((troop_stack->owner == state->player) ? &troop_stack->private : &troop_stack->public);
			for(i = 0; i < troops->count; i += 1)
			{
				const struct troop *restrict troop = troops->troops[i];

				position = object_position(&widgets[Troops], i);
				if ((i == state->drag_index) && (state->drag_source == DRAG_STACK))
					fill_rectangle(position.x, position.y, TROOP_EDGE, TROOP_EDGE, display_colors[Background]);
				else
				{
					display_troop(troop->unit, troop->count, Player + troop_stack->owner, position.x, position.y, Text);
					if (troop_stack->owner == state->player)
					{
						switch (troop->task)
						{
						case TASK_BUILD:
							offset_x = position.x + widgets[Troops].width - image_construction.width;
							fill_image(&image_construction, offset_x, position.y, image_construction.width, image_construction.height, 0);
							break;

						case TASK_DISMISS:
							offset_x = position.x + widgets[Troops].width - image_dismiss.width;
							fill_image(&image_dismiss, offset_x, position.y, image_dismiss.width, image_dismiss.height, 0);
							break;
						}
					}
				}

				if (state->selected[i])
					draw_rectangle(position.x - 1, position.y - 1, TROOP_EDGE + 2, TROOP_EDGE + 2, display_colors[Active]);
			}
		}
		else
		{
			position = object_position(&widgets[Troops], 0);
			draw_string(slice("Unknown"), position.x, position.y, &font12, display_colors[Text]);
		}
	}

	if (state->hexagon && state->selected_count)
	{
		// dismiss
		position = object_position(&widgets[Orders], 0);
		fill_image(&image_order_dismiss, position.x, position.y, image_order_dismiss.width, image_order_dismiss.height, 0);
	}

	if (state->hexagon && (state->drag_index >= 0))
	{
		// split
		position = object_position(&widgets[Orders], 0);
		fill_image(&image_order_add, position.x, position.y, image_order_add.width, image_order_add.height, 0);
	}

	// resources
	position = object_position(&widgets[Resources], 0);
	show_resource(&image_gold, player->treasury.gold, player->income.gold, position.x, position.y);
	position = object_position(&widgets[Resources], 1);
	show_resource(&image_wood, player->treasury.wood, player->income.wood, position.x, position.y);
	position = object_position(&widgets[Resources], 2);
	show_resource(&image_stone, player->treasury.stone, player->income.stone, position.x, position.y);
	position = object_position(&widgets[Resources], 3);
	show_resource(&image_food, player->treasury.food, player->income.food, position.x, position.y);

	// TODO display granary size; display expenses

	if_window(w);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// map
	hexagons_total = hexagons_count(game);
	for(i = 0; i < hexagons_total; i += 1)
	{
		struct tile tile = hexagon_location(game, i);
		struct array_troop_stacks *array = troop_stacks(game->hexagons + i, state->player);
		struct army *army;
		struct image *image;
		size_t j;

		offset_x = hexagon_offset_x(-state->scroll_x, tile.x, tile.y, HEXAGON_EDGE);
		offset_y = hexagon_offset_y(-state->scroll_y, tile.x, tile.y, HEXAGON_EDGE);

		if (!bitmap_get(state->visible, i))
		{
			// Hexagon is not visible.
			display_hexagon(offset_x, offset_y, 1, display_colors[Hidden]);
			continue;
		}

		switch (game->hexagons[i].terrain)
		{
		case TERRAIN_GROUND:
			image = &image_terrain_ground;
			break;

		case TERRAIN_MOUNTAIN:
			image = &image_terrain_mountain;
			break;

		case TERRAIN_WATER:
			image = &image_terrain_water;
			break;
		}
		fill_image(image, offset_x, offset_y, image->width, image->height, 0);
		if (game->hexagons[i].terrain == TERRAIN_WATER)
		{
			unsigned char coast[6] = {0};
			unsigned char coastal = 0;

			struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
			size_t count = hexagon_neighbors(game, game->hexagons + i, neighbors);

			for(j = 0; j < count; j += 1)
				if (neighbors[j].hexagon->terrain != TERRAIN_WATER)
				{
					coast[neighbors[j].direction] = 1;
					coastal = 1;
				}
			if (coastal)
				fill_image_hexagon(&image_terrain_coast, offset_x, offset_y, coast);
		}

		army = &game->hexagons[i].armies;
		if (!army->field && !game->hexagons[i].buildings)
			army = 0;

		// Display hexagon object.
		if (game->hexagons[i].environment & (1 << ENVIRONMENT_SETTLEMENT))
			display_settlement(game, tile, -state->scroll_x, -state->scroll_y, game->hexagons[i].garrison, army, state->player, HEXAGON_EDGE);
		else if (game->hexagons[i].environment & (1 << ENVIRONMENT_FOREST))
			display_object(game, &image_forest, army, state->player, tile, -state->scroll_x, -state->scroll_y, HEXAGON_EDGE);
		else if (game->hexagons[i].environment & (1 << ENVIRONMENT_QUARRY))
			display_object(game, &image_quarry, army, state->player, tile, -state->scroll_x, -state->scroll_y, HEXAGON_EDGE);
		else if (array->count)
		{ 
			if (game->hexagons[i].terrain == TERRAIN_GROUND)
			{
				for(j = 0; j < array->count; j += 1)
					if (!array->data[j]->vessel->autonomous)
					{
						display_object(game, &image_army, army, state->player, tile, -state->scroll_x, -state->scroll_y, HEXAGON_EDGE);
						break;
					}
			}
			else if (game->hexagons[i].terrain == TERRAIN_WATER)
				display_object(game, &image_ship, army, state->player, tile, -state->scroll_x, -state->scroll_y, HEXAGON_EDGE);
			else
				assert(0);
		}

#if defined(VERBOSE)
		{
			char buffer[FORMAT_SIZE_BASE10(int)], *end;
			end = format_int(buffer, i, 10);
			draw_string(slice(buffer, end - buffer), offset_x + 10, offset_y + 10, &font12, display_colors[Text]);
		}
#endif
	}

	for(i = 0; i < hexagons_total; i += 1)
	{
		struct tile tile = hexagon_location(game, i);
		struct array_troop_stacks *array = troop_stacks(game->hexagons + i, state->player);
		struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
		size_t count = hexagon_neighbors(game, game->hexagons + i, neighbors);
		size_t j;

		offset_x = hexagon_offset_x(-state->scroll_x, tile.x, tile.y, HEXAGON_EDGE);
		offset_y = hexagon_offset_y(-state->scroll_y, tile.x, tile.y, HEXAGON_EDGE);

		if (game->hexagons[i].terrain == TERRAIN_GROUND)
			for(j = 0; j < array->count; j += 1)
				if (array->data[j]->vessel->autonomous)
				{
					display_ship(game, offset_x, offset_y, neighbors, count, array->data[j]->owner);
					break;
				}
	}

	// troop stack movement
	if (stacks && stacks->count)
	{
		const struct troop_stack *troop_stack = stacks->data[state->troop_stack.selected];

		if (troop_stack->owner == state->player)
		{
			for(i = 1; i < troop_stack->movements_count; i += 1)
			{
				struct tile tile = hexagon_location(game, troop_stack->movements[i] - game->hexagons);
				offset_x = hexagon_offset_x(-state->scroll_x, tile.x, tile.y, HEXAGON_EDGE);
				offset_y = hexagon_offset_y(-state->scroll_y, tile.x, tile.y, HEXAGON_EDGE);

				if (i + 1 == troop_stack->movements_count)
					fill_image(&image_move_end, offset_x, offset_y, image_move_end.width, image_move_end.height, 0);
				else
				{
					struct frame frame;
					struct vector center = {offset_x + image_move.width / 2 + 0.5, offset_y + image_move.height / 2 + 0.5};
					int from = troop_stack->movements[i] - game->hexagons;
					int to = troop_stack->movements[i + 1] - game->hexagons;
					double orientation = hexagon_neighbor_direction(game, from, to) * (TAU / DIRECTIONS_COUNT);

					// Display arrow in the direction of movement.
					frame_center(&frame, center, image_move.width, image_move.height, (struct vector){0, 0}, orientation);
					frame_image(&image_move, &frame);
				}
			}
		}
	}

	// tooltips
	switch (state->hover_type)
	{
	case HOVER_UNIT:
		{
			const struct unit *unit = UNITS + state->hover_index;
			const unsigned char *color;

			if (resources_enough(&player->treasury, &unit->cost) && population_enough(state->region, unit))
				color = display_colors[TextMenu];
			else
				color = display_colors[Error];
			show_cost(&player->treasury, slice(unit->name, unit->name_size), &unit->cost, unit->time, color);
		}
		break;

	case HOVER_BUILDING:
		assert(state->hexagon);
		{
			const struct building *building = BUILDINGS + state->hover_index;
			if (hexagon_built(state->hexagon, state->hover_index))
				draw_string(slice(building->name, building->name_size), TOOLTIP_X, TOOLTIP_Y, &font12, display_colors[TextMenu]);
			else
			{
				const unsigned char *color;
				unsigned time = ceil(building->time / workers_progress(state->selected_workers));

				if (state->hover_index == state->hexagon->build_index)
					color = display_colors[TextMenu];
				else if (resources_enough(&player->treasury, &building->cost))
					color = display_colors[TextMenu];
				else
					color = display_colors[Error];
				show_cost(&player->treasury, slice(building->name, building->name_size), &building->cost, time, color);
			}
		}
		break;

	case HOVER_VESSEL:
		{
			const struct vessel *vessel = VESSELS + VesselWater;
			const unsigned char *color = display_colors[resources_enough(&player->treasury, &vessel->cost) ? TextMenu : Error];

			// TODO time calculation is messy

			show_cost(&player->treasury, slice(vessel->name, vessel->name_size), &vessel->cost, 0, color);
		}
		break;
	}

	if_window_default();
}

static int input_none(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	if (code == EVENT_MOTION)
	{
		int change = ((state->hover_type != HOVER_NONE) || needs_scroll(state, progress));

		state->hover_type = HOVER_NONE;

		if (x == 0)
			state->delta_x = -1;
		else if (x == WINDOW_WIDTH - 1)
			state->delta_x = 1;
		else
			state->delta_x = 0;

		if (y == 0)
			state->delta_y = -1;
		else if (y == WINDOW_HEIGHT - 1)
			state->delta_y = 1;
		else
			state->delta_y = 0;

		if (state->delta_x || state->delta_y)
			state->scroll_start = progress;
		else
			state->scroll_start = NAN;

		return (change ? INPUT_DONE : INPUT_IGNORE);
	}
	if (code == EVENT_DRAG_STOP)
	{
		if_cursor_set(cursors[UNITS_COUNT]);
		state->drag_index = -1;
		return INPUT_DONE;
	}
	return INPUT_IGNORE;
}

static int input_menu(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	const struct game *restrict game = state->game;
	int status;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_IGNORE;

	if_window_hide(w);
	status = input_menu_save(game);
	if_window_show(w);
	return status;
}

static int troop_drag(struct state_map *restrict state, struct troops *restrict source, unsigned index)
{
	if (index >= source->count)
		return INPUT_IGNORE;

	if_cursor_set(cursors[source->troops[index]->unit - UNITS]);
	state->drag_index = index;
	state->drag_source = (source == &state->region->garrison.private) ? DRAG_GARRISON : DRAG_STACK;

	memset(state->selected, 0, sizeof(state->selected));
	state->selected_count = 0;
	state->selected_workers = 0;

	return INPUT_DONE;
}

static int troop_drop(struct state_map *restrict state, struct troops *restrict target, unsigned index, unsigned capacity)
{
	struct troop_stack *source_stack = 0;
	struct troops *source;

	if (state->drag_index < 0)
		return INPUT_IGNORE;

	// Find the troop that is being dragged.
	if (state->drag_source == DRAG_GARRISON)
		source = &state->region->garrison.private;
	else
	{
		assert(state->drag_source == DRAG_STACK);

		source_stack = troop_stacks(state->hexagon, state->player)->data[state->troop_stack.selected];
		source = &source_stack->private;
	}

	if (index >= target->count)
	{
		// If there is space left, move troop to last position.
		if ((target == source) || (target->count < capacity))
			troop_move(source, target, state->drag_index);

		// Check if source troop stack needs to be removed.
		if (source_stack && !source_stack->private.count && !source_stack->vessel->autonomous)
		{
			if (troop_stack_hide(state->game, state->hexagon, state->player, state->troop_stack.selected) < 0)
				return ERROR_MEMORY;
			state->troop_stack.offset = 0;
			state->troop_stack.selected = 0;
		}
		else if (source_stack)
			troop_stack_stamina(source_stack);
	}
	else
	{
		struct troop *troop = source->troops[state->drag_index];

		// Switch places of troops.
		source->troops[state->drag_index] = target->troops[index];
		target->troops[index] = troop;

		if (source_stack)
			troop_stack_stamina(source_stack);
	}

	if_cursor_set(cursors[UNITS_COUNT]);
	state->drag_index = -1;

	return INPUT_DONE;
}

static int input_tab_troops(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	int index;
	struct array_troop_stacks *stacks;

	if ((code != EVENT_MOUSE_LEFT) && (code != EVENT_DRAG_STOP))
		return INPUT_NOTME;

	if (!state->hexagon)
		return INPUT_IGNORE;
	stacks = troop_stacks(state->hexagon, state->player);

	index = object_index(&widgets[TroopsTabs], (struct vector){x, y});
	if ((index < 0) || (state->troop_stack.offset + index >= stacks->count))
	{
		if ((code == EVENT_DRAG_STOP) && (state->drag_index >= 0))
		{
			if_cursor_set(cursors[UNITS_COUNT]);
			state->drag_index = -1;
			return INPUT_DONE;
		}

		return INPUT_IGNORE;
	}
	index += state->troop_stack.offset;

	if (code == EVENT_DRAG_STOP)
	{
		struct troop_stack *restrict target = stacks->data[index];
		int status = troop_drop(state, &target->private, target->vessel->capacity, target->vessel->capacity);
		troop_stack_stamina(target);
		return status;
	}

	assert(code == EVENT_MOUSE_LEFT);

	state->troop_stack.selected = index;
	memset(state->selected, 0, sizeof(state->selected));
	state->selected_count = 0;
	state->selected_workers = 0;

	return INPUT_DONE;
}

static int input_tab_left(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	if (!state->hexagon)
		return INPUT_IGNORE;

	if (state->troop_stack.offset)
		state->troop_stack.offset -= 1;

	return INPUT_DONE;
}

static int input_tab_right(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct array_troop_stacks *stacks;
	size_t end;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	if (!state->hexagon)
		return INPUT_IGNORE;
	stacks = troop_stacks(state->hexagon, state->player);

	end = state->troop_stack.offset + widgets[TroopsTabs].columns;
	if (stacks->count > end)
		state->troop_stack.offset += 1;

	return INPUT_DONE;
}

static int input_garrison(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	int index;

	if ((code != EVENT_DRAG_START) && (code != EVENT_DRAG_STOP))
		return INPUT_NOTME;

	if (!state->region || (state->hexagon->owner != state->player))
		return INPUT_IGNORE; // no access to garrison

	index = object_index(&widgets[GarrisonTroops], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE; // no troop present

	if (code == EVENT_DRAG_STOP)
		return troop_drop(state, &state->region->garrison.private, index, garrison_info(state->hexagon)->capacity);

	assert(code == EVENT_DRAG_START);

	if (!state->region->garrison.private.count)
		return INPUT_IGNORE; // no troop to drag
	return troop_drag(state, &state->region->garrison.private, index);
}

static int input_troop(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	int index;
	struct troop_stack *restrict troop_stack;
	unsigned workers_change;

	if ((code != EVENT_MOUSE_LEFT) && (code != EVENT_DRAG_START) && (code != EVENT_DRAG_STOP))
		return INPUT_NOTME;

	index = object_index(&widgets[Troops], (struct vector){x, y});
	if (index < 0)
		return INPUT_IGNORE; // no troop clicked

	if (!state->hexagon || !troop_stacks(state->hexagon, state->player)->count)
	{
		if (code == EVENT_DRAG_STOP)
		{
			if_cursor_set(cursors[UNITS_COUNT]);
			state->drag_index = -1;
			return INPUT_DONE;
		}
		return INPUT_IGNORE; // no troop stacks present in the hexagon
	}
	troop_stack = troop_stacks(state->hexagon, state->player)->data[state->troop_stack.selected];
	if (troop_stack->owner != state->player)
		return INPUT_IGNORE; // current player does not own troop stack

	switch (code)
	{
		int status;

	case EVENT_DRAG_START:
		return troop_drag(state, &troop_stack->private, index);

	case EVENT_DRAG_STOP:
		status = troop_drop(state, &troop_stack->private, index, troop_stack->vessel->capacity);
		troop_stack_stamina(troop_stack);
		return status;
	}

	if (index >= troop_stack->private.count)
		return INPUT_IGNORE;

	workers_change = troop_stack->private.troops[index]->unit->worker * troop_stack->private.troops[index]->count;
	if (modifiers & XCB_MOD_MASK_CONTROL)
	{
		state->selected[index] = !state->selected[index];
		if (state->selected[index])
		{
			state->selected_count += 1;
			state->selected_workers += workers_change;
		}
		else
		{
			state->selected_count -= 1;
			state->selected_workers -= workers_change;
		}
	}
	else
	{
		memset(state->selected, 0, sizeof(state->selected));
		state->selected[index] = 1;
		state->selected_count = 1;
		state->selected_workers = workers_change;
	}

	return INPUT_DONE;
}

static int input_building(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct game *restrict game = state->game;
	int index;

	if ((code != EVENT_MOTION) && (code != EVENT_MOUSE_LEFT))
		return INPUT_NOTME;

	if (!state->hexagon)
		return INPUT_IGNORE;

	index = object_index(&widgets[Buildings], (struct vector){x, y});
	if ((index < 0) || (index >= state->buildings_count))
		goto clean;
	index = state->buildings[index];

	if (!state->selected_workers && (index != state->hexagon->build_index))
	{
		// Player cannot issue a build order.
		if (code == EVENT_MOUSE_LEFT)
			return INPUT_IGNORE;
		else if (!hexagon_built(state->hexagon, index))
			goto clean;
	}

	if (code == EVENT_MOUSE_LEFT)
	{
		struct troop_stack *troop_stack = troop_stacks(state->hexagon, state->player)->data[state->troop_stack.selected];
		size_t i;

		if (hexagon_built(state->hexagon, index))
			return INPUT_IGNORE; // building already constructed; nothing to do

		if (index == state->hexagon->build_index)
		{
			// The clicked building is already under construction. Cancel construction.
			building_cancel(game, state->hexagon);
			return INPUT_DONE;
		}

		assert(state->selected_workers);
		assert(state->hexagon->private.data[0]->owner == state->player);

		if (building_build(game, state->hexagon, index, state->player) < 0)
			return INPUT_IGNORE;

		// Set selected workers as currently building.
		for(i = 0; i < troop_stack->private.count; i += 1)
			if (troop_stack->private.troops[i]->unit->worker && state->selected[i])
				troop_stack->private.troops[i]->task = TASK_BUILD;
	}
	else
	{
		// Hover the building.
		state->hover_type = HOVER_BUILDING;
		state->hover_index = index;
	}
	return INPUT_DONE;

clean:
	if (state->hover_type == HOVER_NONE)
		return INPUT_IGNORE;
	else
	{
		state->hover_type = HOVER_NONE;
		return INPUT_DONE; // no visible building hovered
	}
}

static int input_order(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct game *restrict game = state->game;
	int index;

	if ((code != EVENT_MOTION) && (code != EVENT_MOUSE_LEFT) && (code != EVENT_DRAG_STOP))
		return INPUT_NOTME;

	if (!state->hexagon)
		return INPUT_IGNORE;

	index = object_index(&widgets[Orders], (struct vector){x, y});

	if (state->selected_count)
	{
		struct troop_stack *troop_stack = troop_stacks(state->hexagon, state->player)->data[state->troop_stack.selected];
		size_t i;

		if (code != EVENT_MOUSE_LEFT)
			return INPUT_IGNORE;

		switch (index)
		{
		case 0: // dismiss
			for(i = 0; i < troop_stack->private.count; i += 1)
				if (state->selected[i])
					troop_stack->private.troops[i]->task = ((troop_stack->private.troops[i]->task == TASK_DISMISS) ? TASK_NONE : TASK_DISMISS);
			break;

		default:
			return INPUT_IGNORE;
		}

		memset(state->selected, 0, sizeof(state->selected));
		state->selected_count = 0;
		state->selected_workers = 0;
		return INPUT_DONE;
	}
	else if (state->drag_index >= 0)
	{
		if (code != EVENT_DRAG_STOP)
			return INPUT_IGNORE;

		switch (index)
		{
			struct troop_stack *restrict new;
			int status;

		case 0: // split
			if (state->hexagon->terrain == TERRAIN_WATER)
				break; // troop stacks must be in a ship; cannot create a ship out of nothing

			new = troop_stack_create(game, state->hexagon, state->player, 0);
			if (!new)
				return ERROR_MEMORY;

			status = troop_drop(state, &new->private, 0, new->vessel->capacity);
			troop_stack_stamina(new);
			return status;

		default:
			return INPUT_NOTME;
		}
	}

	if (code == EVENT_DRAG_STOP)
		return INPUT_NOTME;

	if (!state->region || !state->hexagon->buildings || (state->hexagon->owner != state->player))
		return INPUT_IGNORE; // own region must be selected

	if ((index < 0) || (index >= UNITS_COUNT))
	{
		if (state->hover_type == HOVER_NONE)
			return INPUT_IGNORE;
		else
		{
			state->hover_type = HOVER_NONE;
			return INPUT_DONE; // no unit hovered/clicked
		}
	}

	if (!unit_available(state->region, UNITS + index))
		return INPUT_IGNORE;

	if (code == EVENT_MOTION)
	{
		state->hover_type = HOVER_UNIT;
		state->hover_index = index;
		return INPUT_DONE;
	}

	assert(code == EVENT_MOUSE_LEFT);
	return ((training_enqueue(game, state->region, index) < 0) ? INPUT_IGNORE : INPUT_DONE);
}

static int input_training_queue(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct game *restrict game = state->game;
	int index;

	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;

	if (!state->region || (state->region->garrison.hexagon->owner != state->player))
		return INPUT_IGNORE; // own region must be selected

	index = object_index(&widgets[TrainQueue], (struct vector){x, y});
	if ((index < 0) || (index >= TRAIN_QUEUE) || !state->region->train[index])
		return INPUT_IGNORE;

	training_dequeue(game, state->region, index);
	return INPUT_DONE;
}

static int input_vessel(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct game *restrict game = state->game;

	if ((code != EVENT_MOTION) && (code != EVENT_MOUSE_LEFT))
		return INPUT_NOTME;

	if (!state->hexagon || !state->hexagon->buildings || (state->hexagon->owner != state->player))
		return INPUT_IGNORE;

	if (!vessel_available(state->hexagon, VESSELS + VesselWater))
		return INPUT_IGNORE;

	if (code == EVENT_MOTION)
	{
		if ((state->hexagon->vessel_index < 0) && !state->selected_workers)
			return INPUT_IGNORE;

		state->hover_type = HOVER_VESSEL;
		state->hover_index = 0;
		return INPUT_DONE;
	}

	assert(code == EVENT_MOUSE_LEFT);

	if (state->hexagon->vessel_index >= 0)
	{
		vessel_cancel(game, state->hexagon);
		return INPUT_DONE;
	}

	if (state->selected_workers)
	{
		size_t i;
		struct troop_stack *troop_stack = troop_stacks(state->hexagon, state->player)->data[state->troop_stack.selected];

		if (!resources_enough(&game->players[state->player].treasury, &VESSELS[VesselWater].cost))
			return INPUT_IGNORE;

		if (vessel_build(game, state->hexagon, VesselWater) < 0)
			return INPUT_IGNORE;

		for(i = 0; i < troop_stack->private.count; i += 1)
			if (troop_stack->private.troops[i]->unit->worker && state->selected[i])
				troop_stack->private.troops[i]->task = TASK_NONE;

		return INPUT_DONE;
	}

	return INPUT_IGNORE;
}

static int input_hexagon(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	struct state_map *state = argument;
	struct game *restrict game = state->game;
	int hexagon_index;

	if ((code != EVENT_MOUSE_LEFT) && (code != EVENT_MOUSE_RIGHT) && (code != EVENT_DRAG_STOP))
		return INPUT_NOTME;

	// Find clicked hexagon.
	hexagon_index = storage_get(x + state->scroll_x, y + state->scroll_y);
	if ((hexagon_index < 0) || !bitmap_get(state->visible, hexagon_index))
	{
		state->region = 0;
		state->hexagon = 0;
		return INPUT_DONE;
	}

	if (code == EVENT_MOUSE_LEFT)
	{
		state->hexagon = game->hexagons + hexagon_index;

		state->buildings_count = buildings_available(game, state->hexagon, state->buildings, state->player);

		state->troop_stack.offset = 0;
		state->troop_stack.selected = 0;
		if (state->hexagon->environment & (1 << ENVIRONMENT_SETTLEMENT))
			state->region = game->regions + state->hexagon->info.region_index;
		else
			state->region = 0;
		// TODO support other objects
		memset(state->selected, 0, sizeof(state->selected));
		state->selected_count = 0;
		state->selected_workers = 0;

		return INPUT_DONE;
	}
	else if (code == EVENT_MOUSE_RIGHT)
	{
		struct array_troop_stacks *stacks;
		struct troop_stack *restrict troop_stack;

		if (!state->hexagon)
			return INPUT_IGNORE;
		stacks = troop_stacks(state->hexagon, state->player);
		if (!stacks->count)
			return INPUT_IGNORE;

		troop_stack = stacks->data[state->troop_stack.selected];
		if (troop_stack->owner != state->player)
			return INPUT_IGNORE;

		if (!(modifiers & XCB_MOD_MASK_SHIFT))
			troop_stack->movements_count = 1;
		if (&game->hexagons[hexagon_index] != troop_stack->movements[troop_stack->movements_count - 1])
		{
			int status = map_movement_queue(game, troop_stack, hexagon_index, state->visible);
			if (status != ERROR_MISSING)
				return status;
		}

		return INPUT_DONE;
	}

	assert(code == EVENT_DRAG_STOP);
	if_cursor_set(cursors[UNITS_COUNT]);
	state->drag_index = -1;

	return INPUT_IGNORE;
}

int input_map(const struct game *restrict game, unsigned char player)
{
	struct area areas[] = {
		{
			.left = 0,
			.right = WINDOW_WIDTH,
			.top = 0,
			.bottom = WINDOW_HEIGHT,
			.callback = input_none,
		},
		{
			.left = BUTTON_MENU_X,
			.right = BUTTON_MENU_X + BUTTON_WIDTH,
			.top = BUTTONS_Y,
			.bottom = BUTTONS_Y + BUTTON_HEIGHT,
			.callback = input_menu,
		},
		{
			.left = BUTTON_END_X,
			.right = BUTTON_END_X + BUTTON_WIDTH,
			.top = BUTTONS_Y,
			.bottom = BUTTONS_Y + BUTTON_HEIGHT,
			.callback = input_mouse_terminate,
		},
		{
			.left = CONTENT_X,
			.right = CONTENT_X + CONTENT_WIDTH,
			.top = CONTENT_Y,
			.bottom = CONTENT_Y + CONTENT_HEIGHT,
			.callback = input_hexagon
		},
		{
			.left = widgets[TroopsTabs].left,
			.right = widgets[TroopsTabs].right,
			.top = widgets[TroopsTabs].top,
			.bottom = widgets[TroopsTabs].bottom,
			.callback = input_tab_troops
		},
		{
			.left = TROOPS_ARROW_LEFT_X,
			.right = TROOPS_ARROW_LEFT_X + TROOPS_ARROW_WIDTH - 1,
			.top = widgets[TroopsTabs].top,
			.bottom = widgets[TroopsTabs].bottom,
			.callback = input_tab_left
		},
		{
			.left = TROOPS_ARROW_RIGHT_X,
			.right = TROOPS_ARROW_RIGHT_X + TROOPS_ARROW_WIDTH - 1,
			.top = widgets[TroopsTabs].top,
			.bottom = widgets[TroopsTabs].bottom,
			.callback = input_tab_right
		},
		WIDGET_AREA(widgets[GarrisonTroops], &input_garrison),
		WIDGET_AREA(widgets[Troops], &input_troop),
		WIDGET_AREA(widgets[Buildings], &input_building),
		WIDGET_AREA(widgets[Orders], &input_order),
		WIDGET_AREA(widgets[TrainQueue], &input_training_queue),
		WIDGET_AREA(widgets[Ship], &input_vessel),
	};

	struct state_map state;
	int status;

	state.game = (struct game *)game;
	state.player = player;

	state.visible = map_visible_alloc(state.game, player);

	state.hexagon = 0;
	state.region = 0;
	state.building = BUILDINGS_COUNT;
	state.drag_index = -1;
	state.hover_type = HOVER_NONE;

	// initialize scroll
	{
		const struct tile location = hexagon_location(game, game->alive[player] - game->hexagons);
		state.scroll_start = NAN;
		state.scroll_x = 0;
		state.scroll_y = 0;
		scroll_update(&state, hexagon_offset_x(0, location.x, location.y, HEXAGON_EDGE) - (CONTENT_WIDTH / 2), hexagon_offset_y(0, location.x, location.y, HEXAGON_EDGE) - (CONTENT_HEIGHT / 2));
	}

	w = if_window_create(CONTENT_X, CONTENT_Y, CONTENT_WIDTH, CONTENT_HEIGHT);
	status = input_process(areas, sizeof(areas) / sizeof(*areas), show_map, scroll_timer, &state);
	if_window_destroy(w);

	free(state.visible);
	return status;
}
