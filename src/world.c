/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <fcntl.h>
#include <math.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "basic.h"
#include "log.h"
#include "json.h"
#include "display.h"
#include "game.h"
#include "map.h"
#include "world.h"

#define TAX 4

#define S(s) (s), sizeof(s) - 1

static inline const char *type_name(enum json_type type)
{
	const char *names[] = {
		[JSON_NULL] = "null",
		[JSON_BOOLEAN] = "boolean",
		[JSON_INTEGER] = "integer",
		[JSON_REAL] = "real",
		[JSON_STRING] = "string",
		[JSON_ARRAY] = "array",
		[JSON_OBJECT] = "object",
	};
	return names[type];
}

static inline union json *value_get(const struct hashmap *restrict hashmap, struct slice key, enum json_type type)
{
	union json **entry = hashmap_get(hashmap, key.data, key.size);
	if (!entry)
		return 0;
	if (json_type(*entry) != type)
	{
		LOG_WARNING("world: %.*s is of type %s (expected: %s)", (int)key.size, key.data, type_name(json_type(*entry)), type_name(type));
		return 0;
	}
	return *entry;
}

static inline bool string_eq(const union json *json, const struct slice string)
{
	return ((json->string.size == string.size) && !memcmp(json->string.data, string.data, string.size));
}

static const struct vessel *vessel_find(const struct string *restrict name)
{
#if 0
	if (name->size > NAME_LIMIT)
		for(size_t i = 0; i < VESSELS_COUNT; i += 1)
			if ((name->size == VESSELS[i].name_length) && !memcmp(name->data, VESSELS[i].name, VESSELS[i].name_length))
				return VESSELS + i;
	LOG_ERROR("vessel %.*s not found", (int)name->size, name->data);
#endif
	return 0;
}

/*#define NEIGHBORS_SIZE ((REGIONS_LIMIT * REGIONS_LIMIT + 7) / 8)

static inline void neighbors_set(unsigned char neighbors[static NEIGHBORS_SIZE], unsigned a, unsigned b)
{
	unsigned index = a * REGIONS_LIMIT + b;
	neighbors[index / 8] |= (1 << (index % 8));
}

static inline bool neighbors_get(unsigned char neighbors[static NEIGHBORS_SIZE], unsigned a, unsigned b)
{
	unsigned index = a * REGIONS_LIMIT + b;
	return neighbors[index / 8] & (1 << (index % 8));
}*/

int world_empty(struct game *restrict game)
{
	game->regions = 0;
	game->hexagons = 0;
	game->hexagons_graph = 0;

	game->troop_stacks = (struct array_troop_stacks){0};

	// TODO support month names (it will be culture-dependent)
	game->turn = 0;
}

static int troops_init(union json *restrict container, struct troops *restrict troops, size_t limit)
{
	size_t i;
	union json *field, *entry;

	if (field = value_get(&container->object, slice("troops"), JSON_ARRAY))
	{
		if (field->array.count > limit)
		{
			LOG_WARNING("world: troops limit exceeded: %u", (unsigned)limit);
			return ERROR_INPUT;
		}

		for(i = 0; i < field->array.count; i += 1)
		{
			const struct unit *unit;
			int status;

			entry = field->array.data[i];
			if ((json_type(entry) != JSON_ARRAY) || (entry->array.count != 2) || (json_type(entry->array.data[0]) != JSON_STRING) || (json_type(entry->array.data[1]) != JSON_INTEGER))
			{
				LOG_WARNING("world: invalid troop data");
				return ERROR_INPUT;
			}

			unit = unit_find(slice(entry->array.data[0]->string.data, entry->array.data[0]->string.size));
			if (!unit)
				return ERROR_INPUT;
			status = troop_spawn(troops, unit, entry->array.data[1]->integer);
			if (status < 0)
				return status;
		}
	}

	return 0;
}

static int region_init(struct game *restrict game, size_t index, const unsigned char *restrict name_data, size_t name_size, const struct hashmap *restrict data)
{
	union json *item, *field;

	struct region *restrict region = game->regions + index;
	size_t i;
	int status;

	if (name_size > NAME_LIMIT)
		return ERROR_INPUT;
	memcpy(region->name, name_data, name_size);
	region->name_size = name_size;

	item = value_get(data, slice("garrison"), JSON_OBJECT);
	if (!item)
		return ERROR_INPUT;

	// Initialize garrison.
	status = troops_init(item, &region->garrison.private, GARRISON_TROOPS_LIMIT);
	if (status < 0)
	{
		LOG_WARNING("world: invalid garrison troops for region %.*s", (int)name_size, name_data);
		return status;
	}
	region->garrison.hexagon = 0;
	region->sieged = 0;

	// Initialize train queue
	item = value_get(data, slice("train"), JSON_ARRAY);
	if (!item || (item->array.count > TRAIN_QUEUE))
		return ERROR_INPUT;
	for(i = 0; i < item->array.count; i += 1)
	{
		field = item->array.data[i];
		if (json_type(field) != JSON_STRING)
		{
			LOG_WARNING("world: invalid training queue for region %.*s", (int)name_size, name_data);
			return ERROR_INPUT;
		}
		region->train[i] = unit_find(slice(field->string.data, field->string.size));
	}
	for(; i < TRAIN_QUEUE; i += 1)
		region->train[i] = 0;
	item = value_get(data, slice("train_progress"), JSON_INTEGER);
	if (item && (item->integer >= 0) && (!region->train[0] || (item->integer < region->train[0]->time)))
		region->train_progress = item->integer;
	else
		region->train_progress = 0;

	region->tax = TAX;

	item = value_get(data, slice("population"), JSON_INTEGER);
	if (!item)
		return ERROR_INPUT;
	region->population = item->integer;

	item = value_get(data, slice("happiness"), JSON_REAL);
	if (item)
		region->happiness = item->real;
	else
	{
		item = value_get(data, slice("happiness"), JSON_INTEGER);
		if (!item)
			return ERROR_INPUT;
		region->happiness = item->integer;
	}

	return 0;
}

int troop_stack_init(const struct game *restrict game, struct troop_stack *restrict troop_stack, union json *restrict stack, struct hexagon *restrict hexagon)
{
	union json *entry;

	troop_stack->private.count = 0;

	entry = value_get(&stack->object, slice("owner"), JSON_INTEGER);
	if (!entry || (entry->integer < 0) || (entry->integer >= game->players_count))
	{
		LOG_WARNING("world: invalid troop stack owner");
		return ERROR_INPUT;
	}
	troop_stack->owner = entry->integer;

	if (hexagon->terrain == TERRAIN_WATER)
	{
		assert(VESSELS[VesselWater].speed <= MOVEMENT_STEPS);
		troop_stack->vessel = VESSELS + VesselWater; // TODO implement different ships
	}
	else
		troop_stack->vessel = VESSELS + VesselGround; // TODO implement different sizes depending on ethnicity

	if (troops_init(stack, &troop_stack->private, troop_stack->vessel->capacity) < 0)
	{
		LOG_WARNING("world: invalid troop stack");
		return ERROR_INPUT;
	}

	troop_stack->movements[0] = hexagon;
	troop_stack->movements_count = 1;

	return 0;
}

static int hexagon_init(struct game *restrict game, size_t index, const struct hashmap *restrict data)
{
	union json *item, *field;
	size_t i;

	field = value_get(data, slice("terrain"), JSON_STRING);
	if (!field)
		return ERROR_INPUT;
	if (string_eq(field, slice("none")))
		game->hexagons[index].terrain = TERRAIN_MOUNTAIN;
	else if (string_eq(field, slice("ground")))
	{
		game->hexagons[index].terrain = TERRAIN_GROUND;

/*////////////////
		field = value_get(data, slice("region"), JSON_INTEGER);
		if (!field || (field->integer < 0) || (game->regions_count <= field->integer))
			return ERROR_INPUT;
		game->hexagons[index].info.region_index = field->integer;
///////////////*/
		field = value_get(data, slice("region"), JSON_STRING);
		if (!field)
			return ERROR_INPUT;
		for(i = 0; i < game->regions_count; i += 1) // TODO use a hashmap to optimize this
			if (string_eq(field, slice(game->regions[i].name, game->regions[i].name_size)))
			{
				game->hexagons[index].info.region_index = i;
				goto done;
			}
		return ERROR_INPUT;
done:
		;

		// Find the neighboring hexagons and set neighboring regions.
		/*neighbors_count = hexagon_neighbors(game, game->hexagons + index, neighbors);
		while (neighbors_count--)
		{
			size_t other;
			if (neighbors[neighbors_count].hexagon->terrain == TERRAIN_WATER)
				continue;
			other = neighbors[neighbors_count].hexagon->info.region_index;
			neighbors_set(neighbors_table, field->integer, other);
			neighbors_set(neighbors_table, other, field->integer);
		}*/
	}
	else if (string_eq(field, slice("water")))
		game->hexagons[index].terrain = TERRAIN_WATER;
	else
	{
		LOG_WARNING("world: invalid terrain %.*s", (int)field->string.size, field->string.data);
		return ERROR_INPUT;
	}

	// environment
	field = value_get(data, slice("environment"), JSON_ARRAY);
	game->hexagons[index].environment = 0;
	if (field)
	{
		for(i = 0; i < field->array.count; i += 1)
		{
			item = field->array.data[i];

			if (string_eq(item, slice("village")))
			{
				if ((game->hexagons[index].terrain != TERRAIN_GROUND) || game->regions[game->hexagons[index].info.region_index].garrison.hexagon)
				{
					LOG_WARNING("world: garrison for region already set");
					return ERROR_INPUT;
				}
				game->regions[game->hexagons[index].info.region_index].garrison.hexagon = game->hexagons + index;

				game->hexagons[index].environment |= 1 << ENVIRONMENT_SETTLEMENT;
			}
			else if (string_eq(item, slice("forest")))
				game->hexagons[index].environment |= 1 << ENVIRONMENT_FOREST;
			else if (string_eq(item, slice("quarry")))
				game->hexagons[index].environment |= 1 << ENVIRONMENT_QUARRY;
			else
			{
				LOG_WARNING("world: invalid environment %.*s", (int)item->string.size, item->string.data);
				return ERROR_INPUT;
			}
		}
	}

	// buildings and owner
	field = value_get(data, slice("buildings"), JSON_ARRAY);
	if (!field)
		return ERROR_INPUT;
	game->hexagons[index].buildings = 0;
	for(i = 0; i < field->array.count; i += 1)
	{
		int building;
		if (json_type(field->array.data[i]) != JSON_STRING)
			return ERROR_INPUT;
		building = building_find(slice(field->array.data[i]->string.data, field->array.data[i]->string.size));
		if (building < 0)
			return ERROR_INPUT;
		hexagon_build(game->hexagons + index, building);
	}
	if (game->hexagons[index].buildings)
	{
		field = value_get(data, slice("owner"), JSON_INTEGER);
		if (!field || (field->integer < 0) || (field->integer >= game->players_count))
		{
			LOG_WARNING("world: hexagons with buildings without an owner");
			return ERROR_INPUT;
		}
		game->hexagons[index].owner = field->integer;
	}

	if (item = value_get(data, slice("build"), JSON_OBJECT))
	{
		field = value_get(&item->object, slice("index"), JSON_INTEGER);
		if (!field || (field->integer < 0) || (field->integer >= BUILDINGS_COUNT))
			return ERROR_INPUT;
		game->hexagons[index].build_index = field->integer;
		field = value_get(&item->object, slice("progress"), JSON_REAL);
		if (!field || (field->real < 0) || (field->real >= BUILDINGS[game->hexagons[index].build_index].time))
			return ERROR_INPUT;
		game->hexagons[index].build_progress = field->real;
	}
	else
	{
		game->hexagons[index].build_index = -1;
		game->hexagons[index].build_progress = 0;
	}

	if (item = value_get(data, slice("vessel"), JSON_OBJECT))
	{
		field = value_get(&item->object, slice("index"), JSON_INTEGER);
		if (!field || (field->integer < 0) || (field->integer >= BUILDINGS_COUNT))
			return ERROR_INPUT;
		game->hexagons[index].vessel_index = field->integer;
		field = value_get(&item->object, slice("progress"), JSON_REAL);
		if (!field || (field->real < 0) || (field->real >= VESSELS[game->hexagons[index].vessel_index].time))
			return ERROR_INPUT;
		game->hexagons[index].vessel_progress = field->real;
	}
	else
	{
		game->hexagons[index].vessel_index = -1;
		game->hexagons[index].vessel_progress = 0;
	}

	game->hexagons[index].private = (struct array_troop_stacks){0};
	// TODO initialize other players

	field = value_get(data, slice("troop_stacks"), JSON_ARRAY);
	if (field && field->array.count)
	{
		struct hexagon *hexagon = game->hexagons + index;

		if (array_troop_stacks_expand(&hexagon->private, field->array.count) < 0)
		{
			LOG_FATAL("memory allocation error");
			return ERROR_MEMORY;
		}

		for(i = 0; i < field->array.count; i += 1)
		{
			union json *stack = field->array.data[i];
			struct troop_stack *troop_stack;
			int status;

			if (json_type(stack) != JSON_OBJECT)
			{
				LOG_WARNING("world: invalid troop stack type");
				return ERROR_INPUT;
			}

			troop_stack = malloc(sizeof(*troop_stack));
			if (!troop_stack)
				return ERROR_MEMORY; // TODO memory leak?

			status = troop_stack_init(game, troop_stack, stack, hexagon);
			if (status < 0)
				return status;
			if (i && (troop_stack->owner != hexagon->private.data[i - 1]->owner))
			{
				LOG_WARNING("world: multiple troop stack owners");
				return ERROR_INPUT;
			}

			hexagon->private.data[i] = troop_stack;
		}
		hexagon->private.count = field->array.count;
	}

	return 0;
}

static int world_populate(const union json *restrict json, struct game *restrict game)
{
	union json *collection, *regions, *row, *item, *field;
	struct hashmap_iterator it;
	struct hashmap_entry *region;

	//unsigned char neighbors_table[NEIGHBORS_SIZE] = {0};

	size_t i;

	world_empty(game);

	if (json_type(json) != JSON_OBJECT)
		goto error;

	// Initialize players.
	{
		int local_set = 0;

		collection = value_get(&json->object, slice("players"), JSON_ARRAY);
		if (!collection || (collection->array.count < 2) || (PLAYERS_LIMIT < collection->array.count))
		{
			LOG_WARNING("world: invalid players");
			goto error;
		}

		game->players_count = collection->array.count;
		for(i = 0; i < game->players_count; i += 1)
		{
			item = collection->array.data[i];
			if (json_type(item) == JSON_NULL)
			{
				game->players[i].name_size = 0;
				game->players[i].alliance = 0;
				game->players[i].treasury = (struct resources){0};
				game->players[i].type = Neutral;
				continue;
			}
			if (json_type(item) != JSON_OBJECT)
				goto error;

			field = value_get(&item->object, slice("name"), JSON_STRING);
			if (!field || (field->string.size > NAME_LIMIT))
				goto error;
			memcpy(game->players[i].name, field->string.data, field->string.size);
			game->players[i].name_size = field->string.size;

			field = value_get(&item->object, slice("alliance"), JSON_INTEGER);
			if (!field || (field->integer < 0) || (field->integer >= PLAYERS_LIMIT))
				goto error;
			game->players[i].alliance = field->integer;

			field = value_get(&item->object, slice("gold"), JSON_INTEGER);
			if (!field || (field->integer < 0))
				goto error;
			game->players[i].treasury.gold = field->integer;

			field = value_get(&item->object, slice("food"), JSON_INTEGER);
			if (!field || (field->integer < 0))
				goto error;
			game->players[i].treasury.food = field->integer;

			field = value_get(&item->object, slice("wood"), JSON_INTEGER);
			if (!field || (field->integer < 0))
				goto error;
			game->players[i].treasury.wood = field->integer;

			field = value_get(&item->object, slice("stone"), JSON_INTEGER);
			if (!field || (field->integer < 0))
				goto error;
			game->players[i].treasury.stone = field->integer;

			// Initialize player type.
			if (i == PLAYER_NEUTRAL)
				game->players[i].type = Neutral;
			else if (local_set)
				game->players[i].type = Computer;
			else
			{
				game->players[i].type = Local;
				local_set = 1;
			}
		}

		assert(game->players_count > PLAYER_NEUTRAL);
		assert(local_set);
	}

	// Initialize turn.
	field = value_get(&json->object, slice("turn"), JSON_INTEGER);
	if (!field || (field->integer < 0))
	{
		LOG_WARNING("world: invalid turn");
		goto error;
	}
	game->turn = field->integer;

	// Initialize regions.
	{
		regions = value_get(&json->object, slice("regions"), JSON_OBJECT);
		if (!regions || (regions->object.count < 1) || (REGIONS_LIMIT < regions->object.count))
			goto error;

		game->regions_count = regions->object.count;
		game->regions = malloc(game->regions_count * sizeof(*game->regions));
		if (!game->regions)
		{
			LOG_FATAL("memory allocation error");
			goto error;
		}
		for(i = 0; i < game->regions_count; i += 1)
			game->regions[i].garrison.private.count = 0;

		i = 0;
		for(region = hashmap_first(&regions->object, &it); region; region = hashmap_next(&regions->object, &it))
		{
			item = region->value;
			if (json_type(item) != JSON_OBJECT)
				goto error;

			if (region_init(game, i, region->key_data, region->key_size, &item->object) < 0)
			{
				LOG_ERROR("Failed initializing %.*s", (int)region->key_size, region->key_data);
				goto error;
			}

			i += 1;
		}
	}

	// Initialize hexagons.
	{
		size_t x, y;
		size_t j;

		collection = value_get(&json->object, slice("hexagons"), JSON_ARRAY);
		if (!collection || (collection->array.count < 1) || (json_type(collection->array.data[0]) != JSON_ARRAY) || (collection->array.data[0]->array.count < 1))
		{
			LOG_WARNING("world: invalid hexagons");
			goto error;
		}

		game->map_width = collection->array.data[0]->array.count;
		game->map_height = collection->array.count;

		game->hexagons_total = hexagons_count(game);
		game->hexagons = malloc(game->hexagons_total * sizeof(*game->hexagons));
		if (!game->hexagons)
		{
			LOG_FATAL("memory allocation error");
			goto error;
		}

		for(i = 0; i < game->hexagons_total; i += 1)
			game->hexagons[i].public = game->hexagons[i].private = (struct array_troop_stacks){0};

		i = 0;
		for(y = 0; y < game->map_height; y += 1)
		{
			unsigned width;

			//struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
			//size_t neighbors_count;

			row = collection->array.data[y];
			if (json_type(row) != JSON_ARRAY)
				goto error;

			width = hexagons_width(game, y);
			if (row->array.count != width)
			{
				LOG_WARNING("world: invalid width %u (expected: %u)", (unsigned)row->array.count, (unsigned)width);
				goto error;
			}

			for(x = 0; x < width; x += 1)
			{
				item = row->array.data[x];
				if (json_type(item) != JSON_OBJECT)
					goto error;

				if (hexagon_init(game, i, &item->object) < 0)
				{
					LOG_ERROR("Failed initializing hexagon (%u,%u)", (unsigned)x, (unsigned)y);
					goto error;
				}

				i += 1;
			}
		}

		// Detect coasts.
		for(i = 0; i < game->hexagons_total; i += 1)
		{
			struct hexagon_neighbor neighbors[DIRECTIONS_COUNT];
			size_t count = hexagon_neighbors(game, game->hexagons + i, neighbors);

			if (game->hexagons[i].terrain != TERRAIN_GROUND)
				continue;

			for(j = 0; j < count; j += 1)
			{
				if (neighbors[j].hexagon->terrain == TERRAIN_WATER)
				{
					game->hexagons[i].environment |= 1 << ENVIRONMENT_COAST;
					break;
				}
			}
		}

		game->hexagons_graph = hexagons_graph_build(game);
		if (!game->hexagons_graph)
		{
			LOG_FATAL("memory allocation error");
			goto error;
		}
	}

	for(i = 0; i < game->regions_count; i += 1)
		if (!game->regions[i].garrison.hexagon)
		{
			LOG_WARNING("world: region %.*s doesn't have a village", (int)game->regions[i].name_size, game->regions[i].name);
			goto error;
		}

	// Initialize region objects and neighbors.
#if 0
	i = 0;
	for(collection = hashmap_first(&regions->object, &it); collection; collection = hashmap_next(&regions->object, &it))
	{
		size_t neighbors_count = 0;
		size_t region_index;
		size_t ground, water;

		for(j = 0; j < game->regions_count; j += 1)
		{
			if (j == i) continue;

			if (neighbors_get(neighbors_table, i, j))
				game->regions[i].neighbors[neighbors_count++] = game->regions + j;
		}

		i += 1;
	}
#endif

	// TODO Initialize ships

	return 0;

error:
	world_unload(game);
	return ERROR_INPUT;
}

int world_load(const unsigned char *restrict filepath, struct game *restrict game)
{
	int file;
	struct stat info;
	unsigned char *buffer;
	int status;
	union json *json;

	// Read file content.
	file = open(filepath, O_RDONLY);
	if (file < 0) return ERROR_MISSING; // TODO this could be ERROR_ACCESS or something else
	if (fstat(file, &info) < 0)
	{
		close(file);
		return ERROR_MISSING; // TODO this could be ERROR_ACCESS or something else
	}
	buffer = mmap(0, info.st_size, PROT_READ, MAP_SHARED, file, 0);
	close(file);
	if (buffer == MAP_FAILED) return ERROR_MEMORY;

	// Parse file content.
	json = json_parse(buffer, info.st_size);
	munmap(buffer, info.st_size);
	if (!json) return ERROR_INPUT;

	// Populate game data.
	status = world_populate(json, game);
	json_free(json);

	return status;
}

static union json *troops_dump(const struct troops *restrict troops)
{
	size_t i;
	union json *result = json_array(), *entry;

	for(i = 0; i < troops->count; i += 1)
	{
		entry = json_array();
		entry = json_array_insert(entry, json_string(troops->troops[i]->unit->name, troops->troops[i]->unit->name_size));
		entry = json_array_insert(entry, json_integer(troops->troops[i]->count));
		result = json_array_insert(result, entry);
	}

	return result;
}

static union json *world_store(const struct game *restrict game)
{
	union json *json = json_object();

	size_t i, j;

	union json *players = json_array();
	for(i = 0; i < game->players_count; ++i)
	{
		union json *player;

		if (game->alive[i] || (game->players[i].type == Neutral))
		{
			player = json_object();
			player = json_object_insert(player, S("name"), json_string(game->players[i].name, game->players[i].name_size));
			player = json_object_insert(player, S("alliance"), json_integer(game->players[i].alliance));
			player = json_object_insert(player, S("gold"), json_integer(game->players[i].treasury.gold));
			player = json_object_insert(player, S("food"), json_integer(game->players[i].treasury.food));
			player = json_object_insert(player, S("wood"), json_integer(game->players[i].treasury.wood));
			player = json_object_insert(player, S("stone"), json_integer(game->players[i].treasury.stone));
		}
		else
			player = json_null();

		players = json_array_insert(players, player);
	}
	json = json_object_insert(json, S("players"), players);

	json = json_object_insert(json, S("turn"), json_integer(game->turn));

	union json *regions = json_object();
	for(i = 0; i < game->regions_count; ++i)
	{
		union json *region = json_object();
		union json *garrison;
		union json *item;

		garrison = json_object();
		garrison = json_object_insert(garrison, S("troops"), troops_dump(&game->regions[i].garrison.private));

		region = json_object_insert(region, S("garrison"), garrison);

		region = json_object_insert(region, S("population"), json_integer(game->regions[i].population));
		region = json_object_insert(region, S("happiness"), json_real(game->regions[i].happiness));

		item = json_array();
		for(j = 0; j < TRAIN_QUEUE; j += 1)
		{
			const struct unit *unit = game->regions[i].train[j];
			if (!unit)
				break;
			item = json_array_insert(item, json_string(unit->name, unit->name_size));
		}
		region = json_object_insert(region, S("train"), item);
		region = json_object_insert(region, S("train_progress"), json_integer(game->regions[i].train_progress));

		regions = json_object_insert(regions, game->regions[i].name, game->regions[i].name_size, region);
		if (!regions)
			LOG_ERROR("region name duplication: %.*s", (int)game->regions[i].name_size, game->regions[i].name);
	}
	json = json_object_insert(json, S("regions"), regions);

	union json *hexagons = json_array();
	i = 0;
	for(size_t y = 0; y < game->map_height; y += 1)
	{
		unsigned width = hexagons_width(game, y);

		union json *row = json_array();
		for(size_t x = 0; x < width; x += 1)
		{
			union json *hexagon = json_object(), *terrain, *stacks;
			union json *item;

			switch (game->hexagons[i].terrain)
			{
			case TERRAIN_MOUNTAIN:
				terrain = json_string(S("none"));
				break;

			case TERRAIN_GROUND:
				terrain = json_string(S("ground"));
				{
					const struct region *region = game->regions + game->hexagons[i].info.region_index;
					//hexagon = json_object_insert(hexagon, S("region"), json_integer(game->hexagons[i].info.region_index));
					hexagon = json_object_insert(hexagon, S("region"), json_string(region->name, region->name_size));
				}
				break;

			case TERRAIN_WATER:
				terrain = json_string(S("water"));
				break;
			}
			hexagon = json_object_insert(hexagon, S("terrain"), terrain);

			// Store hexagon object data.
			if (game->hexagons[i].environment)
			{
				item = json_array();
				if (game->hexagons[i].environment & (1 << ENVIRONMENT_SETTLEMENT))
					item = json_array_insert(item, json_string(S("village")));
				if (game->hexagons[i].environment & (1 << ENVIRONMENT_FOREST))
					item = json_array_insert(item, json_string(S("forest")));
				if (game->hexagons[i].environment & (1 << ENVIRONMENT_QUARRY))
					item = json_array_insert(item, json_string(S("quarry")));
				// ENVIRONMENT_COAST is automatically detected. No need to store it.

				hexagon = json_object_insert(hexagon, S("environment"), item);
			}

			// buildings and owner
			item = json_array();
			for(j = 0; j < BUILDINGS_COUNT; j += 1)
				if (hexagon_built(game->hexagons + i, j))
					item = json_array_insert(item, json_string(BUILDINGS[j].name, BUILDINGS[j].name_size));
			hexagon = json_object_insert(hexagon, S("buildings"), item);
			if (item->array.count)
				hexagon = json_object_insert(hexagon, S("owner"), json_integer(game->hexagons[i].owner));

			if (game->hexagons[i].build_index >= 0)
			{
				item = json_object();
				item = json_object_insert(item, S("index"), json_integer(game->hexagons[i].build_index));
				item = json_object_insert(item, S("progress"), json_real(game->hexagons[i].build_progress));
				hexagon = json_object_insert(hexagon, S("build"), item);
			}

			stacks = json_array();
			for(j = 0; j < game->hexagons[i].public.count; j += 1)
			{
				union json *stack = json_object();
				const struct troop_stack *troop_stack = game->hexagons[i].private.data[j];
				stack = json_object_insert(stack, S("owner"), json_integer(troop_stack->owner));
				stack = json_object_insert(stack, S("troops"), troops_dump(&troop_stack->public));
				stacks = json_array_insert(stacks, stack);
			}
			hexagon = json_object_insert(hexagon, S("troop_stacks"), stacks);

			row = json_array_insert(row, hexagon);
			i += 1;
		}
		hexagons = json_array_insert(hexagons, row);
	}
	json = json_object_insert(json, S("hexagons"), hexagons);

	return json;
}

int world_save(const struct game *restrict game, const unsigned char *restrict filepath)
{
	union json *json;
	size_t size;
	unsigned char *buffer;
	int file;
	size_t progress;
	ssize_t written;

	json = world_store(game);
	if (!json) return ERROR_MEMORY;

	size = json_size(json);
	buffer = malloc(size);
	if (!buffer)
	{
		json_free(json);
		return ERROR_MEMORY;
	}

	json_dump(buffer, json);
	json_free(json);

	file = creat(filepath, 0644);
	if (file < 0)
	{
		free(buffer);
		LOG_WARNING("world: unable to create %s", filepath);
		return ERROR_ACCESS; // TODO this could be several different errors
	}

	// Write the serialized world into the file.
	for(progress = 0; progress < size; progress += written)
	{
		written = write(file, buffer + progress, size - progress);
		if (written < 0)
		{
			unlink(filepath);
			close(file);
			free(buffer);
			return ERROR_WRITE;
		}
	}
	write(file, "\n", 1); // TODO this can fail

	close(file);
	free(buffer);
	return 0;
}

void world_unload(struct game *restrict game)
{
	size_t i, j;
	size_t hexagons_total = hexagons_count(game);

	free(game->troop_stacks.data);
	hexagons_graph_free(game->hexagons_graph);

	if (game->hexagons)
	{
		for(i = 0; i < hexagons_total; i += 1)
		{
			struct array_troop_stacks *public = &game->hexagons[i].public;
			struct array_troop_stacks *private = &game->hexagons[i].private;
			size_t public_index, private_index = 0;

			for(public_index = 0; public_index < public->count; public_index += 1)
			{
				if (public->data[public_index] == private->data[private_index])
					private_index += 1;
				else
					free(public->data[public_index]);
			}
			if (public->data != private->data)
				free(public->data);

			for(j = 0; j < private->count; j += 1)
			{
				size_t k;
				for(k = 0; k < private->data[j]->private.count; k += 1)
					free(private->data[j]->private.troops[k]);
				free(private->data[j]);
			}
			free(private->data);
		}
		free(game->hexagons);
	}

	if (game->regions)
	{
		for(i = 0; i < game->regions_count; i += 1)
		{
			struct region *restrict region = game->regions + i;
			for(j = 0; j < region->garrison.private.count; j += 1)
				free(region->garrison.private.troops[j]);
		}
		free(game->regions);
	}
}
