/*
 * Conquest of Levidon
 * Copyright (C) 2022  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#define _VA_ARGS_EMPTY(...) (sizeof(#__VA_ARGS__) == 1)

#define FORMAT_SIZE_BASE10(value) ((size_t)(1 + sizeof(value) * 8 / 3 + 1)) /* bytes sufficient for base-10 representation of integer of the specified type (without NUL) */

// Supported values for base are the integers in the interval [2, 36].

uint8_t *format_uint(uint8_t *buffer, uintmax_t number, uint8_t base);
uint8_t *format_uint_pad(uint8_t *buffer, uintmax_t number, uint8_t base, uint32_t length, uint8_t fill);
uint32_t format_uint_length(uintmax_t number, uint8_t base);

uint8_t *format_int(uint8_t *buffer, intmax_t number, uint8_t base);
uint8_t *format_int_pad(uint8_t *buffer, intmax_t number, uint8_t base, uint32_t length, uint8_t fill);
uint32_t format_int_length(intmax_t number, uint8_t base);

// TODO format_byte() is broken
static inline uint8_t *format_byte_one(uint8_t *restrict buffer, uint8_t byte) // TODO is this necessary
{
	*buffer = byte;
	return buffer + 1;
}
static inline uint8_t *format_byte_many(uint8_t *restrict buffer, uint8_t byte, size_t size)
{
	memset(buffer, byte, size);
	return buffer + size;
}
#define format_byte(buffer, byte, ...) (_VA_ARGS_EMPTY(__VA_ARGS__) ? format_byte_one((buffer), (byte)) : format_byte_many((buffer), (byte), __VA_ARGS__))

// TODO mempcpy does exactly what format_bytes is supposed to do but is GNU extension; could I use it?
static inline uint8_t *format_bytes(uint8_t *restrict buffer, const uint8_t *restrict bytes, size_t size)
{
	return (uint8_t *)memcpy(buffer, bytes, size) + size;
}

char *format_hex(char *restrict buffer, const uint8_t *restrict bin, size_t length);
#define format_hex_length(bin, length) ((length) * 2)

char *format_base64(char *buffer, const uint8_t *restrict bytes, size_t length, char *end);
char *base64_flushable_end(const char *buffer, char *end);

// TODO: these should not be here
size_t parse_hex(unsigned char *restrict dest, const unsigned char *restrict src, size_t length);
size_t parse_base64_length(const unsigned char *restrict data, size_t length);
size_t parse_base64(const unsigned char *src, unsigned char *restrict dest, size_t length);
