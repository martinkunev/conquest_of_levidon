/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>
#include <GL/glext.h>

#include <X11/Xlib-xcb.h>

#if defined(ENABLE_CURSOR)
# include <xcb/render.h>
# include <xcb/xcb_image.h>
# include <xcb/xcb_renderutil.h>
#endif

#include "basic.h"
#include "log.h"
#include "font.h"
#include "interface.h"

// http://xcb.freedesktop.org/opengl/
// http://xcb.freedesktop.org/tutorial/events/
// http://techpubs.sgi.com/library/dynaweb_docs/0640/SGI_Developer/books/OpenGL_Porting/sgi_html/ch04.html
// http://tronche.com/gui/x/xlib/graphics/font-metrics/
// http://www.opengl.org/sdk/docs/man2/

// Use xlsfonts to list the available fonts.
// "-misc-dejavu sans mono-bold-r-normal--12-0-0-0-m-0-ascii-0"

#define WM_STATE "_NET_WM_STATE"
#define WM_STATE_FULLSCREEN "_NET_WM_STATE_FULLSCREEN"

struct window
{
	xcb_window_t id;
	GLXDrawable drawable;
	unsigned width, height;
};

struct window internal;

static Display *display;
static int visual_id;
static GLXFBConfig fb_config;
static xcb_screen_t *screen;
static xcb_window_t window;
static GLXContext context;
static GLXDrawable drawable;

xcb_connection_t *connection;
KeySym *keymap;
int keysyms_per_keycode;
int keycode_min, keycode_max;

unsigned WINDOW_WIDTH, WINDOW_HEIGHT;

int if_init(void)
{
	display = XOpenDisplay(0);
	if (!display)
		return -1;

	connection = XGetXCBConnection(display);
	if (!connection)
		goto error;

	XSetEventQueueOwner(display, XCBOwnsEventQueue);

	// find XCB screen
	// TODO better handling for multiple screens
	int screen_index = XDefaultScreen(display);
	xcb_screen_iterator_t iterator = xcb_setup_roots_iterator(xcb_get_setup(connection));
	int offset = screen_index;
	while (offset)
	{
		if (!iterator.rem)
			goto error;
		xcb_screen_next(&iterator);
		offset -= 1;
	}
	screen = iterator.data;

	// Choose a framebuffer configuration.
	//const int attributes[] = {GLX_BUFFER_SIZE, 32, GLX_DEPTH_SIZE, 24, GLX_RENDER_TYPE, GLX_RGBA_BIT, GLX_DOUBLEBUFFER, True, None};
	const int attributes[] = {GLX_RENDER_TYPE, GLX_RGBA_BIT, None};
	int fb_configs_count = 0;
	GLXFBConfig *fb_configs = glXChooseFBConfig(display, screen_index, attributes, &fb_configs_count);
	if (!fb_configs || (fb_configs_count == 0))
		goto error;
	fb_config = fb_configs[0];

	// Create the window and make it fullscreen.
	{
		window = xcb_generate_id(connection);

		// Create colormap and query visual_id.
		xcb_colormap_t colormap = xcb_generate_id(connection);
		glXGetFBConfigAttrib(display, fb_config, GLX_VISUAL_ID, &visual_id);
		xcb_create_colormap(connection, XCB_COLORMAP_ALLOC_NONE, colormap, screen->root, visual_id);

		XWindowAttributes attributes;
		XGetWindowAttributes(display, screen->root, &attributes);
		WINDOW_WIDTH = attributes.width;
		WINDOW_HEIGHT = attributes.height;

		uint32_t eventmask = XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION | XCB_EVENT_MASK_BUTTON_MOTION;
		uint32_t valuelist[] = {eventmask, colormap, 0};
		uint32_t valuemask = XCB_CW_EVENT_MASK | XCB_CW_COLORMAP;

		XEvent event = {0};
		event.type = ClientMessage;
		event.xclient.window = window;
		event.xclient.message_type = XInternAtom(display, WM_STATE, 0);
		event.xclient.format = 32;
		event.xclient.data.l[0] = 1; // 0 == unset; 1 == set; 2 == toggle
		event.xclient.data.l[1] = XInternAtom(display, WM_STATE_FULLSCREEN, 0);

		// TODO set window parameters
		// TODO set window title, etc.
		xcb_create_window(connection, XCB_COPY_FROM_PARENT, window, screen->root, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, visual_id, valuemask, valuelist);
		xcb_map_window(connection, window); // NOTE: window must be mapped before glXMakeContextCurrent

		XSendEvent(display, XDefaultRootWindow(display), 0, SubstructureRedirectMask | SubstructureNotifyMask, &event);
		XSync(display, 1);
		sleep(1); // TODO see why this hack is necessary to fix buggy fullscreen
	}

	drawable = glXCreateWindow(display, fb_config, window, 0);

	// Create OpenGL context and make it current.
	context = glXCreateNewContext(display, fb_config, GLX_RGBA_TYPE, 0, 1);
	if (!context)
	{
		glXDestroyWindow(display, drawable);
		xcb_destroy_window(connection, window);
		goto error;
	}
	if (if_window_default() < 0)
	{
		glXDestroyContext(display, context);
		glXDestroyWindow(display, drawable);
		xcb_destroy_window(connection, window);
		goto error;
	}

	// Enable transparency.
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.0, 0.0, 0.0, 0.0);

	if (font_init() < 0)
	{
		LOG_FATAL("Cannot initialize fonts");
		glXDestroyWindow(display, drawable);
		glXDestroyContext(display, context);
		xcb_destroy_window(connection, window);
		goto error;
	}

	// TODO handle modifier keys
	// TODO handle dead keys
	// TODO the keyboard mappings don't work as expected for different keyboard layouts

	// Initialize keyboard mapping table.
	XDisplayKeycodes(display, &keycode_min, &keycode_max);
	keymap = XGetKeyboardMapping(display, keycode_min, (keycode_max - keycode_min + 1), &keysyms_per_keycode);
	if (!keymap)
	{
		font_term();
		glXDestroyWindow(display, drawable);
		glXDestroyContext(display, context);
		xcb_destroy_window(connection, window);
		goto error;
	}

	return 0;

error:
	// TODO free whatever needs to be freed
	XCloseDisplay(display);
	return -1;
}

// TODO ? call this after resize
static void if_prepare(unsigned width, unsigned height)
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, 0, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void if_display(void (*display)(const void *, double), const void *context, double progress)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	display(context, progress);
	glFlush();
	glFinish();
}

void if_term(void)
{
	XFree(keymap);
	font_term();

	glXDestroyWindow(display, drawable);
	glXDestroyContext(display, context);
	xcb_destroy_window(connection, window);
	XCloseDisplay(display);
}

void if_window_show(const struct window *restrict window)
{
	xcb_map_window(connection, window->id);
	xcb_flush(connection);
}

void if_window_hide(const struct window *restrict window)
{
	xcb_unmap_window(connection, window->id);
	xcb_flush(connection);
}

const struct window *if_window_create(unsigned x, unsigned y, unsigned width, unsigned height)
{
	struct window *win = &internal; // TODO support more than 1 window

	win->id = xcb_generate_id(connection);
	xcb_create_window(connection, XCB_COPY_FROM_PARENT, win->id, window, x, y, width, height, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, visual_id, 0, 0);
	if_window_show(win);

	win->drawable = glXCreateWindow(display, fb_config, win->id, 0);

	win->width = width;
	win->height = height;

	return win;
}

void if_window_destroy(const struct window *restrict window)
{
	glXDestroyWindow(display, window->drawable);
	xcb_destroy_window(connection, window->id);
	xcb_flush(connection);
}

int if_window_default(void)
{
	glFlush();
	if (!glXMakeContextCurrent(display, drawable, drawable, context))
		return -1;
	if_prepare(WINDOW_WIDTH, WINDOW_HEIGHT);
	return 0;
}

int if_window(const struct window *restrict window)
{
	glFlush();
	if (!glXMakeContextCurrent(display, window->drawable, window->drawable, context))
		return -1;
	if_prepare(window->width, window->height);
	return 0;
}

#if defined(ENABLE_CURSOR)
uint32_t if_cursor_init(uint8_t *restrict pixels, unsigned width, unsigned height)
{
	xcb_cursor_t cursor = xcb_generate_id(connection);
	xcb_render_picture_t picture = xcb_generate_id(connection);
	xcb_pixmap_t pixmap = xcb_generate_id(connection);
	xcb_gcontext_t context = xcb_generate_id(connection);

	xcb_image_t *cursor_image;

	xcb_render_query_pict_formats_reply_t *reply;
	xcb_render_pictformat_t picture_format;

	reply = xcb_render_query_pict_formats_reply(connection, xcb_render_query_pict_formats(connection), 0);
	if (!reply)
		return 0;
	picture_format = xcb_render_util_find_standard_format(reply, XCB_PICT_STANDARD_ARGB_32)->id;
	free(reply);

	xcb_create_pixmap(connection, 4 * 8, pixmap, screen->root, width, height);
	cursor_image = xcb_image_create_native(connection, width, height, XCB_IMAGE_FORMAT_Z_PIXMAP, 4 * 8, 0, 0, pixels);
	if (!cursor_image)
		return 0;
	xcb_create_gc(connection, context, pixmap, 0, 0);
	xcb_image_put(connection, pixmap, context, cursor_image, 0, 0, 0);
	xcb_render_create_picture(connection, picture, pixmap, picture_format, 0, 0);

	xcb_render_create_cursor(connection, cursor, picture, 0, 0);

	xcb_render_free_picture(connection, picture);
	xcb_free_gc(connection, context);
	xcb_free_pixmap(connection, pixmap);
	xcb_image_destroy(cursor_image);

	return cursor;
}

void if_cursor_set(uint32_t cursor)
{
	xcb_change_window_attributes(connection, window, XCB_CW_CURSOR, &cursor);
	xcb_flush(connection);
}

void if_cursor_term(uint32_t cursor)
{
	xcb_free_cursor(connection, cursor);
}
#endif
