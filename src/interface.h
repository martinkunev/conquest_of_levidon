/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

extern unsigned WINDOW_WIDTH, WINDOW_HEIGHT;

struct window;

int if_init(void);
void if_display(void (*display)(const void *, double), const void *context, double progress);
void if_term(void);

const struct window *if_window_create(unsigned x, unsigned y, unsigned width, unsigned height);
void if_window_destroy(const struct window *restrict window);

void if_window_show(const struct window *restrict window);
void if_window_hide(const struct window *restrict window);

int if_window_default(void);
int if_window(const struct window *restrict window);

uint32_t if_cursor_init(uint8_t *restrict pixels, unsigned width, unsigned height);
void if_cursor_set(uint32_t cursor);
void if_cursor_term(uint32_t cursor);
