/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <png.h>

struct image
{
	GLuint texture;
	uint32_t width, height;
};

int image_load_png(struct image *restrict image, const char *restrict filename, void (*modify)(const struct image *restrict, png_byte **));
void image_unload(struct image *restrict image);

int cursor_load_png(uint32_t *restrict cursor, const char *restrict filename);

void filter_grayscale(const struct image *restrict image, png_byte **rows);
void filter_mask(const struct image *restrict image, png_byte **rows);
