/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <pthread.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "basic.h"
#include "log.h"
#include "game.h"
#include "map.h"
#include "turn.h"
#include "pathfinding.h"
#define ARRAY_SOURCE
#include "battle.h"
#include "movement.h"
#include "round.h"

#define PAWNS_STARTUP_LIMIT 7
#define BATTLE_PLAYERS_LIMIT 7

enum {Center = DIRECTIONS_COUNT, Bottom, Top, Right, Left};

// TODO pawns A and B are fighting. pawn C is commanded to attack A but stays blocked by B and never attacks

/*
static inline double distance(const double origin[static restrict 2], const double target[static restrict 2])
{
	double dx = target[0] - origin[0], dy = target[1] - origin[1];
	return sqrt(dx * dx + dy * dy);
}
*/

struct pawn *pawn_find(const struct battlefield *restrict field, struct position position, float radius)
{
	struct pawn *const *pawns = field->pawns;
	size_t i;
	for(i = 0; (i < BATTLEFIELD_PAWNS_LIMIT) && pawns[i]; i += 1)
	{
		if (!pawns[i]->count)
			continue;
		if (battlefield_distance(pawns[i]->position, position) < radius)
			return pawns[i];
	}
	return 0;
}

struct position pawn_position_formation(const struct battle *restrict battle, unsigned char player)
{
	/*static const struct position startup_location[] =
	{
		[East] = {25, 12.5},
		[NorthEast] = {18.75, 3.125},
		[NorthWest] = {6.25, 3.125},
		[West] = {0, 12.5},
		[SouthWest] = {6.25, 21.875},
		[SouthEast] = {18.75, 21.875},
		[Center] = {12.5, 12.5},
		[Bottom] = {12.5, 25},
		[Top] = {12.5, 0},
		[Right] = {25, 12.5},
		[Left] = {0, 12.5},
	};*/
	static const struct position startup_location[] =
	{
		[East] = {24.5, 12.5},
		[NorthEast] = {18.75, 3.125},
		[NorthWest] = {6.25, 3.125},
		[West] = {0.5, 12.5},
		[SouthWest] = {6.25, 21.875},
		[SouthEast] = {18.75, 21.875},
		[Center] = {12.5, 12.5},
		[Bottom] = {12.5, 24.5},
		[Top] = {12.5, 0.5},
		[Right] = {24.5, 12.5},
		[Left] = {0.5, 12.5},
	};

	struct position position = startup_location[battle->players[player].startup];
	size_t x = position.x, y = position.y;

	if (pawn_find(&battle->field[y][x], position, PAWN_RADIUS * 2))
		return POSITION_NONE;
	if ((y < BATTLEFIELD_HEIGHT) && pawn_find(&battle->field[y + 1][x], position, PAWN_RADIUS * 2))
		return POSITION_NONE;
	if ((x < BATTLEFIELD_WIDTH) && pawn_find(&battle->field[y][x + 1], position, PAWN_RADIUS * 2))
		return POSITION_NONE;
	if ((x < BATTLEFIELD_WIDTH) && (y < BATTLEFIELD_HEIGHT) && pawn_find(&battle->field[y + 1][x + 1], position, PAWN_RADIUS * 2))
		return POSITION_NONE;

	return position;
}

static void startup_choose(const struct game *restrict game, struct battle *restrict battle)
{
	size_t startup[BATTLE_PLAYERS_LIMIT];

	unsigned participants[PLAYERS_LIMIT]; // list of players in the battle
	size_t count = 0;

	size_t i;

	for(i = 0; i < game->players_count; i += 1)
		if (battle->players[i].state == PLAYER_ALIVE)
			participants[count++] = i;
	assert(count == battle->players_count);

	if (battle->assault)
	{
		unsigned location[] = {Bottom, Top};
		battle->players[participants[0]].startup = location[participants[0] == battle->hexagon->owner];
		battle->players[participants[1]].startup = location[participants[1] == battle->hexagon->owner];
		return;
	}

	switch (battle->players_count)
	{
	case 4:
		startup[2] = Right;
		startup[3] = Left;
	case 2:
		startup[0] = Bottom;
		startup[1] = Top;
		break;

	case 7:
		startup[6] = Center;
	case 6:
		startup[5] = SouthEast;
	case 5:
		startup[3] = West;
		startup[4] = NorthEast;
	case 3:
		startup[0] = East;
		startup[1] = NorthWest;
		startup[2] = SouthWest;
		break;

	default:
		assert(0);
	}

	// Shuffle battle order randomly to avoid bias.
	// For optimization, we can just overwrite the values in startup.
    for(i = battle->players_count; i > 0; i -= 1)
    {
		size_t j = random() % i;
		battle->players[participants[i - 1]].startup = startup[j];
		startup[j] = startup[i - 1];
	}
}

static void battlefield_init_startup(const struct game *restrict game, struct battle *restrict battle)
{
	size_t i;

	// TODO individual pawn order should go the same for all locations (clockwise or counterclockwise) (for symmetry)
	// TODO the location in the center is at a disadvantage

	static const struct position location_places[][PAWNS_STARTUP_LIMIT] =
	{
		[East] = {{24.5, 12}, {24.5, 13}, {24.5, 11}, {24.5, 14}, {23.5, 12.5}, {23.5, 11.5}, {23.5, 13.5}},
//			[NorthEast] = {{18.75 + 0.25 * SQRT3, 3.375}, {18.75 - 0.25 * SQRT3, 2.875}, {18.75 + 0.75 * SQRT3, 3.875}, {18.75 - SQRT3 * 0.75, 2.375}, {18.75, 3.125}, {18.75 + 0.5 * SQRT3, 3.625}, {18.75 - 0.5 * SQRT3, 2.625}},
		[NorthEast] = {{18.5 + 0.25 * SQRT3, 3.375 + 0.25 * SQRT3}, {18.5 - 0.25 * SQRT3, 2.875 + 0.25 * SQRT3}, {18.5 + 0.75 * SQRT3, 3.875 + 0.25 * SQRT3}, {18.5 - SQRT3 * 0.75, 2.375 + 0.25 * SQRT3}, {18, 3.125 + 0.75 * SQRT3}, {18 + 0.5 * SQRT3, 3.625 + 0.75 * SQRT3}, {18 - 0.5 * SQRT3, 2.625 + 0.75 * SQRT3}},
		[NorthWest] = {{6.5 - 0.25 * SQRT3, 3.375 + 0.25 * SQRT3}, {6.5 + 0.25 * SQRT3, 2.875 + 0.25 * SQRT3}, {6.5 - 0.75 * SQRT3, 3.875 + 0.25 * SQRT3}, {6.5 + SQRT3 * 0.75, 2.375 + 0.25 * SQRT3}, {7, 3.125 + 0.75 * SQRT3}, {7 - 0.5 * SQRT3, 3.625 + 0.75 * SQRT3}, {7 + 0.5 * SQRT3, 2.625 + 0.75 * SQRT3}},
		[West] = {{0.5, 12}, {0.5, 13}, {0.5, 11}, {0.5, 14}, {1.5, 12.5}, {1.5, 11.5}, {1.5, 13.5}},
		[SouthWest] = {{6.5 - 0.25 * SQRT3, 21.625 - 0.25 * SQRT3}, {6.5 + 0.25 * SQRT3, 22.125 - 0.25 * SQRT3}, {6.5 - 0.75 * SQRT3, 21.125 - 0.25 * SQRT3}, {6.5 + SQRT3 * 0.75, 22.625 - 0.25 * SQRT3}, {7, 21.875 - 0.75 * SQRT3}, {7 - 0.5 * SQRT3, 21.375 - 0.75 * SQRT3}, {7 + 0.5 * SQRT3, 22.375 - 0.75 * SQRT3}},
		[SouthEast] = {{18.5 + 0.25 * SQRT3, 21.625 - 0.25 * SQRT3}, {18.5 - 0.25 * SQRT3, 22.125 - 0.25 * SQRT3}, {18.5 + 0.75 * SQRT3, 21.125 - 0.25 * SQRT3}, {18.5 - SQRT3 * 0.75, 22.625 - 0.25 * SQRT3}, {18, 21.875 - 0.75 * SQRT3}, {18 + 0.5 * SQRT3, 21.375 - 0.75 * SQRT3}, {18 - 0.5 * SQRT3, 22.375 - 0.75 * SQRT3}},
		[Center] = {{12.5, 12.5}, {13.5, 12.5}, {12, 12.5 - 0.5 * SQRT3}, {12, 12.5 + 0.5 * SQRT3}, {11.5, 12.5}, {13, 12.5 + 0.5 * SQRT3}, {13, 12.5 - 0.5 * SQRT3}},
		[Bottom] = {{12, 24.5}, {13, 24.5}, {11, 24.5}, {14, 24.5}, {12.5, 23.5}, {11.5, 23.5}, {13.5, 23.5}},
		[Top] = {{12, 0.5}, {13, 0.5}, {11, 0.5}, {14, 0.5}, {12.5, 1.5}, {11.5, 1.5}, {13.5, 1.5}},
		[Right] = {{24.5, 12}, {24.5, 13}, {24.5, 11}, {24.5, 14}, {23.5, 12.5}, {23.5, 11.5}, {23.5, 13.5}},
		[Left] = {{0.5, 12}, {0.5, 13}, {0.5, 11}, {0.5, 14}, {1.5, 12.5}, {1.5, 11.5}, {1.5, 13.5}},
	};

	unsigned counts[PLAYERS_LIMIT] = {0};

	// Choose startup locations.
	startup_choose(game, battle);

	// Determine battlefield location for each player.
	// Place pawns on the battlefield.
	for(i = 0; i < battle->pawns_count; i += 1)
	{
		unsigned owner = battle->pawns[i].owner;

		battle->pawns[i].startup = battle->players[owner].startup;

		if (counts[owner] < PAWNS_STARTUP_LIMIT)
		{
			struct position position = location_places[battle->players[owner].startup][counts[owner]];
			pawn_place(battle->pawns + i, position);
			counts[owner] += 1;
		}
		else
			pawn_place(battle->pawns + i, POSITION_NONE);
	}
}

static void obstacle_set(struct battle *restrict battle, const struct garrison_info *restrict garrison, size_t x, size_t y, enum blockage blockage, unsigned location)
{
	struct battlefield *restrict field = &battle->field[y][x];

	field->blockage = blockage;
	field->blockage_location = location;

	switch (field->blockage_location)
	{
	case SIDE_LEFT:
	case SIDE_RIGHT:
		field->blockage_location |= POSITION_TOP | POSITION_BOTTOM;
		break;

	case SIDE_BOTTOM:
		field->blockage_location |= POSITION_LEFT | POSITION_RIGHT;
		break;
	}

	field->owner = battle->hexagon->owner;
	field->open = 0;
	switch (blockage)
	{
	case BLOCKAGE_WALL:
		field->unit = &garrison->wall;
		break;

	case BLOCKAGE_GATE:
		assert(battle->gates_count < GATES_LIMIT);
		battle->gates[battle->gates_count++] = field;

		field->unit = &garrison->gate;
		break;

	case BLOCKAGE_TOWER:
		field->unit = &garrison->tower;
		break;
	}
	field->strength = field->unit->health;
	//field->pawn = 0;

	if (blockage == BLOCKAGE_TOWER)
		switch (field->blockage_location)
		{
		case SIDE_LEFT:
			field->ladder = (struct tile){x + 1, y};
			break;

		case SIDE_RIGHT:
			field->ladder = (struct tile){x - 1, y};
			break;

		case SIDE_TOP:
			field->ladder = (struct tile){x, y + 1};
			break;

		case SIDE_BOTTOM:
			field->ladder = (struct tile){x, y - 1};
			break;
		}
}

static void battlefield_init_garrison(const struct game *restrict game, struct battle *restrict battle)
{
	const struct garrison_info *restrict garrison = garrison_info(battle->hexagon);

	// Place the garrison in the top part of the battlefield.
	// . gate
	// # wall
	// O tower

	// #       #
	// .       .
	// O       O
	// ###O.O###

	// TODO finish tower implementation

	battle->gates_count = 0;

	obstacle_set(battle, garrison, 8, 0, BLOCKAGE_WALL, POSITION_TOP | POSITION_BOTTOM);
	obstacle_set(battle, garrison, 8, 1, BLOCKAGE_GATE, SIDE_LEFT);
	obstacle_set(battle, garrison, 8, 2, BLOCKAGE_TOWER, SIDE_LEFT);
	obstacle_set(battle, garrison, 8, 3, BLOCKAGE_WALL, POSITION_TOP | POSITION_RIGHT);
	obstacle_set(battle, garrison, 9, 3, BLOCKAGE_WALL, POSITION_LEFT | POSITION_RIGHT);
	obstacle_set(battle, garrison, 10, 3, BLOCKAGE_WALL, POSITION_LEFT | POSITION_RIGHT);
	obstacle_set(battle, garrison, 11, 3, BLOCKAGE_TOWER, SIDE_BOTTOM);
	obstacle_set(battle, garrison, 12, 3, BLOCKAGE_GATE, SIDE_BOTTOM);
	obstacle_set(battle, garrison, 13, 3, BLOCKAGE_TOWER, SIDE_BOTTOM);
	obstacle_set(battle, garrison, 14, 3, BLOCKAGE_WALL, POSITION_LEFT | POSITION_RIGHT);
	obstacle_set(battle, garrison, 15, 3, BLOCKAGE_WALL, POSITION_LEFT | POSITION_RIGHT);
	obstacle_set(battle, garrison, 16, 3, BLOCKAGE_WALL, POSITION_TOP | POSITION_LEFT);
	obstacle_set(battle, garrison, 16, 2, BLOCKAGE_TOWER, SIDE_RIGHT);
	obstacle_set(battle, garrison, 16, 1, BLOCKAGE_GATE, SIDE_RIGHT);
	obstacle_set(battle, garrison, 16, 0, BLOCKAGE_WALL, POSITION_TOP | POSITION_BOTTOM);
}

static void pawn_set(struct battle *restrict battle, size_t index, struct troop *troop, unsigned char owner)
{
	battle->pawns[index].troop = troop;
	battle->pawns[index].owner = owner;
	battle->pawns[index].count = troop->count;
	battle->pawns[index].hurt = 0;
	battle->pawns[index].attackers = 0;

	// battle->pawns[index].position will be initialized during formation

	battle->pawns[index].path = (struct array_path){0};

	// battle->pawns[index].startup will be initialized by battlefield_init_startup

	battle->pawns[index].command.moves.count = 0;
	battle->pawns[index].command.action = 0;
}

int battle_init(const struct game *restrict game, struct battle *restrict battle, struct battle_info *restrict info)
{
	size_t x, y;
	size_t i, j;

	size_t pawns_count = 0;
	size_t pawn_offset[PLAYERS_LIMIT] = {0};
	unsigned pawns_speed_count[1 + UNIT_SPEED_LIMIT] = {0};
	size_t speed_offset[1 + UNIT_SPEED_LIMIT];

	battle->hexagon = info->location;
	battle->assault = (info->type == BATTLE_ASSAULT);

	// Initialize each battle field as empty.
	for(y = 0; y < BATTLEFIELD_HEIGHT; y += 1)
	{
		for(x = 0; x < BATTLEFIELD_WIDTH; x += 1)
		{
			struct battlefield *field = &battle->field[y][x];
			field->tile = (struct tile){x, y};
			field->blockage = BLOCKAGE_NONE;
			field->blockage_location = 0;
		}
	}

	// Determine pawn counts.
	for(i = 0; i < PLAYERS_LIMIT; i += 1)
		battle->players[i].pawns_count = 0;
	for(i = 0; i < info->fighters_count; i += 1)
	{
		for(j = 0; j < info->fighters[i]->private.count; j += 1)
		{
			struct troop *troop = info->fighters[i]->private.troops[j];

			pawns_count += 1; // count pawns
			battle->players[info->fighters[i]->owner].pawns_count += 1; // count pawns by player
			pawns_speed_count[troop->unit->speed] += 1; // count pawns by speed
		}
	}
	if (battle->assault)
	{
		struct region *region = game->regions + battle->hexagon->info.region_index;
		for(i = 0; i < region->garrison.private.count; i += 1)
		{
			struct troop *troop = region->garrison.private.troops[i];

			pawns_count += 1; // count pawns
			battle->players[battle->hexagon->owner].pawns_count += 1; // count pawns by player
			pawns_speed_count[troop->unit->speed] += 1; // count pawns by speed
		}
	}

	// Initialize arrys for pawns and pawns by player.

	battle->pawns = malloc(pawns_count * sizeof(*battle->pawns));
	if (!battle->pawns)
		return ERROR_MEMORY;

#if defined(UI)
	battle->animation.pawns = malloc(pawns_count * sizeof(*battle->animation.pawns));
	if (!battle->animation.pawns)
	{
		free(battle->pawns);
		return ERROR_MEMORY;
	}
#endif

	battle->players_count = 0;
	for(i = 0; i < PLAYERS_LIMIT; ++i)
	{
		if (!battle->players[i].pawns_count)
		{
			battle->players[i].pawns = 0;
			battle->players[i].state = PLAYER_DEAD;
			continue;
		}

		battle->players[i].pawns = malloc(battle->players[i].pawns_count * sizeof(*battle->players[i].pawns));
		if (!battle->players[i].pawns)
		{
			while (i--)
				free(battle->players[i].pawns);
#if defined(UI)
			free(battle->animation.pawns);
#endif
			free(battle->pawns);
			return ERROR_MEMORY;
		}
		battle->players[i].state = PLAYER_ALIVE;
		battle->players_count += 1;
	}

	// Pawns will be stored by speed descending.
	// Initialize offsets for any given speed in the pawns array.
	speed_offset[UNIT_SPEED_LIMIT] = 0;
	i = UNIT_SPEED_LIMIT;
	while (i--)
		speed_offset[i] = speed_offset[i + 1] + pawns_speed_count[i + 1];

	// Initialize pawn for each troop.
	for(i = 0; i < info->fighters_count; i += 1)
	{
		for(j = 0; j < info->fighters[i]->private.count; j += 1)
		{
			struct troop *troop = info->fighters[i]->private.troops[j];
			size_t index = speed_offset[troop->unit->speed]++;

			pawn_set(battle, index, troop, info->fighters[i]->owner);
			battle->players[info->fighters[i]->owner].pawns[pawn_offset[info->fighters[i]->owner]] = battle->pawns + index;
			pawn_offset[info->fighters[i]->owner] += 1;
		}
	}
	if (battle->assault)
	{
		struct region *region = game->regions + battle->hexagon->info.region_index;
		for(i = 0; i < region->garrison.private.count; i += 1)
		{
			struct troop *troop = region->garrison.private.troops[i];
			size_t index = speed_offset[troop->unit->speed]++;

			pawn_set(battle, index, troop, battle->hexagon->owner);
			battle->players[battle->hexagon->owner].pawns[pawn_offset[battle->hexagon->owner]] = battle->pawns + index;
			pawn_offset[battle->hexagon->owner] += 1;
		}
	}

	battle->pawns_count = pawns_count;
	battle->round = 0;

	battlefield_init_startup(game, battle);
	if (battle->assault)
		battlefield_init_garrison(game, battle);
	else
		battle->gates_count = 0;

	battle->obstacles = 0;
	for(i = 0; i < game->players_count; i += 1)
	{
		battle->graph[i] = 0;

		battle->players[i].pawns_waiting = 0;
		if (battle->players[i].pawns_count > PAWNS_STARTUP_LIMIT)
			battle->players[i].pawns_waiting = battle->players[i].pawns_count - PAWNS_STARTUP_LIMIT;
	}

	return 0;
}

#if 0

void battle_retreat(struct battle *restrict battle, unsigned char player)
{
	for(size_t i = 0; i < battle->players[player].pawns_count; ++i)
	{
		battle->players[player].pawns[i]->action = ACTION_HOLD;
		battle->players[player].pawns[i]->moves.count = 0;
	}
	battle->players[player].state = PLAYER_RETREAT;
}

static void retreat(struct battle *restrict battle, unsigned char player)
{
	const struct garrison_info *restrict info;
	struct troop *restrict troop;
	unsigned garrison_places;

	// TODO currently attacker troops retreating from assault are killed; is this okay?
	// TODO currently open battle garrison reinforcements are killed; is this okay?

	info = garrison_info(battle->region);
	if (info && (battle->region->garrison.owner == player))
	{
		// Count the free places for troops in the garrison.
		garrison_places = info->troops;
		for(troop = battle->region->troops; troop; troop = troop->_next)
			if (troop->location == LOCATION_GARRISON)
				garrison_places -= 1;
	}
	else garrison_places = 0;

	for(size_t i = 0; i < battle->players[player].pawns_count; ++i)
	{
		troop = battle->players[player].pawns[i]->troop;

		if (troop->location == battle->region)
		{
			// If there is place for the troop in the garrison, move it there. Otherwise, kill it.
			if (garrison_places)
			{
				troop->move = troop->location = LOCATION_GARRISON;
				garrison_places -= 1;
			}
			else battle->players[player].pawns[i]->count = 0;
		}
		else if (troop->location == LOCATION_GARRISON)
		{
			// Kill troops that retreat from defending a garrison.
			battle->players[player].pawns[i]->count = 0;
		}
		else troop->move = troop->location;
	}

	battle->players[player].state = PLAYER_DEAD;
}
#endif

// Returns winner alliance number if the battle ended or -1 otherwise.
int battle_end(const struct game *restrict game, struct battle *restrict battle)
{
	uint32_t winner;
	uint32_t alliances = 0;
	size_t i, j;

	for(i = 0; i < game->players_count; ++i)
	{
		/*if (battle->players[i].state == PLAYER_RETREAT)
			retreat(battle, i);*/
		if (battle->players[i].state == PLAYER_DEAD)
			continue;

		for(j = 0; j < battle->players[i].pawns_count; ++j)
		{
			if (battle->players[i].pawns[j]->count)
			{
				battle->players[i].state = PLAYER_ALIVE;
				winner = game->players[i].alliance;
				alliances |= (1 << winner);
				goto next;
			}
		}
		battle->players[i].state = PLAYER_DEAD;
next:
		;
	}

	return (alliances & (alliances - 1)) ? -1 : winner;
}

void battle_term(const struct game *restrict game, struct battle *restrict battle)
{
	size_t i;

	free(battle->obstacles);
	for(i = 0; i < game->players_count; i += 1)
		graph_term(battle->graph[i]);

	for(i = 0; i < battle->pawns_count; ++i)
	{
		battle->pawns[i].troop->count = battle->pawns[i].count;
		free(battle->pawns[i].path.data);
	}
	for(i = 0; i < game->players_count; ++i)
		free(battle->players[i].pawns);
#if defined(UI)
	free(battle->animation.pawns);
#endif
	free(battle->pawns);
}
