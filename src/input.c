/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>

#include "basic.h"
#include "log.h"
#include "input.h"
#include "interface.h"

#define DRAG_THRESHOLD 16

// TODO unicode support (special keyboard handling required (e.g. upper-lower transform))

extern xcb_connection_t *connection;
extern KeySym *keymap;
extern int keysyms_per_keycode;
extern int keycode_min, keycode_max;

static int is_modifier(int code)
{
	switch (code)
	{
	default:
		return 0;

	case XK_Shift_L:
	case XK_Shift_R:
	case XK_Control_L:
	case XK_Control_R:
	case XK_Alt_L:
	case XK_Alt_R:
	case XK_Super_L:
	case XK_Super_R:
	case XK_Caps_Lock:
	case XK_Num_Lock:
		return 1;
	}
}

int input_mouse_terminate(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;
	return INPUT_TERMINATE;
}

int input_mouse_cancel(int code, unsigned x, unsigned y, uint16_t modifiers, void *argument, double progress)
{
	if (code != EVENT_MOUSE_LEFT)
		return INPUT_NOTME;
	return ERROR_CANCEL;
}

static inline double timer_progress(const struct timeval *restrict start)
{
	struct timeval now;
	gettimeofday(&now, 0);
	return now.tv_sec - start->tv_sec + ((double)now.tv_usec - start->tv_usec) / 1000000;
}

int input_process(const struct area *restrict areas, size_t areas_count, void (*display)(const void *, double), int (*timer)(void *, double), void *restrict context)
{
	xcb_generic_event_t *event;
	struct timeval start;
	double progress = 0;

	int code; // TODO this is oversimplification
	unsigned x, y;
	uint16_t modifiers;
	enum {Free = 0, Press, Drag} setting = Free;

	// Ignore all the queued events.
	while (event = xcb_poll_for_event(connection))
		free(event);

	if (timer)
		gettimeofday(&start, 0);
	if_display(display, context, progress);

	while (1)
	{
		xcb_button_release_event_t *mouse;
		xcb_key_press_event_t *keyboard;
		xcb_motion_notify_event_t *motion;

		size_t index;

wait:
		if (timer)
		{
			event = xcb_poll_for_event(connection);
			progress = timer_progress(&start);
			if (!event)
			{
				int status = timer(context, progress);
				switch (status)
				{
				case INPUT_DONE:
					if_display(display, context, progress);
				case INPUT_NOTME:
				case INPUT_IGNORE:
					continue;

				case INPUT_TERMINATE:
					status = 0;
				default: // runtime error
					return status;
				}
			}
		}
		else
		{
			event = xcb_wait_for_event(connection);
			if (!event)
				return ERROR_MEMORY;
		}

		switch (event->response_type & ~0x80)
		{
		case XCB_MOTION_NOTIFY:
			motion = (xcb_motion_notify_event_t *)event;
			modifiers = motion->state;
			if (modifiers & XCB_KEY_BUT_MASK_BUTTON_1)
			{
				int dx = motion->event_x - x;
				int dy = motion->event_y - y;
				if ((setting == Press) && (dx * dx + dy * dy >= DRAG_THRESHOLD))
				{
					setting = Drag;
					code = EVENT_DRAG_START;
					break;
				}
				free(event);
				continue;
			}
			else
			{
				code = EVENT_MOTION;
				x = motion->event_x;
				y = motion->event_y;
				break;
			}

		case XCB_BUTTON_PRESS:
			if (setting)
			{
				if (setting == Drag)
				{
					setting = Free;
					code = EVENT_DRAG_STOP;
					break;
				}
				setting = Free;
			}
			else
			{
				setting = Press;

				mouse = (xcb_button_release_event_t *)event;
				code = -mouse->detail;
				x = mouse->event_x;
				y = mouse->event_y;
				modifiers = mouse->state;
			}
			free(event);
			continue;

		case XCB_BUTTON_RELEASE:
			if (setting)
			{
				mouse = (xcb_button_release_event_t *)event;
				x = mouse->event_x;
				y = mouse->event_y;
				modifiers = mouse->state;
				if (setting == Press)
					code = -mouse->detail;
				else
				{
					assert(setting == Drag);
					code = EVENT_DRAG_STOP;
				}
				setting = Free;
			}
			break;

		case XCB_KEY_PRESS:
			keyboard = (xcb_key_press_event_t *)event;
			code = keymap[(keyboard->detail - keycode_min) * keysyms_per_keycode];
			if (is_modifier(code))
			{
				free(event);
				continue;
			}
			x = keyboard->event_x;
			y = keyboard->event_y;

			modifiers = keyboard->state;
			// TODO is this okay?
			if (!(modifiers & ~(XCB_MOD_MASK_SHIFT | XCB_MOD_MASK_LOCK)) && islower(code))
			{
				int flip = (modifiers & XCB_MOD_MASK_SHIFT) != 0;
				flip += (modifiers & XCB_MOD_MASK_LOCK) != 0;
				if (flip % 2)
					code = toupper(code);
				modifiers = 0;
			}
			break;

		case XCB_EXPOSE:
			if_display(display, context, progress);
		default:
			free(event);
			continue;
		}

		free(event);

		// Propagate the event until someone handles it.
		index = areas_count;
		while (index--)
		{
			int status;

			if ((x < areas[index].left) || (areas[index].right < x) || (y < areas[index].top) || (areas[index].bottom < y))
				continue;

			status = areas[index].callback(code, x - areas[index].left, y - areas[index].top, modifiers, context, progress);
			switch (status)
			{
			case INPUT_NOTME:
				continue;

			case INPUT_DONE:
				if_display(display, context, progress);
			case INPUT_IGNORE:
				goto wait;

			case INPUT_TERMINATE:
				status = 0;
			default: // runtime error
				return status;
			}
		}
	}
}
