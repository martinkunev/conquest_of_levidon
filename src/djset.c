/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>

#if defined(TEST)
# include <stdarg.h>
# include <setjmp.h>
# include <cmocka.h>
#endif

#include "djset.h"

void djset_init(struct djset *restrict djset, struct djelement *restrict elements, size_t count)
{
	size_t i;

	djset->elements = elements;
	djset->count = count;
	djset->classes = count;

	for(i = 0; i < count; i += 1)
	{
		elements[i].parent = elements + i;
		elements[i].rank = 0;

#if defined(DJSET_ITERATION)
		elements[i].next = (i + 1 < count) ? (elements + i + 1) : 0;
		elements[i].class_previous = i ? (elements + i - 1) : 0;
		elements[i].class_last = elements + i;
#endif

		elements[i].class_count = 1;
	}

#if defined(DJSET_ITERATION)
	djset->class_first = djset->elements;
#endif
}

struct djelement *djset_find(struct djset *restrict djset, size_t i)
{
	struct djelement *restrict element = djset->elements + i;
	if (element->parent == element)
		return element;
	else
		return element->parent = djset_find(djset, element->parent - djset->elements); // path compression
}

void djset_union(struct djset *restrict djset, size_t i, size_t j)
{
	struct djelement *a = djset_find(djset, i);
	struct djelement *b = djset_find(djset, j);

	if (a == b)
		return;

	if (a->rank <= b->rank)
		b->rank += (a->rank == b->rank);
	else /* a->rank > b->rank */
	{
		struct djelement *swap = a;
		a = b;
		b = swap;
	}

	a->parent = b;
	djset->classes -= 1;

#if defined(DJSET_ITERATION)
	// Update linked list for iteration.
	// Invariant: classes are ordered by index of their root in elements.
	if (a < b)
	{
		if (a->class_previous)
			a->class_previous->next = a->class_last->next;
		else
			djset->class_first = a->class_last->next;
		a->class_last->next->class_previous = a->class_previous;
		if (b->class_last->next)
			b->class_last->next->class_previous = a->class_last;
		a->class_last->next = b->class_last->next;
	}
	else
	{
		a->class_previous->next = a->class_last->next;
		if (a->class_last->next)
			a->class_last->next->class_previous = a->class_previous;
	}
	b->class_last->next = a;
	b->class_last = a->class_last;
#endif

	b->class_count += a->class_count;
}

#if defined(DJSET_ITERATION)
struct djelement *djset_class_first(struct djset *djset)
{
	return djset_find(djset, djset->class_first - djset->elements);
}

struct djelement *djset_class_next(struct djset *djset, struct djelement *current)
{
	struct djelement *root = djset_find(djset, current - djset->elements);
	return root->class_last->next;
}

struct djelement *djset_next(struct djset *restrict djset, struct djelement *current)
{
	if (!current->next || (current->next->parent == current->next))
		return 0;
	return current->next;
}
#endif

#if defined(TEST)

# define SIZE 8
# define SSIZE 4

static void test(void **state)
{
	struct djelement elements[SIZE];
	struct djset djset;
	size_t i;

	for(i = 0; i < SIZE; i += 1)
		elements[i].value.integer = i;

	djset_init(&djset, elements, SIZE);

	assert_int_equal(djset.count, SIZE);
	assert_int_equal(djset.classes, SIZE);

	djset_union(&djset, 0, 1);
	djset_union(&djset, 0, 2);
	djset_union(&djset, 3, 4);
	djset_union(&djset, 0, 5);

	assert_int_equal(djset.count, SIZE);
	assert_int_equal(djset.classes, SIZE - 4);

	assert_int_equal(djset_find(&djset, 0)->value.integer, djset_find(&djset, 2)->value.integer);
	assert_int_equal(djset_find(&djset, 1)->value.integer, djset_find(&djset, 2)->value.integer);
	assert_int_equal(djset_find(&djset, 1)->value.integer, djset_find(&djset, 5)->value.integer);
	assert_int_equal(djset_find(&djset, 5)->value.integer, djset_find(&djset, 1)->value.integer);

	assert_int_equal(djset_find(&djset, 3)->value.integer, djset_find(&djset, 4)->value.integer);

	assert_int_equal(djset_find(&djset, 7)->value.integer, djset_find(&djset, 7)->value.integer);

	assert_int_not_equal(djset_find(&djset, 0)->value.integer, djset_find(&djset, 6)->value.integer);
	assert_int_not_equal(djset_find(&djset, 1)->value.integer, djset_find(&djset, 7)->value.integer);
	assert_int_not_equal(djset_find(&djset, 6)->value.integer, djset_find(&djset, 3)->value.integer);
	assert_int_not_equal(djset_find(&djset, 6)->value.integer, djset_find(&djset, 7)->value.integer);
	assert_int_not_equal(djset_find(&djset, 5)->value.integer, djset_find(&djset, 6)->value.integer);

	djset_union(&djset, 5, 4);

	assert_int_equal(djset.classes, SIZE - 5);

	assert_int_equal(djset_find(&djset, 5), djset_find(&djset, 4));
	assert_int_equal(djset_find(&djset, 3), djset_find(&djset, 0));

	assert_int_not_equal(djset_find(&djset, 5), djset_find(&djset, 6));
}

static void test_iterate(void **state)
{
	struct djelement elements[SSIZE];
	struct djelement *e, *n;
	struct djset djset;
	size_t i;

	for(i = 0; i < SSIZE; i += 1)
		elements[i].value.integer = i;

	djset_init(&djset, elements, SSIZE);

	e = djset_class_first(&djset);
	assert_int_equal(e->value.integer, 0);
	assert_int_equal(e->class_count, 1);
	n = djset_next(&djset, e);
	assert_null(n);
	n = djset_class_next(&djset, e);
	assert_int_equal(n->value.integer, 1);
	assert_int_equal(n->class_count, 1);

	e = n;
	n = djset_next(&djset, e);
	assert_null(n);
	n = djset_class_next(&djset, e);
	assert_int_equal(n->value.integer, 2);
	assert_int_equal(n->class_count, 1);

	e = n;
	n = djset_next(&djset, e);
	assert_null(n);
	n = djset_class_next(&djset, e);
	assert_int_equal(n->value.integer, 3);
	assert_int_equal(n->class_count, 1);

	assert_null(djset_class_next(&djset, n));

	// Merge some classes

	djset_union(&djset, 0, 2);
	djset_union(&djset, 3, 2);

	e = djset_class_first(&djset);
	assert_int_equal(e->value.integer, 1);
	assert_int_equal(e->class_count, 1);
	assert_null(djset_next(&djset, e));

	e = djset_class_next(&djset, e);
	assert_int_equal(e->class_count, 3);

	i = 0;
	do
	{
		assert_int_not_equal(e->value.integer, 1);
		i += 1;
	} while (e = djset_next(&djset, e));
	assert_int_equal(i, 3);
}

int main(void)
{
	const struct CMUnitTest tests[] =
    {
        cmocka_unit_test(test),
        cmocka_unit_test(test_iterate),
    };
    return cmocka_run_group_tests(tests, 0, 0);
}
#endif

// TODO article about disjoint set
// what's the idea (mathematical concept) and when it is useful (e.g. graph algorithms - dealing with connected components; checking for cycles)
// implementation
// complexity and what tricks are used to achieve it; tradeoffs (find is non-const; rank is a heurestic)

// TODO is this the best way to implement iteration?
