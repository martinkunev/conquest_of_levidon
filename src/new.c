#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

enum weapon {WEAPON_NONE, WEAPON_CLUB, WEAPON_ARROW, WEAPON_CLEAVING, WEAPON_POLEARM, WEAPON_BLADE, WEAPON_BLUNT, /* must be last */ WEAPONS_COUNT};
enum armor {ARMOR_NONE, ARMOR_LEATHER, ARMOR_CHAINMAIL, ARMOR_PLATE, ARMOR_WOODEN, ARMOR_STONE, /* must be last */ ARMORS_COUNT};

#define SIGMOID_FACTOR 0.40546510810816438197 /* ln(1.5) */

struct u
{
	int attack, defense;
	unsigned count;
	double stamina; // [0, 1]
	enum weapon weapon;
	enum armor armor;

	// calculte number of fighting troops - not all fight, only those nearby (there is some limit)

	// stamina changes depending on unit parameters (each unit has max and current stamina, the vars here are the normalized current value)

	// TODO there could be multiple attackers
};

static const double damage_boost[WEAPONS_COUNT][ARMORS_COUNT] =
{
// ARMOR:                   NONE        LEATHER     CHAINMAIL   PLATE       WOODEN      STONE
    [WEAPON_NONE] = {       0.0,        0.0,        0.0,        0.0,        0.0,        0.0},
    [WEAPON_CLUB] = {       1.0,        0.8,        0.7,        0.2,        0.0,        0.0},
    [WEAPON_ARROW] = {      1.0,        0.8,        0.7,        0.2,        0.0,        0.0},
    [WEAPON_CLEAVING] = {   1.0,        0.9,        0.7,        0.3,        0.5,        0.0},
    [WEAPON_POLEARM] = {    1.0,        1.0,        0.7,        0.4,        0.2,        0.0},
    [WEAPON_BLADE] = {      1.0,        1.0,        0.8,        0.5,        0.2,        0.0},
    [WEAPON_BLUNT] = {      1.0,        1.0,        1.0,        0.5,        1.0,        0.7},
};

static inline double sigmoid(double x)
{
	return 1 / (1 + exp(- SIGMOID_FACTOR * x));
}

static inline int random_bernoulli(double probability)
{
	return (rand() / (RAND_MAX + 1.0)) < probability;
}

static unsigned victims(struct u attacker, struct u defender)
{
	double stamina_boost;
	double probability;
	unsigned count = 0;
	size_t i;

	//stamina_boost = (1 + attacker.stamina - defender.stamina) / 2; // [0, 1]
	//probability = sigmoid(attacker.attack * attacker.count - defender.defense * defender.count);
	probability = sigmoid(attacker.attack * attacker.count * attacker.stamina - defender.defense * defender.count * defender.stamina);
	probability *= damage_boost[attacker.weapon][defender.armor];

	// TODO see if we can generate random binomial at once
	count = 0;
	for(i = 0; i < defender.count; i += 1)
		count += random_bernoulli(probability);

	// TODO stamina should decrease depending on how strong the enemy is

	return count;
}

int main()
{
	struct u a = {.attack = 1, .defense = 1, .count = 8, .stamina = 0.5, .weapon = WEAPON_BLADE, .armor = ARMOR_PLATE};
	struct u b = {.attack = 1, .defense = 1, .count = 8, .stamina = 0, .weapon = WEAPON_CLEAVING, .armor = ARMOR_LEATHER};

	srand(time(0));

	printf("%u\n", victims(a, b));
	printf("%u\n", victims(a, b));
	printf("%u\n", victims(a, b));
	printf("%u\n", victims(a, b));
	printf("%u\n", victims(a, b));

	return 0;
}
