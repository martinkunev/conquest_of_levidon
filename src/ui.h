/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define HEXAGON_EDGE 36.0

#define TROOP_EDGE 32
#define BUILDING_EDGE 48

#define BUTTON_WIDTH 120
#define BUTTON_HEIGHT 20

#define PANEL_LEFT 0
#define PANEL_TOP 0
#define PANEL_WIDTH 256
#define PANEL_HEIGHT 768
#define PANEL_BORDER 4

#define CONTROLS_X (PANEL_LEFT + PANEL_BORDER)
#define CONTROLS_Y (PANEL_TOP + PANEL_BORDER)
#define CONTROLS_WIDTH (PANEL_WIDTH - 2 * PANEL_BORDER)
#define CONTROLS_HEIGHT (PANEL_HEIGHT - 2 * PANEL_BORDER - 1 - BUTTON_HEIGHT - 1 - PANEL_BORDER)

#define BUTTON_MENU_X (PANEL_BORDER + 1)
#define BUTTON_END_X (PANEL_BORDER + 1 + BUTTON_WIDTH + 1 + 4 + 1)
#define BUTTONS_Y (PANEL_TOP + PANEL_HEIGHT - PANEL_BORDER - 1 - BUTTON_HEIGHT)

#define TURN_X 160
#define TURN_Y 720

#define TROOPS_PADDING 12

#define TROOPS_ARROW_LEFT_X 10
#define TROOPS_ARROW_RIGHT_X 232
#define TROOPS_ARROW_Y 400
#define TROOPS_ARROW_WIDTH 17
#define TROOPS_BORDER_TOP (TROOPS_ARROW_Y + 35)
#define TROOPS_BORDER_HEIGHT (1 + TROOPS_PADDING + 2 * (TROOP_EDGE + 10) + 2 + TROOPS_PADDING + 1)

#undef Success // TODO fix this
enum color {Mountain = 1, Water, Hidden, Background, Text, TextForeground, TextMenu, TextSelected, Success, Error, BackgroundBox, Border, Active, TabBorder, Progress, Self, Ally, Enemy, Player};

extern const unsigned char display_colors[][4];

extern struct font font7, font10, font12, font24;

extern struct image image_world;
extern struct image image_panel, image_battlefield;
extern struct image image_tab_active, image_tab_inactive, image_tab_left, image_tab_right;
extern struct image image_terrain_ground, image_terrain_coast, image_terrain_mountain, image_terrain_water;
extern struct image image_gold, image_wood, image_stone, image_food, image_time;
extern struct image image_village, image_flag;
extern struct image image_army, image_ship, image_ship_small, image_ship_small_mask, image_forest, image_quarry;
extern struct image image_unit_ship, image_unit_ship_mask, image_unit_ship_gray;
extern struct image image_order_add, image_order_dismiss;
extern struct image image_construction, image_movement, image_dismiss, image_anchor, image_assault;
extern struct image image_move, image_move_end;
extern struct image image_pawn_assault, image_pawn_fight, image_pawn_guard, image_pawn_shoot;
extern struct image image_garrison_wall_lr, image_garrison_wall_tb, image_garrison_wall_tl, image_garrison_wall_tr;
extern struct image image_garrison_gate_closed_l, image_garrison_gate_closed_r, image_garrison_gate_closed_b, image_garrison_gate_open_l, image_garrison_gate_open_r, image_garrison_gate_open_b;
extern struct image image_garrison_tower_l, image_garrison_tower_r, image_garrison_tower_b;
extern struct image image_gate_open, image_gate_closed, image_gate_open_gray, image_gate_closed_gray;
extern struct image image_fight, image_shoot_u, image_shoot_d, image_shoot_l, image_shoot_r;

extern uint32_t cursors[UNITS_COUNT + 1];

enum object {Worlds = 1, WorldTabs, Players, GarrisonTroops, TrainQueue, Ship, Buildings, Troops, TroopsTabs, Orders, Resources};

struct widget
{
	unsigned rows, columns;
	unsigned left, top, right, bottom;
	unsigned width, height;
	unsigned width_padded, height_padded;
	unsigned span_x, span_y;
	unsigned padding;
	unsigned count;
};

// rows, columns, left, top, width, height, padding
#define WIDGET(r, c, l, t, w, h, p) \
	{ \
		.rows = (r), \
		.columns = (c), \
		.left = (l), \
		.right = (l) + (w) * (c) + (p) * ((c) - 1), \
		.top = (t), \
		.bottom = (t) + (h) * (r) + (p) * ((r) - 1), \
		.width = (w), \
		.height = (h), \
		.width_padded = (w) + (p), \
		.height_padded = (h) + (p), \
		.span_x = (w) * (c) + (p) * ((c) - 1), \
		.span_y = (h) * (r) + (p) * ((r) - 1), \
		.padding = (p), \
		.count = (r) * (c), \
	}

#define WIDGET_AREA(widget, function) \
	{ \
		.left = (widget).left, \
		.right = (widget).right, \
		.top = (widget).top, \
		.bottom = (widget).bottom, \
		.callback = (function) \
	}

extern const struct widget widgets[];

static inline struct vector object_position(const struct widget *restrict group, unsigned index)
{
	struct vector result;
	result.x = group->left + (index % group->columns) * group->width_padded;
	result.y = group->top + (index / group->columns) * group->height_padded;
	return result;
}

static inline int object_index(const struct widget *restrict group, struct vector position)
{
	int index;
	if (((position.x + group->padding) % group->width_padded) < group->padding)
		return -1; // horizontal padding
	if (((position.y + group->padding) % group->height_padded) < group->padding)
		return -1; // vertical padding
	index = (group->columns * (position.y / group->height_padded) + (position.x / group->width_padded));
	return ((index < group->count) ? index : -1);
}

void if_load(const char *restrict world);
void if_unload(void);

static inline int hexagon_offset_x(int offset, unsigned x, unsigned y, float edge)
{
	return (int)(offset + ((y % 2) ? 0.5 * SQRT3 * edge : 0) + x * SQRT3 * edge + 0.5);
}

static inline int hexagon_offset_y(int offset, unsigned x, unsigned y, float edge)
{
	return (int)(offset + y * 1.5 * edge + 0.5);
}

static inline struct box hexagons_box(unsigned width, unsigned height, float edge)
{
	struct box box;
	box.width = width * SQRT3 * edge;
	box.height = height * 1.5 * edge + 0.5 * edge;
	return box;
}

void display_hexagon(double offset_x, double offset_y, double scale, const unsigned char color[static 4]);
void display_button(struct slice label, unsigned x, unsigned y, int selected);
void display_string_center(struct slice string, struct vector offset, struct box box, struct font *restrict font, const unsigned char color[static 4]);

void display_flag(unsigned x, unsigned y, unsigned player);
void display_settlement(const struct game *restrict game, struct tile location, int left, int top, size_t index, const struct army *restrict army, unsigned char player, unsigned edge);
void display_object(const struct game *restrict game, const struct image *restrict image, const struct army *restrict army, unsigned char player, struct tile location, int left, int top, unsigned edge);
void display_troop(const struct unit *restrict unit, int count, unsigned player, unsigned x, unsigned y, enum color text);
void display_troop_stack(const struct troop_stack *restrict troop_stack, unsigned x, unsigned y, const struct game *restrict game, unsigned player_current);
void display_building(const struct building *restrict building, unsigned position, int built);
void display_unit(const struct region *restrict region, const struct unit *restrict unit);

void display_minimap(const struct game *restrict game);
