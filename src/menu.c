/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dirent.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#if !defined(_DIRENT_HAVE_D_NAMLEN)
# include <string.h>
#endif
#include <sys/stat.h>
#include <unistd.h>

#if defined(__GLIBC__)
# include <features.h>
# if ((__GLIBC__ == 2) && (__GLIBC_MINOR__ >= 24)) || (__GLIBC__ > 2)
#  define READDIR
# endif
#endif

#include "basic.h"
#include "format.h"
#include "game.h"
#include "world.h"
#include "menu.h"

#define DIRECTORY_GAME "/.conquest_of_levidon/"
#define DIRECTORY_SHARED "/share/conquest_of_levidon/worlds/"
#define DIRECTORY_WORLDS "/.conquest_of_levidon/worlds/"
#define DIRECTORY_SAVE "/.conquest_of_levidon/save/"

#define DIRECTORIES_COUNT 3

static struct bytes *directories[DIRECTORIES_COUNT];

static struct bytes *path_cat(const unsigned char *restrict prefix, size_t prefix_size, const unsigned char *restrict suffix, size_t suffix_size)
{
	size_t size;
	struct bytes *path;

	size = prefix_size + suffix_size;
	path = falloc(path, path->data, size + 1);
	if (!path) return 0;
	path->size = size;
	*format_bytes(format_bytes(path->data, prefix, prefix_size), suffix, suffix_size) = 0;

	return path;
}

int menu_init(void)
{
	const char *home;
	size_t home_size;

	struct bytes *game_home;

	home = getenv("HOME");
	if (!home) return ERROR_MISSING;
	home_size = strlen(home);

	// TODO check for very large values of home_size

	game_home = path_cat(home, home_size, DIRECTORY_GAME, sizeof(DIRECTORY_GAME) - 1);
	if (!game_home) return ERROR_MEMORY;
	if ((mkdir(game_home->data, 0755) < 0) && (errno != EEXIST))
	{
		free(game_home);
		return ERROR_ACCESS;
	}
	free(game_home);

	directories[0] = path_cat(PREFIX, sizeof(PREFIX) - 1, DIRECTORY_SHARED, sizeof(DIRECTORY_SHARED) - 1);
	if (!directories[0]) return ERROR_MEMORY;

	directories[1] = path_cat(home, home_size, DIRECTORY_WORLDS, sizeof(DIRECTORY_WORLDS) - 1);
	if (!directories[1])
	{
		free(directories[0]);
		return ERROR_MEMORY;
	}

	directories[2] = path_cat(home, home_size, DIRECTORY_SAVE, sizeof(DIRECTORY_SAVE) - 1);
	if (!directories[2])
	{
		free(directories[0]);
		free(directories[1]);
		return ERROR_MEMORY;
	}

	return 0;
}

void menu_term(void)
{
	free(directories[0]);
	free(directories[1]);
	free(directories[2]);
}

// Returns a list of NUL-terminated paths to the world files.
// TODO on error, indicate what the error was?
struct files *menu_worlds(size_t index)
{
	DIR *dir;
	struct dirent *entry;
#if !defined(READDIR)
	struct dirent *more;
#endif

	size_t count;
	struct files *list = 0;

	const unsigned char *restrict path = directories[index]->data;

	dir = opendir(path);
	while (!dir)
	{
		// If the directory does not exist, try to create and open it.
		if ((errno == ENOENT) && !mkdir(path, 0755) && (dir = opendir(path)))
			break;

		return 0;
	}

#if !defined(READDIR)
	entry = falloc(entry, entry->d_name, pathconf(path, _PC_NAME_MAX) + 1);
	if (!entry)
	{
		closedir(dir);
		return 0;
	}
#endif

	// Count the files in the directory.
	count = 0;
	while (1)
	{
#if defined(READDIR)
		errno = 0;
		if (!(entry = readdir(dir)))
		{
			if (errno)
				goto error;
			else
				break; // no more entries
		}
#else
		if (readdir_r(dir, entry, &more))
			goto error;
		if (!more)
			break; // no more entries
#endif

		// Skip hidden files.
		if (entry->d_name[0] == '.')
			continue;

		count += 1;
	}
	rewinddir(dir);

	list = falloc(list, list->names, count);
	if (!list)
		goto error;
	list->count = 0;

	while (1)
	{
		size_t name_length;
		struct bytes *name;

#if defined(READDIR)
		errno = 0;
		if (!(entry = readdir(dir)))
		{
			if (errno)
				goto error;
			else
				break; // no more entries
		}
#else
		if (readdir_r(dir, entry, &more))
			goto error;
		if (!more)
			break; // no more entries
#endif

		// Skip hidden files.
		if (entry->d_name[0] == '.')
			continue;

#if defined(_DIRENT_HAVE_D_NAMLEN)
		name_length = entry->d_namlen;
#else
		name_length = strlen(entry->d_name);
#endif

		name = falloc(name, name->data, name_length + 1);
		if (!name)
			goto error;

		name->size = name_length;
		*format_bytes(name->data, entry->d_name, name_length) = 0;
		list->names[list->count++] = name;
	}

	closedir(dir);
#if !defined(READDIR)
	free(entry);
#endif

	return list;

error:
	menu_free(list);

	closedir(dir);
#if !defined(READDIR)
	free(entry);
#endif

	return 0;
}

void menu_free(struct files *list)
{
	if (list)
	{
		size_t i;
		for(i = 0; i < list->count; ++i)
			free(list->names[i]);
		free(list);
	}
}

int menu_load(size_t index, const unsigned char *restrict filename, size_t filename_size, struct game *restrict game)
{
	int status;

	const unsigned char *restrict location = directories[index]->data;
	size_t location_size = directories[index]->size;

	struct bytes *filepath = path_cat(location, location_size, filename, filename_size);
	if (!filepath) return ERROR_MEMORY;

	status = world_load(filepath->data, game);
	free(filepath);

	return status;
}

int menu_save(size_t index, const unsigned char *restrict filename, size_t filename_size, const struct game *restrict game)
{
	int status;

	const unsigned char *restrict location = directories[index]->data;
	size_t location_size = directories[index]->size;

	struct bytes *filepath = path_cat(location, location_size, filename, filename_size);
	if (!filepath) return ERROR_MEMORY;

	status = world_save(game, filepath->data);
	free(filepath);

	return status;
}
