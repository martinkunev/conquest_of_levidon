#include <assert.h>
#include <fcntl.h>
#include <math.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "basic.h"
#include "log.h"
#include "json.h"
#include "game.h"
#include "map.h"
#include "turn.h"
#include "economy.h"
#include "world.h"
#include "players.h"
#include "battle.h"
#include "round.h"
#include "combat.h"
#include "menu.h"
#include "interface.h"
#include "interface_storage.h"
#include "display.h"
#include "ui.h"
#include "ui_menu.h"
#include "ui_report.h"
#include "ui_battle.h"

#define S(s) s, sizeof(s) - 1

#define WINNER_NOBODY -1
#define WINNER_BATTLE -2

unsigned MAP_WIDTH, MAP_HEIGHT;

static int play_battle(struct game *restrict game, struct battle_info *restrict info)
{
	struct battle battle;
	int winner;
	int status;

	unsigned round_activity_last = 0;

	switch (info->type)
	{
	case BATTLE_OPEN:
		battle.assault = 0;
		break;

	case BATTLE_ASSAULT:
		battle.assault = 1;
		break;

	default:
		assert(0);
	}
	if (battle_init(game, &battle, info) < 0)
		return ERROR_MEMORY;

	if (game->players_local_count >= 2)
	{
		status = battle_announce(game, &battle);
		if (status < 0)
			goto finally;
	}

	while ((winner = battle_end(game, &battle)) < 0)
	{
		// TODO check if there is a local player in the battle; otherwise, resolve automatically

		status = round_prepare(game, &battle);
		if (status < 0)
			goto finally;

		if (battle.round)
		{
			status = players_formation(game, &battle);
			if (status < 0)
				goto finally;
		}

		// Ask each player to give commands to their pawns.
		status = players_battle(game, &battle);
		if (status < 0)
			goto finally;

		// Deal damage from shooters.
		combat_ranged(game, &battle);
		if (round_cleanup(game, &battle, VICTIMS_RANGED))
			round_activity_last = battle.round;

		status = round_move(game, &battle);
		if (status < 0)
			goto finally;

		combat_melee(game, &battle);
		if (round_cleanup(game, &battle, VICTIMS_MELEE))
			round_activity_last = battle.round;

		status = input_animation(game, &battle);
		if (status < 0)
			goto finally;

		// TODO ? // Cancel the battle if nothing is killed/destroyed for a certain number of rounds.

		round_term(game, &battle);
	}

	// TODO
	// after battle, some troop stacks may have "retreat" flag set; they try to go to a nearby hexagon with no enemy nearby; if they don't succeed they are dismissed
	// some troop stacks may surrender (due to low morale); they are dismissed; "retreat" is set by the player while surrender is automatic

	status = battle_report(game, &battle, winner);

finally:
	battle_term(game, &battle);
	return status;
}

static inline const char *type_name(enum json_type type)
{
	const char *names[] = {
		[JSON_NULL] = "null",
		[JSON_BOOLEAN] = "boolean",
		[JSON_INTEGER] = "integer",
		[JSON_REAL] = "real",
		[JSON_STRING] = "string",
		[JSON_ARRAY] = "array",
		[JSON_OBJECT] = "object",
	};
	return names[type];
}

static inline union json *value_get(const struct hashmap *restrict hashmap, struct slice key, enum json_type type)
{
	union json **entry = hashmap_get(hashmap, key.data, key.size);
	if (!entry)
		return 0;
	if (json_type(*entry) != type)
	{
		LOG_WARNING("battle: %.*s is of type %s (expected: %s)", (int)key.size, key.data, type_name(json_type(*entry)), type_name(type));
		return 0;
	}
	return *entry;
}

static int battle_populate(const union json *restrict json, const struct game *restrict game, struct battle_info *restrict battle)
{
	static struct hexagon hexagon = {.terrain = TERRAIN_GROUND};

	union json *fighters, *fighter;
	struct troop_stack *stack;
	size_t i;
	int status = ERROR_INPUT;

	if (json_type(json) != JSON_OBJECT)
		return ERROR_INPUT;

	fighters = value_get(&json->object, slice("fighters"), JSON_ARRAY);
	if (!fighters || !fighters->array.count)
		return ERROR_INPUT;

	battle->fighters = malloc(fighters->array.count * (sizeof(*battle->fighters) + sizeof(**battle->fighters)));
	if (!battle->fighters)
		return ERROR_MEMORY;
	stack = (struct troop_stack *)(battle->fighters + fighters->array.count);

	for(i = 0; i < fighters->array.count; i += 1)
	{
		fighter = fighters->array.data[i];
		if (json_type(fighter) != JSON_OBJECT)
			goto error;

		status = troop_stack_init(game, stack, fighter, &hexagon);
		if (status)
			goto error;

		battle->fighters[i] = stack++;
		stack += 1;
	}
	battle->fighters_count = fighters->array.count;

	battle->players = 0x2 | 0x4;
	battle->type = BATTLE_OPEN;
	battle->location = &hexagon;

	return 0;

error:
	free(battle->fighters);
	return status;
}

static int battle_load(const unsigned char *restrict filepath, const struct game *restrict game, struct battle_info *restrict battle)
{
	int file;
	struct stat info;
	unsigned char *buffer;
	int status;
	union json *json;

	// Read file content.
	file = open(filepath, O_RDONLY);
	if (file < 0)
		return ERROR_MISSING; // TODO this could be ERROR_ACCESS or something else
	if (fstat(file, &info) < 0)
	{
		close(file);
		return ERROR_MISSING; // TODO this could be ERROR_ACCESS or something else
	}
	buffer = mmap(0, info.st_size, PROT_READ, MAP_SHARED, file, 0);
	close(file);
	if (buffer == MAP_FAILED)
		return ERROR_MEMORY;

	// Parse file content.
	json = json_parse(buffer, info.st_size);
	munmap(buffer, info.st_size);
	if (!json)
		return ERROR_INPUT;

	// Populate battle data.
	status = battle_populate(json, game, battle);
	json_free(json);

	return status;
}

int main(int argc, char *argv[])
{
	assert(!sigaction(SIGPIPE, &(struct sigaction){.sa_handler = SIG_IGN}, 0));

	srandom(time(0));

	if (if_init() < 0)
	{
		LOG_FATAL("Cannot initialize interface.");
		return -1;
	}
	if_load(0);
	if_window_default();

	if (argv[1])
	{
		struct game game;
		struct battle_info info;
		int status;

		game.players[0] = (struct player){.type = Neutral, .alliance = 0};
		game.players[1] = (struct player){.type = Local, .alliance = 1};
		game.players[2] = (struct player){.type = Computer, .alliance = 2};
		game.players_count = 3;

		status = players_init(&game);
		if (status < 0)
			return status;

		status = battle_load(argv[1], &game, &info);
		if (status < 0)
			return status;
		status = play_battle(&game, &info);

		players_term(&game);
	}

	if_unload();
	if_term();

	return 0;
}
