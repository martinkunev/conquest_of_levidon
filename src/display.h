/*
 * Conquest of Levidon
 * Copyright (C) 2019  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Conquest of Levidon.
 *
 * Conquest of Levidon is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Conquest of Levidon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Conquest of Levidon.  If not, see <http://www.gnu.org/licenses/>.
 */

#define TAU (2 * M_PI)

struct vector
{
	int x, y;
};

struct box
{
	unsigned width, height;
};

struct frame
{
	struct vector tr; // top left
	struct vector tl; // top right
	struct vector bl; // bottom left
	struct vector br; // bottom right

	unsigned width, height;
};

struct polygon
{
	size_t vertices_count;
	struct vector points[];
};

struct font;
struct image;

void draw_line(int x0, int y0, int x1, int y1, const unsigned char color[4]);

void fill_circle(int x, int y, unsigned radius, const unsigned char color[4]);

void draw_rectangle(int x, int y, unsigned width, unsigned height, const unsigned char color[static 4]);
void fill_rectangle(int x, int y, unsigned width, unsigned height, const unsigned char color[static 4]);

void draw_polygon(const struct polygon *restrict polygon, int offset_x, int offset_y, const unsigned char color[static 4], double scale);
int fill_polygon(const struct polygon *restrict polygon, int offset_x, int offset_y, const unsigned char color[static 4], double scale);

struct box string_box(struct slice string, struct font *restrict font);
void frame_center(struct frame *restrict frame, struct vector center, unsigned width, unsigned height, struct vector offset, double orientation);

void draw_string_cursor(struct box box, int x, int y, struct font *restrict font, const unsigned char color[static 4]);
void draw_string(struct slice string, int x, int y, struct font *restrict font, const unsigned char color[static 4]);

void fill_image(const struct image *restrict image, int x, int y, unsigned width, unsigned height, int repeat);
void fill_image_mask(const struct image *restrict image, int x, int y, unsigned width, unsigned height, const unsigned char color[static 4]);
void frame_image(const struct image *restrict image, struct frame *restrict frame);

void fill_image_hexagon(const struct image *restrict image, int x, int y, unsigned char points[6]);
