sev	type	description

H	bug		font path and Makefile are broken on mac (and probably some linux distributions)
H	bug		battle: pawn trembling (sometimes when it tries to move to a certain position, but another one is there)
H	bug		battle: when there is no local player left fighting, resolve the battle automatically
H	bug		fix map scrolling
H	feature	battle damage from fighting relative to the duration of fighting (e.g. if the pawns move half the time of the round, the damage is only from the other half the time)
H	feature	battle: improve AI
			battle: when there are many units the computer makes very weird moves
			battle: enemy should stay inside garrison when there are shooters
			battle: the difference between states seems too small; currently the algorithm does pretty much nothing until looking for local maximum in the end
			battle: improve computer behavior in case of equal armies (the computer should attack when attacking a region and escape when defending a region)
			battle: when shooters attack a melee pawn and there is nowhere to run, the melee should attack back
H	if		handle different screen resolutions better
H	bug		map: troop bar for just battering ram is not displayed on the map (because it's just 1 troop)
H	if		startup image in the menu (cropped game screenshot?)
H	feature	battle retreat
			in battle report, display which players retreated (maybe show white flag)
			tell all players if somebody is retreating
			display number of rounds until automatic retreat
H	if		battle: display region name
H	if		display map objects better (multiple map objects at the same time? flag position?)
H	if		battle sprites looking in different directions
H	if		display minimap
H	feature	more complex battle mechanics
			pawn morale
			auto-retreat when morale is too low
			stamina
			chance to miss? (in deaths(), randomly choose how many troops did not hit the target)
			troop formation - when well formed, they receive less damage (e.g. they cover with shields and defend each other)

M	feature improve auto combat
M	feature	improve AI
			treating each troop separately when finding local maximum in AI will have bad results
			map: improve computer orders
			economy: take into account support food required for troops in shortage and make sure to produce that food
			economy: dismiss troops when resources are not enough
			economy: when sorting regions before setting workers for production, ignore buildings for resources for which there is no shortage
			map: predicting troops in unknown regions
			invasion: implement AI
			formation: implement AI
M	feature	workshop on any hexagon; some way to assign a pool of troops (e.g. peasants) which to use for the battering ram
M	optim	handle big maps (e.g. hex with side ~4~5km; about 1M hexes)
M	feature	bridges on rivers (only makes sense when big maps can be handled)
M	feature	networking with SCTP
			add indicator (e.g. lighted bulb) to mark the players who have completed their turn
M	feature	sound
M	feature	battle: assault towers
M	feature	ethnicity and loyalty
			each troop has ethnicity; region (settlement) population can have many troops of different ethnicities
			ethnicity is a tuple (language, religion)
			each troop has loyalty toward each player (influenced by player actions, ethnicity, etc.)
			low loyalty or bad troop treatment (or other factors) can cause a troop to rebel (either become neutral or join another player)
			each player has only an indication about loyalty (e.g. "high", "medium", "low"); they don't know if troops are loyal to other players
			there is a table with language similarity (to determine whether ethnicities tolerate each other - e.g. for loyalty calculation)

L	feature	assault moat
L	feature	battle: catapult, balista
L	feature	map features - roads
			each hexagon may contain roads (one bit for each of the 6 directions; between 2 and 4 bits can be set)
L	if		battle: show troops count, health, etc. only when alt is pressed
L	feature	settlement environment
			water, wild animals, soil fertility
L	feature	seasons
			each world has year length (in number of turns; 12 by default; name for each of the turns in a year)
			each world spans given lattitude range and has a given axial tilt; this determines seasons
			food production changes according to season; troop movement is reduced in winter; extreme weather can cause troop death
			display current month and year on the map
L	if		tell local players when they are defeated (use input_report_players())
L	if		battle: when a target is impossible, set target to the closest possible (e.g. if shooting range is not enough or a position is outside the battlefield)
L	feature	settlement control should be determined by troops or town hall. people can be moved when a settlement is under control (but they may rebel)
L	feature local food management
			each troop and settlement has its own food. everything is split into connected components to manage food
			food can be gathered from neighboring hexagons or, if the player owns the settlement, the entire region
L	feature	herd animals; nomadism
			like troops but can be used for food, for transport or to recruit troops (e.g. horse riders)
			nomads need to move around to deal with food exhaustion
L	feature	pillaging - when a settlement is captured, the player receives some resources; pillaging is used to obtain more resources, make the army happy or enslave people
L	feature	region-specific features (income, units, buildings, battlefield)
L	if		keyboard support in various places in the interface; mouse support in various places in the interface; more adequate handling of errors (display error messages, not just silent ignore)
L	feature	make formation consistent with the new battle mechanics
L	feature	improve world loading/saving
			support comma after last item
			log (level error) the line on which parse error occurs
			log (level error) when a compulsory field is not present or has the wrong type
			log (level warning) when an optional field has the wrong type; assume the field is not present for the execution logic
			refactor (simplify) the code using these ideas
L	if		battle: support showing more pawn stats (not just health)
L	if		group players by alliances and show indicator for the alliance
L	if		map: troop movement animation
L	if		more tooltips
L	if		map: indicate on the map which region is selected (maybe with a border around it?)
L	if		good way to visualize economy (income and expenses per settlement and per troop stack)
L	if		map: support showing troop stats
L	if		better support for hotseat (choose player names, timer (to limit player turn duration))
L	feature	fast ships, big ships, battle ships; naval battles; ships participating in assault
L	feature	building destruction and repair
			buildings get damaged after an open battle; garrisons get damaged after an assault
			buildings get damaged with time and need to be repaired
L	feature	armory, swordsman, crossbowman, heavy cavalry, horse archer
L	feature	trade
L	feature	dynamic alliances; alliance features - vision, no agression, trade
L	feature	supply lines

?	feature	melee units should go closer to the shooter to prevent them from escaping far enough to shoot
?	if		battle: display a mirror image of enemy troops
?	if		display an ellipse with the owner below the troop (instead of changing some pixels in the sprite)










old
L			allocate peasants for each building (the building needs people to run it)
L	bug		button input bottom and right coordinates should be exclusive (... - 1)
L	if		different mouse cursors
L	if		flag patterns
L	if		battle: display calculated pawn moves (not a straight line)
L	if		battle: hide mouse cursor during animation
L	if		battle: change image for pawn selection
L	if		battle: think about displaying reachable locations for a pawn
L	if		battle: add battlefield animation indication when an enemy pawn makes a pawn stop (due to collision)
L	if		when there are too many pawns, the report looks awful
L	if		map: place the flag properly
L	if		map: one should be able to tell all troops to go on assault
L	if		map: right clicking on a garrison on the map should be the same as clicking for assault
L	if		improve player colors
L	if		directory tabs images in menu
L	if		when all local players are defeated, the report screen has title "Winners" and lists all computer players (but they are not really winners)
L	feature	add option for shooters to cover larger area while shooting
L	feature	battle: don't allow attacking if the attacker cannot do damage to the defender (due to weapon and armor type)
L	bug		retreat can be abused for scouting (25 peasants can go and retreat for 1 food)
			maybe make retreat take more time (2 rounds?); that way only fast units will be able to retreat
			maybe allow retreat only when the pawns are near the border of the battlefield?
			retreating troops are killed in several cases (comments in the code describe this); is this okay?
L	feature	limited number of shots

L	bug		if_init fails on mac (in glXMakeContextCurrent) when used with ssh forwarding
?	if		implement exit game while in battle
?	feature	dynamic battlefield size
?	feature	deal damage to each pawn escaping from enemy pawns
?	refact	unify image and its modified versions in the same struct: mask, grayscale

refactoring
	iterator for troops in a given stack (garrison / owned by player, dismissed, etc.)
	use 'struct point' only for drawing (draw.h) (fix point_eq calls)
		remove struct region dependency on struct point by moving center and location_garrison into location
	? rename image to sprite
	? change direction of everything to counterclockwise; currently the y axis is in the opposite direction (and the cross product is negated)
	? redesign input functions (handling mouse movement is tedious; handling writing should be unified; can I unify the two input functions?)
	? unify ACTION_FIGHT and ACTION_ASSAULT
	store region troops in stacks (like troops on ships); one special stack for garrison
	rename open battle to field battle (the same name can be applied at other places)
	revise user/computer battle input functions: combat_fight, combat_shoot, combat_assault, movement_queue; it should be clear what they do and how they interact
	? rename support to maintenance
	rename if_display to if_render
	? rename WEAPON_ARROW to WEAPON_PIERCING and change archer melee weapon to WEAPON_PIERCING
	rename struct building income to production
	update array_moves after the simplification of array.g

https://help.github.com/articles/creating-releases/

http://nehe.gamedev.net/tutorial/freetype_fonts_in_opengl/24001/

http://stackoverflow.com/questions/6543924/is-double-buffering-needed-any-more#6544453

flickering
	https://groups.google.com/forum/#!topic/android-developers/om5Utez9PgA

use stencil buffer when troops images don't fit into their rectangle (and will be on top of other images)
	https://open.gl/depthstencils
